﻿namespace CteCore;

public class PropertyColumnDef<T>
{
    public string PropertyName { get; set; }
    public bool Visible { get; set; }
    public bool Filterable { get; set; }
    public string Format { get; set; }
    public Func<T, string> Selector { get; set; }
}
