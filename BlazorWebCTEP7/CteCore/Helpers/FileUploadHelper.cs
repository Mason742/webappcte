﻿using WebDB.Data.Models;

namespace CteCore.Helpers;

public static class FileUploadHelper
{
    public static readonly string ApiSaveUrl = "api/Files/Save/";
    public static readonly string ApiSaveAttachmentUrl = "api/Files/SaveAttachment/";
    public static readonly string ApiPublicSaveUrl = "api/Files/PublicSave/";
    public static readonly string ApiDeleteUrl = "api/Files/Remove/";
    public static readonly string ApiPublicDeleteUrl = "api/Files/PublicRemove/";
    private static readonly string BaseTempDirectory = "public/temp/";
    private static readonly string BaseProductionDirectory = "public/test/";

    public enum SaveAction
    {
        Default = 0,
        Attachments = 1,
        Image = 2,
        RTE = 3,
        Form = 4,

    }

    public static class SaveDirectory
    {

        /// <summary>
        /// Saves files to public/administrator/images/rte
        /// </summary>
        public static readonly string AuthorizedDistributor = GetSaveUrl(SaveAction.Default, Directory.AuthorizedDistributor);
        public static readonly string Category = GetSaveUrl(SaveAction.Default, Directory.Category);
        public static readonly string CompanyDocument = GetSaveUrl(SaveAction.Attachments, Directory.CompanyDocument);
        public static readonly string Group = GetSaveUrl(SaveAction.Default, Directory.Group);
        public static readonly string Homepage = GetSaveUrl(SaveAction.Default, Directory.Homepage);
        public static readonly string PressRelease = GetSaveUrl(SaveAction.Default, Directory.PressRelease);
        public static readonly string Misc = GetSaveUrl(SaveAction.Default, Directory.Misc);
        public static readonly string ProductImage = GetSaveUrl(SaveAction.Image, Directory.ProductImage);
        public static readonly string RTE = GetSaveUrl(SaveAction.RTE, Directory.RTE);
        public static readonly string SeriesAttachment = GetSaveAttachmentUrl(SaveAction.Attachments, Directory.SeriesAttachment);
        public static readonly string SeriesImage = GetSaveUrl(SaveAction.Image, Directory.SeriesImage);
        public static readonly string TestLeadBuilder = GetSaveUrl(SaveAction.Default, Directory.TestLeadBuilder);
        public static readonly string TestLeadConnector = GetSaveUrl(SaveAction.Default, Directory.TestLeadConnector);
        public static readonly string RmaForm = GetSaveUrl(SaveAction.Form, Directory.RmaForm, true);
    }

    /// <summary>
    /// Directories terminated with a /
    /// </summary>
    public static class Directory
    {
        public static readonly string AuthorizedDistributor = "images/authorized-distributor/";
        public static readonly string Category = "images/category/";
        public static readonly string CompanyDocument = "company-document/";
        public static readonly string Group = "images/group/";
        public static readonly string Homepage = "images/homepage/";
        public static readonly string PressRelease = "press-release/";
        public static readonly string Misc = "misc/";
        public static readonly string ProductImage = "images/product/";
        public static readonly string RTE = "administrator/images/rte/";
        public static readonly string SeriesAttachment = "attachment/";
        public static readonly string SeriesImage = "images/series/";
        public static readonly string TestLeadBuilder = "images/test-lead-builder/";
        public static readonly string TestLeadConnector = "images/test-lead-connector/";
        public static readonly string RmaForm = "forms/rma-form/";
    }

    private static string GetSaveUrl(SaveAction saveAction, string folderName, bool isPublic = false)
    {
        var relativePath = (BaseTempDirectory + folderName).Replace("/", ",");
        if (isPublic)
        {
            return ApiPublicSaveUrl + saveAction + "~" + relativePath;
        }
        return ApiSaveUrl + saveAction + "~" + relativePath;
    }

    private static string GetSaveAttachmentUrl(SaveAction saveAction, string folderName, bool isPublic = false)
    {
        var relativePath = (BaseTempDirectory + folderName).Replace("/", ",");
        if (isPublic)
        {
            return ApiPublicSaveUrl + saveAction + "~" + relativePath;
        }
        return ApiSaveAttachmentUrl + saveAction + "~" + relativePath;
    }

    /// <summary>
    /// Gets production directory terminated with /
    /// </summary>
    /// <param name="type"></param>
    /// <returns>string relative path</returns>
    public static string GetProductionDirectory(Type type)
    {
        if (type == null)
        {
            return BaseProductionDirectory + Directory.Misc;
        }
        else if (type == typeof(AuthorizedDistributor))
        {
            return BaseProductionDirectory + Directory.AuthorizedDistributor;
        }
        else if (type == typeof(CompanyDocument))
        {
            return BaseProductionDirectory + Directory.CompanyDocument;
        }
        else if (type == typeof(Category))
        {
            return BaseProductionDirectory + Directory.Category;
        }
        else if (type == typeof(Group))
        {
            return BaseProductionDirectory + Directory.Group;
        }
        else if (type == typeof(Homepage))
        {
            return BaseProductionDirectory + Directory.Homepage;
        }
        else if (type == typeof(PressRelease))
        {
            return BaseProductionDirectory + Directory.PressRelease;
        }
        else if (type == typeof(Product))
        {
            return BaseProductionDirectory + Directory.ProductImage;
        }
        else if (type == typeof(SeriesAttachment))
        {
            return BaseProductionDirectory + Directory.SeriesAttachment;
        }
        else if (type == typeof(Series))
        {
            return BaseProductionDirectory + Directory.SeriesImage;
        }
        else if (type == typeof(TestLeadConnector))
        {
            return BaseProductionDirectory + Directory.TestLeadConnector;
        }
        else if (type == typeof(TestLeadBuilder))
        {
            return BaseProductionDirectory + Directory.TestLeadBuilder;
        }
        else if (type == typeof(RmaForm))
        {
            return BaseProductionDirectory + Directory.RmaForm;
        }
        else
        {
            throw new ArgumentException("No type found");
        }

    }

    /// <summary>
    /// Gets production directory terminated with /
    /// </summary>
    /// <param name="type"></param>
    /// <returns>string relative path</returns>
    public static string GetTempDirectory(Type type)
    {
        if (type == null)
        {
            return BaseTempDirectory + Directory.Misc;
        }
        else if (type == typeof(AuthorizedDistributor))
        {
            return BaseTempDirectory + Directory.AuthorizedDistributor;
        }
        else if (type == typeof(CompanyDocument))
        {
            return BaseTempDirectory + Directory.CompanyDocument;
        }
        else if (type == typeof(Category))
        {
            return BaseTempDirectory + Directory.Category;
        }
        else if (type == typeof(Group))
        {
            return BaseTempDirectory + Directory.Group;
        }
        else if (type == typeof(Homepage))
        {
            return BaseTempDirectory + Directory.Homepage;
        }
        else if (type == typeof(PressRelease))
        {
            return BaseTempDirectory + Directory.PressRelease;
        }
        else if (type == typeof(Product))
        {
            return BaseTempDirectory + Directory.ProductImage;
        }
        else if (type == typeof(SeriesAttachment))
        {
            return BaseTempDirectory + Directory.SeriesAttachment;
        }
        else if (type == typeof(Series))
        {
            return BaseTempDirectory + Directory.SeriesImage;
        }
        else if (type == typeof(TestLeadConnector))
        {
            return BaseTempDirectory + Directory.TestLeadConnector;
        }
        else if (type == typeof(TestLeadBuilder))
        {
            return BaseTempDirectory + Directory.TestLeadBuilder;
        }
        else if (type == typeof(RmaForm))
        {
            return BaseTempDirectory + Directory.RmaForm;
        }
        else
        {
            throw new ArgumentException("No type found");
        }

    }

    public static string GetRelativeUri(string rawUrl)
    {
        var relativeUri = rawUrl[rawUrl.IndexOf("public")..];
        return relativeUri;
    }

    public static string AppendCustomDomainUrl(string relativeUri)
    {
        return "https://as.caltestelectronics.com/" + relativeUri;
    }

}
