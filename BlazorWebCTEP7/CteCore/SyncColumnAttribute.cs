﻿namespace CteCore;

public class SyncColumnAttribute : Attribute
{
    public bool Visible { get; private set; }
    public bool Filterable { get; private set; }
    public string Format { get; private set; }

    public SyncColumnAttribute(bool visible = true, bool filterable = true, string format = "")
    {
        Visible = visible;
        Filterable = filterable;
        Format = format;
    }
}
