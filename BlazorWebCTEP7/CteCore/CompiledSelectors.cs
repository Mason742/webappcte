﻿using System.Collections.ObjectModel;
using System.Linq.Expressions;
using System.Reflection;

namespace CteCore;

/// <summary>
/// Base class to automatically create compiled selectors for the subclass
/// </summary>
/// <typeparam name="T">The type to generate compiled selectors for</typeparam>
public abstract class CompiledSelectors<T>
{
    /// <summary>
    /// Static constructor is only once per T but idk about blazor since its probably a new static instance every page
    /// </summary>
    static CompiledSelectors()
    {
        Dictionary<string, PropertyColumnDef<T>> columnDefs = new();
        Type orderType = typeof(T);
        var propertInfos = orderType.GetProperties(BindingFlags.Public | BindingFlags.Instance).ToList();
        Dictionary<string, Func<T, string>> propFilters = new();
        foreach (var propertyInfo in propertInfos)
        {
            string property = propertyInfo.Name;
            SyncColumnAttribute attribute = propertyInfo.GetCustomAttributes(typeof(SyncColumnAttribute)).Cast<SyncColumnAttribute>().FirstOrDefault();

            bool visible = attribute?.Visible ?? true;
            bool filterable = attribute?.Filterable ?? true;
            string format = attribute?.Format;
            var selector = CompilePropSelector(property);
            propFilters.Add(property, selector);
            columnDefs.Add(property, new PropertyColumnDef<T>() { PropertyName = property, Visible = visible,Filterable=filterable, Selector = selector, Format = format });
        }
        PropFilters = new ReadOnlyDictionary<string, Func<T, string>>(propFilters);
        Columns = new ReadOnlyDictionary<string, PropertyColumnDef<T>>(columnDefs);
    }

    /// <summary>
    /// Creates a selector by property name for use in Linq. Basically doing selector 'x => x.PropertyName' except by string and not as slow as reflection.
    /// </summary>
    /// <typeparam name="T">The type to get property getter for</typeparam>
    /// <param name="propertyName">The name of the property</param>
    /// <returns>The compiled selector</returns>
    private static Func<T, string> CompilePropSelector(string propertyName)
    {
        var wat = typeof(T);
        var parameter = Expression.Parameter(typeof(T), "x");
        var body = Expression.PropertyOrField(parameter, propertyName);
        var lambda = Expression.Lambda<Func<T, string>>(body, parameter);
        Func<T, string> compiled = lambda.Compile();
        return compiled;
    }

    /// <summary>
    /// All Properties and their filters
    /// </summary>
    public static readonly ReadOnlyDictionary<string, Func<T, string>> PropFilters;
    public static readonly ReadOnlyDictionary<string, PropertyColumnDef<T>> Columns;


    public string PartNumber { get; set; }
    public string ImageFileLink { get; set; }
    public string Description { get; set; }
    public string Msrp { get; set; }
    public string NewProduct { get; set; }


}
