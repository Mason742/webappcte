﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using StorageService.Services;
using System.IO.Compression;
using WebDB.Data.Context;
using WebDB.Data.Models;

namespace ProductImageZips
{
    public class GenerateProductImageZipsService : BackgroundService
    {
        private readonly IServiceScopeFactory _serviceScopeFactory;
        private readonly IHttpClientFactory _httpClientFactory;

        public GenerateProductImageZipsService(IServiceScopeFactory serviceScopeFactory, IHttpClientFactory httpClientFactory)
        {
            _serviceScopeFactory = serviceScopeFactory;
            _httpClientFactory = httpClientFactory;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            // Your core logic for the background service
            await GenerateImageZips();
        }

        public static List<Product> ProductImages { get; set; }
        const string AzureDestinationPath = "public/images/distributors/zips/";
        const string ProjectDirectory = @"C:\Projects\ProductImages\";

        public async Task GenerateImageZips()
        {

            ProductImages = new List<Product>();
            using (var scope = _serviceScopeFactory.CreateScope())
            {
                var _dbContext = scope.ServiceProvider.GetRequiredService<DefaultDbContext>();
                ProductImages = _dbContext.Products.ToList();
            }

            Directory.CreateDirectory(ProjectDirectory);
            Directory.Delete(ProjectDirectory, true);
            Directory.CreateDirectory(ProjectDirectory);
            await DownloadProductImages();
            CreateZips();
            await UploadZipsToAzure();
            UpdateDB();
        }

        private void UpdateDB()
        {
            List<string> zipPaths = Directory.GetFiles(ProjectDirectory).ToList();
            using (var scope = _serviceScopeFactory.CreateScope())
            {
                var _dbContext = scope.ServiceProvider.GetRequiredService<DefaultDbContext>();
                _dbContext.Database.ExecuteSqlRaw("TRUNCATE TABLE ProductImageArchive");
                foreach (string zp in zipPaths.Where(x => x.EndsWith(".zip")))
                {
                    string imageZipURL = "https://as.caltestelectronics.com/public/images/distributors/zips/" + Path.GetFileName(zp);
                    _dbContext.ProductImageArchives.Add(new ProductImageArchive { ProductId = int.Parse(Path.GetFileNameWithoutExtension(zp)), ZipFileLink = imageZipURL, Modified = DateTime.Now, Created = DateTime.Now });
                    Console.WriteLine("SeriesArchive: " + imageZipURL);

                    _dbContext.SaveChanges();
                    Console.WriteLine("Changes saved to DB");
                }
            }
        }
        async Task UploadZipsToAzure()
        {

            List<string> zipPaths = Directory.GetFiles(ProjectDirectory).ToList();
            foreach (string zipPath in zipPaths.Where(x => x.EndsWith(".zip")))
            {
                using (var scope = _serviceScopeFactory.CreateScope())
                {
                    var azureStorageService = scope.ServiceProvider.GetRequiredService<IStorageService>();
                    bool done = await azureStorageService.UploadFileAsync(zipPath, AzureDestinationPath);
                    if (done)
                    {
                        Console.WriteLine("Uploaded Zip: " + AzureDestinationPath + Path.GetFileName(zipPath));
                    }
                }
            }
        }

        void CreateZips()
        {
            Directory.CreateDirectory(ProjectDirectory);
            List<string> paths = Directory.GetDirectories(ProjectDirectory).ToList();
            foreach (string p in paths.Where(x => !x.EndsWith(".zip")))
            {
                ZipFile.CreateFromDirectory(p, p + ".zip");
                Console.WriteLine("Zipped: " + p + ".zip");
            }
        }

        async Task DownloadProductImages()
        {
            foreach (var pi in ProductImages)
            {
                string familyDirectory = ProjectDirectory + pi.Id;
                Directory.CreateDirectory(familyDirectory);
                Console.WriteLine("Directory Created: " + familyDirectory);
            }
            foreach (var pi in ProductImages)
            {
                //change this to download all images not just first
                string imagePath = ProjectDirectory + pi.Id + @"\" + pi.Id + "-" + Path.GetFileName(pi.Image1FileLinkLg);
                var httpClient = _httpClientFactory.CreateClient();
                var response = httpClient.GetAsync(pi.Image1FileLinkLg).Result;
                using (var fs = new FileStream(imagePath, FileMode.Create))
                {
                    await response.Content.CopyToAsync(fs);
                }
                Console.WriteLine("Image Downloaded: " + imagePath);
            }
        }
    }
}