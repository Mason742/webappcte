﻿using Microsoft.EntityFrameworkCore;
using WebDB.Data.Context;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProductImageZips;
using StorageService.Services;
using WebDB.Data.Models;
using System.Net.Http;


var host = Host.CreateDefaultBuilder(args)
 .ConfigureAppConfiguration((hostContext, config) =>
 {
     var environmentName = hostContext.HostingEnvironment.EnvironmentName;
     Console.WriteLine(environmentName);
     config.SetBasePath(Directory.GetCurrentDirectory());
     config.AddJsonFile("../../../../CteCore/appsettings.json", optional: true);
     config.AddJsonFile($"../../../../CteCore/appsettings.{environmentName}.json", optional: true, reloadOnChange: true);

     config.AddJsonFile("appsettings.json", optional: true);
     config.AddJsonFile($"appsettings.{environmentName}.json", optional: true, reloadOnChange: true);
     config.AddEnvironmentVariables();
     config.AddCommandLine(args);
 })
 .ConfigureServices((hostContext, services) =>
 {
     services.AddDbContext<DefaultDbContext>(options =>
     {
         string connectionString = hostContext.Configuration.GetConnectionString("DefaultConnection");
         Console.WriteLine($"SQL: {connectionString}");
         options.UseSqlServer(connectionString);
         if (hostContext.HostingEnvironment.IsDevelopment())
         {
             options.EnableSensitiveDataLogging();
         }
     });

     services.AddHttpClient();
     services.AddScoped<IStorageService, AzureStorageService>();
     services.AddHostedService<GenerateProductImageZipsService>();

 }).Build();


await host.RunAsync();

