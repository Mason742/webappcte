﻿// See https://aka.ms/new-console-template for more information

using CTEWebApp.Shared.Helpers;

Console.WriteLine("CT Upload Begin...");
Console.WriteLine("Retrieving Inventory Data...");
List<ErpInventory> inventoryData = await InventoryUploadService.GetInventoryData();

Console.WriteLine("Retrieving list of Amazon Products...");
List<string> publishedProducts = await InventoryUploadService.GetPublishedProductDataFromFile();

Console.WriteLine("Retrieving MOQ (Sale as Packs)...");
List<AmazonMoq> amazonProductsWithMoq = await InventoryUploadService.GetAmazonMoqProductDataFromFile();

Console.WriteLine("Parsing Data...");
List<ErpInventory> inventoryToUpload = InventoryUploadService.ProcessInventoryData(inventoryData, publishedProducts, amazonProductsWithMoq);

Console.WriteLine("Creating File...");
string filenameCt = await InventoryUploadService.CreateAmazonInvetoryFile(inventoryToUpload, BrandHelper.CT.Id);
string filenameGs = await InventoryUploadService.CreateAmazonInvetoryFile(inventoryToUpload, BrandHelper.GS.Id);

//Move AmazonInventoryFile to AMTU upload directory
Console.WriteLine("Moving files to AMTU upload directory...");
File.Move(Constants.OutputDirectory + filenameCt, Constants.AmazonUploadDirectoryCT + filenameCt);
File.Move(Constants.OutputDirectory + filenameGs, Constants.AmazonUploadDirectoryGS + filenameGs);

Console.WriteLine("Completed. Closing...");

