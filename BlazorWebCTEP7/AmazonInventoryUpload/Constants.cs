﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmazonInventoryUpload
{
    internal static class Constants
    {
        public const string ConnectionString = "Server=tcp:ctesys.database.windows.net,1433;Initial Catalog=ctedb;Persist Security Info=False;User ID=mchanner;Password=M@s0n123;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;";
        public const string GlobalDirectory = @"C:\Projects\AmazonInventoryUpload\";
        public const string OutputDirectory = GlobalDirectory + @"out\";
        public const string AmazonUploadDirectoryCT = @"C:\Users\mchanner\amtu2\DocumentTransport\production\outgoing\";
        public const string AmazonUploadDirectoryGS = @"C:\Users\mchanner\amtu2\DocumentTransport1\production\outgoing\";
        public const string PublishedProductsFlatFileCT = GlobalDirectory + @"published_products\AmazonProductsCT.txt";
        public const string PublishedProductsFlatFileGS = GlobalDirectory + @"published_products\AmazonProductsGS.txt";
        public const string ProductsWithMOQFlatFileCT = GlobalDirectory + @"sale_as_pack\moqCT.txt";
        public const string ProductsWithMOQFlatFileGS = GlobalDirectory + @"sale_as_pack\moqGS.txt";
        public static class Brand
        {
            public const string CT = "CT";
            public const string GS = "GS";
        }

    }
}
