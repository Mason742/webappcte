﻿using Microsoft.EntityFrameworkCore;

namespace AmazonInventoryUpload.Services;

internal static class InventoryUploadService
{
    public static async Task<List<ErpInventory>> GetInventoryData()
    {
        using var context = new DefaultDbContext();
        return await context.ErpInventories.ToListAsync();
    }

    public static async Task<List<string>> GetPublishedProductDataFromFile()
    {
        List<string> products = new();
        var lines = (await File.ReadAllLinesAsync(Constants.PublishedProductsFlatFileCT)).Skip(1).ToList();
        lines.AddRange((await File.ReadAllLinesAsync(Constants.PublishedProductsFlatFileGS)).Skip(1).ToList());
        products.AddRange(lines.Select(x => x.Trim()));
        return products;
    }


    public static async Task<List<AmazonMoq>> GetAmazonMoqProductDataFromFile()
    {
        List<AmazonMoq> products = new();
        List<string> lines = (await File.ReadAllLinesAsync(Constants.ProductsWithMOQFlatFileCT)).ToList();
        lines.AddRange((await File.ReadAllLinesAsync(Constants.ProductsWithMOQFlatFileGS)).ToList());
        if (lines != null)
        {
            foreach (string l in lines)
            {
                string[] rawMOQ = l.Split("\t");
                if (!string.IsNullOrEmpty(rawMOQ[0]) && !string.IsNullOrEmpty(rawMOQ[1]))
                {
                    products.Add(new AmazonMoq { PartNumber = rawMOQ[0].Trim(), Quantity = Convert.ToInt32(rawMOQ[1].Trim()) });
                }
                else
                {
                    Console.WriteLine("Empty string in MOQ flatfile found!");
                }
            }
        }
        return products;
    }

    public static List<ErpInventory> ProcessInventoryData(List<ErpInventory> inventoryData, List<string> publishedProducts, List<AmazonMoq> amazonProductsWithMoq)
    {
        List<ErpInventory> upload = new();
        foreach (string publishedProduct in publishedProducts)
        {
            var inventory = inventoryData.Where(x => x.PartNumber.Contains(publishedProduct.ToUpper())).FirstOrDefault();

            if (inventory != null)
            {
                var Moq = amazonProductsWithMoq.FirstOrDefault(x => x.PartNumber.Contains(inventory.PartNumber.ToUpper()));
                if (Moq != null)
                {
                    int salePackQty = inventory.Quantity / Moq.Quantity;
                    inventory.Quantity = salePackQty;
                }
                upload.Add(new ErpInventory { PartNumber = publishedProduct, Quantity = inventory.Quantity, Price = inventory.Price, Brand = inventory.Brand.Trim() });
            }
            else
            {
                Console.WriteLine($"Product not found: {publishedProduct}. Consider adding/removing from Amazon Seller Central.");
            }
        }
        return upload;
    }

    public static async Task<string> CreateAmazonInvetoryFile(List<ErpInventory> inventoryToUpload, string brand, bool useMsrp = false)
    {
        string outputFile;
        if (useMsrp)
        {
            outputFile = "sku\tquantity\tprice\n";
            foreach (var itu in inventoryToUpload.Where(x => x.Brand == brand))
            {
                outputFile += itu.PartNumber + "\t" + itu.Quantity + "\t" + itu.Price + "\n";
            }
        }
        else
        {
            outputFile = "sku\tquantity\n";
            foreach (var itu in inventoryToUpload.Where(x => x.Brand == brand))
            {
                outputFile += itu.PartNumber + "\t" + itu.Quantity + "\n";
            }
        }
        string filename = $"{brand}_Inventory_" + DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss") + ".txt";
        await File.WriteAllTextAsync(Constants.OutputDirectory + filename, outputFile);
        return filename;
    }
}
