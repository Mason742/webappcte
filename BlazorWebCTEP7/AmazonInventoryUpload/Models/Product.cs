﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmazonInventoryUpload.Models
{
    public class AmazonMoq
    {
        public string PartNumber { get; set; }
        public int Quantity { get; set; }
    }
}
