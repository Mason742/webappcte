﻿using HubSpot.NET.Api.Company.Dto;
using System.Runtime.Serialization;

namespace HubSpotAutomation
{
    public class HubSpotCompany : CompanyHubSpotModel
    {
        [DataMember(Name = "billing_address")]
        public string BillingAddress { get; set; }

        [DataMember(Name = "brand")]
        public string Brand { get; set; }

        [DataMember(Name = "customer_id")]
        public string CustomerId { get; set; }

        [DataMember(Name = "dfi_ct")]
        public string DFI_CT { get; set; }

        [DataMember(Name = "fax_number")]
        public string FaxNumber { get; set; }

        [DataMember(Name = "type")]
        public string Type { get; set; }

        [DataMember(Name="phone")]
        public string PhoneNumber { get; set; }
    }
}