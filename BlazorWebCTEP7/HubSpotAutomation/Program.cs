﻿using HubSpot.NET.Core;
using HubSpotAutomation;
using RestSharp;
using SolomonDB;
using SolomonDB.Models;
//HubSpot allows 100 requests every 10 seconds
//Header information contains remaining requests.

List<Customer> customers = new List<Customer>();
//var allowedCustomerTypes = new[] { "DISTR", "OEM", "OEMDIS" };
using (var context = new CTEAPPContext())
{
    //get a list of customers added in the last 1 days where status is active, classId is distr, oem ,or oemdis, and order by created date
    customers = context.Customers.Where(x => x.CrtdDateTime >= DateTime.Now.AddDays(-1) && !string.IsNullOrWhiteSpace(x.BillName) && !string.IsNullOrWhiteSpace(x.EmailAddr) && x.Status == "A").OrderBy(x => x.CrtdDateTime).ToList();
}

var api = new HubSpotApi("cccf72f9-081e-40c3-b92c-edf6f3ded038");
var client = new RestClient("https://api.hubapi.com/crm/v3/objects/");
//loop through list of customers 
string lines = "Domain, Name, CustomerId";
foreach (var c in customers)
{
    //get domain from emailaddr
    string domain = c.EmailAddr.Split('@')[1].Trim();
    string type = (string)c.ClassId.Trim() switch
    {
        "DISTR" => "Distributor",
        "OEM" => "OEM",
        "OEMDIS" => "OEM_Distributor",
        "GOVT" => "Government",
        "EMPLOY" => "Employee",
        "EDGE" => "EDGE",
        "SCHOOL" => "SCHOOL",
        "SDISTR" => "SDISTR",
        "ENDR" => "ENDR",
        "END" => "End_User",
        _ => "",
    };
    long? companyId = null;
    //search Company in HubSpot by domain
    var companySearch = api.Company.GetByDomain<HubSpotCompany>(domain, new HubSpot.NET.Api.Company.CompanySearchByDomain()
    {
        Limit = 10
    });
    //check if company doesnt already exists with domain    
    if (companySearch.Results.Count <= 0 && type!="End_User")
    {
        //create company
        //custid, domain, name, and type
        var createCompanyResponse = api.Company.Create<HubSpotCompany>(new HubSpotCompany()
        {
            Domain = domain,
            Name = c.BillName.Trim(),
            Type = type,
            CustomerId = c.CustId.Trim(),
            PhoneNumber = c.Phone.Trim()
        });
        companyId = createCompanyResponse.Id;
    }
    else if (companySearch.Results.Count == 1)
    {
        companyId = companySearch.Results[0].Id;
        HubSpotCompany company = api.Company.GetById<HubSpotCompany>((long)companyId);
        company.Name = c.BillName.Trim();
        company.CustomerId = c.CustId.Trim();
        company.Type = type;
        api.Company.Update<HubSpotCompany>(company);
    }
    else
    {
        foreach (var cs in companySearch.Results)
        {
            lines += $"{cs.Domain},{cs.Name},{cs.CustomerId}\n";
        }
        await File.AppendAllTextAsync(@"C:\Projects\HubSpotAutomation\DuplicateCompanies.txt", lines);
    }
    long? contactId;
    //search Contact in HubSpot by Email
    var contactSearch = api.Contact.GetByEmail<HubSpotContact>(c.EmailAddr.Trim());
    //check if contact doesnt already exists
    if (contactSearch == null)
    {
        //create contact
        var createContactResponse = api.Contact.CreateOrUpdate<HubSpotContact>(new HubSpotContact()
        {
            Email = c.EmailAddr.Trim(),
            FirstName = c.BillAttn.Split(' ')[0].Trim(),
            LastName = c.BillAttn.Split(' ')[1].Trim(),
            Phone = c.Phone.Trim(),
        });
        contactId = createContactResponse.Id;
    }
    else
    {
        contactId = contactSearch.Id;
        //api.Contact.Update<HubSpotContact>(contactSearch);
    }
    //associate contact to company
    await Task.Delay(100);
    if (companyId != null && c.ClassId.Trim() != "END")
    {
        string associationUrl = "/companies/" + companyId + "/associations/contacts/" + contactId + "/company_to_contact?" + "hapikey=cccf72f9-081e-40c3-b92c-edf6f3ded038";
        var request = new RestRequest(associationUrl, Method.Put);
        request.AddHeader("accept", "application/json");
        RestResponse associationJsonReponse = await client.ExecuteAsync(request);
    }
    Console.WriteLine(c.BillName.Trim() + " : " + domain + " : " + type +" : "+c.CustId.Trim()+" : "+c.EmailAddr.Trim());
}