﻿using HubSpot.NET.Api.Contact.Dto;
using System.Runtime.Serialization;

namespace HubSpotAutomation
{
    public class HubSpotContact : ContactHubSpotModel
    {
        [DataMember(Name = "communication_type")]
        public string CommunicationType { get; set; }

        [DataMember(Name = "notes")]
        public string Description { get; set; }

        [DataMember(Name = "pr_subscriber")]
        public bool MailListSubscriber { get; set; }

        [DataMember(Name = "skype_username")]
        public string SkypeUsername { get; set; }

    }
}