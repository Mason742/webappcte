﻿//https://github.com/microsoftgraph/msgraph-sdk-dotnet/blob/7a2be45d2cf37f18a32cc9a60d0edf441fd23a08/docs/upgrade-to-v5.md

// Read application settings from appsettings.json (tenant ID, app ID, client secret, etc.)
AppSettings config = AppSettingsFile.ReadFromJsonFile();

// Initialize the client credential auth provider
var scopes = new[] { "https://graph.microsoft.com/.default" };
var clientSecretCredential = new ClientSecretCredential(config.TenantId, config.AppId, config.ClientSecret);
var graphClient = new GraphServiceClient(clientSecretCredential, scopes);

MicrosoftGraph.Helpers.PrintCommandsHelpers.PrintCommands();
try
{
    while (true)
    {
        Console.Write("Enter command, then press ENTER: ");
        string decision = Console.ReadLine();
        if(decision == null)
        {
            throw new NotImplementedException();
        }
        switch (decision.ToLower())
        {
            case "1":
                await UserService.ListUsers(graphClient);
                break;
            case "2":
                await UserService.GetUserById(graphClient);
                break;
            case "3":
                await UserService.GetUserBySignInName(config, graphClient);
                break;
            case "4":
                await UserService.DeleteUserById(graphClient);
                break;
            case "5":
                await UserService.SetPasswordByUserId(graphClient);
                break;
            case "6":
                await UserService.BulkCreate(config, graphClient);
                break;
            case "7":
                //await UserService.BulkCreateUserWithCustomAttribute(graphClient, config.B2cExtensionAppClientId, config.TenantId);
                break;
            case "8":
                await UserService.CreateUserWithCustomAttribute(graphClient, config.B2cExtensionAppClientId, config.TenantId);
                break;
            case "9":
                await UserService.ListUsersWithCustomAttribute(graphClient, config.B2cExtensionAppClientId);
                break;
            case "10":
                await UserService.CountUsers(graphClient);
                break;
            case "help":
                MicrosoftGraph.Helpers.PrintCommandsHelpers.PrintCommands();
                break;
            case "exit":
                return;
            default:
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Invalid command. Enter 'help' to show a list of commands.");
                Console.ResetColor();
                break;
        }

        Console.ResetColor();
    }
}
catch (Exception ex)
{
    Console.ForegroundColor = ConsoleColor.Red;
    Console.WriteLine($"An error occurred: {ex}");
    Console.ResetColor();
}



Console.ReadLine();