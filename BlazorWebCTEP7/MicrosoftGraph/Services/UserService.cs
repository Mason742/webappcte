﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDB.Data.Context;
using WebDB.Data.Models;

namespace MicrosoftGraph.Services;

public class UserService
{

    //<ms_docref_get_list_of_user_accounts>
    public static async Task<List<Microsoft.Graph.User>> ListOfUsers(GraphServiceClient graphClient)
    {
        Console.WriteLine("Getting list of users...");
        List<Microsoft.Graph.User> graphUsers = new();
        try
        {
            // Get all users
            IGraphServiceUsersCollectionPage users = await graphClient.Users
                .Request()
                .Select(e => new
                {
                    e.DisplayName,
                    e.Id,
                    e.Identities
                })
                .GetAsync();
            // Iterate over all the users in the directory
            var pageIterator = PageIterator<Microsoft.Graph.User>
                .CreatePageIterator(
                    graphClient,
                    users,
                    // Callback executed for each user in the collection
                    (user) =>
                    {
                        graphUsers.Add(user);
                        Console.WriteLine(JsonSerializer.Serialize(user));
                        return true;
                    },
                    // Used to configure subsequent page requests
                    (req) =>
                    {
                        Console.WriteLine($"Reading next page of users...");
                        return req;
                    }
                );

            await pageIterator.IterateAsync();
            return graphUsers.ToList();
        }
        catch (Exception ex)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(ex.Message);
            Console.ResetColor();
            return graphUsers.ToList();
        }
    }

    public static async Task ListUsers(GraphServiceClient graphClient)
    {
        Console.WriteLine("Getting list of users...");

        try
        {
            // Get all users
            var users = await graphClient.Users
                .Request()
                .Select(e => new
                {
                    e.DisplayName,
                    e.Id,
                    e.Identities
                })
                .GetAsync();
            // Iterate over all the users in the directory
            var pageIterator = PageIterator<Microsoft.Graph.User>
                .CreatePageIterator(
                    graphClient,
                    users,
                    // Callback executed for each user in the collection
                    (user) =>
                    {
                        Console.WriteLine(JsonSerializer.Serialize(user));
                        return true;
                    },
                    // Used to configure subsequent page requests
                    (req) =>
                    {
                        Console.WriteLine($"Reading next page of users...");
                        return req;
                    }
                );

            await pageIterator.IterateAsync();
        }
        catch (Exception ex)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(ex.Message);
            Console.ResetColor();
        }
    }
    //</ms_docref_get_list_of_user_accounts>

    public static async Task CountUsers(GraphServiceClient graphClient)
    {
        int i = 0;
        Console.WriteLine("Getting list of users...");

        try
        {
            // Get all users 
            var users = await graphClient.Users
                .Request()
                .Select(e => new
                {
                    e.DisplayName,
                    e.Id,
                    e.Identities
                })
                .GetAsync();

            // Iterate over all the users in the directory
            var pageIterator = PageIterator<Microsoft.Graph.User>
                .CreatePageIterator(
                    graphClient,
                    users,
                    // Callback executed for each user in the collection
                    (user) =>
                    {
                        i += 1;
                        return true;
                    },
                    // Used to configure subsequent page requests
                    (req) =>
                    {
                        Console.WriteLine($"Reading next page of users. Number of users: {i}");
                        return req;
                    }
                );

            await pageIterator.IterateAsync();

            Console.WriteLine("========================");
            Console.WriteLine($"Number of users in the directory: {i}");
            Console.WriteLine("========================");
        }
        catch (Exception ex)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(ex.Message);
            Console.ResetColor();
        }
    }

    public static async Task ListUsersWithCustomAttribute(GraphServiceClient graphClient, string b2cExtensionAppClientId)
    {
        if (string.IsNullOrWhiteSpace(b2cExtensionAppClientId))
        {
            throw new ArgumentException("B2cExtensionAppClientId (its Application ID) is missing from appsettings.json. Find it in the App registrations pane in the Azure portal. The app registration has the name 'b2c-extensions-app. Do not modify. Used by AADB2C for storing user data.'.", nameof(b2cExtensionAppClientId));
        }

        // Declare the names of the custom attributes
        const string customAttributeName1 = "FavouriteSeason";
        const string customAttributeName2 = "LovesPets";

        // Get the complete name of the custom attribute (Azure AD extension)
        Helpers.B2cCustomAttributeHelper helper = new(b2cExtensionAppClientId);
        string favouriteSeasonAttributeName = helper.GetCompleteAttributeName(customAttributeName1);
        string lovesPetsAttributeName = helper.GetCompleteAttributeName(customAttributeName2);

        Console.WriteLine($"Getting list of users with the custom attributes '{customAttributeName1}' (string) and '{customAttributeName2}' (boolean)");
        Console.WriteLine();

        // Get all users (one page)
        var result = await graphClient.Users
            .Request()
            .Select($"id,displayName,identities,{favouriteSeasonAttributeName},{lovesPetsAttributeName}")
            .GetAsync();

        foreach (var user in result.CurrentPage)
        {
            Console.WriteLine(JsonSerializer.Serialize(user));

            // Only output the custom attributes...
            //Console.WriteLine(JsonSerializer.Serialize(user.AdditionalData));
        }
    }

    public static async Task<List<Microsoft.Graph.User>> ListOfUsersWithCustomAttribute(GraphServiceClient graphClient, string b2cExtensionAppClientId = "6df63c96-d879-496d-b5c8-65162950daec")
    {
        List<Microsoft.Graph.User> graphUsers = new();
        if (string.IsNullOrWhiteSpace(b2cExtensionAppClientId))
        {
            throw new ArgumentException("B2cExtensionAppClientId (its Application ID) is missing from appsettings.json. Find it in the App registrations pane in the Azure portal. The app registration has the name 'b2c-extensions-app. Do not modify. Used by AADB2C for storing user data.'.", nameof(b2cExtensionAppClientId));
        }

        // Declare the names of the custom attributes
        const string customAttributeName1 = "CustomerId";
        const string customAttributeName2 = "Role";

        // Get the complete name of the custom attribute (Azure AD extension)
        Helpers.B2cCustomAttributeHelper helper = new(b2cExtensionAppClientId);
        string favouriteSeasonAttributeName = helper.GetCompleteAttributeName(customAttributeName1);
        string lovesPetsAttributeName = helper.GetCompleteAttributeName(customAttributeName2);

        Console.WriteLine($"Getting list of users with the custom attributes '{customAttributeName1}' (string) and '{customAttributeName2}' (boolean)");
        Console.WriteLine();

        // Get all users (one page)
        var result = await graphClient.Users
            .Request()
            .Select($"id,givenName,surname,userPrincipalName,displayName,companyName,identities,{favouriteSeasonAttributeName},{lovesPetsAttributeName}")
            .GetAsync();

        foreach (var user in result.CurrentPage)
        {
            //Console.WriteLine(JsonSerializer.Serialize(user));
            graphUsers.Add(user);
            // Only output the custom attributes...
            //Console.WriteLine(JsonSerializer.Serialize(user.AdditionalData));
        }
        return graphUsers;
    }

    public static async Task GetUserById(GraphServiceClient graphClient)
    {
        Console.Write("Enter user object ID: ");
        string userId = Console.ReadLine();

        Console.WriteLine($"Looking for user with object ID '{userId}'...");

        try
        {
            // Get user by object ID
            var result = await graphClient.Users[userId]
                .Request()
                .Select(e => new
                {
                    e.DisplayName,
                    e.Id,
                    e.Identities
                })
                .GetAsync();

            if (result != null)
            {
                Console.WriteLine(JsonSerializer.Serialize(result));
            }
        }
        catch (Exception ex)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(ex.Message);
            Console.ResetColor();
        }
    }

    public static async Task GetUserBySignInName(AppSettings config, GraphServiceClient graphClient)
    {
        Console.Write("Enter user sign-in name (username or email address): ");
        string userId = Console.ReadLine();

        Console.WriteLine($"Looking for user with sign-in name '{userId}'...");

        try
        {
            // Get user by sign-in name
            var result = await graphClient.Users
                .Request()
                .Filter($"identities/any(c:c/issuerAssignedId eq '{userId}' and c/issuer eq '{config.TenantId}')")
                .Select(e => new
                {
                    e.DisplayName,
                    e.Id,
                    e.Identities
                })
                .GetAsync();

            if (result != null)
            {
                Console.WriteLine(JsonSerializer.Serialize(result));
            }
        }
        catch (Exception ex)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(ex.Message);
            Console.ResetColor();
        }
    }

    public static async Task DeleteUserById(GraphServiceClient graphClient, string userId = null)
    {
        if (userId == null)
        {
            Console.Write("Enter user object ID: ");
            userId = Console.ReadLine();
        }

        Console.WriteLine($"Looking for user with object ID '{userId}'...");

        try
        {
            // Delete user by object ID
            await graphClient.Users[userId]
               .Request()
               .DeleteAsync();

            Console.WriteLine($"User with object ID '{userId}' successfully deleted.");
        }
        catch (Exception ex)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(ex.Message);
            Console.ResetColor();
        }
    }

    //!!! really need to fix before launching .net 7
    public static async Task UpdateUserByUserModel(GraphServiceClient graphClient, dynamic userModel, string b2cExtensionAppClientId = "6df63c96-d879-496d-b5c8-65162950daec")
    {

        // Declare the names of the custom attributes
        const string customAttributeName1 = "CustomerId";
        const string customAttributeName2 = "Role";

        // Get the complete name of the custom attribute (Azure AD extension)
        Helpers.B2cCustomAttributeHelper helper = new(b2cExtensionAppClientId);
        string customerIdAttributeName = helper.GetCompleteAttributeName(customAttributeName1);
        string roleAttributeName = helper.GetCompleteAttributeName(customAttributeName2);

        Console.WriteLine($"Create a user with the custom attributes '{customAttributeName1}' (string) and '{customAttributeName2}' (boolean)");

        // Fill custom attributes
        IDictionary<string, object> extensionInstance = new Dictionary<string, object>
        {
            { customerIdAttributeName, userModel.CustomerId.Trim() },
            { roleAttributeName, userModel.Role.Trim() }
        };


        var user = new Microsoft.Graph.User
        {
            GivenName = userModel.GivenName.Trim(),
            Surname = userModel.Surname.Trim(),
            DisplayName = $"{userModel.GivenName.Trim()} {userModel.Surname.Trim()}",
            CompanyName = userModel.CompanyName.Trim(),
            AdditionalData = extensionInstance,
        };
        try
        {
            // Update user by object ID
            await graphClient.Users[userModel.Id]
               .Request()
               .UpdateAsync(user);

            Console.WriteLine($"User with object ID '{userModel.Id}' successfully updated.");
        }
        catch (Exception ex)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(ex.Message);
            Console.ResetColor();
        }

    }

    public static async Task SetPasswordByUserId(GraphServiceClient graphClient)
    {
        Console.Write("Enter user object ID: ");
        string userId = Console.ReadLine();

        Console.Write("Enter new password: ");
        string password = Console.ReadLine();

        Console.WriteLine($"Looking for user with object ID '{userId}'...");

        var user = new Microsoft.Graph.User
        {
            PasswordPolicies = "DisablePasswordExpiration,DisableStrongPassword",
            PasswordProfile = new PasswordProfile
            {
                ForceChangePasswordNextSignIn = false,
                Password = password,
            }
        };

        try
        {
            // Update user by object ID
            await graphClient.Users[userId]
               .Request()
               .UpdateAsync(user);

            Console.WriteLine($"User with object ID '{userId}' successfully updated.");
        }
        catch (Exception ex)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(ex.Message);
            Console.ResetColor();
        }
    }

    public static async Task BulkCreate(AppSettings config, GraphServiceClient graphClient)
    {
        // Get the users to import
        string appDirectoryPath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        string dataFilePath = Path.Combine(appDirectoryPath + @"\DataFiles\", config.UsersFileName);

        // Verify and notify on file existence
        if (!System.IO.File.Exists(dataFilePath))
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"File '{dataFilePath}' not found.");
            Console.ResetColor();
            Console.ReadLine();
            return;
        }

        Console.WriteLine("Starting bulk create operation...");

        // Read the data file and convert to object
        UsersModel users = UsersModel.Parse(System.IO.File.ReadAllText(dataFilePath));

        foreach (var user in users.Users)
        {
            user.SetB2CProfile(config.TenantId);

            try
            {
                // Create the user account in the directory
                Microsoft.Graph.User user1 = await graphClient.Users
                                .Request()
                                .AddAsync(user);

                Console.WriteLine($"User '{user.DisplayName}' successfully created.");
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ex.Message);
                Console.ResetColor();
            }
        }
    }

    //public static async Task BulkCreateUserWithCustomAttribute(GraphServiceClient graphClient, string? b2cExtensionAppClientId, string? tenantId)
    //{
    //    if (string.IsNullOrWhiteSpace(b2cExtensionAppClientId))
    //    {
    //        throw new ArgumentException("B2C Extension App ClientId (ApplicationId) is missing in the appsettings.json. Get it from the App Registrations blade in the Azure portal. The app registration has the name 'b2c-extensions-app. Do not modify. Used by AADB2C for storing user data.'.", nameof(b2cExtensionAppClientId));
    //    }

    //    // Declare the names of the custom attributes
    //    const string customAttributeName1 = "CustomerId";
    //    const string customAttributeName2 = "Role";

    //    // Get the complete name of the custom attribute (Azure AD extension)
    //    Helpers.B2cCustomAttributeHelper helper = new Helpers.B2cCustomAttributeHelper(b2cExtensionAppClientId);
    //    string customerIdAttributeName = helper.GetCompleteAttributeName(customAttributeName1);
    //    string roleAttributeName = helper.GetCompleteAttributeName(customAttributeName2);

    //    Console.WriteLine($"Create a user with the custom attributes '{customAttributeName1}' (string) and '{customAttributeName2}' (boolean)");

    //    List<AspNetUser> aspNetUsers = new List<AspNetUser>();
    //    List<WebDB.Data.Models.DistributorAuthorizedDomain> distributorAuthorizedDomains = new List<WebDB.Data.Models.DistributorAuthorizedDomain>();
    //    using (CTEDbContext webDB = new CTEDbContext())
    //    {
    //        aspNetUsers = webDB.AspNetUsers.Where(x => !string.IsNullOrWhiteSpace(x.CustomerId)).Include("Roles").ToList();
    //        distributorAuthorizedDomains = webDB.DistributorAuthorizedDomains.ToList();
    //    }

    //    string logFile = "";

    //    foreach (var user in aspNetUsers)
    //    {
    //        // Fill custom attributes
    //        IDictionary<string, object> extensionInstance = new Dictionary<string, object>();
    //        extensionInstance.Add(customerIdAttributeName, user.CustomerId.Trim());
    //        if (user.NormalizedEmail.Contains("CALTESTELECTRONICS.COM"))
    //        {
    //            extensionInstance.Add(roleAttributeName, "Administrator");
    //        }
    //        else
    //        {
    //            extensionInstance.Add(roleAttributeName, user.Roles.Any(x => x.Name == "Distributor") ? "Distributor" : "Visitor");
    //        }

    //        try
    //        {
    //            var dad = distributorAuthorizedDomains.Where(x => x.CustomerId == user.CustomerId);
    //            string? companyName = "NA";
    //            if (dad.Count() != 0)
    //            {
    //                companyName = dad.FirstOrDefault().CompanyName;

    //            }
    //            var password = Helpers.PasswordHelper.GenerateNewPassword(4, 8, 4);
    //            // Create user
    //            var result = await graphClient.Users
    //            .Request()
    //            .AddAsync(new User
    //            {
    //                GivenName = user.FirstName,
    //                Surname = user.LastName,
    //                DisplayName = $"{user.FirstName} {user.LastName}",
    //                CompanyName = companyName,
    //                Identities = new List<ObjectIdentity>
    //                {
    //                new ObjectIdentity()
    //                {
    //                    SignInType = "emailAddress",
    //                    Issuer = tenantId,
    //                    IssuerAssignedId = user.Email.ToLower(),
    //                }
    //                },
    //                PasswordProfile = new PasswordProfile()
    //                {
    //                    Password = password
    //                },
    //                PasswordPolicies = "DisablePasswordExpiration",
    //                AdditionalData = extensionInstance
    //            });

    //            string userId = result.Id;

    //            Console.WriteLine($"Created the new user. Now get the created user with object ID '{userId}'...");

    //            // Get created user by object ID
    //            result = await graphClient.Users[userId]
    //                .Request()
    //                .Select($"id,givenName,surName,displayName,companyName,identities,{customerIdAttributeName},{roleAttributeName}")
    //                .GetAsync();

    //            if (result != null)
    //            {
    //                Console.ForegroundColor = ConsoleColor.Blue;
    //                Console.WriteLine($"DisplayName: {result.DisplayName}");
    //                Console.WriteLine($"CompanyName: {result.CompanyName}");
    //                Console.WriteLine($"{customAttributeName1}: {result.AdditionalData[customerIdAttributeName].ToString()}");
    //                Console.WriteLine($"{customAttributeName2}: {result.AdditionalData[roleAttributeName].ToString()}");
    //                Console.WriteLine($"Email: {user.Email.ToLower()}");
    //                Console.WriteLine($"Password: {password}");
    //                Console.WriteLine();
    //                Console.ResetColor();
    //                //Console.WriteLine(JsonSerializer.Serialize(result, new JsonSerializerOptions { WriteIndented = true }));
    //                logFile += $"{result.DisplayName},{user.Email.ToLower()},{result.GivenName},{result.Surname},{password},{result.AdditionalData[customerIdAttributeName].ToString()},{result.AdditionalData[roleAttributeName].ToString()}\n";
    //            }
    //        }
    //        catch (ServiceException ex)
    //        {
    //            if (ex.StatusCode == System.Net.HttpStatusCode.BadRequest)
    //            {
    //                Console.ForegroundColor = ConsoleColor.Red;
    //                Console.WriteLine($"Have you created the custom attributes '{customAttributeName1}' (string) and '{customAttributeName2}' (boolean) in your tenant?");
    //                Console.WriteLine();
    //                Console.WriteLine(ex.Message);
    //                Console.ResetColor();
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            Console.ForegroundColor = ConsoleColor.Red;
    //            Console.WriteLine(ex.Message);
    //            Console.ResetColor();
    //        }

    //    }

    //    // Get the users to import
    //    string appDirectoryPath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
    //    string dataFilePath = Path.Combine(appDirectoryPath + @"\DataFiles\", "Log.txt");

    //    System.IO.File.WriteAllText(dataFilePath, logFile);



    //}


    public static async Task CreateUserWithCustomAttribute(GraphServiceClient graphClient, string b2cExtensionAppClientId, string tenantId)
    {
        if (string.IsNullOrWhiteSpace(b2cExtensionAppClientId))
        {
            throw new ArgumentException("B2C Extension App ClientId (ApplicationId) is missing in the appsettings.json. Get it from the App Registrations blade in the Azure portal. The app registration has the name 'b2c-extensions-app. Do not modify. Used by AADB2C for storing user data.'.", nameof(b2cExtensionAppClientId));
        }

        // Declare the names of the custom attributes
        const string customAttributeName1 = "FavouriteSeason";
        const string customAttributeName2 = "LovesPets";

        // Get the complete name of the custom attribute (Azure AD extension)
        Helpers.B2cCustomAttributeHelper helper = new(b2cExtensionAppClientId);
        string favouriteSeasonAttributeName = helper.GetCompleteAttributeName(customAttributeName1);
        string lovesPetsAttributeName = helper.GetCompleteAttributeName(customAttributeName2);

        Console.WriteLine($"Create a user with the custom attributes '{customAttributeName1}' (string) and '{customAttributeName2}' (boolean)");

        // Fill custom attributes
        IDictionary<string, object> extensionInstance = new Dictionary<string, object>
        {
            { favouriteSeasonAttributeName, "summer" },
            { lovesPetsAttributeName, true }
        };

        try
        {
            // Create user
            var result = await graphClient.Users
            .Request()
            .AddAsync(new Microsoft.Graph.User
            {
                GivenName = "Casey",
                Surname = "Jensen",
                DisplayName = "Casey Jensen",
                Identities = new List<ObjectIdentity>
                {
                    new ObjectIdentity()
                    {
                        SignInType = "emailAddress",
                        Issuer = tenantId,
                        IssuerAssignedId = "casey.jensen@example.com"
                    }
                },
                PasswordProfile = new PasswordProfile()
                {
                    Password = Helpers.PasswordHelper.GenerateNewPassword(4, 8, 4)
                },
                PasswordPolicies = "DisablePasswordExpiration",
                AdditionalData = extensionInstance
            });

            string userId = result.Id;

            Console.WriteLine($"Created the new user. Now get the created user with object ID '{userId}'...");

            // Get created user by object ID
            result = await graphClient.Users[userId]
                .Request()
                .Select($"id,givenName,surName,displayName,identities,{favouriteSeasonAttributeName},{lovesPetsAttributeName}")
                .GetAsync();

            if (result != null)
            {
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine($"DisplayName: {result.DisplayName}");
                Console.WriteLine($"{customAttributeName1}: {result.AdditionalData[favouriteSeasonAttributeName]}");
                Console.WriteLine($"{customAttributeName2}: {result.AdditionalData[lovesPetsAttributeName]}");
                Console.WriteLine();
                Console.ResetColor();
                Console.WriteLine(JsonSerializer.Serialize(result, new JsonSerializerOptions { WriteIndented = true }));
            }
        }
        catch (ServiceException ex)
        {
            if (ex.StatusCode == System.Net.HttpStatusCode.BadRequest)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"Have you created the custom attributes '{customAttributeName1}' (string) and '{customAttributeName2}' (boolean) in your tenant?");
                Console.WriteLine();
                Console.WriteLine(ex.Message);
                Console.ResetColor();
            }
        }
        catch (Exception ex)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(ex.Message);
            Console.ResetColor();
        }
    }
}
