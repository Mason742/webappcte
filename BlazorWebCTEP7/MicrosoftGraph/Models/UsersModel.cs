﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicrosoftGraph.Models;

public class UsersModel
{
    [JsonPropertyName("users")]
    public UserModel[] Users { get; set; }

    public static UsersModel Parse(string JSON)
    {
        return JsonSerializer.Deserialize<UsersModel>(JSON);
    }
}
