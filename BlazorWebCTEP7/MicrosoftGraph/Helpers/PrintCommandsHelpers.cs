﻿namespace MicrosoftGraph.Helpers
{
    public static class PrintCommandsHelpers
    {
        public static void PrintCommands()
        {
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Command  Description");
            Console.WriteLine("====================");
            Console.WriteLine("[1]      Get all users");
            Console.WriteLine("[2]      Get user by object ID");
            Console.WriteLine("[3]      Get user by sign-in name");
            Console.WriteLine("[4]      Delete user by object ID");
            Console.WriteLine("[5]      Update user password");
            Console.WriteLine("[6]      Create users (bulk import)");
            Console.WriteLine("[7]      Create users (bulk import) using legacy Identity Table");
            Console.WriteLine("[8]      Create user with custom attributes and show result");
            Console.WriteLine("[9]      Get all users (one page) with custom attributes");
            Console.WriteLine("[10]      Get the number of useres in the directory");
            Console.WriteLine("[help]   Show available commands");
            Console.WriteLine("[exit]   Exit the program");
            Console.WriteLine("-------------------------");
        }
    }
}