﻿global using Azure.Identity;
global using Microsoft.Graph;
global using MicrosoftGraph.Models;
global using MicrosoftGraph.Services;
global using System.Text.Json.Serialization;
global using System.Text.Json;
