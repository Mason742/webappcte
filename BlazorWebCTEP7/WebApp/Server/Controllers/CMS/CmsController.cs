﻿using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.Graph;
using NuGet.Packaging;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Text;
using WebApp.Client.Pages.Admin.Series;
using WebApp.Server.Controllers.Web;
using WebDB.Data.Models;
using Product = WebDB.Data.Models.Product;
namespace WebApp.Server.Controllers;


[Route("cms")]
[ApiController]
[ResponseCache(NoStore = true, Location = ResponseCacheLocation.None)]

//[Authorize("Admin")] 
public class CmsController : ControllerBase
{
    private readonly ApplicationDbContext _context;
    private readonly ILogger<ProductController> _logger;
    private readonly IStorageService _storageService;

    public CmsController(ApplicationDbContext context, ILogger<ProductController> logger, IStorageService storageService)
    {
        _context = context;
        _logger = logger;
        _storageService = storageService;
    }

    [HttpGet("categories")]
    public async Task<IActionResult> GetCategories()
    {
        return Ok(await _context.Categories.Select(c => new CategoriesDTO
        {
            Id = c.Id,
            TableName = c.TableName,
        }).ToListAsync());
    }

    [HttpGet("series")]
    public async Task<IActionResult> GetSeries()
    {
        return Ok(await _context.Series.Select(s => s.Name).ToListAsync());
    }

    [HttpGet("{seriesParam}")]
    public async Task<IActionResult> GetSeries(string seriesParam)
    {
        var seriesName = seriesParam?.ToLower()?.Trim();
        if (string.IsNullOrWhiteSpace(seriesName)) { return NotFound(); }

        var series = await _context.Series
            .Include(s => s.Products)
            .Include(s => s.SeriesAttachments)
            .Include(sr => sr.SeriesRelatedSeries).ThenInclude(t => t.RelatedSeries)
            .FirstOrDefaultAsync(s => s.Name.ToLower().Trim() == seriesName);
        if (series == null) { return NotFound(); }

        //use seriesId to get all the data
        var seriesId = series.Id;

        CmsObject cmsData = new CmsObject();
        cmsData.SereiesId = seriesId;
        cmsData.Name = series.Name;
        cmsData.Title = series.Title;
        cmsData.Summary = series.Summary;
        cmsData.SpecificationsLegacy = series.SpecificationsLegacy;
        cmsData.Description = series.Description;
        cmsData.Brand = series.Brand;

        cmsData.Image1 = series.Image1FileLinkTmb;
        cmsData.Image2 = series.Image2FileLinkTmb;
        cmsData.Image3 = series.Image3FileLinkTmb;
        cmsData.Image4 = series.Image4FileLinkTmb;
        cmsData.Image5 = series.Image5FileLinkTmb;
        cmsData.Image6 = series.Image6FileLinkTmb;
        cmsData.Image7 = series.Image7FileLinkTmb;
        cmsData.Image8 = series.Image8FileLinkTmb;
        cmsData.Image9 = series.Image9FileLinkTmb;

        cmsData.Video1 = series.Video1Link;
        cmsData.Video2 = series.Video2Link;
        cmsData.Video3 = series.Video3Link;


        cmsData.ZipFileLink = series.ImageZipFileLink;
        cmsData.CategoriesSelection = _context.Categories.FirstOrDefault(c => c.Id == series.CategoryId).TableName;

        cmsData.Attachments = await _context.SeriesAttachments.Where(sa => sa.SeriesId == seriesId).Select(x => new SeriesAttachmentsDTO
        {
            Id = x.Id,
            SeriesId = x.SeriesId,
            AttachmentFileLink = x.AttachmentFileLink,
            Type = x.Type,
            LanguageCode = x.LanguageCode,
            Modified = x.Modified,
            Created = x.Created,
        }).ToListAsync();

        cmsData.RelatedSeries = series.SeriesRelatedSeries.Select(x => new RelatedSeriesDTO
        {
            Id = x.Id,
            Name = x.RelatedSeries.Name,
            MainImage = x.Series.Products.FirstOrDefault().Image1FileLinkMd,
            Title = x.Series.Title,
            RelatedSeriesId = x.RelatedSeriesId,
        }).ToList();
        cmsData.RelatedSeriesSelection = cmsData.RelatedSeries.Select(rs => rs.Name).ToArray();

        //now load product data
        cmsData.Products = await _context.Products.Where(p => p.SeriesId == seriesId).Select(x => new ProductsDTO
        {
            Id = x.Id,
            PartNumber = x.PartNumber,
            Description = x.Description,
            Warranty = x.Warranty,
            Msrp = x.Msrp,
            TransactionStatus = x.TransactionStatus,
            SpecialHandling = x.SpecialHandling,
            UpcCode = x.UpcCode,
            PoLeadTime = x.PoLeadTime,
            ManufacturerLeadTime = x.ManufacturerLeadTime,
            HarmonizedCode = x.HarmonizedCode,
            CountryOfOrigin = x.CountryOfOrigin,
            RepairPrice = x.RepairPrice,
            EvaluationCharge = x.EvaluationCharge,
            CalibrationPrice = x.CalibrationPrice,
            ShippingCost = x.ShippingCost,
            MasterPartNumber = x.MasterPartNumber,
            PacksPerCarton = x.PacksPerCarton,
            PackSize = x.PackSize,
            PackUom = x.PackUom,
            Published = x.Published,
            Discontinued = x.Discontinued,
            RoHsCompliant = x.RoHsCompliant,
            Image1 = x.Image1FileLinkTmb,
            Image2 = x.Image2FileLinkTmb,
            Image3 = x.Image3FileLinkTmb,
            Image4 = x.Image4FileLinkTmb,
            Image5 = x.Image5FileLinkTmb,
            Image6 = x.Image6FileLinkTmb,
            Image7 = x.Image7FileLinkTmb,
            Image8 = x.Image8FileLinkTmb,
            Image9 = x.Image9FileLinkTmb,

        }).ToListAsync();

        return Ok(cmsData);
    }
    [HttpPost("create")]
    public async Task<ActionResult> PostCreateSeries(CmsObject data)
    {
        var seriesCheck = await _context.Series.FirstOrDefaultAsync(s => s.Id == data.SereiesId);
        if (seriesCheck != null)
        {
            return BadRequest();
        }

        var newSeries = new Series();
        newSeries.Id = default;
        newSeries.Name = data.Name;
        newSeries.Title = data.Title;
        newSeries.SpecificationsLegacy = data.SpecificationsLegacy;
        newSeries.Description = data.Description;
        newSeries.Brand = data.Brand;
        newSeries.Modified = DateTime.Now;
        newSeries.Created = DateTime.Now;
        newSeries.ReplacementSeries = await _context.Series.FirstOrDefaultAsync(s => s.Id == data.ReplacementSeriesId);
        newSeries.Published = data.Published;


        newSeries.Video1Link = data.Video1;
        newSeries.Video2Link = data.Video2;
        newSeries.Video3Link = data.Video3;

        var seriesEntity = await _context.Series.AddAsync(newSeries);
        await _context.SaveChangesAsync();

        var cat = await _context.Categories.FirstOrDefaultAsync(c => c.TableName == data.CategoriesSelection);
        newSeries.CategoryId = cat.Id;

        await _context.SaveChangesAsync();

        // Fetch related series
        var relatedSeries = await _context.Series
                                         .Where(s => data.RelatedSeriesSelection.Contains(s.Name))
                                         .ToListAsync();

        // Transform related series to associative entries for the new series
        var currentDateTime = DateTime.Now;
        var relatedSeriesAssociations = relatedSeries.Select(rs => new SeriesRelated
        {
            Id = default,
            Created = currentDateTime,
            Modified = currentDateTime,
            SeriesId = newSeries.Id,
            RelatedSeriesId = rs.Id
        }).ToList();

        // Add the associative entries to the new series' related series list
        newSeries.SeriesRelatedSeries.AddRange(relatedSeriesAssociations);

        for (int i = 1; i <= 9; i++)
        {
            string imageUrl = (string)typeof(CmsObject).GetProperty($"Image{i}")?.GetValue(data);
            if (!string.IsNullOrEmpty(imageUrl))
            {
                var result = await MoveAllImages(newSeries.Id, FileUploadHelper.GetRelativeUri(imageUrl), FileUploadHelper.GetProductionDirectory(typeof(Series)));
                if (result.originalLinkString == null)
                {
                    return BadRequest();
                }

                typeof(Series).GetProperty($"Image{i}FileLink")?.SetValue(newSeries, result.originalLinkString);
                typeof(Series).GetProperty($"Image{i}FileLinkTmb")?.SetValue(newSeries, result.tmbLinkString);
                typeof(Series).GetProperty($"Image{i}FileLinkMd")?.SetValue(newSeries, result.mdLinkString);
                typeof(Series).GetProperty($"Image{i}FileLinkLg")?.SetValue(newSeries, result.lgLinkString);
            }
        }



        await _context.SaveChangesAsync();

        var attachments = new List<SeriesAttachment>();
        foreach (var attach in data.Attachments)
        {
            var attachment = new SeriesAttachment();
            attachment.Id = default;
            attachment.SeriesId = newSeries.Id;
            attachment.Modified = DateTime.Now;
            attachment.Created = DateTime.Now;
            attachment.LanguageCode = attach.LanguageCode;
            attachment.Type = attach.Type;
            var result = await MoveAttachment(newSeries.Id, FileUploadHelper.GetRelativeUri(attach.AttachmentFileLink), FileUploadHelper.GetProductionDirectory(typeof(SeriesAttachment)));
            if (result == null)
            {
                return BadRequest();
            }
            attachment.AttachmentFileLink = result;
            attachments.Add(attachment);

        }
        newSeries.SeriesAttachments.AddRange(attachments);
        await _context.SaveChangesAsync();

        foreach (var p in data.Products)
        {
            var newProduct = new Product();
            newProduct.Id = default;
            newProduct.PartNumber = p.PartNumber;
            newProduct.SeriesId = newSeries.Id;
            newProduct.Modified = DateTime.Now;
            newProduct.Created = DateTime.Now;
            newSeries.Products.Add(newProduct);
            await _context.SaveChangesAsync();


            for (int i = 1; i <= 9; i++)
            {
                string imageUrl = (string)typeof(ProductsDTO).GetProperty($"Image{i}")?.GetValue(p);
                if (!string.IsNullOrEmpty(imageUrl))
                {
                    var result = await MoveAllImages(newProduct.Id, FileUploadHelper.GetRelativeUri(imageUrl), FileUploadHelper.GetProductionDirectory(typeof(Product)));
                    if (result.originalLinkString == null)
                    {
                        return BadRequest();
                    }

                    typeof(Product).GetProperty($"Image{i}FileLink")?.SetValue(newProduct, result.originalLinkString);
                    typeof(Product).GetProperty($"Image{i}FileLinkTmb")?.SetValue(newProduct, result.tmbLinkString);
                    typeof(Product).GetProperty($"Image{i}FileLinkMd")?.SetValue(newProduct, result.mdLinkString);
                    typeof(Product).GetProperty($"Image{i}FileLinkLg")?.SetValue(newProduct, result.lgLinkString);
                }
            }
            await _context.SaveChangesAsync();

        }



        return Ok(data.Name);

    }
    [HttpPost("clone/{seriesId}")]
    public async Task<ActionResult> PostCloneSeries(int seriesId)
    {
        var series = await _context.Series.FirstOrDefaultAsync(s => s.Id == seriesId);
        if (series == null)
        {
            return BadRequest();
        }
        var clone = new Series();
        clone.Id = default;
        clone.Name += series.Name + "_Clone";
        clone.Title = series.Title;
        clone.Summary = series.Summary;
        clone.SpecificationsLegacy = series.SpecificationsLegacy;
        clone.Description = series.Description;
        clone.Brand = series.Brand;
        clone.Modified = DateTime.Now;
        clone.Created = DateTime.Now;
        clone.Published = false;
        var entity = await _context.Series.AddAsync(clone);
        await _context.SaveChangesAsync();

        return Ok(clone.Name);

    }


    [HttpPost("update")]
    public async Task<ActionResult> PostUpdateSeries(CmsObject data)
    {
        var series = await _context.Series
            .Include(s => s.Products)
            .Include(s => s.SeriesAttachments)
            .Include(sr => sr.SeriesRelatedSeries)
            .FirstOrDefaultAsync(s => s.Id == data.SereiesId);
        if (series == null)
        {
            return BadRequest();
        }
        series.Name = data.Name;
        series.Title = data.Title;
        series.SpecificationsLegacy = data.SpecificationsLegacy;
        series.Description = data.Description;
        series.Brand = data.Brand;
        series.Modified = DateTime.Now;
        series.ReplacementSeries = await _context.Series.FirstOrDefaultAsync(s => s.Id == data.ReplacementSeriesId);
        series.Published = data.Published;

        series.Video1Link = data.Video1;
        series.Video2Link = data.Video2;
        series.Video3Link = data.Video3;


        await _context.SaveChangesAsync();

        // Delete the related series first
        foreach (var sr in series.SeriesRelatedSeries.ToList())
        {
            _context.SeriesRelateds.Remove(sr);
        }
        await _context.SaveChangesAsync();

        // Fetch related series
        if (data.RelatedSeriesSelection is not null)
        {

            var relatedSeries = await _context.Series
                                             .Where(s => data.RelatedSeriesSelection.Contains(s.Name))
                                             .ToListAsync();

            // Transform related series to associative entries for the new series
            var currentDateTime = DateTime.Now;
            var relatedSeriesAssociations = relatedSeries.Select(rs => new SeriesRelated
            {
                Id = default,
                Created = currentDateTime,
                Modified = currentDateTime,
                SeriesId = series.Id,
                RelatedSeriesId = rs.Id
            }).ToList();

            // Add the associative entries to the new series' related series list
            series.SeriesRelatedSeries.AddRange(relatedSeriesAssociations);
            await _context.SaveChangesAsync();
        }

        //not allowed to update category after creation.
        //var cat = await _context.Categories.FirstOrDefaultAsync(c => c.TableName == data.CategoriesSelection);
        //series.CategoryId = cat.Id;

        // Remember to save changes at the end
        await _context.SaveChangesAsync();



        for (int i = 1; i <= 9; i++)
        {
            string seriesImageTmbProperty = $"Image{i}FileLinkTmb";
            string dataImageProperty = $"Image{i}";

            string seriesImageTmbValue = (string)typeof(Series).GetProperty(seriesImageTmbProperty)?.GetValue(series);
            string dataImageValue = (string)typeof(CmsObject).GetProperty(dataImageProperty)?.GetValue(data);

            if (seriesImageTmbValue == dataImageValue)
            {
                //if image is the same skip
                continue;
            }

            (string originalLinkString, string tmbLinkString, string mdLinkString, string lgLinkString) result;
            if (string.IsNullOrEmpty(dataImageValue))
            {
                //if the image was cleared out delete it and update the database values to null
                result = (null, null, null, null);
                UpdateSeriesWithResult(series, result, i);
                continue;
            }
            result = await MoveAllImages(series.Id, FileUploadHelper.GetRelativeUri(dataImageValue), FileUploadHelper.GetProductionDirectory(typeof(Series)));
            if (result.originalLinkString == null)
            {
                return BadRequest();
            }
            UpdateSeriesWithResult(series, result, i);
        }
        await _context.SaveChangesAsync();


        // Convert data.Attachments to a list of Ids
        var attachmentIds = data.Attachments.Select(i => i.Id).ToList();

        // Find all images in series.SeriesImages but not in data.SharedImages
        var attachmentsToDelete = series.SeriesAttachments
            .Where(si => !attachmentIds.Contains(si.Id))
            .ToList();

        // Remove these images from the DbContext
        foreach (var attachmentToDelete in attachmentsToDelete)
        {
            _context.SeriesAttachments.Remove(attachmentToDelete);
            await _storageService.DeleteFileAsync(FileUploadHelper.GetRelativeUri(attachmentToDelete.AttachmentFileLink));
            // Save changes after each removal
            await _context.SaveChangesAsync();
        }
        foreach (var attach in data.Attachments)
        {
            //search for existing
            var attachmentSearch = series.SeriesAttachments.FirstOrDefault(sa => sa.Id == attach.Id);
            if (attachmentSearch.AttachmentFileLink == attach.AttachmentFileLink)
            {
                continue;
            }
            if (attachmentSearch == null)
            {
                //create if doesn't exist
                var attachment = new SeriesAttachment();
                attachment.Id = default;
                attachment.SeriesId = series.Id;
                attachment.Modified = DateTime.Now;
                attachment.Created = DateTime.Now;
                attachment.LanguageCode = attach.LanguageCode;
                attachment.Type = attach.Type;
                var result = await MoveAttachment(series.Id, FileUploadHelper.GetRelativeUri(attach.AttachmentFileLink), FileUploadHelper.GetProductionDirectory(typeof(SeriesAttachment)));
                if (result == null)
                {
                    return BadRequest();
                }
                attachment.AttachmentFileLink = result;
                series.SeriesAttachments.Add(attachment);
                await _context.SaveChangesAsync();
            }
            else
            {
                //update existing
                attachmentSearch.Modified = DateTime.Now;
                attachmentSearch.LanguageCode = attach.LanguageCode;
                attachmentSearch.Type = attach.Type;
                if (attach.AttachmentFileLink != attachmentSearch.AttachmentFileLink)
                {
                    var result1 = await MoveAttachment(series.Id, FileUploadHelper.GetRelativeUri(attach.AttachmentFileLink), FileUploadHelper.GetProductionDirectory(typeof(SeriesAttachment)));
                    if (result1 == null)
                    {
                        return BadRequest();
                    }
                    attachmentSearch.AttachmentFileLink = result1;
                }
                await _context.SaveChangesAsync();
            }
        }

        //delete products
        // Convert data.Attachments to a list of Ids
        var productIds = data.Products.Select(i => i.Id).ToList();

        // Find all images in series.SeriesImages but not in data.SharedImages
        var productsToDelete = series.Products
            .Where(p => !productIds.Contains(p.Id))
            .ToList();

        // Remove these images from the DbContext
        foreach (var productToDelete in productsToDelete)
        {
            _context.Products.Remove(productToDelete);
            for (int i = 1; i <= 9; i++)
            {
                var orig = (string)typeof(Product).GetProperty($"Image{i}FileLink")?.GetValue(productToDelete);
                var tmb = (string)typeof(Product).GetProperty($"Image{i}FileLinkTmb")?.GetValue(productToDelete);
                var md = (string)typeof(Product).GetProperty($"Image{i}FileLinkMd")?.GetValue(productToDelete);
                var lg = (string)typeof(Product).GetProperty($"Image{i}FileLinkLg")?.GetValue(productToDelete);
                if (orig == null)
                {
                    continue;
                }
                await _storageService.DeleteFileAsync(FileUploadHelper.GetRelativeUri(orig));
                await _storageService.DeleteFileAsync(FileUploadHelper.GetRelativeUri(tmb));
                await _storageService.DeleteFileAsync(FileUploadHelper.GetRelativeUri(md));
                await _storageService.DeleteFileAsync(FileUploadHelper.GetRelativeUri(lg));
            }

            // Save changes after each removal
            await _context.SaveChangesAsync();
        }
        //update products
        foreach (var p in data.Products)
        {
            var productSearch = series.Products.FirstOrDefault(sp => sp.Id == p.Id);
            if (productSearch == null)
            {
                //create
                var newProduct = new Product();
                newProduct.Id = default;
                newProduct.PartNumber = p.PartNumber;
                newProduct.SeriesId = series.Id;
                newProduct.Modified = DateTime.Now;
                newProduct.Created = DateTime.Now;
                series.Products.Add(newProduct);
                await _context.SaveChangesAsync();


                for (int i = 1; i <= 9; i++)
                {
                    string imageUrl = (string)typeof(ProductsDTO).GetProperty($"Image{i}")?.GetValue(p);

                    if (!string.IsNullOrEmpty(imageUrl))
                    {
                        var result = await MoveAllImages(newProduct.Id, FileUploadHelper.GetRelativeUri(imageUrl), FileUploadHelper.GetProductionDirectory(typeof(Product)));
                        if (result.originalLinkString == null)
                        {
                            return BadRequest();
                        }

                        typeof(Product).GetProperty($"Image{i}FileLink")?.SetValue(newProduct, result.originalLinkString);
                        typeof(Product).GetProperty($"Image{i}FileLinkTmb")?.SetValue(newProduct, result.tmbLinkString);
                        typeof(Product).GetProperty($"Image{i}FileLinkMd")?.SetValue(newProduct, result.mdLinkString);
                        typeof(Product).GetProperty($"Image{i}FileLinkLg")?.SetValue(newProduct, result.lgLinkString);
                    }
                }
                await _context.SaveChangesAsync();
            }
            else
            {
                //update
                productSearch.PartNumber = p.PartNumber;
                productSearch.SeriesId = series.Id;
                productSearch.Modified = DateTime.Now;
                productSearch.Created = DateTime.Now;

                for (int i = 1; i <= 9; i++)
                {
                    string imageUrl = (string)typeof(ProductsDTO).GetProperty($"Image{i}")?.GetValue(p);
                    var tmb = (string)typeof(Product).GetProperty($"Image{i}FileLinkTmb")?.GetValue(productSearch);
                    if (imageUrl == tmb)
                    {
                        continue;
                    }

                    (string originalLinkString, string tmbLinkString, string mdLinkString, string lgLinkString) result;
                    if (string.IsNullOrEmpty(imageUrl))
                    {
                        //if the image was cleared out delete it and update the database values to null
                        result = (null, null, null, null);
                        UpdateProductWithResult(productSearch, result, i);
                        continue;
                    }
                    result = await MoveAllImages(series.Id, FileUploadHelper.GetRelativeUri(imageUrl), FileUploadHelper.GetProductionDirectory(typeof(Product)));
                    if (result.originalLinkString == null)
                    {
                        return BadRequest();
                    }
                    UpdateProductWithResult(productSearch, result, i);
                }
                await _context.SaveChangesAsync();
            }
        }


        return Ok(data.Name);
    }
    [HttpPost("delete/{seriesId}")]
    public async Task<ActionResult> PostDeleteSeries(int seriesId)
    {
        var series = await _context.Series
            .Include(s => s.Products)
            .Include(s => s.SeriesAttachments)
            .Include(sr => sr.SeriesRelatedSeries)
            .FirstOrDefaultAsync(s => s.Id == seriesId);
        if (series == null)
        {
            return BadRequest();
        }

        // Delete the associated entities
        foreach (var product in series.Products.ToList())
        {
            _context.Products.Remove(product);
        }

        foreach (var attachment in series.SeriesAttachments.ToList())
        {
            _context.SeriesAttachments.Remove(attachment);
        }

        foreach (var related in series.SeriesRelatedSeries.ToList())
        {
            _context.SeriesRelateds.Remove(related);
        }



        _context.Series.Remove(series);
        await _context.SaveChangesAsync();

        return Ok(seriesId);
    }

    private async Task<string> MoveAttachment(int id, string relativeSourceUri, string relativeDestinationUri)
    {
        var filename = Path.GetFileName(relativeSourceUri);
        var filenameWithoutExtension = Path.GetFileNameWithoutExtension(filename);
        var extension = Path.GetExtension(filename);
        var relativeDestinationUriOrig = relativeDestinationUri + id + "/" + filenameWithoutExtension + "-orig" + extension;
        await Task.Delay(TimeSpan.FromSeconds(1)); // Added delay
        var originalSuccess = await _storageService.MoveFileAsync(relativeSourceUri, relativeDestinationUriOrig);

        if (originalSuccess)
        {
            var originalLinkString = FileUploadHelper.AppendCustomDomainUrl(relativeDestinationUriOrig);
            return (originalLinkString);
        }
        return null;
    }

    private async Task<(string originalLinkString, string tmbLinkString, string mdLinkString, string lgLinkString)> MoveAllImages(int id, string relativeSourceUri, string relativeDestinationUri)
    {
        var filename = Path.GetFileName(relativeSourceUri);
        var filenameWithoutExtension = Path.GetFileNameWithoutExtension(filename);
        var extension = Path.GetExtension(filename);

        var relativeDestinationUriOrig = relativeDestinationUri + id + "/" + filenameWithoutExtension + "-orig" + extension;
        var relativeDestinationUriTmb = relativeDestinationUri + id + "/" + filenameWithoutExtension + "-tmb.jpg";
        var relativeDestinationUriMd = relativeDestinationUri + id + "/" + filenameWithoutExtension + "-md.jpg";
        var relativeDestinationUriLg = relativeDestinationUri + id + "/" + filenameWithoutExtension + "-lg.jpg";

        await Task.Delay(TimeSpan.FromSeconds(1)); // Added delay
        var originalSuccess = await _storageService.MoveFileAsync(relativeSourceUri, relativeDestinationUriOrig);
        var tmbMoveSuccess = await _storageService.MoveFileAsync(Path.GetDirectoryName(relativeSourceUri) + Path.DirectorySeparatorChar + filenameWithoutExtension + "-tmb.jpg", relativeDestinationUriTmb);
        var mdMoveSuccess = await _storageService.MoveFileAsync(Path.GetDirectoryName(relativeSourceUri) + Path.DirectorySeparatorChar + filenameWithoutExtension + "-md.jpg", relativeDestinationUriMd);
        var lgMoveSuccess = await _storageService.MoveFileAsync(Path.GetDirectoryName(relativeSourceUri) + Path.DirectorySeparatorChar + filenameWithoutExtension + "-lg.jpg", relativeDestinationUriLg);

        if (tmbMoveSuccess && mdMoveSuccess && lgMoveSuccess && originalSuccess)
        {
            var originalLinkString = FileUploadHelper.AppendCustomDomainUrl(relativeDestinationUriOrig);
            var tmbLinkString = FileUploadHelper.AppendCustomDomainUrl(relativeDestinationUriTmb);
            var mdLinkString = FileUploadHelper.AppendCustomDomainUrl(relativeDestinationUriMd);
            var lgLinkString = FileUploadHelper.AppendCustomDomainUrl(relativeDestinationUriLg);
            return (originalLinkString, tmbLinkString, mdLinkString, lgLinkString);
        }

        return (null, null, null, null);
    }

    private async Task DeleteImagesByNumber(Series series, int number)
    {
        await _storageService.DeleteFileAsync(FileUploadHelper.GetRelativeUri((string)typeof(Series).GetProperty($"Image{number}FileLink")?.GetValue(series)));
        await _storageService.DeleteFileAsync(FileUploadHelper.GetRelativeUri((string)typeof(Series).GetProperty($"Image{number}FileLinkLg")?.GetValue(series)));
        await _storageService.DeleteFileAsync(FileUploadHelper.GetRelativeUri((string)typeof(Series).GetProperty($"Image{number}FileLinkMd")?.GetValue(series)));
        await _storageService.DeleteFileAsync(FileUploadHelper.GetRelativeUri((string)typeof(Series).GetProperty($"Image{number}FileLinkTmb")?.GetValue(series)));
    }

    private void UpdateSeriesWithResult(Series series, (string originalLinkString, string tmbLinkString, string mdLinkString, string lgLinkString) result, int number)
    {
        typeof(Series).GetProperty($"Image{number}FileLink")?.SetValue(series, result.originalLinkString);
        typeof(Series).GetProperty($"Image{number}FileLinkLg")?.SetValue(series, result.lgLinkString);
        typeof(Series).GetProperty($"Image{number}FileLinkMd")?.SetValue(series, result.mdLinkString);
        typeof(Series).GetProperty($"Image{number}FileLinkTmb")?.SetValue(series, result.tmbLinkString);
    }

    private void UpdateProductWithResult(Product product, (string originalLinkString, string tmbLinkString, string mdLinkString, string lgLinkString) result, int number)
    {
        typeof(Product).GetProperty($"Image{number}FileLink")?.SetValue(product, result.originalLinkString);
        typeof(Product).GetProperty($"Image{number}FileLinkLg")?.SetValue(product, result.lgLinkString);
        typeof(Product).GetProperty($"Image{number}FileLinkMd")?.SetValue(product, result.mdLinkString);
        typeof(Product).GetProperty($"Image{number}FileLinkTmb")?.SetValue(product, result.tmbLinkString);
    }
}

