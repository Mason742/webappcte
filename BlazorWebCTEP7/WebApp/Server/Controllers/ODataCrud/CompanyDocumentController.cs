﻿using Azure.Storage.Blobs.Models;

namespace WebApp.Server.Controllers.Admin;

public class CompanyDocumentController : GenericBaseODataController<CompanyDocument>
{
    public CompanyDocumentController(ApplicationDbContext context, ILogger<GenericBaseODataController<CompanyDocument>> logger, IEmailSenderService emailSenderService, IStorageService storageService) : base(context, logger, emailSenderService, storageService)
    {
         
    }
    public override ActionResult<IQueryable<CompanyDocument>> Get()
    {
        return base.Get();
    }
}
