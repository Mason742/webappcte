﻿using Microsoft.AspNetCore.OData.Deltas;
using Microsoft.AspNetCore.OData.Formatter;
using Microsoft.AspNetCore.OData.Query;
using Microsoft.AspNetCore.OData.Routing.Controllers;
/// <summary>
/// https://devblogs.microsoft.com/odata/routing-in-asp-net-core-8-0-preview/
/// </summary>
namespace WebApp.Server.Controllers.Admin;

[ResponseCache(NoStore = true, Location = ResponseCacheLocation.None)]
public class GenericBaseODataController<T> : ODataController
    where T : class, IEntityBase
{
    protected readonly ApplicationDbContext _context;
    protected readonly ILogger<GenericBaseODataController<T>> _logger;
    protected readonly IEmailSenderService _emailSender;
    protected readonly IStorageService _storageService;
    public GenericBaseODataController(ApplicationDbContext context, ILogger<GenericBaseODataController<T>> logger, IEmailSenderService emailSender)
    {
        _context = context;
        _emailSender = emailSender;
        _logger = logger;
    }

    public GenericBaseODataController(ApplicationDbContext context, ILogger<GenericBaseODataController<T>> logger, IEmailSenderService emailSender, IStorageService storageService)
    {
        _context = context;
        _emailSender = emailSender;
        _logger = logger;
        _storageService = storageService;
    }

    [Authorize(Roles = "Administrator")]
    [EnableQuery]
    public virtual ActionResult<IQueryable<T>> Get()
    {
        Response.Headers.TryAdd("Cache-Control", "no-store, no-cache");
        Response.Headers.TryAdd("Pragma", "no-cache");
        IQueryable<T> data;
        try
        {
            if (Request.Headers.ContainsKey("WebCMS"))
            {
                data = _context.Set<T>().IgnoreQueryFilters().AsQueryable();
            }
            else
            {
                data = _context.Set<T>().AsQueryable();
            }
            return Ok(data);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Could not find resource");
            return NotFound(ex.InnerException.Message);
        }

    }

    [Authorize(Roles = "Administrator")]
    [EnableQuery]
    public virtual IActionResult Get(int key)
    {
        Response.Headers.TryAdd("Cache-Control", "no-store, no-cache");
        Response.Headers.TryAdd("Pragma", "no-cache");
        try
        {
            return Ok(_context.Set<T>().FirstOrDefault(c => c.Id == key));
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Could not find resource by Id");
            return NotFound(ex.InnerException.Message);
        }
    }

    [Authorize(Roles = "Administrator")]
    public virtual async Task<IActionResult> Post([FromBody] T entity)
    {
        try
        {
            await _context.Set<T>().AddAsync(entity);
            await _context.SaveChangesAsync();

            bool success = await UploadFileLinkIfNeeded(entity.GetType(), entity);
            return Ok(entity);
        }
        catch (DbUpdateException ex)
        {
            _logger.LogError(ex, "Could not save entity");
            if (ex.InnerException != null)
            {
                return BadRequest(ex.InnerException.Message);
            }
            return BadRequest(ex.Message);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Could not save entity");
            if (ex.InnerException != null)
            {
                return BadRequest(ex.InnerException.Message);
            }
            return BadRequest(ex.Message);
        }

    }

    [Authorize(Roles = "Administrator")]
    public virtual async Task<IActionResult> Patch([FromODataUri] int key, [FromBody] Delta<T> data)
    {
        var entity = await _context.Set<T>().FindAsync(key);
        if (entity == null)
        {
            return NotFound();
        }
        data.Patch(entity);
        try
        {
            await _context.SaveChangesAsync();
        }
        catch (DbUpdateConcurrencyException)
        {
            if (!EntityExists(key))
            {
                return NotFound();
            }
            else
            {
                throw;
            }
        }
        return Ok();
    }

    [Authorize(Roles = "Administrator")]
    public virtual async Task<IActionResult> Put([FromODataUri] int key, [FromBody] T entity)
    {
        if (key != entity.Id)
        {
            return BadRequest();
        }
        _context.Entry(entity).State = EntityState.Modified;
        try
        {
            bool success = await UploadFileLinkIfNeeded(entity.GetType(), entity);
            await _context.SaveChangesAsync();
            return Ok(entity);
        }
        catch (DbUpdateConcurrencyException)
        {
            if (!EntityExists(key))
            {
                return NotFound();
            }
            else
            {
                throw;
            }
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Could not save entity");
            if (ex.InnerException != null)
            {
                return BadRequest(ex.InnerException.Message);
            }
            return BadRequest(ex.Message);
        }
    }

    [Authorize(Roles = "Administrator")]
    public virtual async Task<ActionResult> Delete([FromODataUri] int key)
    {
        var entity = await _context.Set<T>().FindAsync(key);
        if (entity == null)
        {
            return NotFound("Entity not found by that Id");
        }
        try
        {
            _context.Set<T>().Remove(entity);
            await _context.SaveChangesAsync();
            var entityType = entity.GetType();
            var prop = GetFileLinkPropertyInfo(entityType);
            if (prop != null)
            {
                var propValueFileLink = prop.GetValue(entity).ToString();
                var relativeSourceUri = FileUploadHelper.GetRelativeUri(propValueFileLink);
                
                //since all files are uploaded under their folder id it ensures they are unique and no other resource/image is used by another resource
                //thus not requiring to check if used by other resource we can simply delete it
                await _storageService.DeleteFileAsync(relativeSourceUri);
            }
            return NoContent();
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Failed to delete entity");
            return BadRequest(ex.InnerException.Message);
        }

    }

    private bool EntityExists(int key)
    {
        return _context.Set<T>().Any(x => x.Id == key);
    }

    private async Task<bool> UploadFileLinkIfNeeded(Type entityType, T entity)
    {
        var prop = GetFileLinkPropertyInfo(entityType);
        if (prop == null|| prop.GetValue(entity).ToString().Contains("www.youtube.com/embed"))
        {
            return false;
        }

        var propValueFileLink = prop.GetValue(entity).ToString();
        var relativeSourceUri = FileUploadHelper.GetRelativeUri(propValueFileLink);
        var filename = Path.GetFileName(relativeSourceUri);
        var relativeDestinationUri = FileUploadHelper.GetProductionDirectory(entityType) + entity.Id + "/" + filename;

        //do not do anything if it is the same exact image
        if (relativeSourceUri == relativeDestinationUri)
        {
            return false;
        }

        // !!! if type is image then move all images sizes -tmb -md -lg -original
        if (entityType == typeof(Series) || entityType == typeof(Product))
        {
            return await MovalAllImages(entityType, entity, relativeSourceUri, filename, relativeDestinationUri, prop);
        }

        var success = await _storageService.MoveFileAsync(relativeSourceUri, relativeDestinationUri);
        if (success)
        {
            var destinationUrl = FileUploadHelper.AppendCustomDomainUrl(relativeDestinationUri);
            prop.SetValue(entity, destinationUrl);
            await _context.SaveChangesAsync();
        }
        return success;
    }

    private static PropertyInfo GetFileLinkPropertyInfo(Type entityType)
    {
        return entityType.GetProperties().Where(x => x.Name.Contains("FileLink")).FirstOrDefault();
    }
    private static PropertyInfo GetImageFileLinkPropertyInfo(Type entityType, string searchValue)
    {
        return entityType.GetProperties().Where(x => x.Name.Contains(searchValue)).FirstOrDefault();
    }

    private async Task<bool> MovalAllImages(Type entityType, T entity, string relativeSourceUri, string filename, string relativeDestinationUri, PropertyInfo prop)
    {
        var filenameWithoutExtension = Path.GetFileNameWithoutExtension(filename);
        var ext = Path.GetExtension(relativeSourceUri);

        var relativeDestinationUriTmb = FileUploadHelper.GetProductionDirectory(entityType) + entity.Id + "/" + filenameWithoutExtension + "-tmb.jpg";
        var relativeDestinationUriMd = FileUploadHelper.GetProductionDirectory(entityType) + entity.Id + "/" + filenameWithoutExtension + "-md.jpg";
        var relativeDestinationUriLg = FileUploadHelper.GetProductionDirectory(entityType) + entity.Id + "/" + filenameWithoutExtension + "-lg.jpg";


        var tmbProp = GetImageFileLinkPropertyInfo(entityType, nameof(Series.Image1FileLinkTmb));
        var mdProp = GetImageFileLinkPropertyInfo(entityType, nameof(Series.Image1FileLinkMd));
        var lgProp = GetImageFileLinkPropertyInfo(entityType, nameof(Series.Image1FileLinkLg));

        //allow at least an additional two seconds before moving files
        //if user is moving to fast they may upload and save before there are files from temp folder to move.
        await Task.Delay(2);
        var tmbMoveSuccess = await _storageService.MoveFileAsync(Path.GetDirectoryName(relativeSourceUri) + Path.DirectorySeparatorChar + filenameWithoutExtension + "-tmb.jpg", relativeDestinationUriTmb);
        var mdMoveSuccess = await _storageService.MoveFileAsync(Path.GetDirectoryName(relativeSourceUri) + Path.DirectorySeparatorChar + filenameWithoutExtension + "-md.jpg", relativeDestinationUriMd);
        var lgMoveSuccess = await _storageService.MoveFileAsync(Path.GetDirectoryName(relativeSourceUri) + Path.DirectorySeparatorChar + filenameWithoutExtension + "-lg.jpg", relativeDestinationUriLg);
        //move original png too.
        var originalSuccess = await _storageService.MoveFileAsync(relativeSourceUri, relativeDestinationUri);

        if (tmbMoveSuccess && mdMoveSuccess && lgMoveSuccess && originalSuccess)
        {
            var destinationUrl = FileUploadHelper.AppendCustomDomainUrl(relativeDestinationUri);
            tmbProp.SetValue(entity, FileUploadHelper.AppendCustomDomainUrl(relativeDestinationUriTmb));
            mdProp.SetValue(entity, FileUploadHelper.AppendCustomDomainUrl(relativeDestinationUriMd));
            lgProp.SetValue(entity, FileUploadHelper.AppendCustomDomainUrl(relativeDestinationUriLg));
            prop.SetValue(entity, destinationUrl);
            await _context.SaveChangesAsync();
            return true;
        }
        return false;
    }
}
