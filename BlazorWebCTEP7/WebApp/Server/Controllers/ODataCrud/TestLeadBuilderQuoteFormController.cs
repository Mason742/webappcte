﻿using EmailSenderService.Services;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Server.Controllers.Admin;

public class TestLeadBuilderQuoteFormController : GenericBaseODataController<TestLeadBuilderQuoteForm>
{
    public TestLeadBuilderQuoteFormController(ApplicationDbContext context, ILogger<GenericBaseODataController<TestLeadBuilderQuoteForm>> logger, IEmailSenderService emailSenderService) : base(context, logger, emailSenderService)
    {
    }
}
