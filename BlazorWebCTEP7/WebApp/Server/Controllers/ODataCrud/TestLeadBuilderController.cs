﻿using EmailSenderService.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace WebApp.Server.Controllers.Admin;

public class TestLeadBuilderController : GenericBaseODataController<TestLeadBuilder>
{
    public TestLeadBuilderController(ApplicationDbContext context, ILogger<GenericBaseODataController<TestLeadBuilder>> logger, IEmailSenderService emailSenderService) : base(context, logger, emailSenderService)
    {}
    [HttpPost("api/TestLeadBuilder/ConfiguratorGenerator")]
    public async Task<IActionResult> PostConfiguratorGenerator([FromBody] ConfiguratorGenerator data)
    {
        foreach (var cv1 in data.Connector1Ids)
        {
            foreach (var cv2 in data.Connector2Ids)
            {
                foreach (var w in data.WireIds)
                {
                    if (await _context.TestLeadBuilders.Where(x => x.ConnectorOneId == cv1 && x.ConnectorTwoId == cv2 && x.WireId == w).CountAsync() > 0)
                    {
                        continue;
                    }

                    await _context.TestLeadBuilders.AddAsync(new TestLeadBuilder
                    {
                        ConnectorOneId = (int)cv1,
                        ConnectorTwoId = (int)cv2,
                        WireId = (int)w,
                        MinCentimeters = data.MinCentimeter,
                        MaxCentimeters = data.MaxCentimeter,
                        
                        DiagramFileLink = $"{cv1}{cv2}.jpg",
                        PartNumber = data.PartNumber,
                        Published = false
                    });
                }
            }
        }
        try
        {
            _context.SaveChanges();            
            return Ok(data);
        }
        catch(Exception ex)
        {
            return BadRequest(ex.InnerException.Message);
        }
    }
}
