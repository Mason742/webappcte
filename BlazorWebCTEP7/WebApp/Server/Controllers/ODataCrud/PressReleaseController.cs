﻿using EmailSenderService.Services;
using Microsoft.AspNetCore.Mvc;
using StorageService.Services;

namespace WebApp.Server.Controllers.Admin;

public class PressReleaseController : GenericBaseODataController<PressRelease>
{
    public PressReleaseController(ApplicationDbContext context, ILogger<GenericBaseODataController<PressRelease>> logger, IEmailSenderService emailSenderService, IStorageService storageService) : base(context, logger, emailSenderService, storageService)
    {
    }
}
