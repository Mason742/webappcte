﻿using EmailSenderService.Services;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Server.Controllers.Admin;

public class TestLeadLengthController : GenericBaseODataController<TestLeadLength>
{
    public TestLeadLengthController(ApplicationDbContext context, ILogger<GenericBaseODataController<TestLeadLength>> logger, IEmailSenderService emailSenderService) : base(context, logger, emailSenderService)
    {
    }
}
