﻿namespace WebApp.Server.Controllers.Admin;

public class CategoryController : GenericBaseODataController<Category>
{
    public CategoryController(ApplicationDbContext context, ILogger<GenericBaseODataController<Category>> logger, IEmailSenderService emailSenderService, IStorageService storageService) : base(context, logger, emailSenderService, storageService)
    {
    }
}
