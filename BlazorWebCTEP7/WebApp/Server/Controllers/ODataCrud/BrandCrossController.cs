﻿namespace WebApp.Server.Controllers.Admin;

public class BrandCrossController : GenericBaseODataController<BrandCross>
{
    public BrandCrossController(ApplicationDbContext context, ILogger<GenericBaseODataController<BrandCross>> logger, IEmailSenderService emailSenderService) : base(context, logger, emailSenderService)
    {
    }
}
