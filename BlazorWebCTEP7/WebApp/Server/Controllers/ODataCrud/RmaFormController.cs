﻿using EmailSenderService.Services;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Server.Controllers.Admin;

public class RmaFormController : GenericBaseODataController<RmaForm>
{
    public RmaFormController(ApplicationDbContext context, ILogger<GenericBaseODataController<RmaForm>> logger, IEmailSenderService emailSenderService) : base(context, logger, emailSenderService)
    {
    }
}
