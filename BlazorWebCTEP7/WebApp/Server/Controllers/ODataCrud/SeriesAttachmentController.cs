﻿using EmailSenderService.Services;
using Microsoft.AspNetCore.Mvc;
using StorageService.Services;

namespace WebApp.Server.Controllers.Admin;

public class SeriesAttachmentController : GenericBaseODataController<SeriesAttachment>
{
    public SeriesAttachmentController(ApplicationDbContext context, ILogger<GenericBaseODataController<SeriesAttachment>> logger, IEmailSenderService emailSenderService, IStorageService storageService) : base(context, logger, emailSenderService, storageService)
    {
    }
}
