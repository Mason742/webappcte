﻿namespace WebApp.Server.Controllers.Admin;

public class DistributorStockController : GenericBaseODataController<DistributorStock>
{
    public DistributorStockController(ApplicationDbContext context, ILogger<GenericBaseODataController<DistributorStock>> logger, IEmailSenderService emailSenderService) : base(context, logger, emailSenderService)
    {
    }
}
