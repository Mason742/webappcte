﻿namespace WebApp.Server.Controllers.Admin;

public class AuthorizedDistributorController : GenericBaseODataController<AuthorizedDistributor>
{
    public AuthorizedDistributorController(ApplicationDbContext context, ILogger<GenericBaseODataController<AuthorizedDistributor>> logger, IEmailSenderService emailSenderService) : base(context, logger, emailSenderService)
    {
    }
}
