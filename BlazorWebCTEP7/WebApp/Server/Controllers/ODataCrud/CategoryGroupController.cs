﻿namespace WebApp.Server.Controllers.Admin;

public class CategoryGroupController : GenericBaseODataController<CategoryGroup>
{
    public CategoryGroupController(ApplicationDbContext context, ILogger<GenericBaseODataController<CategoryGroup>> logger, IEmailSenderService emailSenderService) : base(context, logger, emailSenderService)
    {
    }
}
