﻿using EmailSenderService.Services;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Server.Controllers.Admin;

public class SeriesRelatedController : GenericBaseODataController<SeriesRelated>
{
    public SeriesRelatedController(ApplicationDbContext context, ILogger<GenericBaseODataController<SeriesRelated>> logger, IEmailSenderService emailSenderService) : base(context, logger, emailSenderService)
    {
    }
}
