﻿namespace WebApp.Server.Controllers.Admin;

public class ArticleController : GenericBaseODataController<Article>
{
    public ArticleController(ApplicationDbContext context, ILogger<GenericBaseODataController<Article>> logger, IEmailSenderService emailSenderService) : base(context, logger, emailSenderService)
    {
    }
}

