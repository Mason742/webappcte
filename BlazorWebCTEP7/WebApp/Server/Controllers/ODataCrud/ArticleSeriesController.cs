﻿namespace WebApp.Server.Controllers.Admin;

public class ArticleSeriesController : GenericBaseODataController<ArticleSeries>
{
    public ArticleSeriesController(ApplicationDbContext context, ILogger<GenericBaseODataController<ArticleSeries>> logger, IEmailSenderService emailSenderService) : base(context, logger, emailSenderService)
    {}

    public override async Task<IActionResult> Post([FromBody] ArticleSeries entity)
    {
        if (await _context.ArticleSeries.AnyAsync(x => x.ArticleId == entity.ArticleId && x.SeriesId == entity.SeriesId))
        {
            return BadRequest("Cannot add duplicate entities.");
        }

        return await base.Post(entity);
    }

    [Authorize(Roles = "Administrator")]
    [HttpPost("api/ArticleSeries/MultipleAssociation")]
    public async Task<IActionResult> PostArticleSerieses([FromBody] IDictionary<string, int[]> data)
    {

        var seriesIds = data["seriesValue"];
        var articleIds = data["articleValue"];

        foreach (var sId in seriesIds)
        {
            foreach (var aId in articleIds)
            {
                if (await _context.ArticleSeries.Where(x => x.Id == sId && x.ArticleId == aId).CountAsync() > 0) { continue; }
                await _context.ArticleSeries.AddAsync(new ArticleSeries { ShowOnSeries = false, ArticleId = (int)aId, SeriesId = (int)sId });
            }
        }
        try
        {
            _context.SaveChanges(); 
            return Ok(data);
        }
        catch(Exception ex)
        {
            return BadRequest(ex.InnerException.Message);
        }
    }
}
