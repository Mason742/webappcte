﻿namespace WebApp.Server.Controllers.Admin;

public class ErpInventoryController : GenericBaseODataController<ErpInventory>
{
    public ErpInventoryController(ApplicationDbContext context, ILogger<GenericBaseODataController<ErpInventory>> logger, IEmailSenderService emailSenderService) : base(context, logger, emailSenderService)
    {
    }
}
