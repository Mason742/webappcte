﻿using EmailSenderService.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace WebApp.Server.Controllers.Admin;

public class TestLeadWireController : GenericBaseODataController<TestLeadWire>
{
    public TestLeadWireController(ApplicationDbContext context, ILogger<GenericBaseODataController<TestLeadWire>> logger, IEmailSenderService emailSenderService) : base(context, logger, emailSenderService)
    { }
    [HttpGet("api/TestLeadWire/PartNumberAndDescription")]
    public ActionResult<IQueryable<object>> GetPartNumberAndDescription()
    {
        IQueryable<object> data;
        if (Request.Headers.ContainsKey("WebCMS"))
        {
            data = _context.TestLeadWires.Select(x => new { TestLeadWireId = x.Id, PartNumberAndDescription = ($"{x.PartNumber} - {x.Description}") }).IgnoreQueryFilters().AsQueryable();

        }
        else
        {
            data = _context.TestLeadWires.Select(x => new { TestLeadWireId = x.Id, PartNumberAndDescription = ($"{x.PartNumber} - {x.Description}") }).IgnoreQueryFilters().AsQueryable();
        }
        return Ok(data);
    }

}
