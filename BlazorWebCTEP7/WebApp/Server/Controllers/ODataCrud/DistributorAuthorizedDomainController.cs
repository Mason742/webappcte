﻿namespace WebApp.Server.Controllers.Admin;

public class DistributorAuthorizedDomainController : GenericBaseODataController<DistributorAuthorizedDomain>
{
    public DistributorAuthorizedDomainController(ApplicationDbContext context, ILogger<GenericBaseODataController<DistributorAuthorizedDomain>> logger, IEmailSenderService emailSenderService) : base(context, logger, emailSenderService)
    {
    }
}
