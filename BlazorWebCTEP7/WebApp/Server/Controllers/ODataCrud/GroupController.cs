﻿using EmailSenderService.Services;
using Microsoft.AspNetCore.Mvc;
using StorageService.Services;

namespace WebApp.Server.Controllers.Admin;

public class GroupController : GenericBaseODataController<Group>
{
    public GroupController(ApplicationDbContext context, ILogger<GenericBaseODataController<Group>> logger, IEmailSenderService emailSenderService, IStorageService storageService) : base(context, logger, emailSenderService, storageService)
    {
    }
}
