﻿using EmailSenderService.Services;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Server.Controllers.Admin;

public class TestLeadColorController : GenericBaseODataController<TestLeadColor>
{
    public TestLeadColorController(ApplicationDbContext context, ILogger<GenericBaseODataController<TestLeadColor>> logger, IEmailSenderService emailSenderService) : base(context, logger, emailSenderService)
    {
    }
}
