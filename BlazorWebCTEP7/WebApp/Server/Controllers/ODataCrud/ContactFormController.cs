﻿namespace WebApp.Server.Controllers.Admin;

public class ContactFormController : GenericBaseODataController<ContactForm>
{
    public ContactFormController(ApplicationDbContext context, ILogger<GenericBaseODataController<ContactForm>> logger, IEmailSenderService emailSenderService) : base(context, logger, emailSenderService)
    {
    }
}
