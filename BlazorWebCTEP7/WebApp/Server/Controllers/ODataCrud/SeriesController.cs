﻿using EmailSenderService.Services;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Server.Controllers.Admin;

public class SeriesController : GenericBaseODataController<Series>
{
    public SeriesController(ApplicationDbContext context, ILogger<GenericBaseODataController<Series>> logger, IEmailSenderService emailSenderService) : base(context, logger, emailSenderService)
    {
    }
}
