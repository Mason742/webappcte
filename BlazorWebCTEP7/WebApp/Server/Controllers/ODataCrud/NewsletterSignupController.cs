﻿using EmailSenderService.Services;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Server.Controllers.Admin;

public class NewsletterSignupController : GenericBaseODataController<NewsletterSignup>
{
    public NewsletterSignupController(ApplicationDbContext context, ILogger<GenericBaseODataController<NewsletterSignup>> logger, IEmailSenderService emailSenderService) : base(context, logger, emailSenderService)
    { }

    [HttpGet("api/Test222")]
    public IActionResult Test()
    {
        return Ok();
    }
}
