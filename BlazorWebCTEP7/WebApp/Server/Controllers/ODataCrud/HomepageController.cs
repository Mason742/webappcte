﻿using EmailSenderService.Services;
using Microsoft.AspNetCore.Mvc;
using StorageService.Services;

namespace WebApp.Server.Controllers.Admin;

public class HomepageController : GenericBaseODataController<Homepage>
{
    public HomepageController(ApplicationDbContext context, ILogger<GenericBaseODataController<Homepage>> logger, IEmailSenderService emailSenderService, IStorageService storageService) : base(context, logger, emailSenderService, storageService)
    {
    }
}
