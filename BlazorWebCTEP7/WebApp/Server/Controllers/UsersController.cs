﻿using Microsoft.Graph;
using MicrosoftGraph.Services;

namespace WebApp.Server.Controllers;


[ApiExplorerSettings(IgnoreApi = true)]
[Route("api/[controller]")]
[ApiController]
public class UsersController : ControllerBase
{
    private readonly ApplicationDbContext _context;
    private readonly IEmailSenderService _emailSender;
    private readonly GraphServiceClient _graphServiceClient;
    private readonly ILogger<UsersController> _logger;

    public UsersController(ApplicationDbContext context, IEmailSenderService emailSender, GraphServiceClient graphServiceClient, ILogger<UsersController> logger)
    {
        _context = context;
        _emailSender = emailSender;
        _graphServiceClient = graphServiceClient;
        _logger = logger;
    }

    #region Users
    [Authorize(Roles = "Administrator")]
    [HttpGet]
    public async Task<ActionResult<List<MicrosoftGraphUserDTO>>> GetUsers()
    {
        var users = await UserService.ListOfUsersWithCustomAttribute(_graphServiceClient);
        var userModels = users.Where(x => !x.Identities.Where(x => x.Issuer == "ExternalAzureAD" || x.IssuerAssignedId == "Admin@cteappb2c.onmicrosoft.com").Any()).Select(x =>
              {
                  return new MicrosoftGraphUserDTO
                  {
                      Id = x.Id,
                      GivenName = x.GivenName,
                      Surname = x.Surname,
                      Email = x.Identities.Where(x => x.SignInType == "emailAddress" && x.Issuer == "cteappb2c.onmicrosoft.com").FirstOrDefault().IssuerAssignedId,
                      DisplayName = x.DisplayName,
                      CompanyName = x.CompanyName,
                      CustomerId = x.AdditionalData.Any(x => x.Key == "extension_6df63c96d879496db5c865162950daec_CustomerId") ? x.AdditionalData["extension_6df63c96d879496db5c865162950daec_CustomerId"].ToString() : null,
                      Role = x.AdditionalData.Any(x => x.Key == "extension_6df63c96d879496db5c865162950daec_Role") ? x.AdditionalData["extension_6df63c96d879496db5c865162950daec_Role"].ToString() : null
                  };
              }).ToList();
        return Ok(userModels);
    }

    [Authorize(Roles = "Administrator")]
    [HttpPost]
    public async Task<ActionResult> UpdateUsers([FromBody] MicrosoftGraphUserDTO userModel)
    {
        if (userModel == null)
            return BadRequest("No userModel sent");

        try
        {
            await UserService.UpdateUserByUserModel(_graphServiceClient, userModel);
            return Ok("User updated");
        }
        catch (Exception ex)
        {
            return BadRequest("Failed to update user" + ex.InnerException.Message);
        }

    }


    [Authorize(Roles = "Administrator")]
    [HttpDelete("{userId}")]
    public async Task<ActionResult> DeleteUsers(string userId)
    {
        if (string.IsNullOrWhiteSpace(userId))
        {
            return BadRequest("No userId sent");
        }

        try
        {
            await UserService.DeleteUserById(_graphServiceClient, userId);
            return Ok("User deleted");
        }
        catch (Exception ex)
        {
            return BadRequest("Failed to delete userId: " + ex.Message);
        }
    }
    #endregion

}
