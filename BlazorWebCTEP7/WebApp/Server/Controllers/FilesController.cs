﻿using Microsoft.AspNetCore.Http.Features;
using ImageResizerService.Services;
using System.Net.Http.Headers;
using static CteCore.Helpers.FileUploadHelper;

namespace WebApp.Server.Controllers;

[ApiExplorerSettings(IgnoreApi = true)]
[Route("api/[controller]")]
[ApiController]
public class FilesController : ControllerBase
{
    private readonly ApplicationDbContext _context;
    private readonly IStorageService _storageService;
    private readonly IResizeImageService _resizeImageService;
    public FilesController(ApplicationDbContext context, IStorageService storageService, IResizeImageService resizeImageService)
    {
        _context = context;
        _storageService = storageService;
        _resizeImageService = resizeImageService;
    }

    [Authorize(Roles = "Administrator")]
    [HttpPost("Save/{path}")]
    public async Task<ActionResult> Save(IList<IFormFile> UploadFiles, string path)
    {
        try
        {
            await SaveMethod(UploadFiles, path);
            return new OkObjectResult("Ok");
        }
        catch (Exception)
        {
            return new BadRequestObjectResult("Error saving file");
        }
    }

    [Authorize(Roles = "Administrator")]
    [HttpPost("SaveAttachment/{path}")]
    public async Task<ActionResult> SaveAttachments(IList<IFormFile> UploadAttachments, string path)
    {
        try
        {
            await SaveMethod(UploadAttachments, path);
            return new OkObjectResult("Ok");
        }
        catch (Exception)
        {
            return new BadRequestObjectResult("Error saving file");
        }
    }

    public async Task SaveMethod(IList<IFormFile> UploadFiles, string path, SaveAction? saveAction = null, string azureDestinationPath = null)
    {
        if (saveAction == null)
        {
            var array = path.Split("~");
            saveAction = (SaveAction)Enum.Parse(typeof(SaveAction), array[0]);

            if (azureDestinationPath == null)
            {
                string rawPath = array[1];
                azureDestinationPath = rawPath.Replace(',', '/');
            }
        }


        foreach (var file in UploadFiles)
        {
            if (UploadFiles != null)
            {
                string filename = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                string ext = Path.GetExtension(filename).ToLower();

                filename = Guid.NewGuid().ToString();
                if (saveAction == SaveAction.Image)
                {
                    //handle image resizing for different qualities, thumbnail md lg
                    using (var stream = file.OpenReadStream())
                    {
                        (MemoryStream Thumbnail, MemoryStream Medium, MemoryStream Large) = _resizeImageService.ResizeImageToMemoryStream(stream);
                        var success = await _storageService.UploadFileAsync(Thumbnail, $"{filename}-tmb.jpg", azureDestinationPath);
                        success = await _storageService.UploadFileAsync(Medium, $"{filename}-md.jpg", azureDestinationPath);
                        success = await _storageService.UploadFileAsync(Large, $"{filename}-lg.jpg", azureDestinationPath);
                        //save original too
                        success = await _storageService.UploadFileAsync(stream, filename + ext, azureDestinationPath);
                        Response.Headers.Add("name", FileUploadHelper.AppendCustomDomainUrl(azureDestinationPath + filename + ext));
                        Response.StatusCode = 200;
                        return;
                    }
                }
                else
                {
                    filename = filename + ext;
                }

                using (var stream = file.OpenReadStream())
                {
                    var success = await _storageService.UploadFileAsync(stream, filename, azureDestinationPath);
                }
                Response.Headers.Add("name", FileUploadHelper.AppendCustomDomainUrl(azureDestinationPath + filename));
                Response.StatusCode = 200;
            }
            else
            {
                Response.StatusCode = 204;
            }
        }
    }

    [HttpPost("PublicSave/{path}")]
    public async Task<ActionResult> PublicSave(IList<IFormFile> UploadFiles, string path)
    {
        //for now we only have one form that takes files from public endpoint so hard code it to set to RmaForm
        //later add double checks from client requested save path and add a catch all save path for malicious save path requests.
        //this is because we cannot trust the save action from an anonymous file upload.
        try
        {
            string azureDestinationPath = FileUploadHelper.GetTempDirectory(typeof(RmaForm));
            await SaveMethod(UploadFiles, path, SaveAction.Form, azureDestinationPath);
            return new OkObjectResult("Ok");
        }
        catch (Exception)
        {
            return new BadRequestObjectResult("Error saving file");
        }
    }



    [Authorize(Roles = "Administrator")]
    [HttpPost("[action]")]
    public void Remove(IList<IFormFile> UploadFiles)
    {
        var filename = $@"{UploadFiles[0].FileName}";
        //var filename = UploadFiles;

        if (filename == null)
        {
            return;
        }
        try
        {
            string tempDirectory = null;
            if (Request.Headers.TryGetValue("TempDirectory", out var value))
            {
                tempDirectory = value;
            }

            if (tempDirectory == null)
            {
                return;
            }

            var azureFilePath = tempDirectory + filename;

            var success = _storageService.DeleteFileAsync(azureFilePath).Result;
            if (!success)
            {
                throw new Exception("Failed to delete");
            }
            //return new OkObjectResult("ok");
        }
        catch (Exception ex)
        {

            Response.Clear();
            Response.StatusCode = 200;
            Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = "File removed successfully";
            Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = ex.Message;
            //return new BadRequestObjectResult($"Error deleting file: {filename} \n" + ex.Message);
        }


    }

    [Authorize(Roles = "Administrator")]
    [HttpPost("DeleteByUrl")]
    public async Task<ActionResult<UrlLink>> Delete(UrlLink urlLink)
    {
        try
        {
            var azureFilePath = GetRelativeUri(urlLink.Url);
            var success = await _storageService.DeleteFileAsync(azureFilePath);
            if (!success)
            {
                throw new Exception("Failed to delete");
            }
            return new OkObjectResult("ok");
        }
        catch (Exception ex)
        {
            return new BadRequestObjectResult($"Error deleting file: {urlLink.Url} \n" + ex.Message);
        }


    }



    [Authorize(Roles = "Administrator")]
    [HttpPost("ImportProductCategories")]
    public ActionResult ImportProductCategories(IList<IFormFile> UploadFiles)
    {
        try
        {
            foreach (var file in UploadFiles)
            {
                if (UploadFiles != null)
                {
                    //List<ProductCategory> products = new List<ProductCategory>();
                    //using (var stream = file.OpenReadStream())
                    //{
                    //    products = ExcelService.ImportProductCategoriesToSQL(stream);
                    //}
                    //if (products != null)
                    //{

                    //    products.Where(x => string.IsNullOrWhiteSpace(x.MainCategory)).ToList().ForEach(s => s.MainCategory = null);
                    //    products.Where(x => string.IsNullOrWhiteSpace(x.SubCategory)).ToList().ForEach(s => s.SubCategory = null);
                    //    products.Where(x => string.IsNullOrWhiteSpace(x.SubSubCategory)).ToList().ForEach(s => s.SubSubCategory = null);

                    //    foreach (var p in products)
                    //    {
                    //        var dbProduct = _context.ProductCategories.Find(p.Id);

                    //        // Copy product's property values to dbProduct.
                    //        _context.Entry(dbProduct).CurrentValues.SetValues(p);
                    //        //dont allow user to change ProductFamilyId by ignoring values from list
                    //        _context.Entry(dbProduct).Property(x => x.Id).IsModified = false;
                    //        _context.Entry(dbProduct).Property(x => x.ProductFamilyId).IsModified = false;
                    //    }
                    //    await _context.SaveChangesAsync();
                    //}
                    Response.StatusCode = 200;
                }
                else
                {
                    Response.StatusCode = 204;
                }
            }

        }
        catch (Exception)
        {
            return new BadRequestObjectResult("Error parsing file");

        }
        return new OkObjectResult("ok");

    }

    [Authorize(Roles = "Administrator")]
    [HttpPost("ImportProducts")]
    public ActionResult ImportProducts(IList<IFormFile> UploadFiles)
    {
        try
        {
            foreach (var file in UploadFiles)
            {
                //if (UploadFiles != null)
                //{
                //    List<Product> products = new List<Product>();
                //    using (var stream = file.OpenReadStream())
                //    {
                //        products = ExcelService.ImportProductsToSQL(stream);
                //    }
                //    if (products != null)
                //    {
                //        foreach (var p in products)
                //        {
                //            var dbProduct = _context.Products.Find(p.Id);

                //            // Copy product's property values to dbProduct.
                //            _context.Entry(dbProduct).CurrentValues.SetValues(p);
                //            //dont allow user to change ProductFamilyId by ignoring values from list
                //            _context.Entry(dbProduct).Property(x => x.ProductFamilyId).IsModified = false;
                //        }
                //        await _context.SaveChangesAsync();
                //    }
                //    Response.StatusCode = 200;
                //}
                //else
                //{
                //    Response.StatusCode = 204;
                //}
            }

        }
        catch (Exception)
        {
            return new BadRequestObjectResult("Error parsing file");

        }
        return new OkObjectResult("ok");

    }


    [Authorize(Roles = "Administrator")]
    [HttpGet("ProductCategories")]
    public IActionResult GetProductCategories()
    {
        throw new NotImplementedException();
        //var data = await _context.ProductCategories.Include("ProductFamily")
        //    .Select(x => new
        //    {
        //        Id = x.Id,
        //        ProductFamilyId = x.ProductFamilyId,
        //        ProductFamily = x.ProductFamily.Name,
        //        MainCategory = x.MainCategory,
        //        SubCategory = x.SubCategory,
        //        SubSubCategory = x.SubSubCategory,
        //        Brand = x.Brand
        //    })
        //    .ToListAsync();

        //// do logic to create csv in memoryStream
        //MemoryStream memoryStream;
        //using (var package = new ExcelPackage())
        //{
        //    ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("ProductCategories");
        //    worksheet.Cells["A1"].LoadFromCollection(data, true);

        //    worksheet.Protection.IsProtected = true;
        //    worksheet.Column(4).Style.Locked = false;
        //    worksheet.Column(5).Style.Locked = false;
        //    worksheet.Column(6).Style.Locked = false;
        //    worksheet.Column(7).Style.Locked = false;


        //    //create a range for the table
        //    ExcelRange range = worksheet.Cells[1, 1, worksheet.Dimension.End.Row, worksheet.Dimension.End.Column];

        //    //add a table to the range
        //    ExcelTable tab = worksheet.Tables.Add(range, "Table1");

        //    //format the table
        //    tab.TableStyle = TableStyles.Medium2;

        //    //Make all text fit the cells
        //    worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();

        //    memoryStream = new MemoryStream(package.GetAsByteArray());
        //}
        //return File(memoryStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "ProductCategories.xlsx");

    }

}
