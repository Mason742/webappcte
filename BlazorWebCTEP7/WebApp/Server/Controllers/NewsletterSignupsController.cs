﻿namespace WebApp.Server.Controllers;


[Route("api/[controller]")]
[ApiController]
public class NewsletterSignupsController : GenericBaseApiController<NewsletterSignup>
{
    public NewsletterSignupsController(ApplicationDbContext context, ILogger<GenericBaseApiController<NewsletterSignup>> logger) : base(context, logger)
    {
    }
}
