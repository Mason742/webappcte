﻿namespace WebApp.Server.Controllers.Web;

[Route("api/[controller]")]
[ApiController]

public class ProductController : ControllerBase
{
    private readonly ApplicationDbContext _context;
    private readonly ILogger<ProductController> _logger;
    private readonly IMemoryCache _memoryCache;
    public ProductController(ApplicationDbContext context, ILogger<ProductController> logger, IMemoryCache memoryCache)
    {
        _context = context;
        _logger = logger;
        _memoryCache = memoryCache;
    }


    [HttpGet("PageData/{partNumber}")]
    public async Task<ActionResult<ProductPageDTO>> GetProductPageData(string partNumber)
    {
        Series series = new Series();
        if (_memoryCache.TryGetValue($"product-series-cache", out List<Series> seriesList))
        {
            _logger.LogInformation("using product-series-cache");
        }
        else
        {
            _logger.LogInformation("setting product-series-cache");

            seriesList = new List<Series>();
            seriesList = await _context.Series.AsNoTracking()
            .Include(x=>x.Products)
            .Include(x => x.SeriesAttachments)
            .Include(x => x.ReplacementSeries)
            .Include(x => x.Category).ThenInclude(x => x.CategoryGroups).ThenInclude(x => x.Group)
            .Include(x => x.SeriesRelatedSeries)
            .ToListAsync();

            _memoryCache.Set("product-series-cache", seriesList, new MemoryCacheEntryOptions()
        .SetAbsoluteExpiration(TimeSpan.FromHours(24)));
        }
        series = seriesList.FirstOrDefault(x => x.Name == partNumber || x.Products.Any(y => y.PartNumber == partNumber));

        if (series is null)
        {
            return NotFound();
        }

        ProductPageDTO dto = new();
        var product = series.Products.FirstOrDefault(p => p.PartNumber == partNumber);
        if (product is null)
        {
            product = series.Products.FirstOrDefault();
        }
        if (product is null)
        {
            return NotFound();
        }

        dto.SelectedPartNumber = product.PartNumber;
        dto.SeriesName = series.Name;
        dto.Title = series.Title;
        dto.SpecificationsLegacy = series.SpecificationsLegacy;
        dto.Overview = series.Description;
        dto.New = product.Created > DateTime.Now.AddDays(-365);
        dto.Discontinued = series.Discontinued || product.Discontinued;
        dto.GroupName = series.Category.CategoryGroups.FirstOrDefault()?.Group.Name;
        dto.CategoryName = series.Category.DisplayName;
        dto.CategoryTableName = series.Category.TableName;
        dto.Products = series.Products.Where(x => x.Discontinued == false).Select(p => new ProductPageDTO.ProductDTO
        {
            Catalog = true, //!!! need db backing field
            Description = p.Description,
            PartNumber = p.PartNumber,
            Msrp = p.Msrp,
            Warranty = p.Warranty,
        }).ToList();
        dto.Documents = series.SeriesAttachments //!!! software could be its own table or add description nullable field for all attachments
            .Where(sa => sa.Type != "Video" && sa.Type != "Software")
            .Select(d => new ProductPageDTO.Attachment { Type = d.Type, AttachmentFileLink = d.AttachmentFileLink })
            .ToList();
        dto.Softwares = series.SeriesAttachments
            .Where(sa => sa.Type == "Software")
            .Select(s => new ProductPageDTO.Attachment { Type = s.Type, AttachmentFileLink = s.AttachmentFileLink })
            .ToList();
        dto.Videos = series.SeriesAttachments
            .Where(sa => sa.Type == "Video")
            .Select(v => new ProductPageDTO.Attachment { Type = v.Type, AttachmentFileLink = v.AttachmentFileLink })
            .ToList();

        if (series.ReplacementSeries != null)
        {
            dto.ReplacementSeries = new()
            {
                Name = series.ReplacementSeries?.Name,
                Title = series.ReplacementSeries?.Title,
                MainImage = series
                        .ReplacementSeries
                        .Image1FileLinkTmb ?? series.ReplacementSeries.Products.FirstOrDefault()?.Image1FileLinkTmb
            };
        }


        if (_memoryCache.TryGetValue($"product-controller-images-cache", out List<VImageFileLinkTmb> images))
        {
            _logger.LogInformation("using product-controller-images-cache");
        }
        else
        {
            images = new();
            images = await _context.VImageFileLinkTmbs.ToListAsync();
            _memoryCache.Set("product-controller-images-cache", images, new MemoryCacheEntryOptions()
        .SetAbsoluteExpiration(TimeSpan.FromHours(24)));
            _logger.LogInformation("setting product-controller-images-cache");
        }

        dto.Images = new();
        //get all product images by partnumber currently viewing


        //get all product images
        var originals = new[] {
    product.Image1FileLink, product.Image2FileLink, product.Image3FileLink, product.Image4FileLink,
    product.Image5FileLink, product.Image6FileLink, product.Image7FileLink, product.Image8FileLink,
    product.Image9FileLink,
    series.Image1FileLink, series.Image2FileLink, series.Image3FileLink, series.Image4FileLink,
    series.Image5FileLink, series.Image6FileLink, series.Image7FileLink, series.Image8FileLink,
    series.Image9FileLink, 
};
        var larges = new[] {
    product.Image1FileLinkLg, product.Image2FileLinkLg, product.Image3FileLinkLg, product.Image4FileLinkLg,
    product.Image5FileLinkLg, product.Image6FileLinkLg, product.Image7FileLinkLg, product.Image8FileLinkLg,
    product.Image9FileLinkLg,
    series.Image1FileLinkLg, series.Image2FileLinkLg, series.Image3FileLinkLg, series.Image4FileLinkLg,
    series.Image5FileLinkLg, series.Image6FileLinkLg, series.Image7FileLinkLg, series.Image8FileLinkLg,
    series.Image9FileLinkLg, 
};
        var mediums = new[] {
    product.Image1FileLinkMd, product.Image2FileLinkMd, product.Image3FileLinkMd, product.Image4FileLinkMd,
    product.Image5FileLinkMd, product.Image6FileLinkMd, product.Image7FileLinkMd, product.Image8FileLinkMd,
    product.Image9FileLinkMd,
    series.Image1FileLinkMd, series.Image2FileLinkMd, series.Image3FileLinkMd, series.Image4FileLinkMd,
    series.Image5FileLinkMd, series.Image6FileLinkMd, series.Image7FileLinkMd, series.Image8FileLinkMd,
    series.Image9FileLinkMd, 
};
        var thumbnails = new[] {
    product.Image1FileLinkTmb, product.Image2FileLinkTmb, product.Image3FileLinkTmb,  product.Image4FileLinkTmb,
    product.Image5FileLinkTmb, product.Image6FileLinkTmb, product.Image7FileLinkTmb,  product.Image8FileLinkTmb,
    product.Image9FileLinkTmb,
    series.Image1FileLinkTmb, series.Image2FileLinkTmb, series.Image3FileLinkTmb,  series.Image4FileLinkTmb,
    series.Image5FileLinkTmb, series.Image6FileLinkTmb, series.Image7FileLinkTmb,  series.Image8FileLinkTmb,
    series.Image9FileLinkTmb, 
};

        for (int i = 0; i < 18; i++)
        {
            if (!string.IsNullOrEmpty(larges[i]))
            {
                dto.Images.Add(new ImageGalleryItem
                {
                    Original = originals[i],
                    Large = larges[i],
                    Medium = mediums[i],
                    Thumbnail = thumbnails[i]
                });
            }
        }


        dto.Warranty = product.Warranty;
        dto.Msrp = product.Msrp;

        dto.RelatedProducts = series.SeriesRelatedSeries.Select(sr => new ProductPageDTO.RelatedProductDTO
        {
            ProductName = sr.Series.Name,
            MainImage = sr.Series.Products.FirstOrDefault()?.Image1FileLinkTmb,
            Title = sr.Series.Title
        }).ToList();

        return Ok(dto);
    }
}