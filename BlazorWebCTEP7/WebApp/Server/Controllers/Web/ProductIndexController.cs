﻿namespace WebApp.Server.Controllers.Web;

[Route("api/[controller]")]
[ApiController]
[ResponseCache(CacheProfileName = "Default6Hours")]
public class ProductIndexController : ControllerBase
{
    private readonly ApplicationDbContext _context;
    private readonly ILogger<ProductIndexController> _logger;
    private readonly IMemoryCache _memoryCache;
    public ProductIndexController(ApplicationDbContext context, ILogger<ProductIndexController> logger, IMemoryCache memoryCache)
    {
        _context = context;
        _logger = logger;
        _memoryCache = memoryCache;
    }

    [HttpGet("PageData/{brand}")]
    public async Task<ActionResult<ProductIndexPageDTO>> PageData(string brand, bool NewProduct, bool RoHsCompliant, string SearchParameter)
    {
        ProductIndexPageDTO productIndexDTO = new()
        {
            Caterogies = new List<ProductIndexPageDTO.Category>()
        };
        if (_memoryCache.TryGetValue($"product-index-cache-{brand}-{NewProduct}-{RoHsCompliant}", out List<ProductIndexPageDTO.Category> query))
        {
            _logger.LogInformation("using product index cache");
        }
        else
        {
            _logger.LogInformation("setting product index cache");


            query = await (from series in _context.Series
                           where series.Brand == brand
                           join category in _context.Categories on series.CategoryId equals category.Id
                           join product in _context.Products on series.Id equals product.SeriesId
                           join categoryGroup in _context.CategoryGroups on category.Id equals categoryGroup.CategoryId
                           join groups in _context.Groups on categoryGroup.GroupId equals groups.Id
                           where product.RoHsCompliant == RoHsCompliant
                           && product.Created >= (NewProduct ? DateTime.Now.AddDays(-365) : DateTime.Now.AddDays(-50000))
                           && product.Discontinued == false
                           group new { series, category, product, categoryGroup, groups } by new
                           {
                               product.PartNumber,
                               groups.Name,
                               category.DisplayName,
                               category.TableName,
                               category.ImageFileLink,
                               series.Brand
                           } into view
                           orderby view.Key.Name, view.Key.DisplayName ascending
                           select new ProductIndexPageDTO.Category
                           {
                               PartNumber = view.Key.PartNumber,
                               GroupName = view.Key.Name,
                               DisplayName = view.Key.DisplayName,
                               TableName = view.Key.TableName,
                               ImageFileLink = view.Key.ImageFileLink,
                               ProductCount = view.Count(),
                           }).ToListAsync();
            _memoryCache.Set($"product-index-cache-{brand}-{NewProduct}-{RoHsCompliant}", query, new MemoryCacheEntryOptions()
    .SetAbsoluteExpiration(TimeSpan.FromHours(24)));
        }


        if (SearchParameter != null)
        {
            char[] delimiters = { ',', ' ' };
            string[] searchParameters = SearchParameter.Split(delimiters);
            query = query.Where(y => searchParameters.All(x => (y.PartNumber?.Contains(x, StringComparison.OrdinalIgnoreCase) ?? false) || (y.GroupName?.Contains(x, StringComparison.OrdinalIgnoreCase) ?? false) || (y.DisplayName?.Contains(x, StringComparison.OrdinalIgnoreCase) ?? false) || (y.TypeName?.Contains(x, StringComparison.OrdinalIgnoreCase) ?? false))).ToList();

            //query.ForEach(x => x.PartNumber = null);
            query = query.GroupBy(x => new { x.GroupName, x.DisplayName, x.TableName, x.TypeName, x.ImageFileLink }).Select(y => new ProductIndexPageDTO.Category
            {
                GroupName = y.Key.GroupName,
                DisplayName = y.Key.DisplayName,
                TypeName = y.Key.TypeName,
                TableName = y.Key.TableName,
                ImageFileLink = y.Key.ImageFileLink,
                ProductCount = y.Count(),
                ProductsFound = y.Select(t => t.PartNumber).ToArray()
            }).ToList();
            query = query.Distinct().ToList();
            productIndexDTO.Caterogies.AddRange(query);
            productIndexDTO.TotalCount = productIndexDTO.Caterogies.Sum(x => x.ProductCount);

            return Ok(productIndexDTO);
        }

        query = query.GroupBy(x => new { x.GroupName, x.DisplayName, x.TableName, x.TypeName,x.ImageFileLink }).Select(y => new ProductIndexPageDTO.Category
        {
            GroupName = y.Key.GroupName,
            DisplayName = y.Key.DisplayName,
            TypeName = y.Key.TypeName,
            TableName = y.Key.TableName,
            ImageFileLink=y.Key.ImageFileLink,
            ProductCount = y.Count(),
        }).ToList();
        query = query.Distinct().ToList();
        productIndexDTO.Caterogies.AddRange(query);
        productIndexDTO.TotalCount = productIndexDTO.Caterogies.Sum(x => x.ProductCount);
        return Ok(productIndexDTO);
    }
}
