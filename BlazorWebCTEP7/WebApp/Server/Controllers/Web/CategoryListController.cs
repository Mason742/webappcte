﻿namespace WebApp.Server.Controllers.Web;

[Route("api/[controller]")]
[ApiController]
[ResponseCache(CacheProfileName = "Default6Hours")]
public class CategoryListController : ControllerBase
{
    private readonly ApplicationDbContext _context;
    private readonly ILogger<CategoryListController> _logger;
    private readonly IMemoryCache _memoryCache;
    public CategoryListController(ApplicationDbContext context, ILogger<CategoryListController> logger, IEmailSenderService emailSender, CrmService hubSpotService, IMemoryCache memoryCache)
    {
        _context = context;
        _logger = logger;
        _memoryCache = memoryCache;
    }

    [HttpGet("PageData/{tableName}")]
    public async Task<ActionResult> GetProductListPageData(string tableName)
    {

        if (_memoryCache.TryGetValue($"{nameof(Category)}-category-list-cache", out List<Category> categories))
        {
            _logger.LogInformation($"using {nameof(Category)}-category-list-cache");
        }
        else
        {
            categories = new List<Category>();
            categories = await _context.Categories.ToListAsync();
            _memoryCache.Set($"{nameof(Category)}-category-list-cache", categories, new MemoryCacheEntryOptions()
        .SetAbsoluteExpiration(TimeSpan.FromHours(24)));
        }

        var category = categories.FirstOrDefault(x => x.TableName == tableName);
        var content = new JumbotronContent
        {
            Title = category?.DisplayName,
            Subtitle = category?.Description,
            ImageLink = category?.ImageFileLink
        };

        if (tableName == "A_Alligator_CT")
        {
            CategoryPageDTO<AvAlligatorCtDTO> dto = new();

            if (_memoryCache.TryGetValue($"{nameof(AvAlligatorCtDTO)}-category-list-cache", out List<AvAlligatorCt> data))
            {
                _logger.LogInformation($"using {nameof(AvAlligatorCtDTO)}-category-list-cache");
            }
            else
            {
                data = new List<AvAlligatorCt>();
                data = await _context.AvAlligatorCts.ToListAsync();
                _memoryCache.Set($"{nameof(AvAlligatorCtDTO)}-category-list-cache", data, new MemoryCacheEntryOptions()
        .SetAbsoluteExpiration(TimeSpan.FromHours(24)));
            }


            dto.Data = data
            .Select(
            x => new AvAlligatorCtDTO
            {
                ProductId = x.ProductId.ToString(),

                Iec = x.Iec,
                MaxCurrent = x.MaxCurrent,
                RoHs = x.RoHs,
                Temperature = x.Temperature,
                Color = x.Color,
                Voltage = x.Voltage,
                Materials = x.Materials,
                Type = x.Type,
                MaxResistance = x.MaxResistance,
                JawOpening = x.JawOpening,
                MaxConductorDiameter = x.MaxConductorDiameter,
                Type1 = x.Type1,


                SpecialHandling = x.SpecialHandling,
                NewProduct = x.NewProduct,
                ImageFileLink = x.ImageFileLink,
                PartNumber = x.PartNumber,
                Description = x.Description,
                Warranty = x.Warranty,
                Msrp = x.Msrp.ToString(),
                UpcCode = x.UpcCode,
                HarmonizedCode = x.HarmonizedCode,
                CountryOfOrigin = x.CountryOfOrigin,
                Discontinued = x.Discontinued.ToString(),
                RoHsCompliant = x.RoHsCompliant.ToString(),

            }).ToList();
            dto.CategoryJumbotron = content;
            return Ok(dto);
        }
        else if (tableName == "A_Banana_CT")
        {
            CategoryPageDTO<AvBananaCtDTO> dto = new();
            if (_memoryCache.TryGetValue($"{nameof(AvBananaCt)}-category-list-cache", out List<AvBananaCt> data))
            {
                _logger.LogInformation($"using {nameof(AvBananaCt)}-category-list-cache");
            }
            else
            {
                data = new List<AvBananaCt>();
                data = await _context.AvBananaCts.ToListAsync();
                _memoryCache.Set($"{nameof(AvBananaCt)}-category-list-cache", data, new MemoryCacheEntryOptions()
        .SetAbsoluteExpiration(TimeSpan.FromHours(24)));
            }

            dto.Data = data.Select(x => new AvBananaCtDTO
            {
                ProductId = x.ProductId.ToString(),
                Color = x.Color,
                Iec = x.Iec,
                MaxCurrent = x.MaxCurrent,
                MaxResistance = x.MaxResistance,
                RoHs = x.RoHs,
                Temperature = x.Temperature,
                Ulrecognized = x.Ulrecognized,
                MaxVoltage = x.MaxVoltage,
                Modifier = x.Modifier,
                Impedance = x.Impedance,
                MaxFloatingVoltage = x.MaxFloatingVoltage,
                ContactMaterial = x.ContactMaterial,
                InsulationMaterial = x.InsulationMaterial,
                Magnet = x.Magnet,
                BodyMaterial = x.BodyMaterial,
                Ce = x.Ce,
                Voltage = x.Voltage,
                Type1 = x.Type1,
                Type = x.Type,



                SpecialHandling = x.SpecialHandling,
                NewProduct = x.NewProduct,
                ImageFileLink = x.ImageFileLink,
                PartNumber = x.PartNumber,
                Description = x.Description,
                Warranty = x.Warranty,
                Msrp = x.Msrp.ToString(),
                UpcCode = x.UpcCode,
                HarmonizedCode = x.HarmonizedCode,
                CountryOfOrigin = x.CountryOfOrigin,
                Discontinued = x.Discontinued.ToString(),
                RoHsCompliant = x.RoHsCompliant.ToString(),
            }).ToList();
            dto.CategoryJumbotron = content;
            return Ok(dto);

        }
        else if (tableName == "A_BindingPost_CT")
        {
            CategoryPageDTO<AvBindingPostCtDTO> dto = new();
            if (_memoryCache.TryGetValue($"{nameof(AvBindingPostCt)}-category-list-cache", out List<AvBindingPostCt> data))
            {
                _logger.LogInformation($"using {nameof(AvBindingPostCt)}-category-list-cache");
            }
            else
            {
                data = new List<AvBindingPostCt>();
                data = await _context.AvBindingPostCts.ToListAsync();
                _memoryCache.Set($"{nameof(AvBindingPostCt)}-category-list-cache", data, new MemoryCacheEntryOptions()
        .SetAbsoluteExpiration(TimeSpan.FromHours(24)));
            }

            dto.Data = data.Select(
                x => new AvBindingPostCtDTO
                {
                    ProductId = x.ProductId.ToString(),

                    Color = x.Color,
                    Iec = x.Iec,
                    MaxCurrent = x.MaxCurrent,
                    MaxResistance = x.MaxResistance,
                    MaxVoltage = x.MaxVoltage,
                    RoHs = x.RoHs,
                    Temperature = x.Temperature,
                    BodyMaterial = x.BodyMaterial,
                    PollutionDegree = x.PollutionDegree,
                    MasterPartNumber = x.MasterPartNumber,
                    Type = x.Type,


                    NewProduct = x.NewProduct,
                    ImageFileLink = x.ImageFileLink,
                    PartNumber = x.PartNumber,
                    Description = x.Description,
                    Warranty = x.Warranty,
                    Msrp = x.Msrp.ToString(),
                    UpcCode = x.UpcCode,
                    HarmonizedCode = x.HarmonizedCode,
                    CountryOfOrigin = x.CountryOfOrigin,
                    Discontinued = x.Discontinued.ToString(),
                    RoHsCompliant = x.RoHsCompliant.ToString(),
                }).ToList();
            dto.CategoryJumbotron = content;
            return Ok(dto);
        }
        else if (tableName == "A_CoaxialAssemblies_CT")
        {
            CategoryPageDTO<AvCoaxialAssembliesCtDTO> dto = new();
            if (_memoryCache.TryGetValue($"{nameof(AvCoaxialAssembliesCt)}-category-list-cache", out List<AvCoaxialAssembliesCt> data))
            {
                _logger.LogInformation($"using {nameof(AvCoaxialAssembliesCt)}-category-list-cache");
            }
            else
            {
                data = new List<AvCoaxialAssembliesCt>();
                data = await _context.AvCoaxialAssembliesCts.ToListAsync();
                _memoryCache.Set($"{nameof(AvCoaxialAssembliesCt)}-category-list-cache", data, new MemoryCacheEntryOptions()
        .SetAbsoluteExpiration(TimeSpan.FromHours(24)));
            }

            dto.Data = data
                .Select(
                x => new AvCoaxialAssembliesCtDTO
                {
                    ProductId = x.ProductId.ToString(),

                    Bandwidth = x.Bandwidth,
                    CableType = x.CableType,
                    ConnectorGrade = x.ConnectorGrade,
                    Impedance = x.Impedance,
                    InsertionLoss = x.InsertionLoss,
                    Length = x.Length,
                    RoHs = x.RoHs,
                    Vswr = x.Vswr,
                    MaxCurrent = x.MaxCurrent,
                    MaxVoltage = x.MaxVoltage,
                    Temperature = x.Temperature,
                    BodyMaterial = x.BodyMaterial,
                    Color = x.Color,
                    FrequencyRange = x.FrequencyRange,
                    Iec = x.Iec,
                    JacketMaterial = x.JacketMaterial,
                    ContactMaterial = x.ContactMaterial,
                    InsulationMaterial = x.InsulationMaterial,
                    Type1 = x.Type1,
                    Type = x.Type,

                    SpecialHandling = x.SpecialHandling,
                    NewProduct = x.NewProduct,
                    ImageFileLink = x.ImageFileLink,
                    PartNumber = x.PartNumber,
                    Description = x.Description,
                    Warranty = x.Warranty,
                    Msrp = x.Msrp.ToString(),
                    UpcCode = x.UpcCode,
                    HarmonizedCode = x.HarmonizedCode,
                    CountryOfOrigin = x.CountryOfOrigin,
                    Discontinued = x.Discontinued.ToString(),
                    RoHsCompliant = x.RoHsCompliant.ToString(),

                }).ToList();
            dto.CategoryJumbotron = content;
            return Ok(dto);
        }
        else if (tableName == "A_CoaxialsAdapters_CT")
        {
            CategoryPageDTO<AvCoaxialsAdaptersCtDTO> dto = new();
            if (_memoryCache.TryGetValue($"{nameof(AvCoaxialsAdaptersCt)}-category-list-cache", out List<AvCoaxialsAdaptersCt> data))
            {
                _logger.LogInformation($"using {nameof(AvCoaxialsAdaptersCt)}-category-list-cache");
            }
            else
            {
                data = new List<AvCoaxialsAdaptersCt>();
                data = await _context.AvCoaxialsAdaptersCts.ToListAsync();
                _memoryCache.Set($"{nameof(AvCoaxialsAdaptersCt)}-category-list-cache", data, new MemoryCacheEntryOptions()
        .SetAbsoluteExpiration(TimeSpan.FromHours(24)));
            }

            dto.Data = data
                .Select(
                x => new AvCoaxialsAdaptersCtDTO
                {
                    ProductId = x.ProductId.ToString(),

                    ConnectorGrade = x.ConnectorGrade,
                    FrequencyRange = x.FrequencyRange,
                    Impedance = x.Impedance,
                    InsertionLoss = x.InsertionLoss,
                    RoHs = x.RoHs,
                    Temperature = x.Temperature,
                    Vswr = x.Vswr,
                    MaxCurrent = x.MaxCurrent,
                    MaxVoltage = x.MaxVoltage,
                    Iec = x.Iec,
                    MaxResistance = x.MaxResistance,
                    Type = x.Type,
                    CableLength = x.CableLength,
                    ConductorArea = x.ConductorArea,
                    JacketMaterial = x.JacketMaterial,
                    Stranding = x.Stranding,
                    WireGauge = x.WireGauge,
                    Color = x.Color,
                    CenterContactResistance = x.CenterContactResistance,
                    WorkingVoltage = x.WorkingVoltage,
                    Frequency = x.Frequency,
                    Type1 = x.Type1,

                    SpecialHandling = x.SpecialHandling,
                    NewProduct = x.NewProduct,
                    ImageFileLink = x.ImageFileLink,
                    PartNumber = x.PartNumber,
                    Description = x.Description,
                    Warranty = x.Warranty,
                    Msrp = x.Msrp.ToString(),
                    UpcCode = x.UpcCode,
                    HarmonizedCode = x.HarmonizedCode,
                    CountryOfOrigin = x.CountryOfOrigin,
                    Discontinued = x.Discontinued.ToString(),
                    RoHsCompliant = x.RoHsCompliant.ToString(),

                }).ToList();
            dto.CategoryJumbotron = content;
            return Ok(dto);
        }
        else if (tableName == "A_CoaxialsAttenuators_CT")
        {
            CategoryPageDTO<AvCoaxialsAttenuatorsCtDTO> dto = new();
            if (_memoryCache.TryGetValue($"{nameof(AvCoaxialsAttenuatorsCt)}-category-list-cache", out List<AvCoaxialsAttenuatorsCt> data))
            {
                _logger.LogInformation($"using {nameof(AvCoaxialsAttenuatorsCt)}-category-list-cache");
            }
            else
            {
                data = new List<AvCoaxialsAttenuatorsCt>();
                data = await _context.AvCoaxialsAttenuatorsCts.ToListAsync();
                _memoryCache.Set($"{nameof(AvCoaxialsAttenuatorsCt)}-category-list-cache", data, new MemoryCacheEntryOptions()
        .SetAbsoluteExpiration(TimeSpan.FromHours(24)));
            }

            dto.Data = data.Select(
                x => new AvCoaxialsAttenuatorsCtDTO
                {
                    ProductId = x.ProductId.ToString(),

                    Accuracy = x.Accuracy,
                    Attenuation = x.Attenuation,
                    AttenuationVoltage = x.AttenuationVoltage,
                    AveragePower = x.AveragePower,
                    FrequencyRange = x.FrequencyRange,
                    Impedance = x.Impedance,
                    PeakPower = x.PeakPower,
                    RoHs = x.RoHs,
                    TempCoefficient = x.TempCoefficient,
                    Temperature = x.Temperature,
                    Vswr = x.Vswr,
                    Length = x.Length,
                    ConnectorGrade = x.ConnectorGrade,
                    AttenuationAccuracy = x.AttenuationAccuracy,
                    MaxVoltage = x.MaxVoltage,
                    PollutionDegree = x.PollutionDegree,
                    RelativeHumidity = x.RelativeHumidity,
                    Safety = x.Safety,
                    SafetySpecification = x.SafetySpecification,
                    Bandwidth = x.Bandwidth,
                    SpecialHandling = x.SpecialHandling,
                    Type = x.Type,


                    NewProduct = x.NewProduct,
                    ImageFileLink = x.ImageFileLink,
                    PartNumber = x.PartNumber,
                    Description = x.Description,
                    Warranty = x.Warranty,
                    Msrp = x.Msrp.ToString(),
                    UpcCode = x.UpcCode,
                    HarmonizedCode = x.HarmonizedCode,
                    CountryOfOrigin = x.CountryOfOrigin,
                    Discontinued = x.Discontinued.ToString(),
                    RoHsCompliant = x.RoHsCompliant.ToString(),
                }).ToList();
            dto.CategoryJumbotron = content;
            return Ok(dto);
        }
        else if (tableName == "A_CoaxialsTerminators_CT")
        {
            CategoryPageDTO<AvCoaxialsTerminatorsCtDTO> dto = new();
            if (_memoryCache.TryGetValue($"{nameof(AvCoaxialsTerminatorsCt)}-category-list-cache", out List<AvCoaxialsTerminatorsCt> data))
            {
                _logger.LogInformation($"using {nameof(AvCoaxialsTerminatorsCt)}-category-list-cache");
            }
            else
            {
                data = new List<AvCoaxialsTerminatorsCt>();
                data = await _context.AvCoaxialsTerminatorsCts.ToListAsync();
                _memoryCache.Set($"{nameof(AvCoaxialsTerminatorsCt)}-category-list-cache", data, new MemoryCacheEntryOptions()
        .SetAbsoluteExpiration(TimeSpan.FromHours(24)));
            }

            dto.Data = data.Select(
                x => new AvCoaxialsTerminatorsCtDTO
                {
                    ProductId = x.ProductId.ToString(),

                    Accuracy = x.Accuracy,
                    AveragePower = x.AveragePower,
                    ConnectorGrade = x.ConnectorGrade,
                    FrequencyRange = x.FrequencyRange,
                    Impedance = x.Impedance,
                    InputVoltage = x.InputVoltage,
                    RoHs = x.RoHs,
                    Temperature = x.Temperature,
                    Vswr = x.Vswr,
                    Ztolerance = x.Ztolerance,
                    Length = x.Length,
                    Type = x.Type,


                    SpecialHandling = x.SpecialHandling,
                    NewProduct = x.NewProduct,
                    ImageFileLink = x.ImageFileLink,
                    PartNumber = x.PartNumber,
                    Description = x.Description,
                    Warranty = x.Warranty,
                    Msrp = x.Msrp.ToString(),
                    UpcCode = x.UpcCode,
                    HarmonizedCode = x.HarmonizedCode,
                    CountryOfOrigin = x.CountryOfOrigin,
                    Discontinued = x.Discontinued.ToString(),
                    RoHsCompliant = x.RoHsCompliant.ToString(),
                }).ToList();
            dto.CategoryJumbotron = content;
            return Ok(dto);
        }
        else if (tableName == "A_DigitalMultimeter_CT")
        {
            CategoryPageDTO<AvDigitalMultimeterCtDTO> dto = new();
            if (_memoryCache.TryGetValue($"{nameof(AvDigitalMultimeterCt)}-category-list-cache", out List<AvDigitalMultimeterCt> data))
            {
                _logger.LogInformation($"using {nameof(AvDigitalMultimeterCt)}-category-list-cache");
            }
            else
            {
                data = new List<AvDigitalMultimeterCt>();
                data = await _context.AvDigitalMultimeterCts.ToListAsync();
                _memoryCache.Set($"{nameof(AvDigitalMultimeterCt)}-category-list-cache", data, new MemoryCacheEntryOptions()
        .SetAbsoluteExpiration(TimeSpan.FromHours(24)));
            }

            dto.Data = data.Select(
                x => new AvDigitalMultimeterCtDTO
                {
                    ProductId = x.ProductId.ToString(),

                    Iec = x.Iec,
                    MaxCurrent = x.MaxCurrent,
                    RoHs = x.RoHs,
                    Temperature = x.Temperature,
                    InterruptingRating = x.InterruptingRating,
                    MaxVoltage = x.MaxVoltage,
                    Color = x.Color,
                    MaxResistance = x.MaxResistance,
                    Impedance = x.Impedance,
                    MeterImpedance = x.MeterImpedance,
                    TempCoefficient = x.TempCoefficient,
                    Dimension = x.Dimension,
                    Voltage = x.Voltage,
                    Type = x.Type,


                    SpecialHandling = x.SpecialHandling,
                    NewProduct = x.NewProduct,
                    ImageFileLink = x.ImageFileLink,
                    PartNumber = x.PartNumber,
                    Description = x.Description,
                    Warranty = x.Warranty,
                    Msrp = x.Msrp.ToString(),
                    UpcCode = x.UpcCode,
                    HarmonizedCode = x.HarmonizedCode,
                    CountryOfOrigin = x.CountryOfOrigin,
                    Discontinued = x.Discontinued.ToString(),
                    RoHsCompliant = x.RoHsCompliant.ToString(),
                }).ToList();
            dto.CategoryJumbotron = content;
            return Ok(dto);
        }
        else if (tableName == "A_GrabbersandHooks_CT")
        {
            CategoryPageDTO<AvGrabbersandHooksCtDTO> dto = new();
            if (_memoryCache.TryGetValue($"{nameof(AvGrabbersandHooksCt)}-category-list-cache", out List<AvGrabbersandHooksCt> data))
            {
                _logger.LogInformation($"using {nameof(AvGrabbersandHooksCt)}-category-list-cache");
            }
            else
            {
                data = new List<AvGrabbersandHooksCt>();
                data = await _context.AvGrabbersandHooksCts.ToListAsync();
                _memoryCache.Set($"{nameof(AvGrabbersandHooksCt)}-category-list-cache", data, new MemoryCacheEntryOptions()
        .SetAbsoluteExpiration(TimeSpan.FromHours(24)));
            }

            dto.Data = data.Select(
                x => new AvGrabbersandHooksCtDTO
                {
                    ProductId = x.ProductId.ToString(),

                    MaxCurrent = x.MaxCurrent,
                    MaxResistance = x.MaxResistance,
                    RoHs = x.RoHs,
                    Temperature = x.Temperature,
                    Color = x.Color,
                    Od = x.Od,
                    MaxVoltage = x.MaxVoltage,
                    Dimension = x.Dimension,
                    Type = x.Type,

                    SpecialHandling = x.SpecialHandling,
                    NewProduct = x.NewProduct,
                    ImageFileLink = x.ImageFileLink,
                    PartNumber = x.PartNumber,
                    Description = x.Description,
                    Warranty = x.Warranty,
                    Msrp = x.Msrp.ToString(),
                    UpcCode = x.UpcCode,
                    HarmonizedCode = x.HarmonizedCode,
                    CountryOfOrigin = x.CountryOfOrigin,
                    Discontinued = x.Discontinued.ToString(),
                    RoHsCompliant = x.RoHsCompliant.ToString(),
                }).ToList();
            dto.CategoryJumbotron = content;
            return Ok(dto);
        }
        else if (tableName == "A_Kits_CT")
        {
            CategoryPageDTO<AvKitsCtDTO> dto = new();
            if (_memoryCache.TryGetValue($"{nameof(AvKitsCt)}-category-list-cache", out List<AvKitsCt> data))
            {
                _logger.LogInformation($"using {nameof(AvKitsCt)}-category-list-cache");
            }
            else
            {
                data = new List<AvKitsCt>();
                data = await _context.AvKitsCts.ToListAsync();
                _memoryCache.Set($"{nameof(AvKitsCt)}-category-list-cache", data, new MemoryCacheEntryOptions()
        .SetAbsoluteExpiration(TimeSpan.FromHours(24)));
            }

            dto.Data = data.Select(
                x => new AvKitsCtDTO
                {
                    ProductId = x.ProductId.ToString(),

                    Type = x.Type,
                    RoHs = x.RoHs,
                    Temperature = x.Temperature,
                    Type1 = x.Type1,



                    SpecialHandling = x.SpecialHandling,
                    NewProduct = x.NewProduct,
                    ImageFileLink = x.ImageFileLink,
                    PartNumber = x.PartNumber,
                    Description = x.Description,
                    Warranty = x.Warranty,
                    Msrp = x.Msrp.ToString(),
                    UpcCode = x.UpcCode,
                    HarmonizedCode = x.HarmonizedCode,
                    CountryOfOrigin = x.CountryOfOrigin,
                    Discontinued = x.Discontinued.ToString(),
                    RoHsCompliant = x.RoHsCompliant.ToString(),
                }).ToList();
            dto.CategoryJumbotron = content;
            return Ok(dto);
        }
        else if (tableName == "A_OscilloscopeCurrent_CT")
        {
            CategoryPageDTO<AvOscilloscopeCurrentCtDTO> dto = new();
            if (_memoryCache.TryGetValue($"{nameof(AvOscilloscopeCurrentCt)}-category-list-cache", out List<AvOscilloscopeCurrentCt> data))
            {
                _logger.LogInformation($"using {nameof(AvOscilloscopeCurrentCt)}-category-list-cache");
            }
            else
            {
                data = new List<AvOscilloscopeCurrentCt>();
                data = await _context.AvOscilloscopeCurrentCts.ToListAsync();
                _memoryCache.Set($"{nameof(AvOscilloscopeCurrentCt)}-category-list-cache", data, new MemoryCacheEntryOptions()
        .SetAbsoluteExpiration(TimeSpan.FromHours(24)));
            }

            dto.Data = data.Select(
                x => new AvOscilloscopeCurrentCtDTO
                {
                    ProductId = x.ProductId.ToString(),

                    Bandwidth = x.Bandwidth,
                    BatteryLife = x.BatteryLife,
                    CurrentDcac = x.CurrentDcac,
                    DcmeasurementAccuracy = x.DcmeasurementAccuracy,
                    Dimension = x.Dimension,
                    Humidity = x.Humidity,
                    MaxCableDiameter = x.MaxCableDiameter,
                    MaxFloatingVoltage = x.MaxFloatingVoltage,
                    MaxVoltage = x.MaxVoltage,
                    MeasurementRanges = x.MeasurementRanges,
                    Modifier = x.Modifier,
                    RiseFallTime = x.RiseFallTime,
                    RoHs = x.RoHs,
                    Temperature = x.Temperature,
                    Type = x.Type,
                    Weight = x.Weight,
                    Impedance = x.Impedance,
                    MaxCurrent = x.MaxCurrent,



                    SpecialHandling = x.SpecialHandling,
                    NewProduct = x.NewProduct,
                    ImageFileLink = x.ImageFileLink,
                    PartNumber = x.PartNumber,
                    Description = x.Description,
                    Warranty = x.Warranty,
                    Msrp = x.Msrp.ToString(),
                    UpcCode = x.UpcCode,
                    HarmonizedCode = x.HarmonizedCode,
                    CountryOfOrigin = x.CountryOfOrigin,
                    Discontinued = x.Discontinued.ToString(),
                    RoHsCompliant = x.RoHsCompliant.ToString(),
                }).ToList();
            dto.CategoryJumbotron = content;
            return Ok(dto);
        }
        else if (tableName == "A_OscilloscopeDifferential_CT")
        {
            CategoryPageDTO<AvOscilloscopeDifferentialCtDTO> dto = new();
            if (_memoryCache.TryGetValue($"{nameof(AvOscilloscopeDifferentialCt)}-category-list-cache", out List<AvOscilloscopeDifferentialCt> data))
            {
                _logger.LogInformation($"using {nameof(AvOscilloscopeDifferentialCt)}-category-list-cache");
            }
            else
            {
                data = new List<AvOscilloscopeDifferentialCt>();
                data = await _context.AvOscilloscopeDifferentialCts.ToListAsync();
                _memoryCache.Set($"{nameof(AvOscilloscopeDifferentialCt)}-category-list-cache", data, new MemoryCacheEntryOptions()
        .SetAbsoluteExpiration(TimeSpan.FromHours(24)));
            }

            dto.Data = data
                .Select(
                x => new AvOscilloscopeDifferentialCtDTO
                {
                    ProductId = x.ProductId.ToString(),

                    Accuracy = x.Accuracy,
                    Altitude = x.Altitude,
                    Attenuation = x.Attenuation,
                    Bandwidth = x.Bandwidth,
                    BnccableLength = x.BnccableLength,
                    Dimension = x.Dimension,
                    Iec = x.Iec,
                    InputImpedance = x.InputImpedance,
                    InputLeadsLength = x.InputLeadsLength,
                    InputVoltageAbsoluteMaximumRated = x.InputVoltageAbsoluteMaximumRated,
                    InputVoltageMaximumCommonMode = x.InputVoltageMaximumCommonMode,
                    InputVoltageMaximumDifferential = x.InputVoltageMaximumDifferential,
                    OperatingTemperature = x.OperatingTemperature,
                    OutputVoltageSourceImpedance = x.OutputVoltageSourceImpedance,
                    OutputVoltageSwing = x.OutputVoltageSwing,
                    OutputVoltageTypicalNoise = x.OutputVoltageTypicalNoise,
                    OutputVoltageTypicalOffset = x.OutputVoltageTypicalOffset,
                    PollutionDegree = x.PollutionDegree,
                    RiseTime = x.RiseTime,
                    RoHs = x.RoHs,
                    StorageTemperature = x.StorageTemperature,
                    Weight = x.Weight,
                    Humidity = x.Humidity,
                    Temperature = x.Temperature,
                    Type = x.Type,
                    TypicalCmrr = x.TypicalCmrr,
                    Power = x.Power,
                    ArCmrr = x.ArCmrr,
                    CableLength = x.CableLength,
                    PowerConsumption = x.PowerConsumption,
                    PowerSupply = x.PowerSupply,
                    Capacitance = x.Capacitance,
                    InputConnectorLength = x.InputConnectorLength,
                    InputResistance = x.InputResistance,
                    Modifier = x.Modifier,
                    AcCmrr = x.AcCmrr,
                    Type1 = x.Type1,


                    SpecialHandling = x.SpecialHandling,
                    NewProduct = x.NewProduct,
                    ImageFileLink = x.ImageFileLink,
                    PartNumber = x.PartNumber,
                    Description = x.Description,
                    Warranty = x.Warranty,
                    Msrp = x.Msrp.ToString(),
                    UpcCode = x.UpcCode,
                    HarmonizedCode = x.HarmonizedCode,
                    CountryOfOrigin = x.CountryOfOrigin,
                    Discontinued = x.Discontinued.ToString(),
                    RoHsCompliant = x.RoHsCompliant.ToString(),

                }).ToList();
            dto.CategoryJumbotron = content;
            return Ok(dto);
        }
        else if (tableName == "A_OscilloscopeHighVoltage_CT")
        {
            CategoryPageDTO<AvOscilloscopeHighVoltageCtDTO> dto = new();
            if (_memoryCache.TryGetValue($"{nameof(AvOscilloscopeHighVoltageCt)}-category-list-cache", out List<AvOscilloscopeHighVoltageCt> data))
            {
                _logger.LogInformation($"using {nameof(AvOscilloscopeHighVoltageCt)}-category-list-cache");
            }
            else
            {
                data = new List<AvOscilloscopeHighVoltageCt>();
                data = await _context.AvOscilloscopeHighVoltageCts.ToListAsync();
                _memoryCache.Set($"{nameof(AvOscilloscopeHighVoltageCt)}-category-list-cache", data, new MemoryCacheEntryOptions()
        .SetAbsoluteExpiration(TimeSpan.FromHours(24)));
            }

            dto.Data = data.Select(
                x => new AvOscilloscopeHighVoltageCtDTO
                {
                    ProductId = x.ProductId.ToString(),

                    AccuracyVac = x.AccuracyVac,
                    AccuracyVdc = x.AccuracyVdc,
                    Attenuation = x.Attenuation,
                    Bandwidth = x.Bandwidth,
                    Capacitance = x.Capacitance,
                    CompensationRange = x.CompensationRange,
                    Dimension = x.Dimension,
                    Humidity = x.Humidity,
                    Impedance = x.Impedance,
                    Length = x.Length,
                    MaxVoltage = x.MaxVoltage,
                    MaxVoltageAcrms = x.MaxVoltageAcrms,
                    OutputVoltageSourceImpedance = x.OutputVoltageSourceImpedance,
                    ReadoutActuator = x.ReadoutActuator,
                    RiseTime = x.RiseTime,
                    RoHs = x.RoHs,
                    StorageTemperature = x.StorageTemperature,
                    TempCoefficient = x.TempCoefficient,
                    Temperature = x.Temperature,
                    Type = x.Type,
                    Weight = x.Weight,
                    CableLength = x.CableLength,
                    InputImpedance = x.InputImpedance,
                    InputCapacitance = x.InputCapacitance,
                    MaxCurrentLoading = x.MaxCurrentLoading,
                    OperatingTemperature = x.OperatingTemperature,
                    OutputImpedance = x.OutputImpedance,
                    SignalNoise = x.SignalNoise,
                    Frequency = x.Frequency,
                    Type1 = x.Type1,

                    SpecialHandling = x.SpecialHandling,
                    NewProduct = x.NewProduct,
                    ImageFileLink = x.ImageFileLink,
                    PartNumber = x.PartNumber,
                    Description = x.Description,
                    Warranty = x.Warranty,
                    Msrp = x.Msrp.ToString(),
                    UpcCode = x.UpcCode,
                    HarmonizedCode = x.HarmonizedCode,
                    CountryOfOrigin = x.CountryOfOrigin,
                    Discontinued = x.Discontinued.ToString(),
                    RoHsCompliant = x.RoHsCompliant.ToString(),
                }).ToList();
            dto.CategoryJumbotron = content;
            return Ok(dto);
        }
        else if (tableName == "A_OscilloscopePassiveVoltage_CT")
        {
            CategoryPageDTO<AvOscilloscopePassiveVoltageCtDTO> dto = new();
            if (_memoryCache.TryGetValue($"{nameof(AvOscilloscopePassiveVoltageCt)}-category-list-cache", out List<AvOscilloscopePassiveVoltageCt> data))
            {
                _logger.LogInformation($"using {nameof(AvOscilloscopePassiveVoltageCt)}-category-list-cache");
            }
            else
            {
                data = new List<AvOscilloscopePassiveVoltageCt>();
                data = await _context.AvOscilloscopePassiveVoltageCts.ToListAsync();
                _memoryCache.Set($"{nameof(AvOscilloscopePassiveVoltageCt)}-category-list-cache", data, new MemoryCacheEntryOptions()
        .SetAbsoluteExpiration(TimeSpan.FromHours(24)));
            }

            dto.Data = data.Select(
                x => new AvOscilloscopePassiveVoltageCtDTO
                {
                    ProductId = x.ProductId.ToString(),

                    Attenuation = x.Attenuation,
                    Bandwidth = x.Bandwidth,
                    CableLength = x.CableLength,
                    Compensation = x.Compensation,
                    GroundBarrelDiameter = x.GroundBarrelDiameter,
                    Humidity = x.Humidity,
                    Impedance = x.Impedance,
                    InputCapacitance = x.InputCapacitance,
                    InputImpedance = x.InputImpedance,
                    MaxVoltage = x.MaxVoltage,
                    OutputImpedance = x.OutputImpedance,
                    ReadoutActuator = x.ReadoutActuator,
                    RiseTime = x.RiseTime,
                    RoHs = x.RoHs,
                    Series = x.Series,
                    Temperature = x.Temperature,
                    Type = x.Type,
                    Capacitance = x.Capacitance,
                    Length = x.Length,
                    OutputVoltageSourceImpedance = x.OutputVoltageSourceImpedance,
                    AttenuationAccuracy = x.AttenuationAccuracy,
                    Iec = x.Iec,
                    OperatingTemperature = x.OperatingTemperature,
                    VoltageCoefficient = x.VoltageCoefficient,
                    CompensationRange = x.CompensationRange,
                    TempCoefficient = x.TempCoefficient,
                    Type1 = x.Type1,

                    SpecialHandling = x.SpecialHandling,
                    NewProduct = x.NewProduct,
                    ImageFileLink = x.ImageFileLink,
                    PartNumber = x.PartNumber,
                    Description = x.Description,
                    Warranty = x.Warranty,
                    Msrp = x.Msrp.ToString(),
                    UpcCode = x.UpcCode,
                    HarmonizedCode = x.HarmonizedCode,
                    CountryOfOrigin = x.CountryOfOrigin,
                    Discontinued = x.Discontinued.ToString(),
                    RoHsCompliant = x.RoHsCompliant.ToString(),
                }).ToList();
            dto.CategoryJumbotron = content;
            return Ok(dto);
        }
        else if (tableName == "A_ProbeAccessories_CT")
        {
            CategoryPageDTO<AvProbeAccessoriesCtDTO> dto = new();
            if (_memoryCache.TryGetValue($"{nameof(AvProbeAccessoriesCt)}-category-list-cache", out List<AvProbeAccessoriesCt> data))
            {
                _logger.LogInformation($"using {nameof(AvProbeAccessoriesCt)}-category-list-cache");
            }
            else
            {
                data = new List<AvProbeAccessoriesCt>();
                data = await _context.AvProbeAccessoriesCts.ToListAsync();
                _memoryCache.Set($"{nameof(AvProbeAccessoriesCt)}-category-list-cache", data, new MemoryCacheEntryOptions()
        .SetAbsoluteExpiration(TimeSpan.FromHours(24)));
            }

            dto.Data = data.Select(
                x => new AvProbeAccessoriesCtDTO
                {
                    ProductId = x.ProductId.ToString(),

                    Iec = x.Iec,
                    MaxCurrent = x.MaxCurrent,
                    MaxResistance = x.MaxResistance,
                    RoHs = x.RoHs,
                    Temperature = x.Temperature,
                    Color = x.Color,
                    InterruptingRating = x.InterruptingRating,
                    MaxVoltage = x.MaxVoltage,
                    Type = x.Type,
                    Impedance = x.Impedance,
                    Length = x.Length,
                    InputPower = x.InputPower,
                    OutputPower = x.OutputPower,
                    Modifier = x.Modifier,
                    Accuracy = x.Accuracy,
                    AveragePower = x.AveragePower,
                    Bandwidth = x.Bandwidth,
                    MeterImpedance = x.MeterImpedance,
                    ContactMaterial = x.ContactMaterial,
                    InsulationMaterial = x.InsulationMaterial,
                    Dimension = x.Dimension,
                    Voltage = x.Voltage,
                    Ulrating = x.Ulrating,
                    JacketMaterial = x.JacketMaterial,
                    Od = x.Od,
                    Stranding = x.Stranding,
                    TipDimension = x.TipDimension,
                    WireGauge = x.WireGauge,
                    Type1 = x.Type1,

                    SpecialHandling = x.SpecialHandling,
                    NewProduct = x.NewProduct,
                    ImageFileLink = x.ImageFileLink,
                    PartNumber = x.PartNumber,
                    Description = x.Description,
                    Warranty = x.Warranty,
                    Msrp = x.Msrp.ToString(),
                    UpcCode = x.UpcCode,
                    HarmonizedCode = x.HarmonizedCode,
                    CountryOfOrigin = x.CountryOfOrigin,
                    Discontinued = x.Discontinued.ToString(),
                    RoHsCompliant = x.RoHsCompliant.ToString(),
                }).ToList();
            dto.CategoryJumbotron = content;
            return Ok(dto);
        }
        else if (tableName == "A_SpadeLugs_CT")
        {
            CategoryPageDTO<AvSpadeLugsCtDTO> dto = new();
            if (_memoryCache.TryGetValue($"{nameof(AvSpadeLugsCt)}-category-list-cache", out List<AvSpadeLugsCt> data))
            {
                _logger.LogInformation($"using {nameof(AvSpadeLugsCt)}-category-list-cache");
            }
            else
            {
                data = new List<AvSpadeLugsCt>();
                data = await _context.AvSpadeLugsCts.ToListAsync();
                _memoryCache.Set($"{nameof(AvSpadeLugsCt)}-category-list-cache", data, new MemoryCacheEntryOptions()
        .SetAbsoluteExpiration(TimeSpan.FromHours(24)));
            }

            dto.Data = data.Select(
                x => new AvSpadeLugsCtDTO
                {
                    ProductId = x.ProductId.ToString(),

                    Color = x.Color,
                    Iec = x.Iec,
                    MaxCurrent = x.MaxCurrent,
                    MaxResistance = x.MaxResistance,
                    RoHs = x.RoHs,
                    Temperature = x.Temperature,
                    Type = x.Type,

                    SpecialHandling = x.SpecialHandling,
                    NewProduct = x.NewProduct,
                    ImageFileLink = x.ImageFileLink,
                    PartNumber = x.PartNumber,
                    Description = x.Description,
                    Warranty = x.Warranty,
                    Msrp = x.Msrp.ToString(),
                    UpcCode = x.UpcCode,
                    HarmonizedCode = x.HarmonizedCode,
                    CountryOfOrigin = x.CountryOfOrigin,
                    Discontinued = x.Discontinued.ToString(),
                    RoHsCompliant = x.RoHsCompliant.ToString(),
                }).ToList();
            dto.CategoryJumbotron = content;
            return Ok(dto);
        }
        else if (tableName == "A_TestLeadAssemblies_CT")
        {
            CategoryPageDTO<AvTestLeadAssembliesCtDTO> dto = new();
            if (_memoryCache.TryGetValue($"{nameof(AvTestLeadAssembliesCt)}-category-list-cache", out List<AvTestLeadAssembliesCt> data))
            {
                _logger.LogInformation($"using {nameof(AvTestLeadAssembliesCt)}-category-list-cache");
            }
            else
            {
                data = new List<AvTestLeadAssembliesCt>();
                data = await _context.AvTestLeadAssembliesCts.ToListAsync();
                _memoryCache.Set($"{nameof(AvTestLeadAssembliesCt)}-category-list-cache", data, new MemoryCacheEntryOptions()
        .SetAbsoluteExpiration(TimeSpan.FromHours(24)));
            }

            dto.Data = data.Select(
                x => new AvTestLeadAssembliesCtDTO
                {
                    ProductId = x.ProductId.ToString(),

                    ConductorArea = x.ConductorArea,
                    Iec = x.Iec,
                    JacketMaterial = x.JacketMaterial,
                    Length = x.Length,
                    MaxCurrent = x.MaxCurrent,
                    MaxResistance = x.MaxResistance,
                    Od = x.Od,
                    RoHs = x.RoHs,
                    Stranding = x.Stranding,
                    Temperature = x.Temperature,
                    WireGauge = x.WireGauge,
                    Color = x.Color,
                    JacketOd = x.JacketOd,
                    Ulrating = x.Ulrating,
                    MaxVoltage = x.MaxVoltage,
                    Type = x.Type,
                    CableType = x.CableType,
                    CableLength = x.CableLength,
                    Impedance = x.Impedance,
                    VoltageRating = x.VoltageRating,
                    ContactMaterial = x.ContactMaterial,
                    InsulationMaterial = x.InsulationMaterial,
                    Ul94flameRating = x.Ul94flameRating,
                    Type1 = x.Type1,

                    SpecialHandling = x.SpecialHandling,
                    NewProduct = x.NewProduct,
                    ImageFileLink = x.ImageFileLink,
                    PartNumber = x.PartNumber,
                    Description = x.Description,
                    Warranty = x.Warranty,
                    Msrp = x.Msrp.ToString(),
                    UpcCode = x.UpcCode,
                    HarmonizedCode = x.HarmonizedCode,
                    CountryOfOrigin = x.CountryOfOrigin,
                    Discontinued = x.Discontinued.ToString(),
                    RoHsCompliant = x.RoHsCompliant.ToString(),
                }).ToList();
            dto.CategoryJumbotron = content;
            return Ok(dto);
        }
        else if (tableName == "A_TestLeadWire_CT")
        {
            CategoryPageDTO<AvTestLeadWireCtDTO> dto = new();
            if (_memoryCache.TryGetValue($"{nameof(AvTestLeadWireCt)}-category-list-cache", out List<AvTestLeadWireCt> data))
            {
                _logger.LogInformation($"using {nameof(AvTestLeadWireCt)}-category-list-cache");
            }
            else
            {
                data = new List<AvTestLeadWireCt>();
                data = await _context.AvTestLeadWireCts.ToListAsync();
                _memoryCache.Set($"{nameof(AvTestLeadWireCt)}-category-list-cache", data, new MemoryCacheEntryOptions()
        .SetAbsoluteExpiration(TimeSpan.FromHours(24)));
            }

            dto.Data = data.Select(
                x => new AvTestLeadWireCtDTO
                {
                    ProductId = x.ProductId.ToString(),

                    Color = x.Color,
                    ConductorArea = x.ConductorArea,
                    Iec = x.Iec,
                    JacketMaterial = x.JacketMaterial,
                    Length = x.Length,
                    MaxCurrent = x.MaxCurrent,
                    Od = x.Od,
                    RoHs = x.RoHs,
                    Stranding = x.Stranding,
                    Temperature = x.Temperature,
                    WireGauge = x.WireGauge,
                    Type = x.Type,

                    SpecialHandling = x.SpecialHandling,
                    NewProduct = x.NewProduct,
                    ImageFileLink = x.ImageFileLink,
                    PartNumber = x.PartNumber,
                    Description = x.Description,
                    Warranty = x.Warranty,
                    Msrp = x.Msrp.ToString(),
                    UpcCode = x.UpcCode,
                    HarmonizedCode = x.HarmonizedCode,
                    CountryOfOrigin = x.CountryOfOrigin,
                    Discontinued = x.Discontinued.ToString(),
                    RoHsCompliant = x.RoHsCompliant.ToString(),
                }).ToList();
            dto.CategoryJumbotron = content;
            return Ok(dto);
        }
        else if (await _context.Categories.AnyAsync(x => x.DisplayName == tableName))
        {
            CategoryPageDTO<VBaseProductListDTO> dto = new();
            var category1 = categories.FirstOrDefault(x => x.DisplayName == tableName);
            var content1 = new JumbotronContent
            {
                Title = category1?.DisplayName,
                Subtitle = category1?.Description,
                ImageLink = category1?.ImageFileLink
            };

            if (_memoryCache.TryGetValue($"{nameof(VBaseProductList)}-category-list-cache", out List<VBaseProductList> data))
            {
                _logger.LogInformation($"using {nameof(VBaseProductList)}-category-list-cache");
            }
            else
            {
                data = new List<VBaseProductList>();
                data = await _context.VBaseProductLists.ToListAsync();
                _memoryCache.Set($"{nameof(VBaseProductList)}-category-list-cache", data, new MemoryCacheEntryOptions()
        .SetAbsoluteExpiration(TimeSpan.FromHours(12)));
            }



            dto.Data = data.Where(pl => pl.CategoryId == category1.Id).Select(
                x => new VBaseProductListDTO
                {
                    ProductId = x.Id.ToString(),
                    ImageFileLink = x.ImageFileLink,
                    PartNumber = x.PartNumber,
                    Description = x.Description,
                    SpecialHandling = x.SpecialHandling,
                    Warranty = x.Warranty,
                    Msrp = x.Msrp.ToString(),
                    NewProduct = x.NewProduct,
                    UpcCode = x.UpcCode,
                    HarmonizedCode = x.HarmonizedCode,
                    CountryOfOrigin = x.CountryOfOrigin,
                    Discontinued = x.Discontinued.ToString(),
                    RoHsCompliant = x.RoHsCompliant.ToString(),
                }).ToList();
            dto.CategoryJumbotron = content1;
            return Ok(dto);

        }

        return NotFound();

    }
}