﻿namespace WebApp.Server.Controllers.Web;

[Route("api/[controller]")]
[ApiController]
[ResponseCache(CacheProfileName = "Default1Day")]
public class HomepageController : ControllerBase
{
    private readonly ApplicationDbContext _context;
    private readonly ILogger<HomepageController> _logger;
    private readonly IMemoryCache _memoryCache;
    public HomepageController(ApplicationDbContext context, ILogger<HomepageController> logger, IMemoryCache memoryCache)
    {
        _context = context;
        _logger = logger;
        _memoryCache = memoryCache;
    }

    [HttpGet("PageData/{brand}")]
    public async Task<ActionResult<List<Homepage>>> PageData(string brand)
    {

        if (_memoryCache.TryGetValue($"homepage-cache-{brand}", out List<HomepageDTO> data))
        {
            _logger.LogInformation($"using homepage-cache-{brand}");
        }
        else
        {
            _logger.LogInformation($"NOT using homepage-cache-{brand}");
            var query = await _context.Homepages.Where(x => x.Brand == brand).ToListAsync();
            data = query.Select(x => new HomepageDTO
            {
                Type = x.Type,
                Title = x.Title,
                Subtitle = x.Subtitle,
                Summary = x.Summary,
                MediaFileLink = x.MediaFileLink,
                Url = x.Url,
            }).ToList();
            _memoryCache.Set($"homepage-cache-{brand}", data ?? null, new MemoryCacheEntryOptions()
    .SetAbsoluteExpiration(TimeSpan.FromHours(24)));
        }

        return Ok(data);
    }
}
