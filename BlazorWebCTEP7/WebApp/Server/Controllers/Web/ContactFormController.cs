﻿using WebApp.Server.Helpers;

namespace WebApp.Server.Controllers.Web;

[Route("api/[controller]")]
[ApiController]
public class ContactFormController : ControllerBase
{
    private readonly ApplicationDbContext _context;
    private readonly ILogger<ContactFormController> _logger;
    private readonly IEmailSenderService _emailSender;
    private readonly CrmService _crmService;
    public ContactFormController(ApplicationDbContext context, ILogger<ContactFormController> logger, IEmailSenderService emailSender, CrmService crmService)
    {
        _context = context;
        _logger = logger;
        _emailSender = emailSender;
        _crmService = crmService;
    }

    [HttpPost]
    public async Task<IActionResult> Post([FromBody] ContactFormDTO data)
    {
        if (data == null)
        {
            return BadRequest("No data sent");
        }

        if (string.IsNullOrWhiteSpace(data.Brand) || string.IsNullOrWhiteSpace(data.Email) || string.IsNullOrWhiteSpace(data.FirstName) || string.IsNullOrWhiteSpace(data.LastName))
        {
            return BadRequest("Form not filled out");
        }

        ContactForm contactForm = new ContactForm();
        contactForm.FirstName = data.FirstName;
        contactForm.LastName = data.LastName;
        contactForm.Email = data.Email;
        contactForm.Company = data.Company;
        contactForm.Phone = data.Phone;
        contactForm.PhoneExt = data.PhoneExt;
        contactForm.Address1 = data.Address1;
        contactForm.Address2 = data.Address2;

        contactForm.City = data.City;
        contactForm.StateOrProvince = data.StateOrProvince;
        contactForm.PostalCode = data.PostalCode;
        contactForm.Country = data.Country;
        contactForm.ContactReasonChoice = data.ContactReasonChoice;
        contactForm.Message = data.Message;
        contactForm.Brand = BrandHelper.GetIdById(data.Brand);

        //save to db
        try
        {
            _context.ContactForms.Add(contactForm);
            await _context.SaveChangesAsync();

        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Failed to save contact form to DB.");
        }

        //then try to send email
        try
        {
            HubSpotOwner hubSpotOwner = new HubSpotOwner(contactForm.ContactReasonChoice);
            var email = WebFormTemplateHelper.GenerateContactEmail(contactForm, hubSpotOwner);
            await _emailSender.SendEmailAsync(email);
            contactForm.EmailConfirmationSent = true;
            await _context.SaveChangesAsync();

        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Failed to email contact form receipt to customer.");
        }

        //then try to save to hubspot
        try
        {
            HubSpotOwner hubSpotOwner = new HubSpotOwner(contactForm.ContactReasonChoice);
            await _crmService.HubspotIntegration(contactForm.Email, hubSpotOwner, contactForm);
            contactForm.HubSpotTicketCreated = true;
            await _context.SaveChangesAsync();
        }
        catch (Exception ex)
        {
            await _emailSender.SendEmailAsync("mchanner@caltestelectronics.com", "Failed to log to hubspot.", (data.FirstName + " " + data.LastName + "\n" + data.Email + "\n" + data.Message + "\n\n" + ex.Message));

            _logger.LogError(ex.Message, "Failed to log to hubspot.");
        }

        if (contactForm.EmailConfirmationSent && contactForm.HubSpotTicketCreated)
        {
            return Ok(data);
        }
        return BadRequest(data);
    }



}
