﻿using Microsoft.Data.Edm;
using Microsoft.OData.Edm;
using Microsoft.OData.ModelBuilder;
using WebDB.Data.Context;

namespace WebApp.Server.Data;

public class ApplicationDbContext : CTEDbContext
{
    private readonly IWebHostEnvironment env;
    private readonly ILogger<ApplicationDbContext> logger;

    public ApplicationDbContext(IWebHostEnvironment env, ILogger<ApplicationDbContext> logger, DbContextOptions<ApplicationDbContext> options)
         : base(options)
    {
        this.env = env;
        this.logger = logger;

    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        base.OnConfiguring(optionsBuilder);
        if (env.IsDevelopment())
        {
            optionsBuilder.LogTo(Console.WriteLine);
            optionsBuilder.EnableSensitiveDataLogging();
        }
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        if (env.IsProduction())
        {
            logger.LogInformation("Production DB mode: Applying query filter to hide not published data.");
            modelBuilder.Entity<Series>().HasQueryFilter(pf => pf.Published);
            modelBuilder.Entity<Product>().HasQueryFilter(p => p.Published);
            //modelBuilder.Entity<ProductListView>().HasQueryFilter(plv => (bool)plv.Published);

            modelBuilder.Entity<Article>().HasQueryFilter(plv => (bool)plv.Published);
            modelBuilder.Entity<TestLeadColor>().HasQueryFilter(plv => (bool)plv.Published);
            modelBuilder.Entity<TestLeadConnector>().HasQueryFilter(plv => (bool)plv.Published);
            modelBuilder.Entity<TestLeadLength>().HasQueryFilter(plv => (bool)plv.Published);
            modelBuilder.Entity<TestLeadWire>().HasQueryFilter(plv => (bool)plv.Published);
            modelBuilder.Entity<CompanyDocument>().HasQueryFilter(plv => (bool)plv.Published);
            modelBuilder.Entity<AuthorizedDistributor>().HasQueryFilter(plv => (bool)plv.Published);

        }
    }

    public static Microsoft.OData.Edm.IEdmModel GetEdmModel()
    {
        ODataConventionModelBuilder modelBuilder = new();
        AddEntities(modelBuilder);
        return modelBuilder.GetEdmModel();
    }

    public static void AddEntities(ODataConventionModelBuilder builder)
    {
        var contextType = typeof(ApplicationDbContext);
        var properties = contextType.GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance)
            .Where(x => x.PropertyType.IsGenericType)
            .ToArray();

        var entitySetMethod = typeof(ODataConventionModelBuilder).GetMethod(nameof(ODataConventionModelBuilder.EntitySet));



        var dbSetProperties = properties.Where(x => x.PropertyType.GetGenericTypeDefinition() == typeof(DbSet<>)).ToArray();
        foreach (var property in dbSetProperties)
        {
            if (property.Name == "ViewOrderTrackingNumberAggregates")
            {
                Console.WriteLine();
            }
            var dbSetType = property.PropertyType.GetGenericArguments().FirstOrDefault();
            var isKeyless = dbSetType
                .GetCustomAttributes(typeof(KeylessAttribute),
                false).Any() ||
                property.Name.StartsWith("View") ||
                property.Name.StartsWith("VImageFileLinkTmb") ||
                property.Name.StartsWith("AV_") ||
                property.Name.StartsWith("Resources")||
                property.Name.StartsWith("ReportTlb");
            if (isKeyless == false)
            {
                var entitySetMethodGeneric = entitySetMethod.MakeGenericMethod(dbSetType);
                entitySetMethodGeneric?.Invoke(builder, new[] { dbSetType.Name });
            }
            else
            {
                var keyProperty = DetermineKeyProperty(dbSetType);
                if (keyProperty != null)
                {
                    EntityTypeConfiguration entityType = builder.AddEntityType(dbSetType);
                    entityType.HasKey(keyProperty);
                    builder.AddEntitySet(dbSetType.Name, entityType);
                }
            }
        }
    }

    private static PropertyInfo DetermineKeyProperty(Type dbSetType)
    {
        var propertyInfo = dbSetType.GetProperty("Id");
        if (propertyInfo == null)
        {
            return null;
        }
        return propertyInfo;
    }
}