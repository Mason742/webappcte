﻿using Microsoft.AspNetCore.Components.WebAssembly.Authentication;
using System.Text.Json.Serialization;

namespace WebApp.Client;

public class CustomUserAccount : RemoteUserAccount
{
    [JsonPropertyName("ver")]
    public string Version { get; set; }

    [JsonPropertyName("extension_CustomerId")]
    public string CustomerId { get; set; }

    [JsonPropertyName("given_name")]
    public string GivenName { get; set; }
    [JsonPropertyName("family_name")]
    public string FamilyName { get; set; }

}
