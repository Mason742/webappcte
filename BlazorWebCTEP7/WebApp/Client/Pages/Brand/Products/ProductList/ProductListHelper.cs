﻿namespace WebApp.Client.Pages.Brand.Products.ProductList;

public static class ProductListHelper
{
    public static string GetHeader(string columnName)
    {
        string header = columnName switch
        {
            "ProductId" => "Id",
            "PartNumber" => "Model",
            "MainImage" => "Image",
            "Iec" => "IEC Rating",
            "MaxCurrent" => "Max Current",
            "RoHs" => "RoHS Status",
            "Price" => "MSRP.",
            "NewProduct" => "New Product",
            _ => columnName,
        };
        return header;
    }

}
