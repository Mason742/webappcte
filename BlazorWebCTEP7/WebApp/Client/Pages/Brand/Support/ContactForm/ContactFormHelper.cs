﻿namespace WebApp.Client.Pages.Brand.Support.ContactForm;

public static class ContactFormHelper
{
    public static readonly ContactReason SalesReason = new() { Value = "Contact_Sales", Name = "Sales - for request for quotes and general inquiries" };
    public static readonly ContactReason TechSupportReason = new() { Value = "Contact_TechSupport", Name = "Tech Support - for technical questions" };
    public static readonly ContactReason ContactOtherReason = new() { Value = "Contact_Other", Name = "Other - for all other general requests" };

    public static readonly List<ContactReason> ContactReasons = new List<ContactReason>()
    {
           SalesReason,
           TechSupportReason,
           ContactOtherReason
    };

    public class ContactReason
    {
        public string Value { get; set; }
        public string Name { get; set; }
    }
}
