using Blazor.Analytics;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.Localization;
using Microsoft.JSInterop;
using Syncfusion.Blazor;
using Syncfusion.Blazor.Popups;
using System.Globalization;
using WebApp.Client;
using WebApp.Client.Helper;
using WebApp.Client.Resources;
using WebApp.Client.Services;
using WebApp.Client.Shared;
using WebDB.Data.Models;

var builder = WebAssemblyHostBuilder.CreateDefault(args);
builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");

//Register Syncfusion license
Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("MjM5NTIyOUAzMjMxMmUzMDJlMzBLZjB2OWJ2OEZrMi9MWFV2dmd3SW9tdU1DSkk1RU9WZ29MaDl4RHB3YURNPQ==");

//default named client
builder.Services.AddHttpClient("CTEWebApp.AnonymousAPI", client => client.BaseAddress = new Uri(builder.HostEnvironment.BaseAddress));
//default named client
builder.Services.AddHttpClient("", client => client.BaseAddress = new Uri(builder.HostEnvironment.BaseAddress));

//authorized named  client
builder.Services.AddHttpClient("CTEWebApp.ServerAPI", client => client.BaseAddress = new Uri(builder.HostEnvironment.BaseAddress)).AddHttpMessageHandler<BaseAddressAuthorizationMessageHandler>();

builder.Services.AddMsalAuthentication<RemoteAuthenticationState,
    CustomUserAccount>(options =>
    {
        options.ProviderOptions.Authentication.Authority = builder.Configuration["AzureAdB2C:Authority"];
        options.ProviderOptions.Authentication.ClientId = builder.Configuration["AzureAdB2C:ClientId"];
        options.ProviderOptions.Authentication.RedirectUri = builder.Configuration["AzureAdB2C:RedirectUri"];
        options.ProviderOptions.DefaultAccessTokenScopes.Add("https://cteappb2c.onmicrosoft.com/7dfe6bf8-1d97-4601-8d14-1521317947f9/API.Access");
        options.UserOptions.RoleClaim = "extension_Role";
        options.ProviderOptions.LoginMode = "redirect";
    }).AddAccountClaimsPrincipalFactory<RemoteAuthenticationState, CustomUserAccount, CustomAccountFactory>();


//https://github.com/isc30/blazor-analytics
builder.Services.AddGoogleAnalytics("UA-179252841-1");

builder.Services.AddSingleton<StateService>();

builder.Services.AddLocalization();

builder.Services.AddScoped<SfDialogService>();
builder.Services.AddSyncfusionBlazor();


builder.Services.AddScoped<IToastService, ToastService>();

var host = builder.Build();

//builder.Services.AddScoped<IStringLocalizer<SharedResources>, StringLocalizer<SharedResources>>();

await SetApplicationCultureAsync(host);
// Get the localizer service
Console.WriteLine("UI culture: "+CultureInfo.CurrentUICulture.Name);
//for whatever reason UI culture is not be respected when getting the IStringLocalizer service after changing culture.
var localizer = builder.Services.BuildServiceProvider().GetService<IStringLocalizer<SharedResources>>();
ToastModelHelper.InitializeToastModels(localizer);

// Iterate through the dictionary and write out the keys and values
foreach (var keyValuePair in localizer.GetAllStrings())
{
    Console.WriteLine($"Key: {keyValuePair.Name}, Value: {keyValuePair.Value}");
}
await host.RunAsync();

async Task SetApplicationCultureAsync(WebAssemblyHost webAssemblyHost)
{
    CultureInfo culture;
    var js = webAssemblyHost.Services.GetRequiredService<IJSRuntime>();
    var result = await js.InvokeAsync<string>("blazorCulture.get");

    if (result != null)
    {
        culture = new CultureInfo(result);
    }
    else
    {
        culture = new CultureInfo("en-US");
        await js.InvokeVoidAsync("blazorCulture.set", "en-US");
    }

    CultureInfo.DefaultThreadCurrentCulture = culture;
    CultureInfo.DefaultThreadCurrentUICulture = culture;
    CultureInfo.CurrentUICulture = culture;
    Console.WriteLine(culture.Name);
}