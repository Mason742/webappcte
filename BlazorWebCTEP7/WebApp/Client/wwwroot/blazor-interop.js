﻿//check if webassembly is supported
const webassemblySupported = (function () {
    try {
        if (typeof WebAssembly === "object"
            && typeof WebAssembly.instantiate === "function") {
            const module = new WebAssembly.Module(
                Uint8Array.of(0x0, 0x61, 0x73, 0x6d,
                    0x01, 0x00, 0x00, 0x00));
            if (module instanceof WebAssembly.Module)
                return new WebAssembly.Instance(module)
                    instanceof WebAssembly.Instance;
        }
    } catch (e) {
    }
    return false;
})();

// Modern browsers e.g. Microsoft Edge
if (webassemblySupported) {
    Blazor.start({});
}
// Older browsers e.g. IE11
else {
    window.location = "https://as.caltestelectronics.com/page/support.html";
}

//scroll to #
function BlazorScrollToId(id) {
    const element = document.getElementById(id);
    if (element instanceof HTMLElement) {
        element.scrollIntoView({
            behavior: "auto",
            block: "start",
            inline: "nearest"
        });
    }
}

//get Timezone Offset of client
function blazorGetTimezoneOffset() {
    return new Date().getTimezoneOffset();
}


window.SetFocusToElement = (element) => {
    element.focus();
};

function saveAsFile(filename, bytesBase64) {
    var link = document.createElement('a');
    link.download = filename;
    link.href = "data:application/octet-stream;base64," + bytesBase64;
    document.body.appendChild(link); // Needed for Firefox
    link.click();
    document.body.removeChild(link);
}

function triggerFileDownload(fileName, url) {
    const anchorElement = document.createElement('a');
    anchorElement.href = url;
    anchorElement.download = fileName ?? '';
    anchorElement.click();
    anchorElement.remove();
}

async function downloadFileFromStream(fileName, contentStreamReference) {
    const arrayBuffer = await contentStreamReference.arrayBuffer();
    const blob = new Blob([arrayBuffer]);
    const url = URL.createObjectURL(blob);

    triggerFileDownload(fileName, url);

    URL.revokeObjectURL(url);
}

function clickElement(element) {
    element.click();
}
function downloadFromUrl(options) {
    var _a;
    var anchorElement = document.createElement('a');
    anchorElement.href = options.url;
    anchorElement.download = (_a = options.fileName) !== null && _a !== void 0 ? _a : '';
    anchorElement.click();
    anchorElement.remove();
}
function downloadFromByteArray(options) {
    var url = typeof (options.byteArray) === 'string' ?
        // .NET 5 or earlier, the byte array in .NET is encoded to base64 string when it passes to JavaScript.
        // In that case, that base64 encoded string can be pass to the browser as a "data URL" directly.
        "data:" + options.contentType + ";base64," + options.byteArray :
        // .NET 6 or later, the byte array in .NET is passed to JavaScript as an UInt8Array efficiently.
        // - https://docs.microsoft.com/en-us/dotnet/core/compatibility/aspnet-core/6.0/byte-array-interop
        // In that case, the UInt8Array can be converted to an object URL and passed to the browser.
        // But don't forget to revoke the object URL when it is no longer needed.
        URL.createObjectURL(new Blob([options.byteArray], { type: options.contentType }));
    downloadFromUrl({ url: url, fileName: options.fileName });
    if (typeof (options.byteArray) !== 'string')
        URL.revokeObjectURL(url);
}