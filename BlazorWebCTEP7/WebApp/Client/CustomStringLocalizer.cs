﻿using Microsoft.Extensions.Localization;
using System.Globalization;

namespace WebApp.Client
{
    public class CustomStringLocalizer : IStringLocalizer
    {
        private readonly IDictionary<string, string> _dictionary;

        public CustomStringLocalizer(IDictionary<string, string> dictionary)
        {
            _dictionary = dictionary;
        }

        public LocalizedString this[string name]
        {
            get
            {
                if (_dictionary.TryGetValue(name, out var value))
                {
                    return new LocalizedString(name, value);
                }
                else
                {
                    // Return the key as the localized string if no matching key is found
                    return new LocalizedString(name, name, true);
                }
            }
        }

        public LocalizedString this[string name, params object[] arguments]
        {
            get
            {
                if (_dictionary.TryGetValue(name, out var value))
                {
                    return new LocalizedString(name, string.Format(value, arguments));
                }
                else
                {
                    // Return the key as the localized string if no matching key is found
                    return new LocalizedString(name, string.Format(name, arguments), true);
                }
            }
        }

        public IEnumerable<LocalizedString> GetAllStrings(bool includeParentCultures)
        {
            foreach (var kvp in _dictionary)
            {
                yield return new LocalizedString(kvp.Key, kvp.Value);
            }
        }

        public IStringLocalizer WithCulture(CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
