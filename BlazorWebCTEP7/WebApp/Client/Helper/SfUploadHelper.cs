﻿using CteCore.Helpers;
using Syncfusion.Blazor.Inputs;

namespace WebApp.Client.Helper;

public static class SfUploadHelper
{
    public static void OnFileSelect(SelectedEventArgs selectedEventArgs, string rawAccessToken)
    {
        var accessToken = $"Bearer {rawAccessToken}";
        selectedEventArgs.CurrentRequest = new List<object> { new { Authorization = accessToken } };
    }

    public static void OnFileSelect(SelectedEventArgs selectedEventArgs)
    {
    }
    public static string UploadSuccess(SuccessEventArgs successEventArgs)
    {
        var headers = successEventArgs.Response.Headers.ToString();
        Console.WriteLine("header: \n" + headers);
        //if caller is upload return filename
        if (headers.Contains("name"))
        {
            string[] header = headers.Split("name: ");
            Console.WriteLine("call in upload success");
            header = header[1].Split("\r");

            // Update the modified image name to display a image in the editor.
            //args.File.Name = header[0];
            Console.WriteLine(header[0]);

            string uploadFilename = header[0];
            return uploadFilename;
        }
        else
        {
            //else return empty filename to clear out bound textbox.
            return string.Empty;
        }
    }


    public static void BeforeRemove(BeforeRemoveEventArgs beforeRemoveEventArgs, Type type)
    {
        beforeRemoveEventArgs.PostRawFile = true;
        beforeRemoveEventArgs.CurrentRequest = new List<object> { new { TempDirectory = FileUploadHelper.GetTempDirectory(type) } };

    }
    public static void BeforeRemove(BeforeRemoveEventArgs beforeRemoveEventArgs, string rawAccessToken, Type type)
    {
        beforeRemoveEventArgs.PostRawFile = true;
        var accessToken = $"Bearer {rawAccessToken}";
        beforeRemoveEventArgs.CurrentRequest = new List<object> { new { Authorization = accessToken }, new { TempDirectory = FileUploadHelper.GetTempDirectory(type) } };

    }
}
