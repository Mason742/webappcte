﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System.Text.RegularExpressions;

namespace WebApp.Client.Helper
{
    public static class ClientHelper
    {
        public static bool IsValidEmail(string strIn)
        {
            return Regex.IsMatch(strIn, @"^([0-9a-zA-Z]([-\.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$");
        }

        public static class FileUtil
        {
            public async static Task SaveAs(IJSRuntime js, string filename, byte[] data)
            {
                await js.InvokeAsync<object>(
                    "saveAsFile",
                    filename,
                    Convert.ToBase64String(data));
            }

            public async static Task DownloadFileFromURL(IJSRuntime js, string filename, string url)
            {
                await js.InvokeAsync<object>(
                    "triggerFileDownload",
                    filename,
                    url);
            }
            public async static Task DownloadFromByteArray(IJSRuntime js, byte[] bytes, string fileName, string contentType)
            {
                await js.InvokeVoidAsync(
                "downloadFromByteArray",
                new
                {
                    ByteArray = bytes,
                    FileName = fileName,
                    ContentType = contentType
                });
            }
            //public async static Task DownloadFileFromStream(IJSRuntime js, string filename, DotNetStreamReference streamRef)
            //{
            //    await js.InvokeAsync<object>(
            //        "downloadFileFromStream",
            //        filename,
            //        streamRef);
            //}
        }

        public async static Task NavigateToUrlAsync(IJSRuntime js, NavigationManager navigationManager, string url, bool openInNewTab)
        {
            if (openInNewTab)
            {
                await js.InvokeAsync<object>("open", url, "_blank");
            }
            else
            {
                navigationManager.NavigateTo(url);
            }
        }


        public async static Task ScrollToFragment(IJSRuntime js, NavigationManager navigationManager, bool delay = false, int milliseconds = 250)
        {
            if (delay)
            {
                await Task.Delay(milliseconds);
            }
            var uri = new Uri(navigationManager.Uri, UriKind.Absolute);
            var fragment = uri.Fragment;
            Console.WriteLine("scrolling to " + fragment);
            if (fragment.StartsWith('#'))
            {
                // Handle text fragment (https://example.org/#test:~:text=foo)
                // https://github.com/WICG/scroll-to-text-fragment/
                var elementId = fragment.Substring(1);
                var index = elementId.IndexOf(":~:", StringComparison.Ordinal);
                if (index > 0)
                {
                    elementId = elementId.Substring(0, index);
                }

                if (!string.IsNullOrEmpty(elementId))
                {
                    await js.InvokeVoidAsync("BlazorScrollToId", elementId);
                }
            }
        }


    }
}
