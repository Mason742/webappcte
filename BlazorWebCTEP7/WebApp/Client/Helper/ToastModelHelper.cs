﻿using Microsoft.Extensions.Localization;
using Syncfusion.Blazor.Notifications;

namespace WebApp.Client.Helper
{
    public static class ToastModelHelper
    {
        public enum ToastKeys
        {
            NetworkWarning,
            SuccessMessageSent,
            DataSubmissionError,
            ReadCommentsCarefully,
            LengthAndColor,
            InvalidLength,
            InvalidColor,
            InvalidPartNumber,
            PartBuiltSuccessfully,
            SelectTwoProducts
        }


        private static Dictionary<ToastKeys, ToastModel> ToastModels = new Dictionary<ToastKeys, ToastModel>();

        public static void InitializeToastModels(IStringLocalizer localizer)
        {
            ToastModels = new Dictionary<ToastKeys, ToastModel>
            {
            {
                ToastKeys.NetworkWarning, new ToastModel
                {
                    Title = localizer["Warning"],
                    Content = localizer["NetworkWarning"],
                    CssClass = "e-toast-warning",
                    Timeout = 5000
                }
            },
            {
                ToastKeys.SuccessMessageSent, new ToastModel
                {
                    Title = localizer["Success"],
                    Content = localizer["SuccessMessageSent"],
                    CssClass = "e-toast-success",
                    Timeout = 5000
                }
            },
            {
                ToastKeys.DataSubmissionError, new ToastModel
                {
                    Title = localizer["Error"],
                    Content = localizer["DataSubmissionError"],
                    CssClass = "e-toast-danger",
                    Timeout = 5000
                }
            },
            {
                ToastKeys.ReadCommentsCarefully, new ToastModel
                {
                    Title = localizer["Information"],
                    Content = localizer["ReadCommentsCarefully"],
                    CssClass = "e-toast-info",
                    Timeout = 5000
                }
            },
            {
                ToastKeys.LengthAndColor, new ToastModel
                {
                    Title = localizer["Information"],
                    Content = localizer["LengthAndColor"],
                    CssClass = "e-toast-info",
                    Timeout = 5000
                }
            },
            {
                ToastKeys.InvalidLength, new ToastModel
                {
                    Title = localizer["Error"],
                    Content = localizer["InvalidLength"],
                    CssClass = "e-toast-danger",
                    Timeout = 5000
                }
            },
            {
                ToastKeys.InvalidColor, new ToastModel
                {
                    Title = localizer["Error"],
                    Content = localizer["InvalidColor"],
                    CssClass = "e-toast-danger",
                    Timeout = 5000
                }
            },
            {
                ToastKeys.InvalidPartNumber, new ToastModel
                {
                    Title = localizer["Error"],
                    Content = localizer["InvalidPartNumber"],
                    CssClass = "e-toast-danger",
                    Timeout = 5000
                }
            },
            {
                ToastKeys.PartBuiltSuccessfully, new ToastModel
                {
                    Title = localizer["Success"],
                    Content = localizer["PartBuiltSuccessfully"],
                    CssClass = "e-toast-success",
                    Timeout = 5000
                }
            },
            {
                ToastKeys.SelectTwoProducts, new ToastModel
                {
                    Title = localizer["Information"],
                    Content = localizer["SelectTwoProducts"],
                    CssClass = "e-toast-info",
                    Timeout = 5000
            }
        },
        };
        }

        public static ToastModel GetToastModel(ToastKeys key)
        {
            if (ToastModels.TryGetValue(key, out var toastModel))
            {
                return toastModel;
            }

            // Return a default ToastModel when the specified key is not found in the dictionary.
            return new ToastModel
            {
                Title = "Default Title",
                Content = "Default Content",
                CssClass = "e-toast-info",
                Timeout = 5000
            };
        }

        //public static ToastModel GetToastModel(ToastKeys key)
        //{
        //    if (ToastModels.ContainsKey(key))
        //    {
        //        return ToastModels[key];
        //    }
        //    else
        //    {
        //        throw new KeyNotFoundException($"The ToastModel with key '{key}' was not found in the dictionary.");
        //    }
        //}
    }
}
