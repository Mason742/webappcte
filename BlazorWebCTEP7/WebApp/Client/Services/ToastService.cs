﻿using Syncfusion.Blazor.Notifications;
using WebApp.Client.Helper;

namespace WebApp.Client.Services
{
    public class ToastService :IToastService
    {
        public event Func<ToastModelHelper.ToastKeys, Task> OnShow;
        public event Func<ToastModel, Task> OnShowCustom;

        public async Task ShowToastAsync(ToastModelHelper.ToastKeys key)
        {
            await OnShow?.Invoke(key);
        }

        public async Task ShowCustomToastAsync(ToastModel toastModel)
        {
            if (OnShowCustom != null)
            {
                await OnShowCustom.Invoke(toastModel);
            }
        }

    }
}
