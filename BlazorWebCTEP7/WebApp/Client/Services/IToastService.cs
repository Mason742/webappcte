﻿using Syncfusion.Blazor.Notifications;
using WebApp.Client.Helper;

namespace WebApp.Client.Services
{
    public interface IToastService
    {
        public event Func<ToastModelHelper.ToastKeys, Task> OnShow;
        public event Func<ToastModel, Task> OnShowCustom;

        public Task ShowToastAsync(ToastModelHelper.ToastKeys key);
        public Task ShowCustomToastAsync(ToastModel toastModel);
    }
}
