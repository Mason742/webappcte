﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApp.Shared.Attributes
{
    public class RequiredCollectionLengthAttribute : ValidationAttribute
    {
        private readonly int _minLength;

        public RequiredCollectionLengthAttribute(int minLength)
        {
            _minLength = minLength;
        }

        public override bool IsValid(object value)
        {
            if (value is ICollection collection)
            {
                return collection.Count >= _minLength;
            }

            return false;
        }
    }
}
