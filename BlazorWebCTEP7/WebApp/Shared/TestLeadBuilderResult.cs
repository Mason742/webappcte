﻿namespace WebApp.Shared;

public class TestLeadBuilderResult
{
    public string PartNumber { get; set; }
    public string ConnectorOne { get; set; }
    public string WireJacket { get; set; }
    public string WireGauge { get; set; }
    public string ConnectorTwo { get; set; }
    public string Length { get; set; }
    public string Color { get; set; }
    public string Voltage { get; set; }
    public string Current { get; set; }
    public string LeadTime { get; set; } = "7 weeks";
    public string MSRP { get; set; }
    public string Diagram { get; set; }
}
