﻿namespace WebApp.Shared.DTOs;

public class StockDTO
{
    public string Model { get; set; }
    public int Quantity { get; set; }
}
