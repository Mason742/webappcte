﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApp.Shared.Attributes;

namespace WebApp.Shared.DTOs
{
    

    public record CmsObject
    {
        //series stuff
        public int SereiesId { get; set; }

        [Required(ErrorMessage = "Field should not be empty")]
        [StringLength(30, ErrorMessage = "{0} length must be between {2} and {1}.", MinimumLength = 3)]
        public string Name { get; set; }

        [Required(ErrorMessage = "Field should not be empty")]
        [StringLength(100, ErrorMessage = "{0} length must be between {2} and {1}.", MinimumLength = 10)]
        public string Title { get; set; }
        public string Summary { get; set; }
        public string SpecificationsLegacy { get; set; }

        [Required(ErrorMessage = "Field should not be empty")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Field should not be empty")]
        public string Brand { get; set; }

        public string ZipFileLink { get; set; }

        [Required(ErrorMessage = "Field should not be empty")]
        public string CategoriesSelection { get; set; }

        public List<SeriesAttachmentsDTO> Attachments { get; set; } = new();
        public List<RelatedSeriesDTO> RelatedSeries { get; set; } = new();
        public string[] RelatedSeriesSelection { get; set; } = new string[] { };

        public string Image1 { get; set; }
        public string Image2 { get; set; }
        public string Image3 { get; set; }
        public string Image4 { get; set; }
        public string Image5 { get; set; }
        public string Image6 { get; set; }
        public string Image7 { get; set; }
        public string Image8 { get; set; }
        public string Image9 { get; set; }

        public string Video1 { get; set; }
        public string Video2 { get; set; }
        public string Video3 { get; set; }

        public int? ReplacementSeriesId { get; set; }

        //product stuff
        [RequiredCollectionLength(1, ErrorMessage = "At least one product selection is required")]
        public List<ProductsDTO> Products { get; set; } = new();

        //shared stuff
        public bool Published { get; set; }
        public bool Discontinued { get; set; }
        public DateTime Modified { get; set; }
        public DateTime Created { get; set; }
    }

    public record ProductsDTO
    {
        public int Id { get; set; }
        public string PartNumber { get; set; }
        public string Description { get; set; }
        public string Warranty { get; set; }
        public decimal? Msrp { get; set; }
        public string TransactionStatus { get; set; }
        public string SpecialHandling { get; set; }
        public string UpcCode { get; set; }
        public string PoLeadTime { get; set; }
        public string ManufacturerLeadTime { get; set; }
        public string HarmonizedCode { get; set; }
        public string CountryOfOrigin { get; set; }
        public decimal? RepairPrice { get; set; }
        public decimal? EvaluationCharge { get; set; }
        public decimal? CalibrationPrice { get; set; }
        public string ShippingCost { get; set; }
        public string MasterPartNumber { get; set; }
        public string PacksPerCarton { get; set; }
        public string PackSize { get; set; }
        public string PackUom { get; set; }
        public string Image1 { get; set; }
        public string Image2 { get; set; }
        public string Image3 { get; set; }
        public string Image4 { get; set; }
        public string Image5 { get; set; }
        public string Image6 { get; set; }
        public string Image7 { get; set; }
        public string Image8 { get; set; }
        public string Image9 { get; set; }

        public bool Published { get; set; }
        public bool Discontinued { get; set; }
        public bool RoHsCompliant { get; set; }


        public DateTime Modified { get; set; }
        public DateTime Created { get; set; }

        public List<ProductImageDTO> ProductImages { get; set; }
    }

    public record ProductImageDTO
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public string ImageFileLink { get; set; }
        public string ImageFileLinkLg { get; set; }
        public string ImageFileLinkMd { get; set; }
        public string ImageFileLinkTmb { get; set; }
        public int Position { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
    }

    public record SeriesAttachmentsDTO
    {
        public int Id { get; set; }
        public int SeriesId { get; set; }
        public string AttachmentFileLink { get; set; }
        public string Type { get; set; }
        public string LanguageCode { get; set; }
        public string CmsAction { get; set; }
        public DateTime Modified { get; set; }
        public DateTime Created { get; set; }
    }

    public record RelatedSeriesDTO
    {
        public int Id { get; set; }
        public int? RelatedSeriesId { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string MainImage { get; set; }
    }

    public record CategoriesDTO
    {
        public int Id { get; set; }
        public string TableName { get; set; }
    }
}
