﻿namespace WebApp.Shared.DTOs;

public class ProductIndexPageDTO
{
    public int TotalCount { get; set; }
    public List<Category> Caterogies { get; set; }
    public class Category
    {
        public string PartNumber { get; set; }
        public string GroupName { get; set; } 
        public string TableName { get; set; } 
        public string DisplayName { get; set; } 
        public string ImageFileLink { get; set; } 
        public string TypeName { get; set; }
        public int ProductCount { get; set; } = 0;
        public string[] ProductsFound { get; set; } 


    }

}
