﻿namespace WebApp.Shared.DTOs;

public class CompanyInfoDTO
{
    public string CompanyName { get; set; }
    public string CustomerId { get; set; }

}
