﻿namespace WebApp.Shared.DTOs;

public class AvGrabbersandHooksCtDTO : CompiledSelectors<AvGrabbersandHooksCtDTO>
{
    public string ProductId { get; set; }
    public string MaxCurrent { get; set; }
    public string MaxResistance { get; set; }
    public string RoHs { get; set; }
    public string Temperature { get; set; }
    public string Color { get; set; }
    public string Od { get; set; }
    public string MaxVoltage { get; set; }
    public string Dimension { get; set; }
    public string Type { get; set; }


    public string Warranty { get; set; }
    public string SpecialHandling { get; set; }
    public string UpcCode { get; set; }
    public string HarmonizedCode { get; set; }
    public string CountryOfOrigin { get; set; }
    public string Discontinued { get; set; }
    public string RoHsCompliant { get; set; }

}
