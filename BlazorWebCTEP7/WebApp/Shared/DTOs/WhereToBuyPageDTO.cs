﻿namespace WebApp.Shared.DTOs;

public class WhereToBuyPageDTO
{
    public List<AuthorizedDistributorDTO> authorizedDistributorDTOs { get; set; }
    public List<string> whereToBuySearchList{ get; set; }
}
