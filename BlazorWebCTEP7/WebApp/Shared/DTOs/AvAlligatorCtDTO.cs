﻿namespace WebApp.Shared.DTOs;

public class AvAlligatorCtDTO : CompiledSelectors<AvAlligatorCtDTO>
{
    [SyncColumn(filterable:false,visible:false)]
    public string ProductId { get; set; }
    public string Iec { get; set; }
    public string MaxCurrent { get; set; }
    public string RoHs { get; set; }

    [SyncColumn(filterable: false, visible: true)]
    public string Temperature { get; set; }
    public string Color { get; set; }
    public string Voltage { get; set; }
    public string Materials { get; set; }
    public string Type { get; set; }
    public string Type1 { get; set; }

    public string MaxResistance { get; set; }
    public string JawOpening { get; set; }
    public string MaxConductorDiameter { get; set; }


    public string Warranty { get; set; }
    public string SpecialHandling { get; set; }
    public string UpcCode { get; set; }
    public string HarmonizedCode { get; set; }
    public string CountryOfOrigin { get; set; }

    [SyncColumn(filterable: false, visible: false)]
    public string Discontinued { get; set; }
    public string RoHsCompliant { get; set; }

}
