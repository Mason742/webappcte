﻿namespace WebApp.Shared.DTOs;

public class AvOscilloscopePassiveVoltageCtDTO : CompiledSelectors<AvOscilloscopePassiveVoltageCtDTO>
{
    public string ProductId { get; set; }
    public string Attenuation { get; set; }
    public string Bandwidth { get; set; }
    public string CableLength { get; set; }
    public string Compensation { get; set; }
    public string GroundBarrelDiameter { get; set; }
    public string Humidity { get; set; }
    public string Impedance { get; set; }
    public string InputCapacitance { get; set; }
    public string InputImpedance { get; set; }
    public string MaxVoltage { get; set; }
    public string OutputImpedance { get; set; }
    public string ReadoutActuator { get; set; }
    public string RiseTime { get; set; }
    public string RoHs { get; set; }
    public string Series { get; set; }
    public string Temperature { get; set; }
    public string Type { get; set; }
    public string Type1 { get; set; }

    public string Capacitance { get; set; }
    public string Length { get; set; }
    public string OutputVoltageSourceImpedance { get; set; }
    public string AttenuationAccuracy { get; set; }
    public string Iec { get; set; }
    public string OperatingTemperature { get; set; }
    public string VoltageCoefficient { get; set; }
    public string CompensationRange { get; set; }
    public string TempCoefficient { get; set; }


    public string Warranty { get; set; }
    public string SpecialHandling { get; set; }
    public string UpcCode { get; set; }
    public string HarmonizedCode { get; set; }
    public string CountryOfOrigin { get; set; }
    public string Discontinued { get; set; }
    public string RoHsCompliant { get; set; }

}
