﻿namespace WebApp.Shared.DTOs;


public class RmaFormDTO
{
    [Required(ErrorMessage = "Field should not be empty")]
    public string FirstName { get; set; }
    [Required(ErrorMessage = "Field should not be empty")]
    public string LastName { get; set; }

    [Required(ErrorMessage = "Field should not be empty")]
    [DataType(DataType.EmailAddress)]
    [EmailAddress]
    public string Email { get; set; }

    public string Company { get; set; }

    [DataType(DataType.PhoneNumber)]
    [Phone]
    [Required(ErrorMessage = "Field should not be empty")]
    public string Phone { get; set; }

    public string PhoneExt { get; set; }

    [Required(ErrorMessage = "Field should not be empty")]
    public string Address1 { get; set; }

    public string Address2 { get; set; }

    [Required(ErrorMessage = "Field should not be empty")]
    public string City { get; set; }

    [Required(ErrorMessage = "Field should not be empty")]
    public string StateOrProvince { get; set; }

    [Required(ErrorMessage = "Field should not be empty")]
    public string PostalCode { get; set; }

    [Required(ErrorMessage = "Field should not be empty")]
    public string Country { get; set; }
    [Required(ErrorMessage = "Field should not be empty")]
    public string PartNumber { get; set; }

    [Required(ErrorMessage = "Field should not be empty")]
    public string RepairOptionChoice { get; set; }

    [Required(ErrorMessage = "Field should not be empty")]
    public string CalibrationServiceChoice { get; set; }

    [Required(ErrorMessage = "Field should not be empty")]
    public string SerialNumber { get; set; }

    [Required(ErrorMessage = "Field should not be empty")]
    [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than 0")]
    public int Quantity { get; set; }

    public string ProofOfPurchaseLink { get; set; }

    //[Required(ErrorMessage = "Field should not be empty")]
    public string RmaProductImageLink { get; set; }

    public string Brand { get; set; }

    [Required]
    [StringLength(1024, ErrorMessage = "{0} length must be between {2} and {1} characters.", MinimumLength = 10)]
    public string ProblemDescription { get; set; }


    public void Clear()
    {
        FirstName = default;
        LastName = default;
        Email = default;
        Company = default;
        Phone = default;
        PhoneExt = default;
        Address1 = default;
        Address2 = default;
        City = default;
        StateOrProvince = default;
        PostalCode = default;
        Country = default;
        PartNumber = default;
        RepairOptionChoice = default;
        CalibrationServiceChoice = default;
        SerialNumber = default;
        Quantity = default;
        ProofOfPurchaseLink = default;
        Brand = default;
        ProblemDescription = default;
    }
}
