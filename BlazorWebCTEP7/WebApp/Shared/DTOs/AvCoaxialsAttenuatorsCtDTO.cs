﻿namespace WebApp.Shared.DTOs;

public class AvCoaxialsAttenuatorsCtDTO : CompiledSelectors<AvCoaxialsAttenuatorsCtDTO>
{
    public string ProductId { get; set; }
    public string Accuracy { get; set; }
    public string Attenuation { get; set; }
    public string AttenuationVoltage { get; set; }
    public string AveragePower { get; set; }
    public string FrequencyRange { get; set; }
    public string Impedance { get; set; }
    public string PeakPower { get; set; }
    public string RoHs { get; set; }
    public string TempCoefficient { get; set; }
    public string Temperature { get; set; }
    public string Vswr { get; set; }
    public string Length { get; set; }
    public string ConnectorGrade { get; set; }
    public string AttenuationAccuracy { get; set; }
    public string MaxVoltage { get; set; }
    public string PollutionDegree { get; set; }
    public string RelativeHumidity { get; set; }
    public string Safety { get; set; }
    public string SafetySpecification { get; set; }
    public string Bandwidth { get; set; }
    public string Type { get; set; }


    public string Warranty { get; set; }
    public string SpecialHandling { get; set; }
    public string UpcCode { get; set; }
    public string HarmonizedCode { get; set; }
    public string CountryOfOrigin { get; set; }
    public string Discontinued { get; set; }
    public string RoHsCompliant { get; set; }

}
