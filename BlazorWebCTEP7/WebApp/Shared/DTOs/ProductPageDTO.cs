﻿using System.Globalization;

namespace WebApp.Shared.DTOs;

public class ProductPageDTO
{
    public string SelectedPartNumber { get; set; }
    public string SeriesName { get; set; }
    public string Title { get; set; }
    //public string Summary { get; set; }
    public string SpecificationsLegacy { get; set; }
    public string Overview { get; set; }
    public bool New { get; set; } = false;
    public bool Discontinued { get; set; }
    public string GroupName { get; set; }
    public string CategoryName { get; set; }
    public string CategoryTableName { get; set; }
    public List<ProductDTO> Products { get; set; }
    public List<Attachment> Documents { get; set; }
    public List<Attachment> Softwares { get; set; }
    public List<Attachment> Videos { get; set; }
    public List<ImageGalleryItem> Images { get; set; }
    public ImageGalleryItem MainImage { get; set; }

    public string Warranty { get; set; }
    public decimal? Msrp { get; set; }

    public string FormattedMsrp
    {
        get
        {
            if (Msrp.HasValue)
            {
                NumberFormatInfo nfi = new NumberFormatInfo();
                nfi.CurrencySymbol = "$";
                nfi.NumberDecimalDigits = 2;
                return Msrp.Value.ToString("C", nfi);
            }
            else
            {
                return null;
            }
        }
    }
    public List<RelatedProductDTO> RelatedProducts { get; set; }

    public ReplacementSeriesDTO ReplacementSeries { get; set; }


    public class RelatedProductDTO
    {
        public string ProductName { get; set; }
        public string Title { get; set; }
        public string MainImage { get; set; }
    }

    public class ReplacementSeriesDTO
    {
        public string Name { get; set; }
        public string Title { get; set; }
        public string MainImage { get; set; }
    }

    public class Attachment
    {
        public string AttachmentFileLink { get; set; }
        public string Type { get; set; }

        public string GetFileNameWithExtension()
        {
            // Get the last index of the '/' character in the AttachmentFileLink string
            int lastSlashIndex = AttachmentFileLink.LastIndexOf('/');

            // Return the substring starting from the index after the last '/' character, which should be the file name with extension
            return AttachmentFileLink.Substring(lastSlashIndex + 1);
        }
    }

    public class ProductDTO
    {
        public string PartNumber { get; set; }
        public string Description { get; set; }
        public bool Catalog { get; set; } = false;

        public decimal? Msrp { get; set; }
        public string FormattedMsrp
        {
            get
            {
                if (Msrp.HasValue)
                {
                    NumberFormatInfo nfi = new NumberFormatInfo();
                    nfi.CurrencySymbol = "$";
                    nfi.NumberDecimalDigits = 2;
                    return Msrp.Value.ToString("C", nfi);
                }
                else
                {
                    return null;
                }
            }
        }
        public string Warranty { get; set; }

    }
}
