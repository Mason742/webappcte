﻿namespace WebApp.Shared.DTOs;

public class WhereToBuyStockDTO
{
    public string PartNumber { get; set; }
    public string DistributorName { get; set; }
    public int Quantity { get; set; }
    public string WebsiteLink { get; set; }

}
