﻿namespace WebApp.Shared.DTOs;

public class CategoryPageDTO<T>
{
    public JumbotronContent CategoryJumbotron { get; set; }
    public List<T> Data { get; set; }
    
}
public class JumbotronContent
{
    public string Title { get; set; }
    public string Subtitle { get; set; }
    public string ImageLink { get; set; }
}
