﻿namespace WebApp.Shared.DTOs;

public class TestLeadBuilderQuoteFormDTO
{
    [Required(ErrorMessage = "Field should not be empty")]
    public string FirstName { get; set; }
    [Required(ErrorMessage = "Field should not be empty")]
    public string LastName { get; set; }

    [Required(ErrorMessage = "Field should not be empty")]
    [DataType(DataType.EmailAddress)]
    [EmailAddress]
    public string Email { get; set; }

    public string Company { get; set; }

    [DataType(DataType.PhoneNumber)]
    [Phone]
    [Required(ErrorMessage = "Field should not be empty")]
    public string Phone { get; set; }

    public string PhoneExt { get; set; }

    [Required(ErrorMessage = "Field should not be empty")]
    public string Address1 { get; set; }

    public string Address2 { get; set; }

    [Required(ErrorMessage = "Field should not be empty")]
    public string City { get; set; }

    [Required(ErrorMessage = "Field should not be empty")]
    public string StateOrProvince { get; set; }

    [Required(ErrorMessage = "Field should not be empty")]
    public string PostalCode { get; set; }

    [Required(ErrorMessage = "Field should not be empty")]
    public string Country { get; set; }

    [Required(ErrorMessage = "Field should not be empty")]
    [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than 0")]
    public int Quantity { get; set; }


    [Required]
    [StringLength(1024, ErrorMessage = "{0} length must be between {2} and {1}.", MinimumLength = 10)]
    public string Message { get; set; }

    public TestLeadBuilderResult TestLeadBuilderResult { get; set; }

    [Required]
    public string Brand { get; set; }


    public void Clear()
    {
        FirstName = default(string);
        LastName = default(string);
        Email = default(string);
        Company = default(string);
        Phone = default(string);
        PhoneExt = default(string);
        Address1 = default(string);
        Address2 = default(string);
        City = default(string);
        StateOrProvince = default(string);
        PostalCode = default(string);
        Country = default(string);
        Quantity = default(int);
        Message = default(string);
        TestLeadBuilderResult = null;
    }
}
