﻿namespace WebApp.Shared.DTOs;

public class VBaseProductListDTO : CompiledSelectors<VBaseProductListDTO>
{
    public string ProductId { get; set; }
    public string Warranty { get; set; }
    public string SpecialHandling { get; set; }
    public string UpcCode { get; set; }
    public string HarmonizedCode { get; set; }
    public string CountryOfOrigin { get; set; }
    public string Discontinued { get; set; }
    public string RoHsCompliant { get; set; }

}
