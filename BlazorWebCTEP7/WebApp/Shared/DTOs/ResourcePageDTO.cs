﻿namespace WebApp.Shared.DTOs;

public class ResourcePageDTO
{
    public int CurrentPage { get; set; }
    public int TotalPages { get; set; }
    public int PageSize { get; set; }
    public int TotalCount { get; set; }
    public bool HasPrevious { get; set; }
    public bool HasNext { get; set; }

    public List<ResourceDTO> Records { get; set; }

    public Dictionary<string, int> Types { get; set; } = new();
}
