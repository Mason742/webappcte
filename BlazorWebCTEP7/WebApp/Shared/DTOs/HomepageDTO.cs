﻿namespace WebApp.Shared.DTOs;

public class HomepageDTO
{
    public string Type { get; set; }
    public string Title { get; set; }
    public string Subtitle { get; set; }
    public string Summary { get; set; }
    public string MediaFileLink { get; set; }
    public string Url { get; set; }
}
