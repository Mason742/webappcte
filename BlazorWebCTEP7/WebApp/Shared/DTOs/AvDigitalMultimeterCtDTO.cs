﻿namespace WebApp.Shared.DTOs;

public class AvDigitalMultimeterCtDTO : CompiledSelectors<AvDigitalMultimeterCtDTO>
{
    public string ProductId { get; set; }
    public string Iec { get; set; }
    public string MaxCurrent { get; set; }
    public string RoHs { get; set; }
    public string Temperature { get; set; }
    public string InterruptingRating { get; set; }
    public string MaxVoltage { get; set; }
    public string Color { get; set; }
    public string MaxResistance { get; set; }
    public string Impedance { get; set; }
    public string MeterImpedance { get; set; }
    public string TempCoefficient { get; set; }
    public string Dimension { get; set; }
    public string Voltage { get; set; }
    public string Type { get; set; }


    public string Warranty { get; set; }
    public string SpecialHandling { get; set; }
    public string UpcCode { get; set; }
    public string HarmonizedCode { get; set; }
    public string CountryOfOrigin { get; set; }
    public string Discontinued { get; set; }
    public string RoHsCompliant { get; set; }

}
