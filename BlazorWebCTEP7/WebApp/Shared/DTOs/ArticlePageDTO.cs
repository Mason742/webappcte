﻿namespace WebApp.Shared.DTOs;

public class ArticlePageDTO
{
    public string Title { get; set; }
    public string BodyHtml { get; set; }
    public DateTime Created { get; set; }
    public List<string> RelatedSeriesNames { get; set; }
}
