﻿namespace WebApp.Shared.DTOs;

public class AuthorizedDistributorDTO
{
    public string LogoLink { get; set; }
    public string Country { get; set; }
    public string PhoneNumber { get; set; }
    public string Website { get; set; }
}
