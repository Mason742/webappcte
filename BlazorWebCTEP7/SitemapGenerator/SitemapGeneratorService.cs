﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using WebDB.Data.Context;
using WebDB.Data.Models;

namespace SitemapGenerator
{
    public class SitemapGeneratorService : BackgroundService
    {
        private readonly IHostApplicationLifetime _applicationLifetime;
        private readonly IServiceScopeFactory _serviceScopeFactory;

        public SitemapGeneratorService(IServiceScopeFactory serviceScopeFactory, IHostApplicationLifetime applicationLifetime)
        {
            _serviceScopeFactory = serviceScopeFactory;
            _applicationLifetime = applicationLifetime;
            // Create DbContext instance and store it in a private field

        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            // Your core logic for the background service
            await GenerateSitemap();
        }

        public async Task GenerateSitemap()
        {
            List<ResourcesView> resources = new();

            try
            {
                using (var scope = _serviceScopeFactory.CreateScope())
                {
                    var _dbContext = scope.ServiceProvider.GetRequiredService<DefaultDbContext>();
                    resources = await _dbContext.ResourcesViews
                        .Where(x => x.Published == true
                    && !x.UrlLink.StartsWith("https://www.youtube.com/")
                    && !x.UrlLink.StartsWith("https://as.caltestelectronics.com/")
                    && x.ResourceType != "External").ToListAsync();
                }



                List<SitemapItem> sitemapItems = new List<SitemapItem>();
                sitemapItems.AddRange(AddDefaultSitemapItems());

                foreach (var resource in resources)
                {
                    SitemapItem item = new SitemapItem
                    {
                        Location = GetAbsoluteResourceLink(resource),
                        LastModified = resource.Created,
                        ChangeFrequency = "monthly",
                        Brand = resource.Brand
                    };

                    switch (resource.ResourceType)
                    {
                        case "Group":
                            item.Priority = 0.9;
                            break;
                        case "Category":
                            item.Priority = 0.8;
                            break;
                        case "Product":
                            item.Priority = 0.7;
                            item.ChangeFrequency = "weekly";
                            break;
                        case "Article":
                            item.Priority = 0.4;
                            item.ChangeFrequency = "weekly";
                            break;
                        case "Page":
                            item.Priority = 0.6;
                            break;
                        case "Company Document":
                            item.Priority = 0.4;
                            item.ChangeFrequency = "weekly";
                            break;
                        case "Marketing Release":
                            item.Priority = 0.5;
                            item.ChangeFrequency = "weekly";
                            break;
                        default:
                            item.Priority = 0.3;
                            break;
                    }

                    sitemapItems.Add(item);
                }

                var cTEsitemapItems = sitemapItems.Where(x => x.Brand == "CT").ToList();
                var gSSitemapItems = sitemapItems.Where(x => x.Brand == "GS").ToList();

                string wwwrootPath = Path.Combine("..", "..", "..", "..", "CTEWebApp", "Client", "wwwroot");
                string sitemapPathCT = Path.Combine(wwwrootPath, "sitemap-ct.xml");
                string sitemapPathGS = Path.Combine(wwwrootPath, "sitemap-gs.xml");
                Console.WriteLine(sitemapPathCT);
                cTEsitemapItems.WriteSitemap(sitemapPathCT);
                gSSitemapItems.WriteSitemap(sitemapPathGS);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine("Closing...");
            _applicationLifetime.StopApplication();
        }

        private IEnumerable<SitemapItem> AddDefaultSitemapItems()
        {
            return new SitemapItem[]
            {
                new SitemapItem()
                {
                    Location="https://www.caltestelectronics.com/",
                    LastModified=DateTime.Now,
                    ChangeFrequency="monthly",
                    Priority=1.0,
                    Brand="CT",
                },
                new SitemapItem()
                {
                    Location="https://www.globalspecialties.com/",
                    LastModified=DateTime.Now,
                    ChangeFrequency="monthly",
                    Priority=1.0,
                    Brand="GS",
                },
                new SitemapItem()
                {
                    Location="https://www.caltestelectronics.com/products",
                    LastModified=DateTime.Now,
                    ChangeFrequency="monthly",
                    Priority=0.9,
                    Brand="CT",
                },
                new SitemapItem()
                {
                    Location="https://www.globalspecialties.com/products",
                    LastModified=DateTime.Now,
                    ChangeFrequency="monthly",
                    Priority=0.9,
                    Brand="GS",
                },
                new SitemapItem()
                {
                    Location="https://www.caltestelectronics.com/where-to-buy",
                    LastModified=DateTime.Now,
                    ChangeFrequency="monthly",
                    Priority=0.9,
                    Brand="CT",
                },
                new SitemapItem()
                {
                    Location="https://www.globalspecialties.com/where-to-buy",
                    LastModified=DateTime.Now,
                    ChangeFrequency="monthly",
                    Priority=0.9,
                    Brand="GS",
                },
                new SitemapItem()
                {
                    Location="https://www.caltestelectronics.com/support",
                    LastModified=DateTime.Now,
                    ChangeFrequency="monthly",
                    Priority=0.9,
                    Brand="CT",
                },
                new SitemapItem()
                {
                    Location="https://www.globalspecialties.com/support",
                    LastModified=DateTime.Now,
                    ChangeFrequency="monthly",
                    Priority=0.9,
                    Brand="GS",
                },
                new SitemapItem()
                {
                    Location="https://www.caltestelectronics.com/resources",
                    LastModified=DateTime.Now,
                    ChangeFrequency="monthly",
                    Priority=0.8,
                    Brand="CT",
                },
                new SitemapItem()
                {
                    Location="https://www.globalspecialties.com/resources",
                    LastModified=DateTime.Now,
                    ChangeFrequency="monthly",
                    Priority=0.8,
                    Brand="GS",
                },
                new SitemapItem()
                {
                    Location="https://www.globalspecialties.com/search",
                    LastModified=DateTime.Now,
                    ChangeFrequency="monthly",
                    Priority=0.8,
                    Brand="CT",
                },
                new SitemapItem()
                {
                    Location="https://www.globalspecialties.com/search",
                    LastModified=DateTime.Now,
                    ChangeFrequency="monthly",
                    Priority=0.8,
                    Brand="GS",
                }
            };
        }

        private string GetAbsoluteResourceLink(ResourcesView resource)
        {
            string url = string.Empty;
            string prefix;
            if (resource.Brand.Contains("CT"))
            {
                prefix = "https://www.caltestelectronics.com/";
            }
            else if (resource.Brand == "GS")
            {
                prefix = "https://www.globalspecialties.com/";
            }
            else
            {
                throw new NotImplementedException();
            }

            if (resource.ResourceType == "Group")
            {
                url = prefix + resource.UrlLink + "#" + Uri.EscapeDataString(resource.Title);
            }
            else if (resource.ResourceType == "Category")
            {
                url = prefix + resource.UrlLink;
                int lastSlashIndex = url.LastIndexOf('/');
                if (lastSlashIndex >= 0)
                {
                    int secondLastSlashIndex = url.LastIndexOf('/', lastSlashIndex - 1);
                    if (secondLastSlashIndex >= 0)
                    {
                        string start = url.Substring(0, secondLastSlashIndex + 1);
                        string end = url.Substring(lastSlashIndex);
                        string middle = url.Substring(secondLastSlashIndex + 1, lastSlashIndex - secondLastSlashIndex - 1);
                        string escapedMiddle = Uri.EscapeDataString(middle);
                        string result = start + escapedMiddle + end;
                        url = result;
                        Console.WriteLine(result);
                    }
                }
            }
            else if (resource.ResourceType == "Product")
            {
                url = prefix + resource.UrlLink;
            }
            else if (resource.ResourceType == "Company Document")
            {
                url = prefix + resource.UrlLink;
            }
            else if (resource.ResourceType == "Article")
            {
                url = prefix + resource.UrlLink;
            }
            else if (resource.ResourceType == "Page")
            {
                url = prefix + resource.UrlLink;
            }
            else
            {
                url = resource.UrlLink;
            }
            return url;
        }
    }
}
