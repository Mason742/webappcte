﻿// See https://aka.ms/new-console-template for more information
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SitemapGenerator;
using WebDB.Data.Context;

Console.WriteLine("Hello, World!");


var host = Host.CreateDefaultBuilder(args)
        .ConfigureAppConfiguration((hostContext, config) =>
        {
            var environmentName = hostContext.HostingEnvironment.EnvironmentName;
            Console.WriteLine(environmentName);
            config.SetBasePath(Directory.GetCurrentDirectory());
            config.AddJsonFile("../../../../CteCore/appsettings.json", optional: true);
            config.AddJsonFile($"../../../../CteCore/appsettings.{environmentName}.json", optional: true, reloadOnChange: true);
            
            config.AddJsonFile("appsettings.json", optional: true);
            config.AddJsonFile($"appsettings.{environmentName}.json", optional: true, reloadOnChange: true);
            config.AddEnvironmentVariables();
            config.AddCommandLine(args);
        })
        .ConfigureServices((hostContext, services) =>
        {
            services.AddDbContext<DefaultDbContext>(options =>
            {
                options.UseSqlServer(hostContext.Configuration.GetConnectionString("DefaultConnection"));
                if (hostContext.HostingEnvironment.IsDevelopment())
                {
                    options.EnableSensitiveDataLogging();
                }
            });

            services.AddHostedService<SitemapGeneratorService>();
        }).Build();


await host.RunAsync();



