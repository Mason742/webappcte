﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using WebDB.Data.Models;

namespace SitemapGenerator
{
    public class SitemapItem
    {
        public string Location { get; set; }
        public DateTime? LastModified { get; set; }
        public string ChangeFrequency { get; set; }
        public double? Priority { get; set; }
        public string Brand { get; set; }
    }

    public static class SitemapExtensions
    {
        public static void WriteSitemap(this IList<SitemapItem> items, string filePath)
        {
            using var writer = XmlWriter.Create(filePath, new XmlWriterSettings { Indent = true });

            writer.WriteStartDocument();
            writer.WriteStartElement("urlset", "http://www.sitemaps.org/schemas/sitemap/0.9");

            foreach (var item in items)
            {
                writer.WriteStartElement("url");
                writer.WriteElementString("loc", item.Location);

                if (item.LastModified.HasValue)
                {
                    writer.WriteElementString("lastmod", item.LastModified.Value.ToString("yyyy-MM-dd"));
                }

                if (!string.IsNullOrEmpty(item.ChangeFrequency))
                {
                    writer.WriteElementString("changefreq", item.ChangeFrequency);
                }

                if (item.Priority.HasValue)
                {
                    writer.WriteElementString("priority", item.Priority.Value.ToString("F1"));
                }

                writer.WriteEndElement();
            }

            writer.WriteEndElement();
            writer.WriteEndDocument();
        }
    }
}
