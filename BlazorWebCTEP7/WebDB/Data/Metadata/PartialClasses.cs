﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebDB.Data.Models
{
    [MetadataType(typeof(IArticleMetadata))]
    public partial class Article : IEntityBase
    {

    }
   
    [MetadataType(typeof(IArticleSeriesMetadata))]
    public partial class ArticleSeries : IEntityBase
    {

    }
   
    [MetadataType(typeof(IAuthorizedDistributorMetadata))]
    public partial class AuthorizedDistributor : IEntityBase
    {

    }
   
    [MetadataType(typeof(IBkGsStockMetadata))]
    public partial class BkGsStock : IEntityBase
    {

    }
 
    [MetadataType(typeof(IBrandCrossMetadata))]
    public partial class BrandCross : IEntityBase
    {

    }
   
    [MetadataType(typeof(ICategoryMetadata))]
    public partial class Category : IEntityBase
    {

    }
  
    [MetadataType(typeof(ICategoryGroupMetadata))]
    public partial class CategoryGroup : IEntityBase
    {

    }

    [MetadataType(typeof(ICompanyDocumentMetadata))]
    public partial class CompanyDocument : IEntityBase
    {

    }

    [MetadataType(typeof(IDistributorAuthorizedDomainMetadata))]
    public partial class DistributorAuthorizedDomain : IEntityBase
    {

    }

    [MetadataType(typeof(IDistributorStockMetadata))]
    public partial class DistributorStock : IEntityBase
    {

    }
 
    [MetadataType(typeof(IErpInventoryMetadata))]
    public partial class ErpInventory : IEntityBase
    {

    }

    [MetadataType(typeof(IGroupMetadata))]
    public partial class Group : IEntityBase
    {

    }

    [MetadataType(typeof(IHomepageMetadata))]
    public partial class Homepage : IEntityBase
    {

    }

    [MetadataType(typeof(IPressReleaseMetadata))]
    public partial class PressRelease : IEntityBase
    {

    }

    [MetadataType(typeof(IProductMetadata))]
    public partial class Product : IEntityBase
    {

    }

    [MetadataType(typeof(ISeriesMetadata))]
    public partial class Series : IEntityBase
    { 

    }


    [MetadataType(typeof(ISeriesAttachmentMetadata))]
    public partial class SeriesAttachment : IEntityBase
    {

    }


    [MetadataType(typeof(ISeriesRelatedMetadata))]
    public partial class SeriesRelated : IEntityBase
    { 

    }

    [MetadataType(typeof(ITestLeadBuilderMetadata))]
    public partial class TestLeadBuilder : IEntityBase
    {

    }

    [MetadataType(typeof(ITestLeadColorMetadata))]
    public partial class TestLeadColor : IEntityBase
    {

    }

    [MetadataType(typeof(ITestLeadConnectorMetadata))]
    public partial class TestLeadConnector : IEntityBase
    {

    }

    [MetadataType(typeof(ITestLeadLengthMetadata))]
    public partial class TestLeadLength : IEntityBase
    {

    }

    [MetadataType(typeof(ITestLeadWireMetadata))]
    public partial class TestLeadWire : IEntityBase
    {

    }

    public partial class ContactForm : IEntityBase
    {

    }

    public partial class TestLeadBuilderQuoteForm : IEntityBase
    {

    }

    public partial class RmaForm : IEntityBase
    {

    }

    public partial class NewsletterSignup : IEntityBase
    {

    }
}

