﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebDB.Data.Models
{


    public interface IArticleMetadata
    {
        [Required]
        [RegularExpression(@"[a-zA-Z0-9-]+$",
        ErrorMessage = "Only alphanumeric and - characters allowed in a Url")]
        public string Url { get; set; }
       
        [Required]
        public string Title { get; set; }
       
        [Required]
        [StringLength(50)]
        public string Summary { get; set; }
       
        [Required]
        public string BodyHtml { get; set; }
        
        [Required]
        [StringLength(6)]
        public string Brand { get; set; }

        [Required]
        public DateTime Modified { get; set; }

        [Required]
        public DateTime Created { get; set; }

    }

    public interface IArticleRelatedMetadata
    {
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
        public int ArticleId { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
        public int RelatedArticleId { get; set; }

    }

    public interface IArticleSeriesMetadata
    {

        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
        public int ArticleId { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
        public int SeriesId { get; set; }

    }

    public interface IAuthorizedDistributorMetadata
    {
        [Required]
        public string CustomerId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Country { get; set; }

        [Required]
        public string Phone { get; set; }

        [Required]
        public string Contact { get; set; }

        [Required]
        [RegularExpression(MetadataHelper.SimpleHttpLinkRegex, ErrorMessage = "Enter a valid Url")]
        public string WebsiteUrl { get; set; }

        [RegularExpression(MetadataHelper.SimpleHttpLinkRegex, ErrorMessage = "Enter a valid Url")]
        public string WebsiteUrlGs { get; set; }


        [Required]
        [RegularExpression(MetadataHelper.SimpleHttpLinkRegex, ErrorMessage = "Enter a valid Url")]
        public string LogoImageFileLink { get; set; }

        [Required]
        public string AccountType { get; set; }

        [Required]
        [StringLength(6)]
        public string Brand { get; set; }
        [Required]
        public DateTime Modified { get; set; }

        [Required]
        public DateTime Created { get; set; }
    }

    public interface IBkGsStockMetadata
    {
        [Required]
        public string PartNumber { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
        public int Quantity { get; set; }
        public DateTime Created { get; set; }

    }


    public interface IBrandCrossMetadata
    {
        [Required]
        public string Manufacturer { get; set; }

        [Required]
        public string CompetitorPartNumber { get; set; }

        [Required]
        public string PartNumber { get; set; }

        [Required]
        [StringLength(1)]
        public string Code { get; set; }

        [Required]
        public DateTime Modified { get; set; }

        [Required]
        public DateTime Created { get; set; }
    }

    public interface ICategoryMetadata
    {
        [Required]
        public string TableName { get; set; }
        [Required]
        public string DisplayName { get; set; }

        [Required]
        [RegularExpression(MetadataHelper.SimpleHttpLinkRegex, ErrorMessage = "Enter a valid Url")]
        public string ImageFileLink { get; set; }
        [Required]
        [StringLength(6)]
        public string Brand { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime Modified { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime Created { get; set; }

    }

    public interface ICategoryGroupMetadata
    {
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
        public int CategoryId { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
        public int GroupId { get; set; }

    }

    public interface ICompanyDocumentMetadata
    {
        [Required]
        public string Title { get; set; }

        [Required]
        [RegularExpression(MetadataHelper.SimpleHttpLinkRegex, ErrorMessage = "Enter a valid Url")]
        public string FileLink { get; set; }

        [Required]
        [StringLength(6)]
        public string Brand { get; set; }

        [Required]
        public DateTime Modified { get; set; }

        [Required]
        public DateTime Created { get; set; }

    }

    public interface IDistributorAuthorizedDomainMetadata
    {
        [Required]
        public string CustomerId { get; set; }
        [Required]
        public string CompanyName { get; set; }

        [Required]
        [RegularExpression(MetadataHelper.TopLevelDomainRegex,
        ErrorMessage = "Only top level domains are allowed. E.g. google.com")]
        public string Domain { get; set; }

        [Required]
        public DateTime Modified { get; set; }

        [Required]
        public DateTime Created { get; set; }

    }

    public interface IDistributorStockMetadata
    {
        [Required]
        public string CustomerId { get; set; }

        [Required]
        public string PartNumber { get; set; }

        [Required]
        [RegularExpression(MetadataHelper.SimpleHttpLinkRegex, ErrorMessage = "Enter a valid Url")]
        public string WebsiteUrl { get; set; }

        [Required]
        public DateTime Modified { get; set; }

        [Required]
        public DateTime Created { get; set; }

    }

    public interface IErpInventoryMetadata
    {
        [Required]
        public string PartNumber { get; set; }

        [Required]
        public string SpecialHandling { get; set; }

        [Required]
        [StringLength(6)]
        public string Brand { get; set; }

        [Required]
        public DateTime Created { get; set; }

    }

    public interface IGroupMetadata
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        [RegularExpression(MetadataHelper.SimpleHttpLinkRegex, ErrorMessage = "Enter a valid Url")]
        public string ImageFileLink { get; set; }
        [Required]
        [StringLength(6)]
        public string Brand { get; set; }

        [Required]
        public DateTime Modified { get; set; }

        [Required]
        public DateTime Created { get; set; }

    }

    public interface IHomepageMetadata
    {
        [Required]
        public string Type { get; set; }
        [Required]
        [StringLength(25, MinimumLength = MetadataHelper.DefaultMinStringLength)]
        public string Title { get; set; }

        [Required]
        [StringLength(75, MinimumLength = MetadataHelper.DefaultMinStringLength)]
        public string Subtitle { get; set; }

        [Required]
        [StringLength(600,MinimumLength = 100)]
        public string Summary { get; set; }

        [Required]
        [RegularExpression(MetadataHelper.SimpleHttpLinkRegex, ErrorMessage = "Enter a valid Url")]
        public string MediaFileLink { get; set; }

        [Required]
        [RegularExpression(MetadataHelper.SimpleHttpLinkRegex, ErrorMessage ="Enter a valid Url")]
        public string Url { get; set; }

        [Required]
        [StringLength(6)]
        public string Brand { get; set; }

        [Required]
        public DateTime Modified { get; set; }

        [Required]
        public DateTime Created { get; set; }

    }

    public interface IPressReleaseMetadata
    {
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
        public int SeriesId { get; set; }

        [Required]
        [RegularExpression(MetadataHelper.SimpleHttpLinkRegex, ErrorMessage = "Enter a valid Url")]
        public string EmailCopyFileLink { get; set; }
        [Required]
        [StringLength(6)]
        public string Brand { get; set; }

        [Required]
        public DateTime Modified { get; set; }

        [Required]
        public DateTime Created { get; set; }

    }

    public interface IProductMetadata
    {
        [Required]
        public string PartNumber { get; set; }

        [Required]
        public DateTime Modified { get; set; }

        [Required]
        public DateTime Created { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
        public int SeriesId { get; set; }

    }

    public interface IProductImageMetadata
    {
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
        public int ProductId { get; set; }

        [Required]
        [RegularExpression(MetadataHelper.SimpleHttpLinkRegex, ErrorMessage = "Enter a valid Url")]
        public string ImageFileLink { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
        public int Position { get; set; }
        [Required]
        public DateTime Created { get; set; }

        [Required]
        public DateTime Modified { get; set; }

    }

    public interface ISeriesMetadata
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        [StringLength(6)]
        public string Brand { get; set; }

        [Required]
        public DateTime Modified { get; set; }

        [Required]
        public DateTime Created { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
        public int? ReplacementSeriesId { get; set; }

    }

    public interface ISeriesArchiveMetadata
    {
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
        public int SeriesId { get; set; }

        [Required]
        [RegularExpression(MetadataHelper.SimpleHttpLinkRegex, ErrorMessage = "Enter a valid Url")]
        public string ZipFileLink { get; set; }

        [Required]
        public DateTime Modified { get; set; }

        [Required]
        public DateTime Created { get; set; }

    }

    public interface ISeriesAttachmentMetadata
    {
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
        public int SeriesId { get; set; }

        [Required]
        [RegularExpression(MetadataHelper.SimpleHttpLinkRegex, ErrorMessage = "Enter a valid Url")]
        public string AttachmentFileLink { get; set; }

        [Required]
        public string Type { get; set; }

        [Required]
        public DateTime Modified { get; set; }

        [Required]
        public DateTime Created { get; set; }

    }

    public interface ISeriesCategoryMetadata
    {
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
        public int SeriesId { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
        public int CategoryId { get; set; }

    }

    public interface ISeriesImageMetadata
    {
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
        public int SeriesId { get; set; }

        [Required]
        [RegularExpression(MetadataHelper.SimpleHttpLinkRegex, ErrorMessage = "Enter a valid Url")]
        public string ImageFileLink { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
        public int Position { get; set; }

        [Required]
        public DateTime Created { get; set; }

        [Required]
        public DateTime Modified { get; set; }
    }

    public interface ISeriesRelatedMetadata
    {
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
        public int SeriesId { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
        public int RelatedSeriesId { get; set; }

        [Required]
        public DateTime Modified { get; set; }

        [Required]
        public DateTime Created { get; set; }

    }

    public interface ITestLeadBuilderMetadata
    {
        [Required]
        public string PartNumber { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
        public int ConnectorOneId { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
        public int ConnectorTwoId { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
        public int WireId { get; set; }

        [DataType(DataType.Url)]
        public string DiagramFileLink { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
        public int MinCentimeters { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
        public int MaxCentimeters { get; set; }

        [Required]
        public DateTime Modified { get; set; }

        [Required]
        public DateTime Created { get; set; }

    }

    public interface ITestLeadColorMetadata
    {
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
        public int ColorId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        [StringLength(7)]
        public string HexCode { get; set; }

        [Required]
        public DateTime Modified { get; set; }

        [Required]
        public DateTime Created { get; set; }
    }

    public interface ITestLeadConnectorMetadata
    {
        [Required]
        public string Type { get; set; }

        [Range(0.0001, double.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
        [Column(MetadataHelper.Decimal19By4)]
        public decimal Cost { get; set; }

        [Required]
        [RegularExpression(MetadataHelper.SimpleHttpLinkRegex, ErrorMessage = "Enter a valid Url")]
        public string ImageFileLink { get; set; }

        [Required]
        public string Information { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
        public int Rating { get; set; }

        [Required]
        public DateTime Modified { get; set; }

        [Required]
        public DateTime Created { get; set; }
    }

    public interface ITestLeadLengthMetadata
    {
        [Range(1, 10000,
        ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public decimal Centimeters { get; set; }
        [Required]
        public DateTime Modified { get; set; }
        [Required]
        public DateTime Created { get; set; }

    }

    public interface ITestLeadWireMetadata
    {
        [Required]
        public string PartNumber { get; set; }

        [Required]
        public string Description { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
        public int MaxCurrent { get; set; }

        [Required]
        public string Material { get; set; }

        [Range(0.0001, double.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
        [Column(MetadataHelper.Decimal19By4)]
        public decimal ConductorAreaMm2 { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
        public int Gauge { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
        public int Strands { get; set; }

        [Range(0.0001, double.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
        [Column(MetadataHelper.Decimal19By4)]
        public decimal StrandDiameterMm { get; set; }

        [Range(0.0001, double.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
        [Column(MetadataHelper.Decimal19By4)]
        public decimal LeadodMm { get; set; }

        [Range(0.0001, double.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
        [Column(MetadataHelper.Decimal19By4)]
        public decimal LeadodIn { get; set; }

        [Range(0.0001, double.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
        [Column(MetadataHelper.Decimal19By4)]
        public decimal CostPerCentimeter { get; set; }

        [Required]
        public DateTime? Modified { get; set; }

        [Required]
        public DateTime Created { get; set; }

    }

    public class MetadataHelper
    {
        public const string Decimal19By4 = "decimal(19, 4)";
        public const int DefaultMinStringLength = 4;
        public const string TopLevelDomainRegex = @"[^.]*\.[^.]{2,3}(?:\.[^.]{2,3})?$";
        public const string SimpleHttpLinkRegex = @"http(s)?://.*$";
    }
}
