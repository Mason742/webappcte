﻿namespace WebDB.Data.Models
{
    public interface IEntityBase
    {
        public int Id { get; set; }

    }
}