﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using WebDB.Data.Models;

namespace WebDB.Data.Context;

public partial class CTEDbContext : DbContext
{
    public CTEDbContext(DbContextOptions options)
        : base(options)
    {
    }

    public virtual DbSet<AAlligatorCt> AAlligatorCts { get; set; }

    public virtual DbSet<AAlligatorCtImport> AAlligatorCtImports { get; set; }

    public virtual DbSet<ABananaCt> ABananaCts { get; set; }

    public virtual DbSet<ABindingPostCt> ABindingPostCts { get; set; }

    public virtual DbSet<ACoaxialAssembliesCt> ACoaxialAssembliesCts { get; set; }

    public virtual DbSet<ACoaxialsAdaptersCt> ACoaxialsAdaptersCts { get; set; }

    public virtual DbSet<ACoaxialsAttenuatorsCt> ACoaxialsAttenuatorsCts { get; set; }

    public virtual DbSet<ACoaxialsTerminatorsCt> ACoaxialsTerminatorsCts { get; set; }

    public virtual DbSet<ADigitalMultimeterCt> ADigitalMultimeterCts { get; set; }

    public virtual DbSet<AGrabbersandHooksCt> AGrabbersandHooksCts { get; set; }

    public virtual DbSet<AKitsCt> AKitsCts { get; set; }

    public virtual DbSet<AOscilloscopeCurrentCt> AOscilloscopeCurrentCts { get; set; }

    public virtual DbSet<AOscilloscopeDifferentialCt> AOscilloscopeDifferentialCts { get; set; }

    public virtual DbSet<AOscilloscopeHighVoltageCt> AOscilloscopeHighVoltageCts { get; set; }

    public virtual DbSet<AOscilloscopePassiveVoltageCt> AOscilloscopePassiveVoltageCts { get; set; }

    public virtual DbSet<AProbeAccessoriesCt> AProbeAccessoriesCts { get; set; }

    public virtual DbSet<ASpadeLugsCt> ASpadeLugsCts { get; set; }

    public virtual DbSet<ATestLeadAssembliesCt> ATestLeadAssembliesCts { get; set; }

    public virtual DbSet<ATestLeadWireCt> ATestLeadWireCts { get; set; }

    public virtual DbSet<Article> Articles { get; set; }

    public virtual DbSet<ArticleSeries> ArticleSeries { get; set; }

    public virtual DbSet<AuthorizedDistributor> AuthorizedDistributors { get; set; }

    public virtual DbSet<AvAlligatorCt> AvAlligatorCts { get; set; }

    public virtual DbSet<AvBananaCt> AvBananaCts { get; set; }

    public virtual DbSet<AvBindingPostCt> AvBindingPostCts { get; set; }

    public virtual DbSet<AvCoaxialAssembliesCt> AvCoaxialAssembliesCts { get; set; }

    public virtual DbSet<AvCoaxialsAdaptersCt> AvCoaxialsAdaptersCts { get; set; }

    public virtual DbSet<AvCoaxialsAttenuatorsCt> AvCoaxialsAttenuatorsCts { get; set; }

    public virtual DbSet<AvCoaxialsTerminatorsCt> AvCoaxialsTerminatorsCts { get; set; }

    public virtual DbSet<AvDigitalMultimeterCt> AvDigitalMultimeterCts { get; set; }

    public virtual DbSet<AvGrabbersandHooksCt> AvGrabbersandHooksCts { get; set; }

    public virtual DbSet<AvKitsCt> AvKitsCts { get; set; }

    public virtual DbSet<AvOscilloscopeCurrentCt> AvOscilloscopeCurrentCts { get; set; }

    public virtual DbSet<AvOscilloscopeDifferentialCt> AvOscilloscopeDifferentialCts { get; set; }

    public virtual DbSet<AvOscilloscopeHighVoltageCt> AvOscilloscopeHighVoltageCts { get; set; }

    public virtual DbSet<AvOscilloscopePassiveVoltageCt> AvOscilloscopePassiveVoltageCts { get; set; }

    public virtual DbSet<AvProbeAccessoriesCt> AvProbeAccessoriesCts { get; set; }

    public virtual DbSet<AvSpadeLugsCt> AvSpadeLugsCts { get; set; }

    public virtual DbSet<AvTestLeadAssembliesCt> AvTestLeadAssembliesCts { get; set; }

    public virtual DbSet<AvTestLeadWireCt> AvTestLeadWireCts { get; set; }

    public virtual DbSet<BkGsStock> BkGsStocks { get; set; }

    public virtual DbSet<Category> Categories { get; set; }

    public virtual DbSet<CategoryGroup> CategoryGroups { get; set; }

    public virtual DbSet<CompanyDocument> CompanyDocuments { get; set; }

    public virtual DbSet<ContactForm> ContactForms { get; set; }

    public virtual DbSet<DistributorAuthorizedDomain> DistributorAuthorizedDomains { get; set; }

    public virtual DbSet<DistributorStock> DistributorStocks { get; set; }

    public virtual DbSet<ErpInventory> ErpInventories { get; set; }

    public virtual DbSet<ErrorLog> ErrorLogs { get; set; }

    public virtual DbSet<Group> Groups { get; set; }

    public virtual DbSet<Homepage> Homepages { get; set; }

    public virtual DbSet<NewsletterSignup> NewsletterSignups { get; set; }

    public virtual DbSet<OrderTrackingNumber> OrderTrackingNumbers { get; set; }

    public virtual DbSet<PressRelease> PressReleases { get; set; }

    public virtual DbSet<Product> Products { get; set; }

    public virtual DbSet<ProductImageArchive> ProductImageArchives { get; set; }

    public virtual DbSet<PurchaseOrder> PurchaseOrders { get; set; }

    public virtual DbSet<ReportPosdataView> ReportPosdataViews { get; set; }

    public virtual DbSet<ReportPosdatum> ReportPosdata { get; set; }

    public virtual DbSet<ReportTlbCurrentPricing> ReportTlbCurrentPricings { get; set; }

    public virtual DbSet<Resource> Resources { get; set; }

    public virtual DbSet<ResourcesArticle> ResourcesArticles { get; set; }

    public virtual DbSet<ResourcesCategory> ResourcesCategories { get; set; }

    public virtual DbSet<ResourcesCompanyDocument> ResourcesCompanyDocuments { get; set; }

    public virtual DbSet<ResourcesGroup> ResourcesGroups { get; set; }

    public virtual DbSet<ResourcesPressRelease> ResourcesPressReleases { get; set; }

    public virtual DbSet<ResourcesProduct> ResourcesProducts { get; set; }

    public virtual DbSet<ResourcesResource> ResourcesResources { get; set; }

    public virtual DbSet<ResourcesSeriesAttachment> ResourcesSeriesAttachments { get; set; }

    public virtual DbSet<ResourcesView> ResourcesViews { get; set; }

    public virtual DbSet<RmaForm> RmaForms { get; set; }

    public virtual DbSet<Series> Series { get; set; }

    public virtual DbSet<SeriesAttachment> SeriesAttachments { get; set; }

    public virtual DbSet<SeriesRelated> SeriesRelateds { get; set; }

    public virtual DbSet<TestLeadBuilder> TestLeadBuilders { get; set; }

    public virtual DbSet<TestLeadBuilderQuoteForm> TestLeadBuilderQuoteForms { get; set; }

    public virtual DbSet<TestLeadColor> TestLeadColors { get; set; }

    public virtual DbSet<TestLeadConnector> TestLeadConnectors { get; set; }

    public virtual DbSet<TestLeadLength> TestLeadLengths { get; set; }

    public virtual DbSet<TestLeadWire> TestLeadWires { get; set; }

    public virtual DbSet<VBaseProductList> VBaseProductLists { get; set; }

    public virtual DbSet<VImageFileLinkTmb> VImageFileLinkTmbs { get; set; }

    public virtual DbSet<ViewOrderTrackingNumberAggregate> ViewOrderTrackingNumberAggregates { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer("Server=.\\SQLExpress;Database=ctedb_refactor;Trusted_Connection=True;MultipleActiveResultSets=true;Encrypt=False");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<AAlligatorCt>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("A_Alligator_CT");

            entity.Property(e => e.Color).HasMaxLength(255);
            entity.Property(e => e.Id).ValueGeneratedOnAdd();
            entity.Property(e => e.Iec).HasMaxLength(255);
            entity.Property(e => e.JawOpening).HasMaxLength(255);
            entity.Property(e => e.Materials).HasMaxLength(255);
            entity.Property(e => e.MaxConductorDiameter).HasMaxLength(255);
            entity.Property(e => e.MaxCurrent).HasMaxLength(255);
            entity.Property(e => e.MaxResistance).HasMaxLength(255);
            entity.Property(e => e.RoHs).HasMaxLength(255);
            entity.Property(e => e.Temperature).HasMaxLength(255);
            entity.Property(e => e.Type).HasMaxLength(255);
            entity.Property(e => e.Type1).HasMaxLength(255);
            entity.Property(e => e.Voltage).HasMaxLength(255);

            entity.HasOne(d => d.Product).WithMany()
                .HasForeignKey(d => d.ProductId)
                .HasConstraintName("FK_A_Alligator_CT_ProductId");
        });

        modelBuilder.Entity<AAlligatorCtImport>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("A_Alligator_CT_Import");

            entity.Property(e => e.Color).HasMaxLength(255);
            entity.Property(e => e.Connector).HasMaxLength(255);
            entity.Property(e => e.IecRating)
                .HasMaxLength(255)
                .HasColumnName("Iec_Rating");
            entity.Property(e => e.InsulatorMaterial)
                .HasMaxLength(255)
                .HasColumnName("Insulator_Material");
            entity.Property(e => e.JawOpening)
                .HasMaxLength(255)
                .HasColumnName("Jaw_Opening");
            entity.Property(e => e.Material).HasMaxLength(255);
            entity.Property(e => e.MaxCurrent)
                .HasMaxLength(255)
                .HasColumnName("Max_Current");
            entity.Property(e => e.NonCategoryMaxVoltage)
                .HasMaxLength(255)
                .HasColumnName("Non_Category_Max_Voltage");
            entity.Property(e => e.Notes).HasMaxLength(255);
            entity.Property(e => e.OperatingTemperature)
                .HasMaxLength(255)
                .HasColumnName("Operating_Temperature");
            entity.Property(e => e.PartNumber)
                .HasMaxLength(255)
                .HasColumnName("Part_Number");
            entity.Property(e => e.RoHs2).HasMaxLength(255);
            entity.Property(e => e.Type).HasMaxLength(255);
        });

        modelBuilder.Entity<ABananaCt>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("A_Banana_CT");

            entity.Property(e => e.BodyMaterial).HasMaxLength(255);
            entity.Property(e => e.Ce).HasMaxLength(255);
            entity.Property(e => e.Color).HasMaxLength(255);
            entity.Property(e => e.ContactMaterial).HasMaxLength(255);
            entity.Property(e => e.Id).ValueGeneratedOnAdd();
            entity.Property(e => e.Iec).HasMaxLength(255);
            entity.Property(e => e.Impedance).HasMaxLength(255);
            entity.Property(e => e.InsulationMaterial).HasMaxLength(255);
            entity.Property(e => e.Magnet).HasMaxLength(255);
            entity.Property(e => e.MaxCurrent).HasMaxLength(255);
            entity.Property(e => e.MaxFloatingVoltage).HasMaxLength(255);
            entity.Property(e => e.MaxResistance).HasMaxLength(255);
            entity.Property(e => e.MaxVoltage).HasMaxLength(255);
            entity.Property(e => e.Modifier).HasMaxLength(255);
            entity.Property(e => e.RoHs).HasMaxLength(255);
            entity.Property(e => e.Temperature).HasMaxLength(255);
            entity.Property(e => e.Type).HasMaxLength(255);
            entity.Property(e => e.Type1).HasMaxLength(255);
            entity.Property(e => e.Ulrecognized).HasMaxLength(255);
            entity.Property(e => e.Voltage).HasMaxLength(255);

            entity.HasOne(d => d.Product).WithMany()
                .HasForeignKey(d => d.ProductId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_A_Banana_CT_ProductId");
        });

        modelBuilder.Entity<ABindingPostCt>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("A_BindingPost_CT");

            entity.Property(e => e.BodyMaterial).HasMaxLength(255);
            entity.Property(e => e.Color).HasMaxLength(255);
            entity.Property(e => e.Id).ValueGeneratedOnAdd();
            entity.Property(e => e.Iec).HasMaxLength(255);
            entity.Property(e => e.MaxCurrent).HasMaxLength(255);
            entity.Property(e => e.MaxResistance).HasMaxLength(255);
            entity.Property(e => e.MaxVoltage).HasMaxLength(255);
            entity.Property(e => e.PollutionDegree).HasMaxLength(255);
            entity.Property(e => e.RoHs).HasMaxLength(255);
            entity.Property(e => e.Temperature).HasMaxLength(255);
            entity.Property(e => e.Type).HasMaxLength(255);

            entity.HasOne(d => d.Product).WithMany()
                .HasForeignKey(d => d.ProductId)
                .HasConstraintName("FK_A_BindingPost_CT_ProductId");
        });

        modelBuilder.Entity<ACoaxialAssembliesCt>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("A_CoaxialAssemblies_CT");

            entity.Property(e => e.Bandwidth).HasMaxLength(255);
            entity.Property(e => e.BodyMaterial).HasMaxLength(255);
            entity.Property(e => e.CableType).HasMaxLength(255);
            entity.Property(e => e.Color).HasMaxLength(255);
            entity.Property(e => e.ConnectorGrade).HasMaxLength(255);
            entity.Property(e => e.ContactMaterial).HasMaxLength(255);
            entity.Property(e => e.FrequencyRange).HasMaxLength(255);
            entity.Property(e => e.Id).ValueGeneratedOnAdd();
            entity.Property(e => e.Iec).HasMaxLength(255);
            entity.Property(e => e.Impedance).HasMaxLength(255);
            entity.Property(e => e.InsertionLoss).HasMaxLength(255);
            entity.Property(e => e.InsulationMaterial).HasMaxLength(255);
            entity.Property(e => e.JacketMaterial).HasMaxLength(255);
            entity.Property(e => e.Length).HasMaxLength(255);
            entity.Property(e => e.MaxCurrent).HasMaxLength(255);
            entity.Property(e => e.MaxVoltage).HasMaxLength(255);
            entity.Property(e => e.RoHs).HasMaxLength(255);
            entity.Property(e => e.Temperature).HasMaxLength(255);
            entity.Property(e => e.Type).HasMaxLength(255);
            entity.Property(e => e.Type1).HasMaxLength(255);
            entity.Property(e => e.Vswr).HasMaxLength(255);

            entity.HasOne(d => d.Product).WithMany()
                .HasForeignKey(d => d.ProductId)
                .HasConstraintName("FK_A_CoaxialAssemblies_CT_ProductId");
        });

        modelBuilder.Entity<ACoaxialsAdaptersCt>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("A_CoaxialsAdapters_CT");

            entity.Property(e => e.CableLength).HasMaxLength(255);
            entity.Property(e => e.CenterContactResistance).HasMaxLength(255);
            entity.Property(e => e.Color).HasMaxLength(255);
            entity.Property(e => e.ConductorArea).HasMaxLength(255);
            entity.Property(e => e.ConnectorGrade).HasMaxLength(255);
            entity.Property(e => e.Frequency).HasMaxLength(255);
            entity.Property(e => e.FrequencyRange).HasMaxLength(255);
            entity.Property(e => e.Id).ValueGeneratedOnAdd();
            entity.Property(e => e.Iec).HasMaxLength(255);
            entity.Property(e => e.Impedance).HasMaxLength(255);
            entity.Property(e => e.InsertionLoss).HasMaxLength(255);
            entity.Property(e => e.JacketMaterial).HasMaxLength(255);
            entity.Property(e => e.MaxCurrent).HasMaxLength(255);
            entity.Property(e => e.MaxResistance).HasMaxLength(255);
            entity.Property(e => e.MaxVoltage).HasMaxLength(255);
            entity.Property(e => e.RoHs).HasMaxLength(255);
            entity.Property(e => e.Stranding).HasMaxLength(255);
            entity.Property(e => e.Temperature).HasMaxLength(255);
            entity.Property(e => e.Type).HasMaxLength(255);
            entity.Property(e => e.Type1).HasMaxLength(255);
            entity.Property(e => e.Vswr).HasMaxLength(255);
            entity.Property(e => e.WireGauge).HasMaxLength(255);
            entity.Property(e => e.WorkingVoltage).HasMaxLength(255);

            entity.HasOne(d => d.Product).WithMany()
                .HasForeignKey(d => d.ProductId)
                .HasConstraintName("FK_A_CoaxialsAdapters_CT_ProductId");
        });

        modelBuilder.Entity<ACoaxialsAttenuatorsCt>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("A_CoaxialsAttenuators_CT");

            entity.Property(e => e.Accuracy).HasMaxLength(255);
            entity.Property(e => e.Attenuation).HasMaxLength(255);
            entity.Property(e => e.AttenuationAccuracy).HasMaxLength(255);
            entity.Property(e => e.AttenuationVoltage).HasMaxLength(255);
            entity.Property(e => e.AveragePower).HasMaxLength(255);
            entity.Property(e => e.Bandwidth).HasMaxLength(255);
            entity.Property(e => e.ConnectorGrade).HasMaxLength(255);
            entity.Property(e => e.FrequencyRange).HasMaxLength(255);
            entity.Property(e => e.Id).ValueGeneratedOnAdd();
            entity.Property(e => e.Impedance).HasMaxLength(255);
            entity.Property(e => e.Length).HasMaxLength(255);
            entity.Property(e => e.MaxVoltage).HasMaxLength(255);
            entity.Property(e => e.PeakPower).HasMaxLength(255);
            entity.Property(e => e.PollutionDegree).HasMaxLength(255);
            entity.Property(e => e.RelativeHumidity).HasMaxLength(255);
            entity.Property(e => e.RoHs).HasMaxLength(255);
            entity.Property(e => e.Safety).HasMaxLength(255);
            entity.Property(e => e.SafetySpecification).HasMaxLength(255);
            entity.Property(e => e.TempCoefficient).HasMaxLength(255);
            entity.Property(e => e.Temperature).HasMaxLength(255);
            entity.Property(e => e.Type).HasMaxLength(255);
            entity.Property(e => e.Vswr).HasMaxLength(255);

            entity.HasOne(d => d.Product).WithMany()
                .HasForeignKey(d => d.ProductId)
                .HasConstraintName("FK_A_CoaxialsAttenuators_CT_ProductId");
        });

        modelBuilder.Entity<ACoaxialsTerminatorsCt>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("A_CoaxialsTerminators_CT");

            entity.Property(e => e.Accuracy).HasMaxLength(255);
            entity.Property(e => e.AveragePower).HasMaxLength(255);
            entity.Property(e => e.ConnectorGrade).HasMaxLength(255);
            entity.Property(e => e.FrequencyRange).HasMaxLength(255);
            entity.Property(e => e.Id).ValueGeneratedOnAdd();
            entity.Property(e => e.Impedance).HasMaxLength(255);
            entity.Property(e => e.InputVoltage).HasMaxLength(255);
            entity.Property(e => e.Length).HasMaxLength(255);
            entity.Property(e => e.RoHs).HasMaxLength(255);
            entity.Property(e => e.Temperature).HasMaxLength(255);
            entity.Property(e => e.Type).HasMaxLength(255);
            entity.Property(e => e.Vswr).HasMaxLength(255);
            entity.Property(e => e.Ztolerance).HasMaxLength(255);

            entity.HasOne(d => d.Product).WithMany()
                .HasForeignKey(d => d.ProductId)
                .HasConstraintName("FK_A_CoaxialsTerminators_CT_ProductId");
        });

        modelBuilder.Entity<ADigitalMultimeterCt>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("A_DigitalMultimeter_CT");

            entity.Property(e => e.Color).HasMaxLength(255);
            entity.Property(e => e.Dimension).HasMaxLength(255);
            entity.Property(e => e.Id).ValueGeneratedOnAdd();
            entity.Property(e => e.Iec).HasMaxLength(255);
            entity.Property(e => e.Impedance).HasMaxLength(255);
            entity.Property(e => e.InterruptingRating).HasMaxLength(255);
            entity.Property(e => e.MaxCurrent).HasMaxLength(255);
            entity.Property(e => e.MaxResistance).HasMaxLength(255);
            entity.Property(e => e.MaxVoltage).HasMaxLength(255);
            entity.Property(e => e.MeterImpedance).HasMaxLength(255);
            entity.Property(e => e.RoHs).HasMaxLength(255);
            entity.Property(e => e.TempCoefficient).HasMaxLength(255);
            entity.Property(e => e.Temperature).HasMaxLength(255);
            entity.Property(e => e.Type).HasMaxLength(255);
            entity.Property(e => e.Voltage).HasMaxLength(255);

            entity.HasOne(d => d.Product).WithMany()
                .HasForeignKey(d => d.ProductId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_A_DigitalMultimeter_CT_ProductId");
        });

        modelBuilder.Entity<AGrabbersandHooksCt>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("A_GrabbersandHooks_CT");

            entity.Property(e => e.Color).HasMaxLength(255);
            entity.Property(e => e.Dimension).HasMaxLength(255);
            entity.Property(e => e.Id).ValueGeneratedOnAdd();
            entity.Property(e => e.Iec).HasMaxLength(255);
            entity.Property(e => e.MaxCurrent).HasMaxLength(255);
            entity.Property(e => e.MaxResistance).HasMaxLength(255);
            entity.Property(e => e.MaxVoltage).HasMaxLength(255);
            entity.Property(e => e.Od).HasMaxLength(255);
            entity.Property(e => e.RoHs).HasMaxLength(255);
            entity.Property(e => e.Temperature).HasMaxLength(255);
            entity.Property(e => e.Type).HasMaxLength(255);

            entity.HasOne(d => d.Product).WithMany()
                .HasForeignKey(d => d.ProductId)
                .HasConstraintName("FK_A_GrabbersandHooks_CT_ProductId");
        });

        modelBuilder.Entity<AKitsCt>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("A_Kits_CT");

            entity.Property(e => e.Amperage).HasMaxLength(255);
            entity.Property(e => e.Bandwidth).HasMaxLength(255);
            entity.Property(e => e.ConnectorGrade).HasMaxLength(255);
            entity.Property(e => e.Id).ValueGeneratedOnAdd();
            entity.Property(e => e.Impedance).HasMaxLength(255);
            entity.Property(e => e.Length).HasMaxLength(255);
            entity.Property(e => e.MaxCurrent).HasMaxLength(255);
            entity.Property(e => e.MaxVoltage).HasMaxLength(255);
            entity.Property(e => e.RoHs).HasMaxLength(255);
            entity.Property(e => e.Temperature).HasMaxLength(255);
            entity.Property(e => e.Type).HasMaxLength(255);
            entity.Property(e => e.Type1).HasMaxLength(255);

            entity.HasOne(d => d.Product).WithMany()
                .HasForeignKey(d => d.ProductId)
                .HasConstraintName("FK_A_Kits_CT_ProductId");
        });

        modelBuilder.Entity<AOscilloscopeCurrentCt>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("A_OscilloscopeCurrent_CT");

            entity.Property(e => e.Bandwidth).HasMaxLength(255);
            entity.Property(e => e.BatteryLife).HasMaxLength(255);
            entity.Property(e => e.CurrentDcac).HasMaxLength(255);
            entity.Property(e => e.DcmeasurementAccuracy).HasMaxLength(255);
            entity.Property(e => e.Dimension).HasMaxLength(255);
            entity.Property(e => e.Humidity).HasMaxLength(255);
            entity.Property(e => e.Id).ValueGeneratedOnAdd();
            entity.Property(e => e.Impedance).HasMaxLength(255);
            entity.Property(e => e.MaxCableDiameter).HasMaxLength(255);
            entity.Property(e => e.MaxCurrent).HasMaxLength(255);
            entity.Property(e => e.MaxFloatingVoltage).HasMaxLength(255);
            entity.Property(e => e.MaxVoltage).HasMaxLength(255);
            entity.Property(e => e.MeasurementRanges).HasMaxLength(255);
            entity.Property(e => e.Modifier).HasMaxLength(255);
            entity.Property(e => e.RiseFallTime).HasMaxLength(255);
            entity.Property(e => e.RoHs).HasMaxLength(255);
            entity.Property(e => e.Temperature).HasMaxLength(255);
            entity.Property(e => e.Type).HasMaxLength(255);
            entity.Property(e => e.Weight).HasMaxLength(255);

            entity.HasOne(d => d.Product).WithMany()
                .HasForeignKey(d => d.ProductId)
                .HasConstraintName("FK_A_OscilloscopeCurrent_CT_ProductId");
        });

        modelBuilder.Entity<AOscilloscopeDifferentialCt>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("A_OscilloscopeDifferential_CT");

            entity.Property(e => e.AcCmrr).HasMaxLength(255);
            entity.Property(e => e.Accuracy).HasMaxLength(255);
            entity.Property(e => e.Altitude).HasMaxLength(255);
            entity.Property(e => e.ArCmrr).HasMaxLength(255);
            entity.Property(e => e.Attenuation).HasMaxLength(255);
            entity.Property(e => e.Bandwidth).HasMaxLength(255);
            entity.Property(e => e.BnccableLength).HasMaxLength(255);
            entity.Property(e => e.CableLength).HasMaxLength(255);
            entity.Property(e => e.Capacitance).HasMaxLength(255);
            entity.Property(e => e.Dimension).HasMaxLength(255);
            entity.Property(e => e.Humidity).HasMaxLength(255);
            entity.Property(e => e.Id).ValueGeneratedOnAdd();
            entity.Property(e => e.Iec).HasMaxLength(255);
            entity.Property(e => e.InputConnectorLength).HasMaxLength(255);
            entity.Property(e => e.InputImpedance).HasMaxLength(255);
            entity.Property(e => e.InputLeadsLength).HasMaxLength(255);
            entity.Property(e => e.InputResistance).HasMaxLength(255);
            entity.Property(e => e.InputVoltageAbsoluteMaximumRated).HasMaxLength(255);
            entity.Property(e => e.InputVoltageMaximumCommonMode).HasMaxLength(255);
            entity.Property(e => e.InputVoltageMaximumDifferential).HasMaxLength(255);
            entity.Property(e => e.Modifier).HasMaxLength(255);
            entity.Property(e => e.OperatingTemperature).HasMaxLength(255);
            entity.Property(e => e.OutputVoltageSourceImpedance).HasMaxLength(255);
            entity.Property(e => e.OutputVoltageSwing).HasMaxLength(255);
            entity.Property(e => e.OutputVoltageTypicalNoise).HasMaxLength(255);
            entity.Property(e => e.OutputVoltageTypicalOffset).HasMaxLength(255);
            entity.Property(e => e.PollutionDegree).HasMaxLength(255);
            entity.Property(e => e.Power).HasMaxLength(255);
            entity.Property(e => e.PowerConsumption).HasMaxLength(255);
            entity.Property(e => e.PowerSupply).HasMaxLength(255);
            entity.Property(e => e.RiseTime).HasMaxLength(255);
            entity.Property(e => e.RoHs).HasMaxLength(255);
            entity.Property(e => e.StorageTemperature).HasMaxLength(255);
            entity.Property(e => e.Temperature).HasMaxLength(255);
            entity.Property(e => e.Type).HasMaxLength(255);
            entity.Property(e => e.Type1).HasMaxLength(255);
            entity.Property(e => e.TypicalCmrr).HasMaxLength(255);
            entity.Property(e => e.Weight).HasMaxLength(255);

            entity.HasOne(d => d.Product).WithMany()
                .HasForeignKey(d => d.ProductId)
                .HasConstraintName("FK_A_OscilloscopeDifferential_CT_ProductId");
        });

        modelBuilder.Entity<AOscilloscopeHighVoltageCt>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("A_OscilloscopeHighVoltage_CT");

            entity.Property(e => e.AccuracyVac).HasMaxLength(255);
            entity.Property(e => e.AccuracyVdc).HasMaxLength(255);
            entity.Property(e => e.Attenuation).HasMaxLength(255);
            entity.Property(e => e.Bandwidth).HasMaxLength(255);
            entity.Property(e => e.CableLength).HasMaxLength(255);
            entity.Property(e => e.Capacitance).HasMaxLength(255);
            entity.Property(e => e.CompensationRange).HasMaxLength(255);
            entity.Property(e => e.Dimension).HasMaxLength(255);
            entity.Property(e => e.Frequency).HasMaxLength(255);
            entity.Property(e => e.Humidity).HasMaxLength(255);
            entity.Property(e => e.Id).ValueGeneratedOnAdd();
            entity.Property(e => e.Impedance).HasMaxLength(255);
            entity.Property(e => e.InputCapacitance).HasMaxLength(255);
            entity.Property(e => e.InputImpedance).HasMaxLength(255);
            entity.Property(e => e.Length).HasMaxLength(255);
            entity.Property(e => e.MaxCurrentLoading).HasMaxLength(255);
            entity.Property(e => e.MaxVoltage).HasMaxLength(255);
            entity.Property(e => e.MaxVoltageAcrms).HasMaxLength(255);
            entity.Property(e => e.OperatingTemperature).HasMaxLength(255);
            entity.Property(e => e.OutputImpedance).HasMaxLength(255);
            entity.Property(e => e.OutputVoltageSourceImpedance).HasMaxLength(255);
            entity.Property(e => e.ReadoutActuator).HasMaxLength(255);
            entity.Property(e => e.RiseTime).HasMaxLength(255);
            entity.Property(e => e.RoHs).HasMaxLength(255);
            entity.Property(e => e.SignalNoise).HasMaxLength(255);
            entity.Property(e => e.StorageTemperature).HasMaxLength(255);
            entity.Property(e => e.TempCoefficient).HasMaxLength(255);
            entity.Property(e => e.Temperature).HasMaxLength(255);
            entity.Property(e => e.Type).HasMaxLength(255);
            entity.Property(e => e.Type1).HasMaxLength(255);
            entity.Property(e => e.Weight).HasMaxLength(255);

            entity.HasOne(d => d.Product).WithMany()
                .HasForeignKey(d => d.ProductId)
                .HasConstraintName("FK_A_OscilloscopeHighVoltage_CT_ProductId");
        });

        modelBuilder.Entity<AOscilloscopePassiveVoltageCt>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("A_OscilloscopePassiveVoltage_CT");

            entity.Property(e => e.Attenuation).HasMaxLength(255);
            entity.Property(e => e.AttenuationAccuracy).HasMaxLength(255);
            entity.Property(e => e.Bandwidth).HasMaxLength(255);
            entity.Property(e => e.CableLength).HasMaxLength(255);
            entity.Property(e => e.Capacitance).HasMaxLength(255);
            entity.Property(e => e.Compensation).HasMaxLength(255);
            entity.Property(e => e.CompensationRange).HasMaxLength(255);
            entity.Property(e => e.GroundBarrelDiameter).HasMaxLength(255);
            entity.Property(e => e.Humidity).HasMaxLength(255);
            entity.Property(e => e.Id).ValueGeneratedOnAdd();
            entity.Property(e => e.Iec).HasMaxLength(255);
            entity.Property(e => e.Impedance).HasMaxLength(255);
            entity.Property(e => e.InputCapacitance).HasMaxLength(255);
            entity.Property(e => e.InputImpedance).HasMaxLength(255);
            entity.Property(e => e.Length).HasMaxLength(255);
            entity.Property(e => e.MaxVoltage).HasMaxLength(255);
            entity.Property(e => e.OperatingTemperature).HasMaxLength(255);
            entity.Property(e => e.OutputImpedance).HasMaxLength(255);
            entity.Property(e => e.OutputVoltageSourceImpedance).HasMaxLength(255);
            entity.Property(e => e.ReadoutActuator).HasMaxLength(255);
            entity.Property(e => e.RiseTime).HasMaxLength(255);
            entity.Property(e => e.RoHs).HasMaxLength(255);
            entity.Property(e => e.Series).HasMaxLength(255);
            entity.Property(e => e.TempCoefficient).HasMaxLength(255);
            entity.Property(e => e.Temperature).HasMaxLength(255);
            entity.Property(e => e.Type).HasMaxLength(255);
            entity.Property(e => e.Type1).HasMaxLength(255);
            entity.Property(e => e.VoltageCoefficient).HasMaxLength(255);

            entity.HasOne(d => d.Product).WithMany()
                .HasForeignKey(d => d.ProductId)
                .HasConstraintName("FK_A_OscilloscopePassiveVoltage_CT_ProductId");
        });

        modelBuilder.Entity<AProbeAccessoriesCt>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("A_ProbeAccessories_CT");

            entity.Property(e => e.Accuracy).HasMaxLength(255);
            entity.Property(e => e.AveragePower).HasMaxLength(255);
            entity.Property(e => e.Bandwidth).HasMaxLength(255);
            entity.Property(e => e.Color).HasMaxLength(255);
            entity.Property(e => e.ContactMaterial).HasMaxLength(255);
            entity.Property(e => e.Dimension).HasMaxLength(255);
            entity.Property(e => e.Id).ValueGeneratedOnAdd();
            entity.Property(e => e.Iec).HasMaxLength(255);
            entity.Property(e => e.Impedance).HasMaxLength(255);
            entity.Property(e => e.InputPower).HasMaxLength(255);
            entity.Property(e => e.InsulationMaterial).HasMaxLength(255);
            entity.Property(e => e.InterruptingRating).HasMaxLength(255);
            entity.Property(e => e.JacketMaterial).HasMaxLength(255);
            entity.Property(e => e.Length).HasMaxLength(255);
            entity.Property(e => e.MaxCurrent).HasMaxLength(255);
            entity.Property(e => e.MaxResistance).HasMaxLength(255);
            entity.Property(e => e.MaxVoltage).HasMaxLength(255);
            entity.Property(e => e.MeterImpedance).HasMaxLength(255);
            entity.Property(e => e.Modifier).HasMaxLength(255);
            entity.Property(e => e.Od).HasMaxLength(255);
            entity.Property(e => e.OutputPower).HasMaxLength(255);
            entity.Property(e => e.RoHs).HasMaxLength(255);
            entity.Property(e => e.Stranding).HasMaxLength(255);
            entity.Property(e => e.Temperature).HasMaxLength(255);
            entity.Property(e => e.TipDimension).HasMaxLength(255);
            entity.Property(e => e.Type).HasMaxLength(255);
            entity.Property(e => e.Type1).HasMaxLength(255);
            entity.Property(e => e.Ulrating).HasMaxLength(255);
            entity.Property(e => e.Voltage).HasMaxLength(255);
            entity.Property(e => e.WireGauge).HasMaxLength(255);

            entity.HasOne(d => d.Product).WithMany()
                .HasForeignKey(d => d.ProductId)
                .HasConstraintName("FK_A_ProbeAccessories_CT_ProductId");
        });

        modelBuilder.Entity<ASpadeLugsCt>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("A_SpadeLugs_CT");

            entity.Property(e => e.Color).HasMaxLength(255);
            entity.Property(e => e.Id).ValueGeneratedOnAdd();
            entity.Property(e => e.Iec).HasMaxLength(255);
            entity.Property(e => e.MaxCurrent).HasMaxLength(255);
            entity.Property(e => e.MaxResistance).HasMaxLength(255);
            entity.Property(e => e.RoHs).HasMaxLength(255);
            entity.Property(e => e.Temperature).HasMaxLength(255);
            entity.Property(e => e.Type).HasMaxLength(255);

            entity.HasOne(d => d.Product).WithMany()
                .HasForeignKey(d => d.ProductId)
                .HasConstraintName("FK_A_SpadeLugs_CT_ProductId");
        });

        modelBuilder.Entity<ATestLeadAssembliesCt>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("A_TestLeadAssemblies_CT");

            entity.Property(e => e.CableLength).HasMaxLength(255);
            entity.Property(e => e.CableType).HasMaxLength(255);
            entity.Property(e => e.Color).HasMaxLength(255);
            entity.Property(e => e.ConductorArea).HasMaxLength(255);
            entity.Property(e => e.ContactMaterial).HasMaxLength(255);
            entity.Property(e => e.Id).ValueGeneratedOnAdd();
            entity.Property(e => e.Iec).HasMaxLength(255);
            entity.Property(e => e.Impedance).HasMaxLength(255);
            entity.Property(e => e.InsulationMaterial).HasMaxLength(255);
            entity.Property(e => e.JacketMaterial).HasMaxLength(255);
            entity.Property(e => e.JacketOd).HasMaxLength(255);
            entity.Property(e => e.Length).HasMaxLength(255);
            entity.Property(e => e.MaxCurrent).HasMaxLength(255);
            entity.Property(e => e.MaxResistance).HasMaxLength(255);
            entity.Property(e => e.MaxVoltage).HasMaxLength(255);
            entity.Property(e => e.Od).HasMaxLength(255);
            entity.Property(e => e.RoHs).HasMaxLength(255);
            entity.Property(e => e.Stranding).HasMaxLength(255);
            entity.Property(e => e.Temperature).HasMaxLength(255);
            entity.Property(e => e.Type).HasMaxLength(255);
            entity.Property(e => e.Type1).HasMaxLength(255);
            entity.Property(e => e.Ul94flameRating).HasMaxLength(255);
            entity.Property(e => e.Ulrating).HasMaxLength(255);
            entity.Property(e => e.VoltageRating).HasMaxLength(255);
            entity.Property(e => e.WireGauge).HasMaxLength(255);

            entity.HasOne(d => d.Product).WithMany()
                .HasForeignKey(d => d.ProductId)
                .HasConstraintName("FK_A_TestLeadAssemblies_CT_ProductId");
        });

        modelBuilder.Entity<ATestLeadWireCt>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("A_TestLeadWire_CT");

            entity.Property(e => e.Color).HasMaxLength(255);
            entity.Property(e => e.ConductorArea).HasMaxLength(255);
            entity.Property(e => e.Id).ValueGeneratedOnAdd();
            entity.Property(e => e.Iec).HasMaxLength(255);
            entity.Property(e => e.JacketMaterial).HasMaxLength(255);
            entity.Property(e => e.Length).HasMaxLength(255);
            entity.Property(e => e.MaxCurrent).HasMaxLength(255);
            entity.Property(e => e.Od).HasMaxLength(255);
            entity.Property(e => e.RoHs).HasMaxLength(255);
            entity.Property(e => e.Stranding).HasMaxLength(255);
            entity.Property(e => e.Temperature).HasMaxLength(255);
            entity.Property(e => e.Type).HasMaxLength(255);
            entity.Property(e => e.WireGauge).HasMaxLength(255);

            entity.HasOne(d => d.Product).WithMany()
                .HasForeignKey(d => d.ProductId)
                .HasConstraintName("FK_A_TestLeadWire_CT_ProductId");
        });

        modelBuilder.Entity<Article>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__Article__9C6270E840CB28B6");

            entity.ToTable("Article");

            entity.HasIndex(e => e.Title, "UQ_Article").IsUnique();

            entity.HasIndex(e => e.Url, "UQ__Article__C5B21431320A6DE4").IsUnique();

            entity.Property(e => e.BodyHtml).IsRequired();
            entity.Property(e => e.Brand)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasDefaultValueSql("('CT')");
            entity.Property(e => e.Created)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.Modified)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.Published)
                .IsRequired()
                .HasDefaultValueSql("((1))");
            entity.Property(e => e.Summary)
                .IsRequired()
                .HasMaxLength(1024);
            entity.Property(e => e.Title)
                .IsRequired()
                .HasMaxLength(255);
            entity.Property(e => e.Url)
                .IsRequired()
                .HasMaxLength(255);
        });

        modelBuilder.Entity<ArticleSeries>(entity =>
        {
            entity.HasIndex(e => new { e.ArticleId, e.SeriesId }, "UQ_ArticleSeries").IsUnique();

            entity.HasOne(d => d.Article).WithMany(p => p.ArticleSeries)
                .HasForeignKey(d => d.ArticleId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ArticleSeries_Article");

            entity.HasOne(d => d.Series).WithMany(p => p.ArticleSeries)
                .HasForeignKey(d => d.SeriesId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ArticleSeries_Series");
        });

        modelBuilder.Entity<AuthorizedDistributor>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__Authoriz__3214EC07CC94B180");

            entity.ToTable("AuthorizedDistributor");

            entity.HasIndex(e => new { e.CustomerId, e.Brand }, "UQ__Authoriz__86B19F82D9303DD2").IsUnique();

            entity.Property(e => e.AccountType)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Brand)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false);
            entity.Property(e => e.City)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Contact)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Country)
                .IsRequired()
                .HasMaxLength(60)
                .IsUnicode(false);
            entity.Property(e => e.Created)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.CustomerId)
                .IsRequired()
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.LogoImageFileLink)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Modified)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Phone)
                .IsRequired()
                .HasMaxLength(20)
                .IsUnicode(false);
            entity.Property(e => e.Published)
                .IsRequired()
                .HasDefaultValueSql("((1))");
            entity.Property(e => e.State)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.TollFreePhone)
                .HasMaxLength(20)
                .IsUnicode(false);
            entity.Property(e => e.WebsiteUrl)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.WebsiteUrlGs)
                .HasMaxLength(255)
                .IsUnicode(false)
                .HasColumnName("WebsiteUrlGS");
        });

        modelBuilder.Entity<AvAlligatorCt>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("AV_Alligator_CT");

            entity.Property(e => e.CalibrationPrice).HasColumnType("money");
            entity.Property(e => e.Color).HasMaxLength(255);
            entity.Property(e => e.CountryOfOrigin)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Created).HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(255);
            entity.Property(e => e.EvaluationCharge).HasColumnType("money");
            entity.Property(e => e.HarmonizedCode)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Iec).HasMaxLength(255);
            entity.Property(e => e.ImageFileLink)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.JawOpening).HasMaxLength(255);
            entity.Property(e => e.ManufacturerLeadTime)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.MasterPartNumber)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Materials).HasMaxLength(255);
            entity.Property(e => e.MaxConductorDiameter).HasMaxLength(255);
            entity.Property(e => e.MaxCurrent).HasMaxLength(255);
            entity.Property(e => e.MaxResistance).HasMaxLength(255);
            entity.Property(e => e.Modified).HasColumnType("datetime");
            entity.Property(e => e.Msrp).HasColumnType("money");
            entity.Property(e => e.NewProduct)
                .IsRequired()
                .HasMaxLength(3)
                .IsUnicode(false);
            entity.Property(e => e.PackSize)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.PackUom)
                .HasMaxLength(25)
                .IsUnicode(false)
                .HasColumnName("PackUOM");
            entity.Property(e => e.PacksPerCarton)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.PartNumber)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.PoLeadTime)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.RepairPrice).HasColumnType("money");
            entity.Property(e => e.RoHs).HasMaxLength(255);
            entity.Property(e => e.ShippingCost)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.SpecialHandling)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Temperature).HasMaxLength(255);
            entity.Property(e => e.TransactionStatus)
                .HasMaxLength(5)
                .IsUnicode(false);
            entity.Property(e => e.Type).HasMaxLength(255);
            entity.Property(e => e.Type1).HasMaxLength(255);
            entity.Property(e => e.UpcCode)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Voltage).HasMaxLength(255);
            entity.Property(e => e.Warranty).HasMaxLength(255);
        });

        modelBuilder.Entity<AvBananaCt>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("AV_Banana_CT");

            entity.Property(e => e.BodyMaterial).HasMaxLength(255);
            entity.Property(e => e.CalibrationPrice).HasColumnType("money");
            entity.Property(e => e.Ce).HasMaxLength(255);
            entity.Property(e => e.Color).HasMaxLength(255);
            entity.Property(e => e.ContactMaterial).HasMaxLength(255);
            entity.Property(e => e.CountryOfOrigin)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Created).HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(255);
            entity.Property(e => e.EvaluationCharge).HasColumnType("money");
            entity.Property(e => e.HarmonizedCode)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Iec).HasMaxLength(255);
            entity.Property(e => e.ImageFileLink)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Impedance).HasMaxLength(255);
            entity.Property(e => e.InsulationMaterial).HasMaxLength(255);
            entity.Property(e => e.Magnet).HasMaxLength(255);
            entity.Property(e => e.ManufacturerLeadTime)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.MasterPartNumber)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.MaxCurrent).HasMaxLength(255);
            entity.Property(e => e.MaxFloatingVoltage).HasMaxLength(255);
            entity.Property(e => e.MaxResistance).HasMaxLength(255);
            entity.Property(e => e.MaxVoltage).HasMaxLength(255);
            entity.Property(e => e.Modified).HasColumnType("datetime");
            entity.Property(e => e.Modifier).HasMaxLength(255);
            entity.Property(e => e.Msrp).HasColumnType("money");
            entity.Property(e => e.NewProduct)
                .IsRequired()
                .HasMaxLength(3)
                .IsUnicode(false);
            entity.Property(e => e.PackSize)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.PackUom)
                .HasMaxLength(25)
                .IsUnicode(false)
                .HasColumnName("PackUOM");
            entity.Property(e => e.PacksPerCarton)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.PartNumber)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.PoLeadTime)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.RepairPrice).HasColumnType("money");
            entity.Property(e => e.RoHs).HasMaxLength(255);
            entity.Property(e => e.ShippingCost)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.SpecialHandling)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Temperature).HasMaxLength(255);
            entity.Property(e => e.TransactionStatus)
                .HasMaxLength(5)
                .IsUnicode(false);
            entity.Property(e => e.Type).HasMaxLength(255);
            entity.Property(e => e.Type1).HasMaxLength(255);
            entity.Property(e => e.Ulrecognized).HasMaxLength(255);
            entity.Property(e => e.UpcCode)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Voltage).HasMaxLength(255);
            entity.Property(e => e.Warranty).HasMaxLength(255);
        });

        modelBuilder.Entity<AvBindingPostCt>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("AV_BindingPost_CT");

            entity.Property(e => e.BodyMaterial).HasMaxLength(255);
            entity.Property(e => e.CalibrationPrice).HasColumnType("money");
            entity.Property(e => e.Color).HasMaxLength(255);
            entity.Property(e => e.CountryOfOrigin)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Created).HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(255);
            entity.Property(e => e.EvaluationCharge).HasColumnType("money");
            entity.Property(e => e.HarmonizedCode)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Iec).HasMaxLength(255);
            entity.Property(e => e.ImageFileLink)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.ManufacturerLeadTime)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.MasterPartNumber)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.MaxCurrent).HasMaxLength(255);
            entity.Property(e => e.MaxResistance).HasMaxLength(255);
            entity.Property(e => e.MaxVoltage).HasMaxLength(255);
            entity.Property(e => e.Modified).HasColumnType("datetime");
            entity.Property(e => e.Msrp).HasColumnType("money");
            entity.Property(e => e.NewProduct)
                .IsRequired()
                .HasMaxLength(3)
                .IsUnicode(false);
            entity.Property(e => e.PackSize)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.PackUom)
                .HasMaxLength(25)
                .IsUnicode(false)
                .HasColumnName("PackUOM");
            entity.Property(e => e.PacksPerCarton)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.PartNumber)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.PoLeadTime)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.PollutionDegree).HasMaxLength(255);
            entity.Property(e => e.RepairPrice).HasColumnType("money");
            entity.Property(e => e.RoHs).HasMaxLength(255);
            entity.Property(e => e.ShippingCost)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.SpecialHandling)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Temperature).HasMaxLength(255);
            entity.Property(e => e.TransactionStatus)
                .HasMaxLength(5)
                .IsUnicode(false);
            entity.Property(e => e.Type).HasMaxLength(255);
            entity.Property(e => e.UpcCode)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Warranty).HasMaxLength(255);
        });

        modelBuilder.Entity<AvCoaxialAssembliesCt>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("AV_CoaxialAssemblies_CT");

            entity.Property(e => e.Bandwidth).HasMaxLength(255);
            entity.Property(e => e.BodyMaterial).HasMaxLength(255);
            entity.Property(e => e.CableType).HasMaxLength(255);
            entity.Property(e => e.CalibrationPrice).HasColumnType("money");
            entity.Property(e => e.Color).HasMaxLength(255);
            entity.Property(e => e.ConnectorGrade).HasMaxLength(255);
            entity.Property(e => e.ContactMaterial).HasMaxLength(255);
            entity.Property(e => e.CountryOfOrigin)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Created).HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(255);
            entity.Property(e => e.EvaluationCharge).HasColumnType("money");
            entity.Property(e => e.FrequencyRange).HasMaxLength(255);
            entity.Property(e => e.HarmonizedCode)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Iec).HasMaxLength(255);
            entity.Property(e => e.ImageFileLink)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Impedance).HasMaxLength(255);
            entity.Property(e => e.InsertionLoss).HasMaxLength(255);
            entity.Property(e => e.InsulationMaterial).HasMaxLength(255);
            entity.Property(e => e.JacketMaterial).HasMaxLength(255);
            entity.Property(e => e.Length).HasMaxLength(255);
            entity.Property(e => e.ManufacturerLeadTime)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.MasterPartNumber)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.MaxCurrent).HasMaxLength(255);
            entity.Property(e => e.MaxVoltage).HasMaxLength(255);
            entity.Property(e => e.Modified).HasColumnType("datetime");
            entity.Property(e => e.Msrp).HasColumnType("money");
            entity.Property(e => e.NewProduct)
                .IsRequired()
                .HasMaxLength(3)
                .IsUnicode(false);
            entity.Property(e => e.PackSize)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.PackUom)
                .HasMaxLength(25)
                .IsUnicode(false)
                .HasColumnName("PackUOM");
            entity.Property(e => e.PacksPerCarton)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.PartNumber)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.PoLeadTime)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.RepairPrice).HasColumnType("money");
            entity.Property(e => e.RoHs).HasMaxLength(255);
            entity.Property(e => e.ShippingCost)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.SpecialHandling)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Temperature).HasMaxLength(255);
            entity.Property(e => e.TransactionStatus)
                .HasMaxLength(5)
                .IsUnicode(false);
            entity.Property(e => e.Type).HasMaxLength(255);
            entity.Property(e => e.Type1).HasMaxLength(255);
            entity.Property(e => e.UpcCode)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Vswr).HasMaxLength(255);
            entity.Property(e => e.Warranty).HasMaxLength(255);
        });

        modelBuilder.Entity<AvCoaxialsAdaptersCt>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("AV_CoaxialsAdapters_CT");

            entity.Property(e => e.CableLength).HasMaxLength(255);
            entity.Property(e => e.CalibrationPrice).HasColumnType("money");
            entity.Property(e => e.CenterContactResistance).HasMaxLength(255);
            entity.Property(e => e.Color).HasMaxLength(255);
            entity.Property(e => e.ConductorArea).HasMaxLength(255);
            entity.Property(e => e.ConnectorGrade).HasMaxLength(255);
            entity.Property(e => e.CountryOfOrigin)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Created).HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(255);
            entity.Property(e => e.EvaluationCharge).HasColumnType("money");
            entity.Property(e => e.Frequency).HasMaxLength(255);
            entity.Property(e => e.FrequencyRange).HasMaxLength(255);
            entity.Property(e => e.HarmonizedCode)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Iec).HasMaxLength(255);
            entity.Property(e => e.ImageFileLink)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Impedance).HasMaxLength(255);
            entity.Property(e => e.InsertionLoss).HasMaxLength(255);
            entity.Property(e => e.JacketMaterial).HasMaxLength(255);
            entity.Property(e => e.ManufacturerLeadTime)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.MasterPartNumber)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.MaxCurrent).HasMaxLength(255);
            entity.Property(e => e.MaxResistance).HasMaxLength(255);
            entity.Property(e => e.MaxVoltage).HasMaxLength(255);
            entity.Property(e => e.Modified).HasColumnType("datetime");
            entity.Property(e => e.Msrp).HasColumnType("money");
            entity.Property(e => e.NewProduct)
                .IsRequired()
                .HasMaxLength(3)
                .IsUnicode(false);
            entity.Property(e => e.PackSize)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.PackUom)
                .HasMaxLength(25)
                .IsUnicode(false)
                .HasColumnName("PackUOM");
            entity.Property(e => e.PacksPerCarton)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.PartNumber)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.PoLeadTime)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.RepairPrice).HasColumnType("money");
            entity.Property(e => e.RoHs).HasMaxLength(255);
            entity.Property(e => e.ShippingCost)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.SpecialHandling)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Stranding).HasMaxLength(255);
            entity.Property(e => e.Temperature).HasMaxLength(255);
            entity.Property(e => e.TransactionStatus)
                .HasMaxLength(5)
                .IsUnicode(false);
            entity.Property(e => e.Type).HasMaxLength(255);
            entity.Property(e => e.Type1).HasMaxLength(255);
            entity.Property(e => e.UpcCode)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Vswr).HasMaxLength(255);
            entity.Property(e => e.Warranty).HasMaxLength(255);
            entity.Property(e => e.WireGauge).HasMaxLength(255);
            entity.Property(e => e.WorkingVoltage).HasMaxLength(255);
        });

        modelBuilder.Entity<AvCoaxialsAttenuatorsCt>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("AV_CoaxialsAttenuators_CT");

            entity.Property(e => e.Accuracy).HasMaxLength(255);
            entity.Property(e => e.Attenuation).HasMaxLength(255);
            entity.Property(e => e.AttenuationAccuracy).HasMaxLength(255);
            entity.Property(e => e.AttenuationVoltage).HasMaxLength(255);
            entity.Property(e => e.AveragePower).HasMaxLength(255);
            entity.Property(e => e.Bandwidth).HasMaxLength(255);
            entity.Property(e => e.CalibrationPrice).HasColumnType("money");
            entity.Property(e => e.ConnectorGrade).HasMaxLength(255);
            entity.Property(e => e.CountryOfOrigin)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Created).HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(255);
            entity.Property(e => e.EvaluationCharge).HasColumnType("money");
            entity.Property(e => e.FrequencyRange).HasMaxLength(255);
            entity.Property(e => e.HarmonizedCode)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.ImageFileLink)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Impedance).HasMaxLength(255);
            entity.Property(e => e.Length).HasMaxLength(255);
            entity.Property(e => e.ManufacturerLeadTime)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.MasterPartNumber)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.MaxVoltage).HasMaxLength(255);
            entity.Property(e => e.Modified).HasColumnType("datetime");
            entity.Property(e => e.Msrp).HasColumnType("money");
            entity.Property(e => e.NewProduct)
                .IsRequired()
                .HasMaxLength(3)
                .IsUnicode(false);
            entity.Property(e => e.PackSize)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.PackUom)
                .HasMaxLength(25)
                .IsUnicode(false)
                .HasColumnName("PackUOM");
            entity.Property(e => e.PacksPerCarton)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.PartNumber)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.PeakPower).HasMaxLength(255);
            entity.Property(e => e.PoLeadTime)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.PollutionDegree).HasMaxLength(255);
            entity.Property(e => e.RelativeHumidity).HasMaxLength(255);
            entity.Property(e => e.RepairPrice).HasColumnType("money");
            entity.Property(e => e.RoHs).HasMaxLength(255);
            entity.Property(e => e.Safety).HasMaxLength(255);
            entity.Property(e => e.SafetySpecification).HasMaxLength(255);
            entity.Property(e => e.ShippingCost)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.SpecialHandling)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.TempCoefficient).HasMaxLength(255);
            entity.Property(e => e.Temperature).HasMaxLength(255);
            entity.Property(e => e.TransactionStatus)
                .HasMaxLength(5)
                .IsUnicode(false);
            entity.Property(e => e.Type).HasMaxLength(255);
            entity.Property(e => e.UpcCode)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Vswr).HasMaxLength(255);
            entity.Property(e => e.Warranty).HasMaxLength(255);
        });

        modelBuilder.Entity<AvCoaxialsTerminatorsCt>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("AV_CoaxialsTerminators_CT");

            entity.Property(e => e.Accuracy).HasMaxLength(255);
            entity.Property(e => e.AveragePower).HasMaxLength(255);
            entity.Property(e => e.CalibrationPrice).HasColumnType("money");
            entity.Property(e => e.ConnectorGrade).HasMaxLength(255);
            entity.Property(e => e.CountryOfOrigin)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Created).HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(255);
            entity.Property(e => e.EvaluationCharge).HasColumnType("money");
            entity.Property(e => e.FrequencyRange).HasMaxLength(255);
            entity.Property(e => e.HarmonizedCode)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.ImageFileLink)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Impedance).HasMaxLength(255);
            entity.Property(e => e.InputVoltage).HasMaxLength(255);
            entity.Property(e => e.Length).HasMaxLength(255);
            entity.Property(e => e.ManufacturerLeadTime)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.MasterPartNumber)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Modified).HasColumnType("datetime");
            entity.Property(e => e.Msrp).HasColumnType("money");
            entity.Property(e => e.NewProduct)
                .IsRequired()
                .HasMaxLength(3)
                .IsUnicode(false);
            entity.Property(e => e.PackSize)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.PackUom)
                .HasMaxLength(25)
                .IsUnicode(false)
                .HasColumnName("PackUOM");
            entity.Property(e => e.PacksPerCarton)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.PartNumber)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.PoLeadTime)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.RepairPrice).HasColumnType("money");
            entity.Property(e => e.RoHs).HasMaxLength(255);
            entity.Property(e => e.ShippingCost)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.SpecialHandling)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Temperature).HasMaxLength(255);
            entity.Property(e => e.TransactionStatus)
                .HasMaxLength(5)
                .IsUnicode(false);
            entity.Property(e => e.Type).HasMaxLength(255);
            entity.Property(e => e.UpcCode)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Vswr).HasMaxLength(255);
            entity.Property(e => e.Warranty).HasMaxLength(255);
            entity.Property(e => e.Ztolerance).HasMaxLength(255);
        });

        modelBuilder.Entity<AvDigitalMultimeterCt>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("AV_DigitalMultimeter_CT");

            entity.Property(e => e.CalibrationPrice).HasColumnType("money");
            entity.Property(e => e.Color).HasMaxLength(255);
            entity.Property(e => e.CountryOfOrigin)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Created).HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(255);
            entity.Property(e => e.Dimension).HasMaxLength(255);
            entity.Property(e => e.EvaluationCharge).HasColumnType("money");
            entity.Property(e => e.HarmonizedCode)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Iec).HasMaxLength(255);
            entity.Property(e => e.ImageFileLink)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Impedance).HasMaxLength(255);
            entity.Property(e => e.InterruptingRating).HasMaxLength(255);
            entity.Property(e => e.ManufacturerLeadTime)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.MasterPartNumber)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.MaxCurrent).HasMaxLength(255);
            entity.Property(e => e.MaxResistance).HasMaxLength(255);
            entity.Property(e => e.MaxVoltage).HasMaxLength(255);
            entity.Property(e => e.MeterImpedance).HasMaxLength(255);
            entity.Property(e => e.Modified).HasColumnType("datetime");
            entity.Property(e => e.Msrp).HasColumnType("money");
            entity.Property(e => e.NewProduct)
                .IsRequired()
                .HasMaxLength(3)
                .IsUnicode(false);
            entity.Property(e => e.PackSize)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.PackUom)
                .HasMaxLength(25)
                .IsUnicode(false)
                .HasColumnName("PackUOM");
            entity.Property(e => e.PacksPerCarton)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.PartNumber)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.PoLeadTime)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.RepairPrice).HasColumnType("money");
            entity.Property(e => e.RoHs).HasMaxLength(255);
            entity.Property(e => e.ShippingCost)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.SpecialHandling)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.TempCoefficient).HasMaxLength(255);
            entity.Property(e => e.Temperature).HasMaxLength(255);
            entity.Property(e => e.TransactionStatus)
                .HasMaxLength(5)
                .IsUnicode(false);
            entity.Property(e => e.Type).HasMaxLength(255);
            entity.Property(e => e.UpcCode)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Voltage).HasMaxLength(255);
            entity.Property(e => e.Warranty).HasMaxLength(255);
        });

        modelBuilder.Entity<AvGrabbersandHooksCt>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("AV_GrabbersandHooks_CT");

            entity.Property(e => e.CalibrationPrice).HasColumnType("money");
            entity.Property(e => e.Color).HasMaxLength(255);
            entity.Property(e => e.CountryOfOrigin)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Created).HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(255);
            entity.Property(e => e.Dimension).HasMaxLength(255);
            entity.Property(e => e.EvaluationCharge).HasColumnType("money");
            entity.Property(e => e.HarmonizedCode)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Iec).HasMaxLength(255);
            entity.Property(e => e.ImageFileLink)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.ManufacturerLeadTime)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.MasterPartNumber)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.MaxCurrent).HasMaxLength(255);
            entity.Property(e => e.MaxResistance).HasMaxLength(255);
            entity.Property(e => e.MaxVoltage).HasMaxLength(255);
            entity.Property(e => e.Modified).HasColumnType("datetime");
            entity.Property(e => e.Msrp).HasColumnType("money");
            entity.Property(e => e.NewProduct)
                .IsRequired()
                .HasMaxLength(3)
                .IsUnicode(false);
            entity.Property(e => e.Od).HasMaxLength(255);
            entity.Property(e => e.PackSize)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.PackUom)
                .HasMaxLength(25)
                .IsUnicode(false)
                .HasColumnName("PackUOM");
            entity.Property(e => e.PacksPerCarton)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.PartNumber)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.PoLeadTime)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.RepairPrice).HasColumnType("money");
            entity.Property(e => e.RoHs).HasMaxLength(255);
            entity.Property(e => e.ShippingCost)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.SpecialHandling)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Temperature).HasMaxLength(255);
            entity.Property(e => e.TransactionStatus)
                .HasMaxLength(5)
                .IsUnicode(false);
            entity.Property(e => e.Type).HasMaxLength(255);
            entity.Property(e => e.UpcCode)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Warranty).HasMaxLength(255);
        });

        modelBuilder.Entity<AvKitsCt>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("AV_Kits_CT");

            entity.Property(e => e.CalibrationPrice).HasColumnType("money");
            entity.Property(e => e.CountryOfOrigin)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Created).HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(255);
            entity.Property(e => e.EvaluationCharge).HasColumnType("money");
            entity.Property(e => e.HarmonizedCode)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.ImageFileLink)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.ManufacturerLeadTime)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.MasterPartNumber)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Modified).HasColumnType("datetime");
            entity.Property(e => e.Msrp).HasColumnType("money");
            entity.Property(e => e.NewProduct)
                .IsRequired()
                .HasMaxLength(3)
                .IsUnicode(false);
            entity.Property(e => e.PackSize)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.PackUom)
                .HasMaxLength(25)
                .IsUnicode(false)
                .HasColumnName("PackUOM");
            entity.Property(e => e.PacksPerCarton)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.PartNumber)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.PoLeadTime)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.RepairPrice).HasColumnType("money");
            entity.Property(e => e.RoHs).HasMaxLength(255);
            entity.Property(e => e.ShippingCost)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.SpecialHandling)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Temperature).HasMaxLength(255);
            entity.Property(e => e.TransactionStatus)
                .HasMaxLength(5)
                .IsUnicode(false);
            entity.Property(e => e.Type).HasMaxLength(255);
            entity.Property(e => e.Type1).HasMaxLength(255);
            entity.Property(e => e.UpcCode)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Warranty).HasMaxLength(255);
        });

        modelBuilder.Entity<AvOscilloscopeCurrentCt>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("AV_OscilloscopeCurrent_CT");

            entity.Property(e => e.Bandwidth).HasMaxLength(255);
            entity.Property(e => e.BatteryLife).HasMaxLength(255);
            entity.Property(e => e.CalibrationPrice).HasColumnType("money");
            entity.Property(e => e.CountryOfOrigin)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Created).HasColumnType("datetime");
            entity.Property(e => e.CurrentDcac).HasMaxLength(255);
            entity.Property(e => e.DcmeasurementAccuracy).HasMaxLength(255);
            entity.Property(e => e.Description).HasMaxLength(255);
            entity.Property(e => e.Dimension).HasMaxLength(255);
            entity.Property(e => e.EvaluationCharge).HasColumnType("money");
            entity.Property(e => e.HarmonizedCode)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Humidity).HasMaxLength(255);
            entity.Property(e => e.ImageFileLink)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Impedance).HasMaxLength(255);
            entity.Property(e => e.ManufacturerLeadTime)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.MasterPartNumber)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.MaxCableDiameter).HasMaxLength(255);
            entity.Property(e => e.MaxCurrent).HasMaxLength(255);
            entity.Property(e => e.MaxFloatingVoltage).HasMaxLength(255);
            entity.Property(e => e.MaxVoltage).HasMaxLength(255);
            entity.Property(e => e.MeasurementRanges).HasMaxLength(255);
            entity.Property(e => e.Modified).HasColumnType("datetime");
            entity.Property(e => e.Modifier).HasMaxLength(255);
            entity.Property(e => e.Msrp).HasColumnType("money");
            entity.Property(e => e.NewProduct)
                .IsRequired()
                .HasMaxLength(3)
                .IsUnicode(false);
            entity.Property(e => e.PackSize)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.PackUom)
                .HasMaxLength(25)
                .IsUnicode(false)
                .HasColumnName("PackUOM");
            entity.Property(e => e.PacksPerCarton)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.PartNumber)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.PoLeadTime)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.RepairPrice).HasColumnType("money");
            entity.Property(e => e.RiseFallTime).HasMaxLength(255);
            entity.Property(e => e.RoHs).HasMaxLength(255);
            entity.Property(e => e.ShippingCost)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.SpecialHandling)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Temperature).HasMaxLength(255);
            entity.Property(e => e.TransactionStatus)
                .HasMaxLength(5)
                .IsUnicode(false);
            entity.Property(e => e.Type).HasMaxLength(255);
            entity.Property(e => e.UpcCode)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Warranty).HasMaxLength(255);
            entity.Property(e => e.Weight).HasMaxLength(255);
        });

        modelBuilder.Entity<AvOscilloscopeDifferentialCt>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("AV_OscilloscopeDifferential_CT");

            entity.Property(e => e.AcCmrr).HasMaxLength(255);
            entity.Property(e => e.Accuracy).HasMaxLength(255);
            entity.Property(e => e.Altitude).HasMaxLength(255);
            entity.Property(e => e.ArCmrr).HasMaxLength(255);
            entity.Property(e => e.Attenuation).HasMaxLength(255);
            entity.Property(e => e.Bandwidth).HasMaxLength(255);
            entity.Property(e => e.BnccableLength).HasMaxLength(255);
            entity.Property(e => e.CableLength).HasMaxLength(255);
            entity.Property(e => e.CalibrationPrice).HasColumnType("money");
            entity.Property(e => e.Capacitance).HasMaxLength(255);
            entity.Property(e => e.CountryOfOrigin)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Created).HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(255);
            entity.Property(e => e.Dimension).HasMaxLength(255);
            entity.Property(e => e.EvaluationCharge).HasColumnType("money");
            entity.Property(e => e.HarmonizedCode)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Humidity).HasMaxLength(255);
            entity.Property(e => e.Iec).HasMaxLength(255);
            entity.Property(e => e.ImageFileLink)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.InputConnectorLength).HasMaxLength(255);
            entity.Property(e => e.InputImpedance).HasMaxLength(255);
            entity.Property(e => e.InputLeadsLength).HasMaxLength(255);
            entity.Property(e => e.InputResistance).HasMaxLength(255);
            entity.Property(e => e.InputVoltageAbsoluteMaximumRated).HasMaxLength(255);
            entity.Property(e => e.InputVoltageMaximumCommonMode).HasMaxLength(255);
            entity.Property(e => e.InputVoltageMaximumDifferential).HasMaxLength(255);
            entity.Property(e => e.ManufacturerLeadTime)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.MasterPartNumber)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Modified).HasColumnType("datetime");
            entity.Property(e => e.Modifier).HasMaxLength(255);
            entity.Property(e => e.Msrp).HasColumnType("money");
            entity.Property(e => e.NewProduct)
                .IsRequired()
                .HasMaxLength(3)
                .IsUnicode(false);
            entity.Property(e => e.OperatingTemperature).HasMaxLength(255);
            entity.Property(e => e.OutputVoltageSourceImpedance).HasMaxLength(255);
            entity.Property(e => e.OutputVoltageSwing).HasMaxLength(255);
            entity.Property(e => e.OutputVoltageTypicalNoise).HasMaxLength(255);
            entity.Property(e => e.OutputVoltageTypicalOffset).HasMaxLength(255);
            entity.Property(e => e.PackSize)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.PackUom)
                .HasMaxLength(25)
                .IsUnicode(false)
                .HasColumnName("PackUOM");
            entity.Property(e => e.PacksPerCarton)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.PartNumber)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.PoLeadTime)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.PollutionDegree).HasMaxLength(255);
            entity.Property(e => e.Power).HasMaxLength(255);
            entity.Property(e => e.PowerConsumption).HasMaxLength(255);
            entity.Property(e => e.PowerSupply).HasMaxLength(255);
            entity.Property(e => e.RepairPrice).HasColumnType("money");
            entity.Property(e => e.RiseTime).HasMaxLength(255);
            entity.Property(e => e.RoHs).HasMaxLength(255);
            entity.Property(e => e.ShippingCost)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.SpecialHandling)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.StorageTemperature).HasMaxLength(255);
            entity.Property(e => e.Temperature).HasMaxLength(255);
            entity.Property(e => e.TransactionStatus)
                .HasMaxLength(5)
                .IsUnicode(false);
            entity.Property(e => e.Type).HasMaxLength(255);
            entity.Property(e => e.Type1).HasMaxLength(255);
            entity.Property(e => e.TypicalCmrr).HasMaxLength(255);
            entity.Property(e => e.UpcCode)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Warranty).HasMaxLength(255);
            entity.Property(e => e.Weight).HasMaxLength(255);
        });

        modelBuilder.Entity<AvOscilloscopeHighVoltageCt>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("AV_OscilloscopeHighVoltage_CT");

            entity.Property(e => e.AccuracyVac).HasMaxLength(255);
            entity.Property(e => e.AccuracyVdc).HasMaxLength(255);
            entity.Property(e => e.Attenuation).HasMaxLength(255);
            entity.Property(e => e.Bandwidth).HasMaxLength(255);
            entity.Property(e => e.CableLength).HasMaxLength(255);
            entity.Property(e => e.CalibrationPrice).HasColumnType("money");
            entity.Property(e => e.Capacitance).HasMaxLength(255);
            entity.Property(e => e.CompensationRange).HasMaxLength(255);
            entity.Property(e => e.CountryOfOrigin)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Created).HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(255);
            entity.Property(e => e.Dimension).HasMaxLength(255);
            entity.Property(e => e.EvaluationCharge).HasColumnType("money");
            entity.Property(e => e.Frequency).HasMaxLength(255);
            entity.Property(e => e.HarmonizedCode)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Humidity).HasMaxLength(255);
            entity.Property(e => e.ImageFileLink)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Impedance).HasMaxLength(255);
            entity.Property(e => e.InputCapacitance).HasMaxLength(255);
            entity.Property(e => e.InputImpedance).HasMaxLength(255);
            entity.Property(e => e.Length).HasMaxLength(255);
            entity.Property(e => e.ManufacturerLeadTime)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.MasterPartNumber)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.MaxCurrentLoading).HasMaxLength(255);
            entity.Property(e => e.MaxVoltage).HasMaxLength(255);
            entity.Property(e => e.MaxVoltageAcrms).HasMaxLength(255);
            entity.Property(e => e.Modified).HasColumnType("datetime");
            entity.Property(e => e.Msrp).HasColumnType("money");
            entity.Property(e => e.NewProduct)
                .IsRequired()
                .HasMaxLength(3)
                .IsUnicode(false);
            entity.Property(e => e.OperatingTemperature).HasMaxLength(255);
            entity.Property(e => e.OutputImpedance).HasMaxLength(255);
            entity.Property(e => e.OutputVoltageSourceImpedance).HasMaxLength(255);
            entity.Property(e => e.PackSize)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.PackUom)
                .HasMaxLength(25)
                .IsUnicode(false)
                .HasColumnName("PackUOM");
            entity.Property(e => e.PacksPerCarton)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.PartNumber)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.PoLeadTime)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.ReadoutActuator).HasMaxLength(255);
            entity.Property(e => e.RepairPrice).HasColumnType("money");
            entity.Property(e => e.RiseTime).HasMaxLength(255);
            entity.Property(e => e.RoHs).HasMaxLength(255);
            entity.Property(e => e.ShippingCost)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.SignalNoise).HasMaxLength(255);
            entity.Property(e => e.SpecialHandling)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.StorageTemperature).HasMaxLength(255);
            entity.Property(e => e.TempCoefficient).HasMaxLength(255);
            entity.Property(e => e.Temperature).HasMaxLength(255);
            entity.Property(e => e.TransactionStatus)
                .HasMaxLength(5)
                .IsUnicode(false);
            entity.Property(e => e.Type).HasMaxLength(255);
            entity.Property(e => e.Type1).HasMaxLength(255);
            entity.Property(e => e.UpcCode)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Warranty).HasMaxLength(255);
            entity.Property(e => e.Weight).HasMaxLength(255);
        });

        modelBuilder.Entity<AvOscilloscopePassiveVoltageCt>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("AV_OscilloscopePassiveVoltage_CT");

            entity.Property(e => e.Attenuation).HasMaxLength(255);
            entity.Property(e => e.AttenuationAccuracy).HasMaxLength(255);
            entity.Property(e => e.Bandwidth).HasMaxLength(255);
            entity.Property(e => e.CableLength).HasMaxLength(255);
            entity.Property(e => e.CalibrationPrice).HasColumnType("money");
            entity.Property(e => e.Capacitance).HasMaxLength(255);
            entity.Property(e => e.Compensation).HasMaxLength(255);
            entity.Property(e => e.CompensationRange).HasMaxLength(255);
            entity.Property(e => e.CountryOfOrigin)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Created).HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(255);
            entity.Property(e => e.EvaluationCharge).HasColumnType("money");
            entity.Property(e => e.GroundBarrelDiameter).HasMaxLength(255);
            entity.Property(e => e.HarmonizedCode)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Humidity).HasMaxLength(255);
            entity.Property(e => e.Iec).HasMaxLength(255);
            entity.Property(e => e.ImageFileLink)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Impedance).HasMaxLength(255);
            entity.Property(e => e.InputCapacitance).HasMaxLength(255);
            entity.Property(e => e.InputImpedance).HasMaxLength(255);
            entity.Property(e => e.Length).HasMaxLength(255);
            entity.Property(e => e.ManufacturerLeadTime)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.MasterPartNumber)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.MaxVoltage).HasMaxLength(255);
            entity.Property(e => e.Modified).HasColumnType("datetime");
            entity.Property(e => e.Msrp).HasColumnType("money");
            entity.Property(e => e.NewProduct)
                .IsRequired()
                .HasMaxLength(3)
                .IsUnicode(false);
            entity.Property(e => e.OperatingTemperature).HasMaxLength(255);
            entity.Property(e => e.OutputImpedance).HasMaxLength(255);
            entity.Property(e => e.OutputVoltageSourceImpedance).HasMaxLength(255);
            entity.Property(e => e.PackSize)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.PackUom)
                .HasMaxLength(25)
                .IsUnicode(false)
                .HasColumnName("PackUOM");
            entity.Property(e => e.PacksPerCarton)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.PartNumber)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.PoLeadTime)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.ReadoutActuator).HasMaxLength(255);
            entity.Property(e => e.RepairPrice).HasColumnType("money");
            entity.Property(e => e.RiseTime).HasMaxLength(255);
            entity.Property(e => e.RoHs).HasMaxLength(255);
            entity.Property(e => e.Series).HasMaxLength(255);
            entity.Property(e => e.ShippingCost)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.SpecialHandling)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.TempCoefficient).HasMaxLength(255);
            entity.Property(e => e.Temperature).HasMaxLength(255);
            entity.Property(e => e.TransactionStatus)
                .HasMaxLength(5)
                .IsUnicode(false);
            entity.Property(e => e.Type).HasMaxLength(255);
            entity.Property(e => e.Type1).HasMaxLength(255);
            entity.Property(e => e.UpcCode)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.VoltageCoefficient).HasMaxLength(255);
            entity.Property(e => e.Warranty).HasMaxLength(255);
        });

        modelBuilder.Entity<AvProbeAccessoriesCt>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("AV_ProbeAccessories_CT");

            entity.Property(e => e.Accuracy).HasMaxLength(255);
            entity.Property(e => e.AveragePower).HasMaxLength(255);
            entity.Property(e => e.Bandwidth).HasMaxLength(255);
            entity.Property(e => e.CalibrationPrice).HasColumnType("money");
            entity.Property(e => e.Color).HasMaxLength(255);
            entity.Property(e => e.ContactMaterial).HasMaxLength(255);
            entity.Property(e => e.CountryOfOrigin)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Created).HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(255);
            entity.Property(e => e.Dimension).HasMaxLength(255);
            entity.Property(e => e.EvaluationCharge).HasColumnType("money");
            entity.Property(e => e.HarmonizedCode)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Iec).HasMaxLength(255);
            entity.Property(e => e.ImageFileLink)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Impedance).HasMaxLength(255);
            entity.Property(e => e.InputPower).HasMaxLength(255);
            entity.Property(e => e.InsulationMaterial).HasMaxLength(255);
            entity.Property(e => e.InterruptingRating).HasMaxLength(255);
            entity.Property(e => e.JacketMaterial).HasMaxLength(255);
            entity.Property(e => e.Length).HasMaxLength(255);
            entity.Property(e => e.ManufacturerLeadTime)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.MasterPartNumber)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.MaxCurrent).HasMaxLength(255);
            entity.Property(e => e.MaxResistance).HasMaxLength(255);
            entity.Property(e => e.MaxVoltage).HasMaxLength(255);
            entity.Property(e => e.MeterImpedance).HasMaxLength(255);
            entity.Property(e => e.Modified).HasColumnType("datetime");
            entity.Property(e => e.Modifier).HasMaxLength(255);
            entity.Property(e => e.Msrp).HasColumnType("money");
            entity.Property(e => e.NewProduct)
                .IsRequired()
                .HasMaxLength(3)
                .IsUnicode(false);
            entity.Property(e => e.Od).HasMaxLength(255);
            entity.Property(e => e.OutputPower).HasMaxLength(255);
            entity.Property(e => e.PackSize)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.PackUom)
                .HasMaxLength(25)
                .IsUnicode(false)
                .HasColumnName("PackUOM");
            entity.Property(e => e.PacksPerCarton)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.PartNumber)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.PoLeadTime)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.RepairPrice).HasColumnType("money");
            entity.Property(e => e.RoHs).HasMaxLength(255);
            entity.Property(e => e.ShippingCost)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.SpecialHandling)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Stranding).HasMaxLength(255);
            entity.Property(e => e.Temperature).HasMaxLength(255);
            entity.Property(e => e.TipDimension).HasMaxLength(255);
            entity.Property(e => e.TransactionStatus)
                .HasMaxLength(5)
                .IsUnicode(false);
            entity.Property(e => e.Type).HasMaxLength(255);
            entity.Property(e => e.Type1).HasMaxLength(255);
            entity.Property(e => e.Ulrating).HasMaxLength(255);
            entity.Property(e => e.UpcCode)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Voltage).HasMaxLength(255);
            entity.Property(e => e.Warranty).HasMaxLength(255);
            entity.Property(e => e.WireGauge).HasMaxLength(255);
        });

        modelBuilder.Entity<AvSpadeLugsCt>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("AV_SpadeLugs_CT");

            entity.Property(e => e.CalibrationPrice).HasColumnType("money");
            entity.Property(e => e.Color).HasMaxLength(255);
            entity.Property(e => e.CountryOfOrigin)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Created).HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(255);
            entity.Property(e => e.EvaluationCharge).HasColumnType("money");
            entity.Property(e => e.HarmonizedCode)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Iec).HasMaxLength(255);
            entity.Property(e => e.ImageFileLink)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.ManufacturerLeadTime)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.MasterPartNumber)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.MaxCurrent).HasMaxLength(255);
            entity.Property(e => e.MaxResistance).HasMaxLength(255);
            entity.Property(e => e.Modified).HasColumnType("datetime");
            entity.Property(e => e.Msrp).HasColumnType("money");
            entity.Property(e => e.NewProduct)
                .IsRequired()
                .HasMaxLength(3)
                .IsUnicode(false);
            entity.Property(e => e.PackSize)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.PackUom)
                .HasMaxLength(25)
                .IsUnicode(false)
                .HasColumnName("PackUOM");
            entity.Property(e => e.PacksPerCarton)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.PartNumber)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.PoLeadTime)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.RepairPrice).HasColumnType("money");
            entity.Property(e => e.RoHs).HasMaxLength(255);
            entity.Property(e => e.ShippingCost)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.SpecialHandling)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Temperature).HasMaxLength(255);
            entity.Property(e => e.TransactionStatus)
                .HasMaxLength(5)
                .IsUnicode(false);
            entity.Property(e => e.Type).HasMaxLength(255);
            entity.Property(e => e.UpcCode)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Warranty).HasMaxLength(255);
        });

        modelBuilder.Entity<AvTestLeadAssembliesCt>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("AV_TestLeadAssemblies_CT");

            entity.Property(e => e.CableLength).HasMaxLength(255);
            entity.Property(e => e.CableType).HasMaxLength(255);
            entity.Property(e => e.CalibrationPrice).HasColumnType("money");
            entity.Property(e => e.Color).HasMaxLength(255);
            entity.Property(e => e.ConductorArea).HasMaxLength(255);
            entity.Property(e => e.ContactMaterial).HasMaxLength(255);
            entity.Property(e => e.CountryOfOrigin)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Created).HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(255);
            entity.Property(e => e.EvaluationCharge).HasColumnType("money");
            entity.Property(e => e.HarmonizedCode)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Iec).HasMaxLength(255);
            entity.Property(e => e.ImageFileLink)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Impedance).HasMaxLength(255);
            entity.Property(e => e.InsulationMaterial).HasMaxLength(255);
            entity.Property(e => e.JacketMaterial).HasMaxLength(255);
            entity.Property(e => e.JacketOd).HasMaxLength(255);
            entity.Property(e => e.Length).HasMaxLength(255);
            entity.Property(e => e.ManufacturerLeadTime)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.MasterPartNumber)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.MaxCurrent).HasMaxLength(255);
            entity.Property(e => e.MaxResistance).HasMaxLength(255);
            entity.Property(e => e.MaxVoltage).HasMaxLength(255);
            entity.Property(e => e.Modified).HasColumnType("datetime");
            entity.Property(e => e.Msrp).HasColumnType("money");
            entity.Property(e => e.NewProduct)
                .IsRequired()
                .HasMaxLength(3)
                .IsUnicode(false);
            entity.Property(e => e.Od).HasMaxLength(255);
            entity.Property(e => e.PackSize)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.PackUom)
                .HasMaxLength(25)
                .IsUnicode(false)
                .HasColumnName("PackUOM");
            entity.Property(e => e.PacksPerCarton)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.PartNumber)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.PoLeadTime)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.RepairPrice).HasColumnType("money");
            entity.Property(e => e.RoHs).HasMaxLength(255);
            entity.Property(e => e.ShippingCost)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.SpecialHandling)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Stranding).HasMaxLength(255);
            entity.Property(e => e.Temperature).HasMaxLength(255);
            entity.Property(e => e.TransactionStatus)
                .HasMaxLength(5)
                .IsUnicode(false);
            entity.Property(e => e.Type).HasMaxLength(255);
            entity.Property(e => e.Type1).HasMaxLength(255);
            entity.Property(e => e.Ul94flameRating).HasMaxLength(255);
            entity.Property(e => e.Ulrating).HasMaxLength(255);
            entity.Property(e => e.UpcCode)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.VoltageRating).HasMaxLength(255);
            entity.Property(e => e.Warranty).HasMaxLength(255);
            entity.Property(e => e.WireGauge).HasMaxLength(255);
        });

        modelBuilder.Entity<AvTestLeadWireCt>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("AV_TestLeadWire_CT");

            entity.Property(e => e.CalibrationPrice).HasColumnType("money");
            entity.Property(e => e.Color).HasMaxLength(255);
            entity.Property(e => e.ConductorArea).HasMaxLength(255);
            entity.Property(e => e.CountryOfOrigin)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Created).HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(255);
            entity.Property(e => e.EvaluationCharge).HasColumnType("money");
            entity.Property(e => e.HarmonizedCode)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Iec).HasMaxLength(255);
            entity.Property(e => e.ImageFileLink)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.JacketMaterial).HasMaxLength(255);
            entity.Property(e => e.Length).HasMaxLength(255);
            entity.Property(e => e.ManufacturerLeadTime)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.MasterPartNumber)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.MaxCurrent).HasMaxLength(255);
            entity.Property(e => e.Modified).HasColumnType("datetime");
            entity.Property(e => e.Msrp).HasColumnType("money");
            entity.Property(e => e.NewProduct)
                .IsRequired()
                .HasMaxLength(3)
                .IsUnicode(false);
            entity.Property(e => e.Od).HasMaxLength(255);
            entity.Property(e => e.PackSize)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.PackUom)
                .HasMaxLength(25)
                .IsUnicode(false)
                .HasColumnName("PackUOM");
            entity.Property(e => e.PacksPerCarton)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.PartNumber)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.PoLeadTime)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.RepairPrice).HasColumnType("money");
            entity.Property(e => e.RoHs).HasMaxLength(255);
            entity.Property(e => e.ShippingCost)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.SpecialHandling)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Stranding).HasMaxLength(255);
            entity.Property(e => e.Temperature).HasMaxLength(255);
            entity.Property(e => e.TransactionStatus)
                .HasMaxLength(5)
                .IsUnicode(false);
            entity.Property(e => e.Type).HasMaxLength(255);
            entity.Property(e => e.UpcCode)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Warranty).HasMaxLength(255);
            entity.Property(e => e.WireGauge).HasMaxLength(255);
        });

        modelBuilder.Entity<BkGsStock>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__BkGsStoc__3214EC079482B657");

            entity.ToTable("BkGsStock");

            entity.HasIndex(e => e.PartNumber, "UQ__BkGsStoc__025D30D97F31F71D").IsUnique();

            entity.Property(e => e.Created)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.PartNumber)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
        });

        modelBuilder.Entity<Category>(entity =>
        {
            entity.ToTable("Category");

            entity.HasIndex(e => e.TableName, "UQ_Category").IsUnique();

            entity.Property(e => e.Brand)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasDefaultValueSql("('CT')");
            entity.Property(e => e.Created)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(1024);
            entity.Property(e => e.DisplayName)
                .IsRequired()
                .HasMaxLength(255);
            entity.Property(e => e.ImageFileLink)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Modified)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.TableName)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
        });

        modelBuilder.Entity<CategoryGroup>(entity =>
        {
            entity.ToTable("CategoryGroup");

            entity.HasIndex(e => new { e.CategoryId, e.GroupId }, "UQ_CategoryGroup").IsUnique();

            entity.HasOne(d => d.Category).WithMany(p => p.CategoryGroups)
                .HasForeignKey(d => d.CategoryId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_CategoryGroup_Category");

            entity.HasOne(d => d.Group).WithMany(p => p.CategoryGroups)
                .HasForeignKey(d => d.GroupId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_CategoryGroup_Group");
        });

        modelBuilder.Entity<CompanyDocument>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__CompanyD__3214EC074A0CC1EA");

            entity.ToTable("CompanyDocument");

            entity.Property(e => e.Brand)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false);
            entity.Property(e => e.Created)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(300);
            entity.Property(e => e.FileLink)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Modified)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.Published)
                .IsRequired()
                .HasDefaultValueSql("((1))");
            entity.Property(e => e.Title)
                .IsRequired()
                .HasMaxLength(255);
        });

        modelBuilder.Entity<ContactForm>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__ContactF__88BB5D89A224B6BA");

            entity.ToTable("ContactForm");

            entity.Property(e => e.Address1)
                .IsRequired()
                .HasMaxLength(255);
            entity.Property(e => e.Address2).HasMaxLength(255);
            entity.Property(e => e.Brand)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false);
            entity.Property(e => e.City)
                .IsRequired()
                .HasMaxLength(255);
            entity.Property(e => e.Company).HasMaxLength(255);
            entity.Property(e => e.ContactReasonChoice)
                .IsRequired()
                .HasMaxLength(60)
                .IsUnicode(false);
            entity.Property(e => e.Country)
                .IsRequired()
                .HasMaxLength(60)
                .IsUnicode(false);
            entity.Property(e => e.Created)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.Email)
                .IsRequired()
                .HasMaxLength(255);
            entity.Property(e => e.FirstName)
                .IsRequired()
                .HasMaxLength(255);
            entity.Property(e => e.LastName)
                .IsRequired()
                .HasMaxLength(255);
            entity.Property(e => e.Message)
                .IsRequired()
                .HasMaxLength(1024);
            entity.Property(e => e.Modified)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.Phone)
                .IsRequired()
                .HasMaxLength(20);
            entity.Property(e => e.PhoneExt).HasMaxLength(6);
            entity.Property(e => e.PostalCode)
                .IsRequired()
                .HasMaxLength(255);
            entity.Property(e => e.StateOrProvince)
                .IsRequired()
                .HasMaxLength(255);
        });

        modelBuilder.Entity<DistributorAuthorizedDomain>(entity =>
        {
            entity.ToTable("DistributorAuthorizedDomain");

            entity.HasIndex(e => e.Domain, "UQ__Distribu__FD349E533FA0B959").IsUnique();

            entity.Property(e => e.CompanyName)
                .IsRequired()
                .HasMaxLength(255);
            entity.Property(e => e.Created)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.CustomerId)
                .IsRequired()
                .HasMaxLength(50);
            entity.Property(e => e.Domain)
                .IsRequired()
                .HasMaxLength(150);
            entity.Property(e => e.Modified)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
        });

        modelBuilder.Entity<DistributorStock>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__Distribu__3214EC0769AE998B");

            entity.ToTable("DistributorStock");

            entity.Property(e => e.Created)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.CustomerId)
                .IsRequired()
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.Modified)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.PartNumber)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.WebsiteUrl)
                .HasMaxLength(255)
                .IsUnicode(false);
        });

        modelBuilder.Entity<ErpInventory>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__ErpInven__3214EC0703F3D8C0");

            entity.ToTable("ErpInventory");

            entity.HasIndex(e => e.PartNumber, "UQ__ErpInven__025D30D9BAFF7B8E").IsUnique();

            entity.Property(e => e.Brand)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasDefaultValueSql("('CT')");
            entity.Property(e => e.Created)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.PartNumber)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Price).HasColumnType("money");
            entity.Property(e => e.SpecialHandling)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
        });

        modelBuilder.Entity<ErrorLog>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("ErrorLog");

            entity.Property(e => e.Id).ValueGeneratedOnAdd();
            entity.Property(e => e.Values).IsRequired();
        });

        modelBuilder.Entity<Group>(entity =>
        {
            entity.ToTable("Group");

            entity.HasIndex(e => e.Name, "UQ_Group").IsUnique();

            entity.Property(e => e.Brand)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasDefaultValueSql("('CT')");
            entity.Property(e => e.Created)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.Description)
                .IsRequired()
                .HasMaxLength(350);
            entity.Property(e => e.ImageFileLink)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Modified)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(255);
        });

        modelBuilder.Entity<Homepage>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__Homepage__3214EC071D26C9A9");

            entity.ToTable("Homepage");

            entity.Property(e => e.Brand)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false);
            entity.Property(e => e.Created)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.MediaFileLink)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Modified)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.Subtitle).IsRequired();
            entity.Property(e => e.Summary).IsRequired();
            entity.Property(e => e.Title).IsRequired();
            entity.Property(e => e.Type)
                .IsRequired()
                .IsUnicode(false);
            entity.Property(e => e.Url)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
        });

        modelBuilder.Entity<NewsletterSignup>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__Newslett__E2348DBCCD959AD9");

            entity.ToTable("NewsletterSignup");

            entity.HasIndex(e => e.Email, "UQ__Newslett__A9D10534F388D364").IsUnique();

            entity.Property(e => e.Brand)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false);
            entity.Property(e => e.Created)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.Email)
                .IsRequired()
                .HasMaxLength(255);
            entity.Property(e => e.FirstName)
                .IsRequired()
                .HasMaxLength(255);
            entity.Property(e => e.LastName)
                .IsRequired()
                .HasMaxLength(255);
        });

        modelBuilder.Entity<OrderTrackingNumber>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__OrderTra__3214EC075D7F394A");

            entity.ToTable("OrderTrackingNumber");

            entity.Property(e => e.Created)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.OrderNumber)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.PartNumber)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.TrackingNumber)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
        });

        modelBuilder.Entity<PressRelease>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK_MarketingRelease");

            entity.ToTable("PressRelease");

            entity.HasIndex(e => e.SeriesId, "UQ_MarketingRelease").IsUnique();

            entity.Property(e => e.AlternateSummary).HasMaxLength(350);
            entity.Property(e => e.AlternateTitle).HasMaxLength(255);
            entity.Property(e => e.Brand)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasDefaultValueSql("('CT')");
            entity.Property(e => e.Created)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.EmailCopyFileLink)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.ImageFileLink)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Modified)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");

            entity.HasOne(d => d.Series).WithOne(p => p.PressRelease)
                .HasForeignKey<PressRelease>(d => d.SeriesId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_MarketingRelease_Series");
        });

        modelBuilder.Entity<Product>(entity =>
        {
            entity.ToTable("Product");

            entity.HasIndex(e => e.PartNumber, "UQ_Product").IsUnique();

            entity.Property(e => e.CalibrationPrice).HasColumnType("money");
            entity.Property(e => e.CountryOfOrigin)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Created)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(255);
            entity.Property(e => e.EvaluationCharge).HasColumnType("money");
            entity.Property(e => e.HarmonizedCode)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Image1FileLink)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image1FileLinkLg)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image1FileLinkMd)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image1FileLinkTmb)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image2FileLink)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image2FileLinkLg)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image2FileLinkMd)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image2FileLinkTmb)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image3FileLink)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image3FileLinkLg)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image3FileLinkMd)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image3FileLinkTmb)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image4FileLink)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image4FileLinkLg)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image4FileLinkMd)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image4FileLinkTmb)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image5FileLink)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image5FileLinkLg)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image5FileLinkMd)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image5FileLinkTmb)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image6FileLink)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image6FileLinkLg)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image6FileLinkMd)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image6FileLinkTmb)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image7FileLink)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image7FileLinkLg)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image7FileLinkMd)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image7FileLinkTmb)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image8FileLink)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image8FileLinkLg)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image8FileLinkMd)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image8FileLinkTmb)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image9FileLink)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image9FileLinkLg)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image9FileLinkMd)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image9FileLinkTmb)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.ManufacturerLeadTime)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.MasterPartNumber)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Modified)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.Msrp).HasColumnType("money");
            entity.Property(e => e.PackSize)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.PackUom)
                .HasMaxLength(25)
                .IsUnicode(false)
                .HasColumnName("PackUOM");
            entity.Property(e => e.PacksPerCarton)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.PartNumber)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.PoLeadTime)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.RepairPrice).HasColumnType("money");
            entity.Property(e => e.ReplacementPartNumber)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.ShippingCost)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.SpecialHandling)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.TransactionStatus)
                .HasMaxLength(5)
                .IsUnicode(false);
            entity.Property(e => e.UpcCode)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Warranty).HasMaxLength(255);

            entity.HasOne(d => d.Series).WithMany(p => p.Products)
                .HasForeignKey(d => d.SeriesId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Product_Series");
        });

        modelBuilder.Entity<ProductImageArchive>(entity =>
        {
            entity.ToTable("ProductImageArchive");

            entity.Property(e => e.Created).HasColumnType("datetime");
            entity.Property(e => e.Modified).HasColumnType("datetime");
            entity.Property(e => e.ZipFileLink)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
        });

        modelBuilder.Entity<PurchaseOrder>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__Purchase__3214EC071F3D13EC");

            entity.ToTable("PurchaseOrder");

            entity.Property(e => e.AlternateModel)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false)
                .HasDefaultValueSql("('N/A')");
            entity.Property(e => e.BillName)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Created)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.CustomerId)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.CustomerOrderNumber)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Eta).HasColumnType("date");
            entity.Property(e => e.OrderDate).HasColumnType("date");
            entity.Property(e => e.OrderNumber)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.PartNumber)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Price).HasColumnType("money");
            entity.Property(e => e.RevisedEta).HasColumnType("smalldatetime");
            entity.Property(e => e.ShippingMethod)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Status)
                .IsRequired()
                .HasMaxLength(1)
                .IsUnicode(false);
            entity.Property(e => e.TotalSale).HasColumnType("money");
        });

        modelBuilder.Entity<ReportPosdataView>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("Report_POSDataView");

            entity.Property(e => e.City)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Country)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Created).HasColumnType("datetime");
            entity.Property(e => e.CustomerName)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.DistributorName)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.InvoiceNumber)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Msrp)
                .HasColumnType("money")
                .HasColumnName("MSRP");
            entity.Property(e => e.PartNumber)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.ReportPrice).HasColumnType("money");
            entity.Property(e => e.SaleDate).HasColumnType("date");
            entity.Property(e => e.SalePrice).HasColumnType("money");
            entity.Property(e => e.State)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.StreetAddress)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.TotalMsrp)
                .HasColumnType("money")
                .HasColumnName("TotalMSRP");
            entity.Property(e => e.TotalSale).HasColumnType("money");
            entity.Property(e => e.Zipcode)
                .HasMaxLength(255)
                .IsUnicode(false);
        });

        modelBuilder.Entity<ReportPosdatum>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__Report_P__3214EC071A60B0A6");

            entity.ToTable("Report_POSData");

            entity.Property(e => e.City)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Country)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Created)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.CustomerName)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.DistributorName)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.InvoiceNumber)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.PartNumber)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.SaleDate).HasColumnType("date");
            entity.Property(e => e.SalePrice).HasColumnType("money");
            entity.Property(e => e.State)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.StreetAddress)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.TotalSale).HasColumnType("money");
            entity.Property(e => e.Zipcode)
                .HasMaxLength(255)
                .IsUnicode(false);
        });

        modelBuilder.Entity<ReportTlbCurrentPricing>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("Report_TlbCurrentPricing");

            entity.Property(e => e.BasePartNumber)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Connector1).HasMaxLength(255);
            entity.Property(e => e.Connector1CurrentCost).HasColumnType("money");
            entity.Property(e => e.Connector2).HasMaxLength(255);
            entity.Property(e => e.Connector2CurrentCost).HasColumnType("money");
            entity.Property(e => e.Wire)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.WireCurrentCost).HasColumnType("money");
        });

        modelBuilder.Entity<Resource>(entity =>
        {
            entity.ToTable("Resource");

            entity.Property(e => e.Brand)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false);
            entity.Property(e => e.Created)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.Description)
                .HasMaxLength(300)
                .IsUnicode(false);
            entity.Property(e => e.Modified)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.ResourceType)
                .IsRequired()
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.ThumbnailLink)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Title)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.UrlLink)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
        });

        modelBuilder.Entity<ResourcesArticle>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("Resources_Article");

            entity.Property(e => e.Brand)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false);
            entity.Property(e => e.Created).HasColumnType("datetime");
            entity.Property(e => e.Description)
                .IsRequired()
                .HasMaxLength(1024);
            entity.Property(e => e.ResourceType)
                .IsRequired()
                .HasMaxLength(7)
                .IsUnicode(false);
            entity.Property(e => e.ThumbnailLink)
                .IsRequired()
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.Title)
                .IsRequired()
                .HasMaxLength(255);
            entity.Property(e => e.UrlLink)
                .IsRequired()
                .HasMaxLength(271);
        });

        modelBuilder.Entity<ResourcesCategory>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("Resources_Category");

            entity.Property(e => e.Brand)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false);
            entity.Property(e => e.Created).HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(1024);
            entity.Property(e => e.ResourceType)
                .IsRequired()
                .HasMaxLength(8)
                .IsUnicode(false);
            entity.Property(e => e.ThumbnailLink)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Title)
                .IsRequired()
                .HasMaxLength(255);
            entity.Property(e => e.UrlLink)
                .IsRequired()
                .HasMaxLength(269)
                .IsUnicode(false);
        });

        modelBuilder.Entity<ResourcesCompanyDocument>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("Resources_CompanyDocument");

            entity.Property(e => e.Brand)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false);
            entity.Property(e => e.Created).HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(300);
            entity.Property(e => e.ResourceType)
                .IsRequired()
                .HasMaxLength(16)
                .IsUnicode(false);
            entity.Property(e => e.ThumbnailLink)
                .IsRequired()
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.Title)
                .IsRequired()
                .HasMaxLength(255);
            entity.Property(e => e.UrlLink)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
        });

        modelBuilder.Entity<ResourcesGroup>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("Resources_Group");

            entity.Property(e => e.Brand)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false);
            entity.Property(e => e.Created).HasColumnType("datetime");
            entity.Property(e => e.Description)
                .IsRequired()
                .HasMaxLength(350);
            entity.Property(e => e.ResourceType)
                .IsRequired()
                .HasMaxLength(5)
                .IsUnicode(false);
            entity.Property(e => e.ThumbnailLink)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Title)
                .IsRequired()
                .HasMaxLength(255);
            entity.Property(e => e.UrlLink)
                .IsRequired()
                .HasMaxLength(9)
                .IsUnicode(false);
        });

        modelBuilder.Entity<ResourcesPressRelease>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("Resources_PressRelease");

            entity.Property(e => e.Brand)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false);
            entity.Property(e => e.Created).HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(350);
            entity.Property(e => e.ResourceType)
                .IsRequired()
                .HasMaxLength(13)
                .IsUnicode(false);
            entity.Property(e => e.ThumbnailLink)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Title).HasMaxLength(255);
            entity.Property(e => e.UrlLink)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
        });

        modelBuilder.Entity<ResourcesProduct>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("Resources_Product");

            entity.Property(e => e.Brand)
                .HasMaxLength(6)
                .IsUnicode(false);
            entity.Property(e => e.Created).HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(255);
            entity.Property(e => e.ResourceType)
                .IsRequired()
                .HasMaxLength(7)
                .IsUnicode(false);
            entity.Property(e => e.ThumbnailLink)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Title)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.UrlLink)
                .IsRequired()
                .HasMaxLength(264)
                .IsUnicode(false);
        });

        modelBuilder.Entity<ResourcesResource>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("Resources_Resource");

            entity.Property(e => e.Brand)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false);
            entity.Property(e => e.Created).HasColumnType("datetime");
            entity.Property(e => e.Description)
                .HasMaxLength(300)
                .IsUnicode(false);
            entity.Property(e => e.ResourceType)
                .IsRequired()
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.ThumbnailLink)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Title)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.UrlLink)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
        });

        modelBuilder.Entity<ResourcesSeriesAttachment>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("Resources_SeriesAttachment");

            entity.Property(e => e.Brand)
                .HasMaxLength(6)
                .IsUnicode(false);
            entity.Property(e => e.Created).HasColumnType("datetime");
            entity.Property(e => e.Description)
                .IsRequired()
                .HasMaxLength(774);
            entity.Property(e => e.ResourceType)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.ThumbnailLink)
                .IsRequired()
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.Title)
                .IsRequired()
                .HasMaxLength(518)
                .IsUnicode(false);
            entity.Property(e => e.UrlLink)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
        });

        modelBuilder.Entity<ResourcesView>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("ResourcesView");

            entity.Property(e => e.Brand)
                .HasMaxLength(6)
                .IsUnicode(false);
            entity.Property(e => e.Created).HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(1024);
            entity.Property(e => e.ResourceType)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.ThumbnailLink)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Title).HasMaxLength(518);
            entity.Property(e => e.UrlLink)
                .IsRequired()
                .HasMaxLength(271);
        });

        modelBuilder.Entity<RmaForm>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__RmaForm__01C86517F912E7D7");

            entity.ToTable("RmaForm");

            entity.Property(e => e.Address1)
                .IsRequired()
                .HasMaxLength(255);
            entity.Property(e => e.Address2).HasMaxLength(255);
            entity.Property(e => e.Brand)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false);
            entity.Property(e => e.CalibrationServiceChoice)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.City)
                .IsRequired()
                .HasMaxLength(255);
            entity.Property(e => e.Company).HasMaxLength(255);
            entity.Property(e => e.Country)
                .IsRequired()
                .HasMaxLength(60)
                .IsUnicode(false);
            entity.Property(e => e.Created)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.Email)
                .IsRequired()
                .HasMaxLength(255);
            entity.Property(e => e.FirstName)
                .IsRequired()
                .HasMaxLength(255);
            entity.Property(e => e.LastName)
                .IsRequired()
                .HasMaxLength(255);
            entity.Property(e => e.Modified)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.PartNumber)
                .IsRequired()
                .HasMaxLength(255);
            entity.Property(e => e.Phone)
                .IsRequired()
                .HasMaxLength(20);
            entity.Property(e => e.PhoneExt).HasMaxLength(6);
            entity.Property(e => e.PoNumber)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.PostalCode)
                .IsRequired()
                .HasMaxLength(255);
            entity.Property(e => e.PreferredShipAccount)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.PreferredShipMethod)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.ProblemDescription)
                .IsRequired()
                .HasMaxLength(255);
            entity.Property(e => e.ProofOfPurchaseFileLink)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.RepairOptionChoice)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.RmaNumber)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.RmaProductImageFileLink)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.SerialNumber)
                .IsRequired()
                .HasMaxLength(255);
            entity.Property(e => e.StateOrProvince)
                .IsRequired()
                .HasMaxLength(255);
        });

        modelBuilder.Entity<Series>(entity =>
        {
            entity.HasIndex(e => e.Name, "UQ_Series").IsUnique();

            entity.Property(e => e.Brand)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false);
            entity.Property(e => e.Created)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.Image1FileLink)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image1FileLinkLg)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image1FileLinkMd)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image1FileLinkTmb)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image2FileLink)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image2FileLinkLg)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image2FileLinkMd)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image2FileLinkTmb)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image3FileLink)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image3FileLinkLg)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image3FileLinkMd)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image3FileLinkTmb)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image4FileLink)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image4FileLinkLg)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image4FileLinkMd)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image4FileLinkTmb)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image5FileLink)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image5FileLinkLg)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image5FileLinkMd)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image5FileLinkTmb)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image6FileLink)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image6FileLinkLg)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image6FileLinkMd)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image6FileLinkTmb)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image7FileLink)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image7FileLinkLg)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image7FileLinkMd)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image7FileLinkTmb)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image8FileLink)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image8FileLinkLg)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image8FileLinkMd)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image8FileLinkTmb)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image9FileLink)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image9FileLinkLg)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image9FileLinkMd)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image9FileLinkTmb)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.ImageZipFileLink)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Modified)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Title)
                .IsRequired()
                .HasMaxLength(255);
            entity.Property(e => e.UnpublishDate).HasColumnType("datetime");
            entity.Property(e => e.Video1Link)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Video2Link)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Video3Link)
                .HasMaxLength(255)
                .IsUnicode(false);

            entity.HasOne(d => d.Category).WithMany(p => p.Series)
                .HasForeignKey(d => d.CategoryId)
                .HasConstraintName("FK_CategoryId_Category");

            entity.HasOne(d => d.ReplacementSeries).WithMany(p => p.InverseReplacementSeries)
                .HasForeignKey(d => d.ReplacementSeriesId)
                .HasConstraintName("FK_ReplacementSeries_Series");
        });

        modelBuilder.Entity<SeriesAttachment>(entity =>
        {
            entity.ToTable("SeriesAttachment");

            entity.HasIndex(e => new { e.SeriesId, e.Type, e.LanguageCode, e.AttachmentFileLink }, "UQ_SeriesAttachment").IsUnique();

            entity.Property(e => e.AttachmentFileLink)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Created)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.LanguageCode)
                .HasMaxLength(3)
                .IsUnicode(false);
            entity.Property(e => e.Modified)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.Type)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);

            entity.HasOne(d => d.Series).WithMany(p => p.SeriesAttachments)
                .HasForeignKey(d => d.SeriesId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_SeriesAttachment_Series");
        });

        modelBuilder.Entity<SeriesRelated>(entity =>
        {
            entity.ToTable("SeriesRelated");

            entity.HasIndex(e => new { e.SeriesId, e.RelatedSeriesId }, "UQ_SeriesRelated").IsUnique();

            entity.Property(e => e.Created)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.Modified)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");

            entity.HasOne(d => d.RelatedSeries).WithMany(p => p.SeriesRelatedRelatedSeries)
                .HasForeignKey(d => d.RelatedSeriesId)
                .HasConstraintName("FK_SeriesRelated_RelatedSeries");

            entity.HasOne(d => d.Series).WithMany(p => p.SeriesRelatedSeries)
                .HasForeignKey(d => d.SeriesId)
                .HasConstraintName("FK_SeriesRelated_Series");
        });

        modelBuilder.Entity<TestLeadBuilder>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__TestLead__3F5D53B4F3774935");

            entity.ToTable("TestLeadBuilder");

            entity.HasIndex(e => new { e.ConnectorOneId, e.ConnectorTwoId, e.WireId }, "UQ__TestLead__910A754086B6EFBC").IsUnique();

            entity.Property(e => e.ColorIds)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.Created)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.DiagramFileLink)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Modified)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.PartNumber)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Published)
                .IsRequired()
                .HasDefaultValueSql("((1))");

            entity.HasOne(d => d.ConnectorOne).WithMany(p => p.TestLeadBuilderConnectorOnes)
                .HasForeignKey(d => d.ConnectorOneId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__TestLeadB__Conne__190BB0C3");

            entity.HasOne(d => d.ConnectorTwo).WithMany(p => p.TestLeadBuilderConnectorTwos)
                .HasForeignKey(d => d.ConnectorTwoId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__TestLeadB__Conne__19FFD4FC");

            entity.HasOne(d => d.Wire).WithMany(p => p.TestLeadBuilders)
                .HasForeignKey(d => d.WireId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__TestLeadB__WireI__1AF3F935");
        });

        modelBuilder.Entity<TestLeadBuilderQuoteForm>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__TestLead__8E3F06C76BB1FC3E");

            entity.ToTable("TestLeadBuilderQuoteForm");

            entity.Property(e => e.Address1)
                .IsRequired()
                .HasMaxLength(255);
            entity.Property(e => e.Address2).HasMaxLength(255);
            entity.Property(e => e.Brand)
                .IsRequired()
                .HasMaxLength(2)
                .IsUnicode(false);
            entity.Property(e => e.City)
                .IsRequired()
                .HasMaxLength(255);
            entity.Property(e => e.Company).HasMaxLength(255);
            entity.Property(e => e.Country)
                .IsRequired()
                .HasMaxLength(60)
                .IsUnicode(false);
            entity.Property(e => e.Created)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.Email)
                .IsRequired()
                .HasMaxLength(255);
            entity.Property(e => e.FirstName)
                .IsRequired()
                .HasMaxLength(255);
            entity.Property(e => e.LastName)
                .IsRequired()
                .HasMaxLength(255);
            entity.Property(e => e.Message)
                .IsRequired()
                .HasMaxLength(1024);
            entity.Property(e => e.Modified)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.PartNumber)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Phone)
                .IsRequired()
                .HasMaxLength(20);
            entity.Property(e => e.PhoneExt).HasMaxLength(6);
            entity.Property(e => e.PostalCode)
                .IsRequired()
                .HasMaxLength(255);
            entity.Property(e => e.StateOrProvince)
                .IsRequired()
                .HasMaxLength(255);
        });

        modelBuilder.Entity<TestLeadColor>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__TestLead__3214EC072A8750A7");

            entity.ToTable("TestLeadColor");

            entity.HasIndex(e => e.ColorId, "UQ__TestLead__8DA7674C3FF6B002").IsUnique();

            entity.Property(e => e.Created)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.HexCode)
                .IsRequired()
                .HasMaxLength(9)
                .IsUnicode(false);
            entity.Property(e => e.Modified)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(255);
            entity.Property(e => e.Published)
                .IsRequired()
                .HasDefaultValueSql("((1))");
        });

        modelBuilder.Entity<TestLeadConnector>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__TestLead__3214EC072FF44A68");

            entity.ToTable("TestLeadConnector");

            entity.HasIndex(e => e.Type, "UQ__TestLead__F9B8A48B4629A2DF").IsUnique();

            entity.Property(e => e.Cost).HasColumnType("money");
            entity.Property(e => e.Created)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.ImageFileLink)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Information)
                .IsRequired()
                .HasMaxLength(255);
            entity.Property(e => e.Modified)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.Published)
                .IsRequired()
                .HasDefaultValueSql("((1))");
            entity.Property(e => e.Type)
                .IsRequired()
                .HasMaxLength(255);
        });

        modelBuilder.Entity<TestLeadLength>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__TestLead__385FAE4B04F8FD07");

            entity.ToTable("TestLeadLength");

            entity.HasIndex(e => e.Centimeters, "UQ__TestLead__34BDA55424429196").IsUnique();

            entity.Property(e => e.Centimeters).HasColumnType("decimal(18, 0)");
            entity.Property(e => e.Created)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.Modified)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.Published)
                .IsRequired()
                .HasDefaultValueSql("((1))");
        });

        modelBuilder.Entity<TestLeadWire>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__TestLead__3214EC07415C840C");

            entity.ToTable("TestLeadWire");

            entity.HasIndex(e => e.PartNumber, "UQ__TestLead__025D30D94113C976").IsUnique();

            entity.Property(e => e.ConductorAreaMm2)
                .HasColumnType("decimal(19, 4)")
                .HasColumnName("ConductorArea_mm2");
            entity.Property(e => e.CostPerCentimeter).HasColumnType("money");
            entity.Property(e => e.Created)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.Description)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.LeadodIn)
                .HasColumnType("decimal(19, 4)")
                .HasColumnName("Leadod_in");
            entity.Property(e => e.LeadodMm)
                .HasColumnType("decimal(19, 4)")
                .HasColumnName("Leadod_mm");
            entity.Property(e => e.Material)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Modified)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.PartNumber)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Published)
                .IsRequired()
                .HasDefaultValueSql("((1))");
            entity.Property(e => e.StrandDiameterMm)
                .HasColumnType("decimal(19, 4)")
                .HasColumnName("StrandDiameter_mm");
        });

        modelBuilder.Entity<VBaseProductList>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("V_BaseProductList");

            entity.Property(e => e.CalibrationPrice).HasColumnType("money");
            entity.Property(e => e.CountryOfOrigin)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Created).HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(255);
            entity.Property(e => e.EvaluationCharge).HasColumnType("money");
            entity.Property(e => e.HarmonizedCode)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.ImageFileLink)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.ManufacturerLeadTime)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.MasterPartNumber)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Modified).HasColumnType("datetime");
            entity.Property(e => e.Msrp).HasColumnType("money");
            entity.Property(e => e.NewProduct)
                .IsRequired()
                .HasMaxLength(3)
                .IsUnicode(false);
            entity.Property(e => e.PackSize)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.PackUom)
                .HasMaxLength(25)
                .IsUnicode(false)
                .HasColumnName("PackUOM");
            entity.Property(e => e.PacksPerCarton)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.PartNumber)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.PoLeadTime)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.RepairPrice).HasColumnType("money");
            entity.Property(e => e.ShippingCost)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.SpecialHandling)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.TransactionStatus)
                .HasMaxLength(5)
                .IsUnicode(false);
            entity.Property(e => e.UpcCode)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Warranty).HasMaxLength(255);
        });

        modelBuilder.Entity<VImageFileLinkTmb>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("V_ImageFileLinkTmb");

            entity.Property(e => e.ImageFileLink)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.ImageFileLinkLg)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.ImageFileLinkMd)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.ImageFileLinkTmb)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.ProductId).ValueGeneratedOnAdd();
        });

        modelBuilder.Entity<ViewOrderTrackingNumberAggregate>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("View_OrderTrackingNumberAggregate");

            entity.Property(e => e.AlternateModel)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.BillName)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Created).HasColumnType("datetime");
            entity.Property(e => e.CustomerId)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.CustomerOrderNumber)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Eta).HasColumnType("date");
            entity.Property(e => e.OrderDate).HasColumnType("date");
            entity.Property(e => e.OrderNumber)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.PartNumber)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Price).HasColumnType("money");
            entity.Property(e => e.RevisedEta).HasColumnType("smalldatetime");
            entity.Property(e => e.ShippingMethod)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Status)
                .IsRequired()
                .HasMaxLength(1)
                .IsUnicode(false);
            entity.Property(e => e.TotalSale).HasColumnType("money");
            entity.Property(e => e.TrackingNumbers)
                .HasMaxLength(8000)
                .IsUnicode(false);
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
