﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebDB.Data.Context
{
    public class DefaultDbContext : CTEDbContext
    {
        public DefaultDbContext() : base(GetDefaultOptions())
        {
        }
        public DefaultDbContext(DbContextOptions<DefaultDbContext> options)
         : base(options)
        {
        }

        private static DbContextOptions<DefaultDbContext> GetDefaultOptions()
        {
            var connectionString = "Server=localhost\\sqlexpress;Database=ctedb_refactor;Trusted_Connection=True;MultipleActiveResultSets=true;TrustServerCertificate=Yes;";
            var optionsBuilder = new DbContextOptionsBuilder<DefaultDbContext>()
                .UseSqlServer(connectionString);

            return optionsBuilder.Options;
        }
    }
}
