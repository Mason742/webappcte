﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebDB.Data.Models
{
    public partial class DistributorStock
    {
        public static DistributorStock FromCsv(string csvLine)
        {
            string[] values = csvLine.Replace("\"","").Split(',');
            DistributorStock distributorStock = new();
            distributorStock.CustomerId = "";
            distributorStock.PartNumber = values[0];
            distributorStock.Quantity = Convert.ToInt32(values[1]);
            distributorStock.WebsiteUrl = values[2];
            distributorStock.Created = DateTime.Now;
            return distributorStock;
        }
    }
}
