﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace WebDB.Data.PartialModels
{
    public partial class ViewConnectorsBananaPlugsCtAttribute
    {
        public static List<string> GetPropertyNames()
        {
            return typeof(ViewConnectorsBananaPlugsCtAttribute).GetProperties(BindingFlags.Public | BindingFlags.Instance).Select(x => x.Name).ToList();
        }
    }
}
