﻿using System;
using System.Collections.Generic;

namespace WebDB.Data.Models;

public partial class DistributorAuthorizedDomain
{
    public int Id { get; set; }

    public string CustomerId { get; set; }

    public string CompanyName { get; set; }

    public string Domain { get; set; }

    public DateTime Modified { get; set; }

    public DateTime Created { get; set; }
}
