﻿using System;
using System.Collections.Generic;

namespace WebDB.Data.Models;

public partial class AvOscilloscopeCurrentCt
{
    public int? ProductId { get; set; }

    public string Bandwidth { get; set; }

    public string BatteryLife { get; set; }

    public string CurrentDcac { get; set; }

    public string DcmeasurementAccuracy { get; set; }

    public string Dimension { get; set; }

    public string Humidity { get; set; }

    public string MaxCableDiameter { get; set; }

    public string MaxFloatingVoltage { get; set; }

    public string MaxVoltage { get; set; }

    public string MeasurementRanges { get; set; }

    public string Modifier { get; set; }

    public string RiseFallTime { get; set; }

    public string RoHs { get; set; }

    public string Temperature { get; set; }

    public string Type { get; set; }

    public string Weight { get; set; }

    public string Impedance { get; set; }

    public string MaxCurrent { get; set; }

    public int Id { get; set; }

    public int SeriesId { get; set; }

    public int CategoryId { get; set; }

    public string ImageFileLink { get; set; }

    public string PartNumber { get; set; }

    public string Description { get; set; }

    public string Warranty { get; set; }

    public decimal? Msrp { get; set; }

    public string TransactionStatus { get; set; }

    public string SpecialHandling { get; set; }

    public string UpcCode { get; set; }

    public string PoLeadTime { get; set; }

    public string ManufacturerLeadTime { get; set; }

    public string HarmonizedCode { get; set; }

    public string CountryOfOrigin { get; set; }

    public decimal? RepairPrice { get; set; }

    public decimal? EvaluationCharge { get; set; }

    public decimal? CalibrationPrice { get; set; }

    public string ShippingCost { get; set; }

    public string MasterPartNumber { get; set; }

    public string PacksPerCarton { get; set; }

    public string PackSize { get; set; }

    public string PackUom { get; set; }

    public DateTime Modified { get; set; }

    public DateTime Created { get; set; }

    public bool Published { get; set; }

    public bool Discontinued { get; set; }

    public bool RoHsCompliant { get; set; }

    public string NewProduct { get; set; }
}
