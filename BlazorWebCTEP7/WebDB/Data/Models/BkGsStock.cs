﻿using System;
using System.Collections.Generic;

namespace WebDB.Data.Models;

public partial class BkGsStock
{
    public int Id { get; set; }

    public string PartNumber { get; set; }

    public int Quantity { get; set; }

    public DateTime Created { get; set; }
}
