﻿using System;
using System.Collections.Generic;

namespace WebDB.Data.Models;

public partial class ErpInventory
{
    public int Id { get; set; }

    public string PartNumber { get; set; }

    public decimal Price { get; set; }

    public int Quantity { get; set; }

    public int MinimumOrderQuantity { get; set; }

    public int LeadTime { get; set; }

    public string SpecialHandling { get; set; }

    public string Brand { get; set; }

    public DateTime Created { get; set; }
}
