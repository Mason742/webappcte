﻿using System;
using System.Collections.Generic;

namespace WebDB.Data.Models
{
    public partial class ReportProductImage
    {
        public int Id { get; set; }
        public string PartNumber { get; set; }
        public string SeriesName { get; set; }
        public string ImageFileLink { get; set; }
        public int? Position { get; set; }
        public DateTime Modified { get; set; }
        public DateTime Created { get; set; }
    }
}
