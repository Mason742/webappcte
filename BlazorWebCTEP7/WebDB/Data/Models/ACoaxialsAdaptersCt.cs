﻿using System;
using System.Collections.Generic;

namespace WebDB.Data.Models;

public partial class ACoaxialsAdaptersCt
{
    public int? ProductId { get; set; }

    public string ConnectorGrade { get; set; }

    public string FrequencyRange { get; set; }

    public string Impedance { get; set; }

    public string InsertionLoss { get; set; }

    public string RoHs { get; set; }

    public string Temperature { get; set; }

    public string Vswr { get; set; }

    public string MaxCurrent { get; set; }

    public string MaxVoltage { get; set; }

    public string Iec { get; set; }

    public string MaxResistance { get; set; }

    public string Type { get; set; }

    public string CableLength { get; set; }

    public string ConductorArea { get; set; }

    public string JacketMaterial { get; set; }

    public string Stranding { get; set; }

    public string WireGauge { get; set; }

    public string Color { get; set; }

    public string CenterContactResistance { get; set; }

    public string WorkingVoltage { get; set; }

    public string Frequency { get; set; }

    public int Id { get; set; }

    public string Type1 { get; set; }

    public virtual Product Product { get; set; }
}
