﻿using System;
using System.Collections.Generic;

namespace WebDB.Data.Models;

public partial class ArticleSeries
{
    public int Id { get; set; }

    public int ArticleId { get; set; }

    public int SeriesId { get; set; }

    public bool ShowOnSeries { get; set; }

    public virtual Article Article { get; set; }

    public virtual Series Series { get; set; }
}
