﻿using System;
using System.Collections.Generic;

namespace WebDB.Data.Models;

public partial class Series
{
    public int Id { get; set; }

    public int? CategoryId { get; set; }

    public string Name { get; set; }

    public string Title { get; set; }

    public string Summary { get; set; }

    public string SpecificationsLegacy { get; set; }

    public string Description { get; set; }

    public string Brand { get; set; }

    public DateTime Modified { get; set; }

    public DateTime Created { get; set; }

    public bool Discontinued { get; set; }

    public DateTime? UnpublishDate { get; set; }

    public int? ReplacementSeriesId { get; set; }

    public bool Published { get; set; }

    public string Video1Link { get; set; }

    public string Video2Link { get; set; }

    public string Video3Link { get; set; }

    public string Image1FileLink { get; set; }

    public string Image1FileLinkLg { get; set; }

    public string Image1FileLinkMd { get; set; }

    public string Image1FileLinkTmb { get; set; }

    public string Image2FileLink { get; set; }

    public string Image2FileLinkLg { get; set; }

    public string Image2FileLinkMd { get; set; }

    public string Image2FileLinkTmb { get; set; }

    public string Image3FileLink { get; set; }

    public string Image3FileLinkLg { get; set; }

    public string Image3FileLinkMd { get; set; }

    public string Image3FileLinkTmb { get; set; }

    public string Image4FileLink { get; set; }

    public string Image4FileLinkLg { get; set; }

    public string Image4FileLinkMd { get; set; }

    public string Image4FileLinkTmb { get; set; }

    public string Image5FileLink { get; set; }

    public string Image5FileLinkLg { get; set; }

    public string Image5FileLinkMd { get; set; }

    public string Image5FileLinkTmb { get; set; }

    public string Image6FileLink { get; set; }

    public string Image6FileLinkLg { get; set; }

    public string Image6FileLinkMd { get; set; }

    public string Image6FileLinkTmb { get; set; }

    public string Image7FileLink { get; set; }

    public string Image7FileLinkLg { get; set; }

    public string Image7FileLinkMd { get; set; }

    public string Image7FileLinkTmb { get; set; }

    public string Image8FileLink { get; set; }

    public string Image8FileLinkLg { get; set; }

    public string Image8FileLinkMd { get; set; }

    public string Image8FileLinkTmb { get; set; }

    public string Image9FileLink { get; set; }

    public string Image9FileLinkLg { get; set; }

    public string Image9FileLinkMd { get; set; }

    public string Image9FileLinkTmb { get; set; }

    public string ImageZipFileLink { get; set; }

    public virtual ICollection<ArticleSeries> ArticleSeries { get; } = new List<ArticleSeries>();

    public virtual Category Category { get; set; }

    public virtual ICollection<Series> InverseReplacementSeries { get; } = new List<Series>();

    public virtual PressRelease PressRelease { get; set; }

    public virtual ICollection<Product> Products { get; } = new List<Product>();

    public virtual Series ReplacementSeries { get; set; }

    public virtual ICollection<SeriesAttachment> SeriesAttachments { get; } = new List<SeriesAttachment>();

    public virtual ICollection<SeriesRelated> SeriesRelatedRelatedSeries { get; } = new List<SeriesRelated>();

    public virtual ICollection<SeriesRelated> SeriesRelatedSeries { get; } = new List<SeriesRelated>();
}
