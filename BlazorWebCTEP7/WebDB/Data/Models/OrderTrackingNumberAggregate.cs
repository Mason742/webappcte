﻿using System;
using System.Collections.Generic;

namespace WebDB.Data.Models
{
    public partial class OrderTrackingNumberAggregate
    {
        public int Id { get; set; }
        public string CustomerId { get; set; } = null!;
        public string BillName { get; set; } = null!;
        public string OrderNumber { get; set; } = null!;
        public string Status { get; set; } = null!;
        public string CustomerOrderNumber { get; set; } = null!;
        public DateTime OrderDate { get; set; }
        public decimal TotalSale { get; set; }
        public decimal Price { get; set; }
        public string PartNumber { get; set; } = null!;
        public string AlternateModel { get; set; } = null!;
        public int QuantityOrdered { get; set; }
        public int QuantityShipped { get; set; }
        public DateTime Eta { get; set; }
        public DateTime RevisedEta { get; set; }
        public string ShippingMethod { get; set; } = null!;
        public DateTime Created { get; set; }
        public string TrackingNumbers { get; set; }
    }
}
