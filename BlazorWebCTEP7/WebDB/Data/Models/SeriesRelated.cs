﻿using System;
using System.Collections.Generic;

namespace WebDB.Data.Models;

public partial class SeriesRelated
{
    public int Id { get; set; }

    public int? SeriesId { get; set; }

    public int? RelatedSeriesId { get; set; }

    public DateTime Modified { get; set; }

    public DateTime Created { get; set; }

    public virtual Series RelatedSeries { get; set; }

    public virtual Series Series { get; set; }
}
