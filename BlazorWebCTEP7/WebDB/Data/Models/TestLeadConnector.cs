﻿using System;
using System.Collections.Generic;

namespace WebDB.Data.Models;

public partial class TestLeadConnector
{
    public int Id { get; set; }

    public string Type { get; set; }

    public decimal Cost { get; set; }

    public string ImageFileLink { get; set; }

    public string Information { get; set; }

    public int Rating { get; set; }

    public bool? Published { get; set; }

    public DateTime Modified { get; set; }

    public DateTime Created { get; set; }

    public virtual ICollection<TestLeadBuilder> TestLeadBuilderConnectorOnes { get; } = new List<TestLeadBuilder>();

    public virtual ICollection<TestLeadBuilder> TestLeadBuilderConnectorTwos { get; } = new List<TestLeadBuilder>();
}
