﻿using System;
using System.Collections.Generic;

namespace WebDB.Data.Models;

public partial class AAlligatorCtImport
{
    public int? Id { get; set; }

    public int? ProductId { get; set; }

    public string PartNumber { get; set; }

    public string IecRating { get; set; }

    public string MaxCurrent { get; set; }

    public string RoHs2 { get; set; }

    public string OperatingTemperature { get; set; }

    public string Color { get; set; }

    public string NonCategoryMaxVoltage { get; set; }

    public string Material { get; set; }

    public string Type { get; set; }

    public string JawOpening { get; set; }

    public string Connector { get; set; }

    public string InsulatorMaterial { get; set; }

    public string Notes { get; set; }
}
