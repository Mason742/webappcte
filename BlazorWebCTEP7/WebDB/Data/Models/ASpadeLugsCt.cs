﻿using System;
using System.Collections.Generic;

namespace WebDB.Data.Models;

public partial class ASpadeLugsCt
{
    public int? ProductId { get; set; }

    public string Color { get; set; }

    public string Iec { get; set; }

    public string MaxCurrent { get; set; }

    public string MaxResistance { get; set; }

    public string RoHs { get; set; }

    public string Temperature { get; set; }

    public int Id { get; set; }

    public string Type { get; set; }

    public virtual Product Product { get; set; }
}
