﻿using System;
using System.Collections.Generic;

namespace WebDB.Data.Models;

public partial class PurchaseOrder
{
    public int Id { get; set; }

    public string CustomerId { get; set; }

    public string BillName { get; set; }

    public string OrderNumber { get; set; }

    public string Status { get; set; }

    public string CustomerOrderNumber { get; set; }

    public DateTime OrderDate { get; set; }

    public decimal TotalSale { get; set; }

    public decimal Price { get; set; }

    public string PartNumber { get; set; }

    public string AlternateModel { get; set; }

    public int QuantityOrdered { get; set; }

    public int QuantityShipped { get; set; }

    public DateTime Eta { get; set; }

    public DateTime? RevisedEta { get; set; }

    public string ShippingMethod { get; set; }

    public DateTime Created { get; set; }
}
