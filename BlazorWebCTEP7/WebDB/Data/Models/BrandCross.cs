﻿using System;
using System.Collections.Generic;

namespace WebDB.Data.Models
{
    public partial class BrandCross
    {
        public int Id { get; set; }
        public string Manufacturer { get; set; }
        public string CompetitorPartNumber { get; set; }
        public string CrossDescription { get; set; }
        public string PartNumber { get; set; }
        public string Code { get; set; }
        public string Comments { get; set; }
        public bool? Published { get; set; }
        public DateTime Modified { get; set; }
        public DateTime Created { get; set; }
    }
}
