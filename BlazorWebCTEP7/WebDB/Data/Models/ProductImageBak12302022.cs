﻿using System;
using System.Collections.Generic;

namespace WebDB.Data.Models
{
    public partial class ProductImageBak12302022
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public string ImageFileLink { get; set; }
        public string ImageFileLinkLg { get; set; }
        public string ImageFileLinkMd { get; set; }
        public string ImageFileLinkTmb { get; set; }
        public int Position { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
    }
}
