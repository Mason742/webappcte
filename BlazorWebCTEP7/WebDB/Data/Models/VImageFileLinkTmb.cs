﻿using System;
using System.Collections.Generic;

namespace WebDB.Data.Models;

public partial class VImageFileLinkTmb
{
    public int ProductId { get; set; }

    public string ImageFileLinkTmb { get; set; }

    public string ImageFileLinkMd { get; set; }

    public string ImageFileLinkLg { get; set; }

    public string ImageFileLink { get; set; }

    public int Position { get; set; }
}
