﻿using System;
using System.Collections.Generic;

namespace WebDB.Data.Models;

public partial class ReportPosdataView
{
    public int Id { get; set; }

    public string DistributorName { get; set; }

    public string PartNumber { get; set; }

    public int SaleQuantity { get; set; }

    public DateTime SaleDate { get; set; }

    public string CustomerName { get; set; }

    public string StreetAddress { get; set; }

    public string City { get; set; }

    public string State { get; set; }

    public string Zipcode { get; set; }

    public string Country { get; set; }

    public decimal SalePrice { get; set; }

    public decimal TotalSale { get; set; }

    public string InvoiceNumber { get; set; }

    public DateTime Created { get; set; }

    public decimal? Msrp { get; set; }

    public decimal? TotalMsrp { get; set; }

    public decimal? ReportPrice { get; set; }
}
