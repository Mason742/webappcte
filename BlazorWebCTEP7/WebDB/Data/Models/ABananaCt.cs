﻿using System;
using System.Collections.Generic;

namespace WebDB.Data.Models;

public partial class ABananaCt
{
    public int Id { get; set; }

    public int ProductId { get; set; }

    public string Color { get; set; }

    public string Iec { get; set; }

    public string MaxCurrent { get; set; }

    public string MaxResistance { get; set; }

    public string RoHs { get; set; }

    public string Temperature { get; set; }

    public string Ulrecognized { get; set; }

    public string MaxVoltage { get; set; }

    public string Modifier { get; set; }

    public string Impedance { get; set; }

    public string MaxFloatingVoltage { get; set; }

    public string ContactMaterial { get; set; }

    public string InsulationMaterial { get; set; }

    public string Magnet { get; set; }

    public string BodyMaterial { get; set; }

    public string Ce { get; set; }

    public string Voltage { get; set; }

    public string Type { get; set; }

    public string Type1 { get; set; }

    public virtual Product Product { get; set; }
}
