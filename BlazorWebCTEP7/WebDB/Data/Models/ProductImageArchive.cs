﻿using System;
using System.Collections.Generic;

namespace WebDB.Data.Models;

public partial class ProductImageArchive
{
    public int Id { get; set; }

    public int ProductId { get; set; }

    public string ZipFileLink { get; set; }

    public DateTime Modified { get; set; }

    public DateTime Created { get; set; }
}
