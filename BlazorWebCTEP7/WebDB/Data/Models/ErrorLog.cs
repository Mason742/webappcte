﻿using System;
using System.Collections.Generic;

namespace WebDB.Data.Models;

public partial class ErrorLog
{
    public int Id { get; set; }

    public string Values { get; set; }

    public DateTimeOffset Created { get; set; }
}
