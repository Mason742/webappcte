﻿using System;
using System.Collections.Generic;

namespace WebDB.Data.Models;

public partial class AuthorizedDistributor
{
    public int Id { get; set; }

    public string CustomerId { get; set; }

    public string Name { get; set; }

    public string City { get; set; }

    public string State { get; set; }

    public string Country { get; set; }

    public string Phone { get; set; }

    public string TollFreePhone { get; set; }

    public string Contact { get; set; }

    public string WebsiteUrl { get; set; }

    public string WebsiteUrlGs { get; set; }

    public string LogoImageFileLink { get; set; }

    public string AccountType { get; set; }

    public string Brand { get; set; }

    public bool? Published { get; set; }

    public DateTime Modified { get; set; }

    public DateTime Created { get; set; }
}
