﻿using System;
using System.Collections.Generic;

namespace WebDB.Data.Models;

public partial class Group
{
    public int Id { get; set; }

    public string Name { get; set; }

    public string Description { get; set; }

    public string ImageFileLink { get; set; }

    public string Brand { get; set; }

    public DateTime Modified { get; set; }

    public DateTime Created { get; set; }

    public virtual ICollection<CategoryGroup> CategoryGroups { get; } = new List<CategoryGroup>();
}
