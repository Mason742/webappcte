﻿using System;
using System.Collections.Generic;

namespace WebDB.Data.Models;

public partial class Category
{
    public int Id { get; set; }

    public string TableName { get; set; }

    public string DisplayName { get; set; }

    public string Description { get; set; }

    public string ImageFileLink { get; set; }

    public string Brand { get; set; }

    public DateTime Modified { get; set; }

    public DateTime Created { get; set; }

    public virtual ICollection<CategoryGroup> CategoryGroups { get; } = new List<CategoryGroup>();

    public virtual ICollection<Series> Series { get; } = new List<Series>();
}
