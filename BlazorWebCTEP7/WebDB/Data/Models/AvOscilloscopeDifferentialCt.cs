﻿using System;
using System.Collections.Generic;

namespace WebDB.Data.Models;

public partial class AvOscilloscopeDifferentialCt
{
    public int? ProductId { get; set; }

    public string Accuracy { get; set; }

    public string Altitude { get; set; }

    public string Attenuation { get; set; }

    public string Bandwidth { get; set; }

    public string BnccableLength { get; set; }

    public string Dimension { get; set; }

    public string Iec { get; set; }

    public string InputImpedance { get; set; }

    public string InputLeadsLength { get; set; }

    public string InputVoltageAbsoluteMaximumRated { get; set; }

    public string InputVoltageMaximumCommonMode { get; set; }

    public string InputVoltageMaximumDifferential { get; set; }

    public string OperatingTemperature { get; set; }

    public string OutputVoltageSourceImpedance { get; set; }

    public string OutputVoltageSwing { get; set; }

    public string OutputVoltageTypicalNoise { get; set; }

    public string OutputVoltageTypicalOffset { get; set; }

    public string PollutionDegree { get; set; }

    public string RiseTime { get; set; }

    public string RoHs { get; set; }

    public string StorageTemperature { get; set; }

    public string Weight { get; set; }

    public string Humidity { get; set; }

    public string Temperature { get; set; }

    public string Type { get; set; }

    public string TypicalCmrr { get; set; }

    public string Power { get; set; }

    public string ArCmrr { get; set; }

    public string CableLength { get; set; }

    public string PowerConsumption { get; set; }

    public string PowerSupply { get; set; }

    public string Capacitance { get; set; }

    public string InputConnectorLength { get; set; }

    public string InputResistance { get; set; }

    public string Modifier { get; set; }

    public string AcCmrr { get; set; }

    public int Id { get; set; }

    public int SeriesId { get; set; }

    public int CategoryId { get; set; }

    public string ImageFileLink { get; set; }

    public string PartNumber { get; set; }

    public string Description { get; set; }

    public string Warranty { get; set; }

    public decimal? Msrp { get; set; }

    public string TransactionStatus { get; set; }

    public string SpecialHandling { get; set; }

    public string UpcCode { get; set; }

    public string PoLeadTime { get; set; }

    public string ManufacturerLeadTime { get; set; }

    public string HarmonizedCode { get; set; }

    public string CountryOfOrigin { get; set; }

    public decimal? RepairPrice { get; set; }

    public decimal? EvaluationCharge { get; set; }

    public decimal? CalibrationPrice { get; set; }

    public string ShippingCost { get; set; }

    public string MasterPartNumber { get; set; }

    public string PacksPerCarton { get; set; }

    public string PackSize { get; set; }

    public string PackUom { get; set; }

    public DateTime Modified { get; set; }

    public DateTime Created { get; set; }

    public bool Published { get; set; }

    public bool Discontinued { get; set; }

    public bool RoHsCompliant { get; set; }

    public string Type1 { get; set; }

    public string NewProduct { get; set; }
}
