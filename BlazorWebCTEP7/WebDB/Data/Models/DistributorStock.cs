﻿using System;
using System.Collections.Generic;

namespace WebDB.Data.Models;

public partial class DistributorStock
{
    public int Id { get; set; }

    public string CustomerId { get; set; }

    public string PartNumber { get; set; }

    public int Quantity { get; set; }

    public string WebsiteUrl { get; set; }

    public DateTime Modified { get; set; }

    public DateTime Created { get; set; }
}
