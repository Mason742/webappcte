﻿using System;
using System.Collections.Generic;

namespace WebDB.Data.Models;

public partial class ADigitalMultimeterCt
{
    public int Id { get; set; }

    public int ProductId { get; set; }

    public string Iec { get; set; }

    public string MaxCurrent { get; set; }

    public string RoHs { get; set; }

    public string Temperature { get; set; }

    public string InterruptingRating { get; set; }

    public string MaxVoltage { get; set; }

    public string Color { get; set; }

    public string MaxResistance { get; set; }

    public string Impedance { get; set; }

    public string MeterImpedance { get; set; }

    public string TempCoefficient { get; set; }

    public string Dimension { get; set; }

    public string Voltage { get; set; }

    public string Type { get; set; }

    public virtual Product Product { get; set; }
}
