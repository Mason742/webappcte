﻿using System;
using System.Collections.Generic;

namespace WebDB.Data.Models;

public partial class CategoryGroup
{
    public int Id { get; set; }

    public int CategoryId { get; set; }

    public int GroupId { get; set; }

    public virtual Category Category { get; set; }

    public virtual Group Group { get; set; }
}
