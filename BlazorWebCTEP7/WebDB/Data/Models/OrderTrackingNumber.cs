﻿using System;
using System.Collections.Generic;

namespace WebDB.Data.Models;

public partial class OrderTrackingNumber
{
    public int Id { get; set; }

    public string PartNumber { get; set; }

    public string OrderNumber { get; set; }

    public string TrackingNumber { get; set; }

    public DateTime Created { get; set; }
}
