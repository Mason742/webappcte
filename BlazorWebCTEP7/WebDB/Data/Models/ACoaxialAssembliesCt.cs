﻿using System;
using System.Collections.Generic;

namespace WebDB.Data.Models;

public partial class ACoaxialAssembliesCt
{
    public int? ProductId { get; set; }

    public string Bandwidth { get; set; }

    public string CableType { get; set; }

    public string ConnectorGrade { get; set; }

    public string Impedance { get; set; }

    public string InsertionLoss { get; set; }

    public string Length { get; set; }

    public string RoHs { get; set; }

    public string Vswr { get; set; }

    public string MaxCurrent { get; set; }

    public string MaxVoltage { get; set; }

    public string Temperature { get; set; }

    public string BodyMaterial { get; set; }

    public string Color { get; set; }

    public string FrequencyRange { get; set; }

    public string Iec { get; set; }

    public string JacketMaterial { get; set; }

    public string ContactMaterial { get; set; }

    public string InsulationMaterial { get; set; }

    public int Id { get; set; }

    public string Type { get; set; }

    public string Type1 { get; set; }

    public virtual Product Product { get; set; }
}
