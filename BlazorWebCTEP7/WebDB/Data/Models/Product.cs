﻿using System;
using System.Collections.Generic;

namespace WebDB.Data.Models;

public partial class Product
{
    public int Id { get; set; }

    public string PartNumber { get; set; }

    public string Description { get; set; }

    public string Warranty { get; set; }

    public int? Quantity { get; set; }

    public int? MinimumOrderQuantity { get; set; }

    public int? LeadTime { get; set; }

    public decimal? Msrp { get; set; }

    public string TransactionStatus { get; set; }

    public string SpecialHandling { get; set; }

    public string UpcCode { get; set; }

    public string PoLeadTime { get; set; }

    public string ManufacturerLeadTime { get; set; }

    public string HarmonizedCode { get; set; }

    public string CountryOfOrigin { get; set; }

    public decimal? RepairPrice { get; set; }

    public decimal? EvaluationCharge { get; set; }

    public decimal? CalibrationPrice { get; set; }

    public string ShippingCost { get; set; }

    public string MasterPartNumber { get; set; }

    public string PacksPerCarton { get; set; }

    public string PackSize { get; set; }

    public string PackUom { get; set; }

    public string ReplacementPartNumber { get; set; }

    public DateTime Modified { get; set; }

    public DateTime Created { get; set; }

    public bool Published { get; set; }

    public bool Discontinued { get; set; }

    public bool RoHsCompliant { get; set; }

    public int SeriesId { get; set; }

    public string Image1FileLink { get; set; }

    public string Image1FileLinkLg { get; set; }

    public string Image1FileLinkMd { get; set; }

    public string Image1FileLinkTmb { get; set; }

    public string Image2FileLink { get; set; }

    public string Image2FileLinkLg { get; set; }

    public string Image2FileLinkMd { get; set; }

    public string Image2FileLinkTmb { get; set; }

    public string Image3FileLink { get; set; }

    public string Image3FileLinkLg { get; set; }

    public string Image3FileLinkMd { get; set; }

    public string Image3FileLinkTmb { get; set; }

    public string Image4FileLink { get; set; }

    public string Image4FileLinkLg { get; set; }

    public string Image4FileLinkMd { get; set; }

    public string Image4FileLinkTmb { get; set; }

    public string Image5FileLink { get; set; }

    public string Image5FileLinkLg { get; set; }

    public string Image5FileLinkMd { get; set; }

    public string Image5FileLinkTmb { get; set; }

    public string Image6FileLink { get; set; }

    public string Image6FileLinkLg { get; set; }

    public string Image6FileLinkMd { get; set; }

    public string Image6FileLinkTmb { get; set; }

    public string Image7FileLink { get; set; }

    public string Image7FileLinkLg { get; set; }

    public string Image7FileLinkMd { get; set; }

    public string Image7FileLinkTmb { get; set; }

    public string Image8FileLink { get; set; }

    public string Image8FileLinkLg { get; set; }

    public string Image8FileLinkMd { get; set; }

    public string Image8FileLinkTmb { get; set; }

    public string Image9FileLink { get; set; }

    public string Image9FileLinkLg { get; set; }

    public string Image9FileLinkMd { get; set; }

    public string Image9FileLinkTmb { get; set; }

    public virtual Series Series { get; set; }
}
