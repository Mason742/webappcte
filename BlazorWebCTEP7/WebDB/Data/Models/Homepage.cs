﻿using System;
using System.Collections.Generic;

namespace WebDB.Data.Models;

public partial class Homepage
{
    public int Id { get; set; }

    public string Type { get; set; }

    public string Title { get; set; }

    public string Subtitle { get; set; }

    public string Summary { get; set; }

    public string MediaFileLink { get; set; }

    public string Url { get; set; }

    public string Brand { get; set; }

    public DateTime Modified { get; set; }

    public DateTime Created { get; set; }
}
