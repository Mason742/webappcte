﻿using System;
using System.Collections.Generic;

namespace WebDB.Data.Models;

public partial class CompanyDocument
{
    public int Id { get; set; }

    public string Title { get; set; }

    public string Description { get; set; }

    public string FileLink { get; set; }

    public string Brand { get; set; }

    public bool? Published { get; set; }

    public DateTime Modified { get; set; }

    public DateTime Created { get; set; }
}
