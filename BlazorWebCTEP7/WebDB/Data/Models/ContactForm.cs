﻿using System;
using System.Collections.Generic;

namespace WebDB.Data.Models;

public partial class ContactForm
{
    public int Id { get; set; }

    public string FirstName { get; set; }

    public string LastName { get; set; }

    public string Email { get; set; }

    public string Phone { get; set; }

    public string PhoneExt { get; set; }

    public string Company { get; set; }

    public string Address1 { get; set; }

    public string Address2 { get; set; }

    public string City { get; set; }

    public string StateOrProvince { get; set; }

    public string PostalCode { get; set; }

    public string Country { get; set; }

    public string ContactReasonChoice { get; set; }

    public string Message { get; set; }

    public string Brand { get; set; }

    public bool EmailConfirmationSent { get; set; }

    public bool HubSpotTicketCreated { get; set; }

    public DateTime Modified { get; set; }

    public DateTime Created { get; set; }
}
