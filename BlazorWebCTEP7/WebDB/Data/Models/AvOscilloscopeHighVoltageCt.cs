﻿using System;
using System.Collections.Generic;

namespace WebDB.Data.Models;

public partial class AvOscilloscopeHighVoltageCt
{
    public int? ProductId { get; set; }

    public string AccuracyVac { get; set; }

    public string AccuracyVdc { get; set; }

    public string Attenuation { get; set; }

    public string Bandwidth { get; set; }

    public string Capacitance { get; set; }

    public string CompensationRange { get; set; }

    public string Dimension { get; set; }

    public string Humidity { get; set; }

    public string Impedance { get; set; }

    public string Length { get; set; }

    public string MaxVoltage { get; set; }

    public string MaxVoltageAcrms { get; set; }

    public string OutputVoltageSourceImpedance { get; set; }

    public string ReadoutActuator { get; set; }

    public string RiseTime { get; set; }

    public string RoHs { get; set; }

    public string StorageTemperature { get; set; }

    public string TempCoefficient { get; set; }

    public string Temperature { get; set; }

    public string Type { get; set; }

    public string Weight { get; set; }

    public string CableLength { get; set; }

    public string InputImpedance { get; set; }

    public string InputCapacitance { get; set; }

    public string MaxCurrentLoading { get; set; }

    public string OperatingTemperature { get; set; }

    public string OutputImpedance { get; set; }

    public string SignalNoise { get; set; }

    public string Frequency { get; set; }

    public int Id { get; set; }

    public int SeriesId { get; set; }

    public int CategoryId { get; set; }

    public string ImageFileLink { get; set; }

    public string PartNumber { get; set; }

    public string Description { get; set; }

    public string Warranty { get; set; }

    public decimal? Msrp { get; set; }

    public string TransactionStatus { get; set; }

    public string SpecialHandling { get; set; }

    public string UpcCode { get; set; }

    public string PoLeadTime { get; set; }

    public string ManufacturerLeadTime { get; set; }

    public string HarmonizedCode { get; set; }

    public string CountryOfOrigin { get; set; }

    public decimal? RepairPrice { get; set; }

    public decimal? EvaluationCharge { get; set; }

    public decimal? CalibrationPrice { get; set; }

    public string ShippingCost { get; set; }

    public string MasterPartNumber { get; set; }

    public string PacksPerCarton { get; set; }

    public string PackSize { get; set; }

    public string PackUom { get; set; }

    public DateTime Modified { get; set; }

    public DateTime Created { get; set; }

    public bool Published { get; set; }

    public bool Discontinued { get; set; }

    public bool RoHsCompliant { get; set; }

    public string Type1 { get; set; }

    public string NewProduct { get; set; }
}
