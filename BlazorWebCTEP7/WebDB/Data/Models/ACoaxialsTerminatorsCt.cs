﻿using System;
using System.Collections.Generic;

namespace WebDB.Data.Models;

public partial class ACoaxialsTerminatorsCt
{
    public int? ProductId { get; set; }

    public string Accuracy { get; set; }

    public string AveragePower { get; set; }

    public string ConnectorGrade { get; set; }

    public string FrequencyRange { get; set; }

    public string Impedance { get; set; }

    public string InputVoltage { get; set; }

    public string RoHs { get; set; }

    public string Temperature { get; set; }

    public string Vswr { get; set; }

    public string Ztolerance { get; set; }

    public string Length { get; set; }

    public int Id { get; set; }

    public string Type { get; set; }

    public virtual Product Product { get; set; }
}
