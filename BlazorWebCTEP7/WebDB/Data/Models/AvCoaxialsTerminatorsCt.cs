﻿using System;
using System.Collections.Generic;

namespace WebDB.Data.Models;

public partial class AvCoaxialsTerminatorsCt
{
    public int? ProductId { get; set; }

    public string Accuracy { get; set; }

    public string AveragePower { get; set; }

    public string ConnectorGrade { get; set; }

    public string FrequencyRange { get; set; }

    public string Impedance { get; set; }

    public string InputVoltage { get; set; }

    public string RoHs { get; set; }

    public string Temperature { get; set; }

    public string Vswr { get; set; }

    public string Ztolerance { get; set; }

    public string Length { get; set; }

    public int Id { get; set; }

    public int SeriesId { get; set; }

    public int CategoryId { get; set; }

    public string ImageFileLink { get; set; }

    public string PartNumber { get; set; }

    public string Description { get; set; }

    public string Warranty { get; set; }

    public decimal? Msrp { get; set; }

    public string TransactionStatus { get; set; }

    public string SpecialHandling { get; set; }

    public string UpcCode { get; set; }

    public string PoLeadTime { get; set; }

    public string ManufacturerLeadTime { get; set; }

    public string HarmonizedCode { get; set; }

    public string CountryOfOrigin { get; set; }

    public decimal? RepairPrice { get; set; }

    public decimal? EvaluationCharge { get; set; }

    public decimal? CalibrationPrice { get; set; }

    public string ShippingCost { get; set; }

    public string MasterPartNumber { get; set; }

    public string PacksPerCarton { get; set; }

    public string PackSize { get; set; }

    public string PackUom { get; set; }

    public DateTime Modified { get; set; }

    public DateTime Created { get; set; }

    public bool Published { get; set; }

    public bool Discontinued { get; set; }

    public bool RoHsCompliant { get; set; }

    public string Type { get; set; }

    public string NewProduct { get; set; }
}
