﻿using System;
using System.Collections.Generic;

namespace WebDB.Data.Models;

public partial class AOscilloscopeCurrentCt
{
    public int? ProductId { get; set; }

    public string Bandwidth { get; set; }

    public string BatteryLife { get; set; }

    public string CurrentDcac { get; set; }

    public string DcmeasurementAccuracy { get; set; }

    public string Dimension { get; set; }

    public string Humidity { get; set; }

    public string MaxCableDiameter { get; set; }

    public string MaxFloatingVoltage { get; set; }

    public string MaxVoltage { get; set; }

    public string MeasurementRanges { get; set; }

    public string Modifier { get; set; }

    public string RiseFallTime { get; set; }

    public string RoHs { get; set; }

    public string Temperature { get; set; }

    public string Type { get; set; }

    public string Weight { get; set; }

    public string Impedance { get; set; }

    public string MaxCurrent { get; set; }

    public int Id { get; set; }

    public virtual Product Product { get; set; }
}
