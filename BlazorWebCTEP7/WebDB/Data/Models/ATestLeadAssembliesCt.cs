﻿using System;
using System.Collections.Generic;

namespace WebDB.Data.Models;

public partial class ATestLeadAssembliesCt
{
    public int? ProductId { get; set; }

    public string ConductorArea { get; set; }

    public string Iec { get; set; }

    public string JacketMaterial { get; set; }

    public string Length { get; set; }

    public string MaxCurrent { get; set; }

    public string MaxResistance { get; set; }

    public string Od { get; set; }

    public string RoHs { get; set; }

    public string Stranding { get; set; }

    public string Temperature { get; set; }

    public string WireGauge { get; set; }

    public string Color { get; set; }

    public string JacketOd { get; set; }

    public string Ulrating { get; set; }

    public string MaxVoltage { get; set; }

    public string Type { get; set; }

    public string CableType { get; set; }

    public string CableLength { get; set; }

    public string Impedance { get; set; }

    public string VoltageRating { get; set; }

    public string ContactMaterial { get; set; }

    public string InsulationMaterial { get; set; }

    public string Ul94flameRating { get; set; }

    public int Id { get; set; }

    public string Type1 { get; set; }

    public virtual Product Product { get; set; }
}
