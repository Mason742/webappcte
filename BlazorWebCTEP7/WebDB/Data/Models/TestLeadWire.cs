﻿using System;
using System.Collections.Generic;

namespace WebDB.Data.Models;

public partial class TestLeadWire
{
    public int Id { get; set; }

    public string PartNumber { get; set; }

    public string Description { get; set; }

    public int MaxCurrent { get; set; }

    public string Material { get; set; }

    public decimal ConductorAreaMm2 { get; set; }

    public int Gauge { get; set; }

    public int Strands { get; set; }

    public decimal StrandDiameterMm { get; set; }

    public decimal LeadodMm { get; set; }

    public decimal LeadodIn { get; set; }

    public decimal CostPerCentimeter { get; set; }

    public bool? Published { get; set; }

    public DateTime? Modified { get; set; }

    public DateTime Created { get; set; }

    public virtual ICollection<TestLeadBuilder> TestLeadBuilders { get; } = new List<TestLeadBuilder>();
}
