﻿using System;
using System.Collections.Generic;

namespace WebDB.Data.Models;

public partial class Article
{
    public int Id { get; set; }

    public string Url { get; set; }

    public string Title { get; set; }

    public string Summary { get; set; }

    public string BodyHtml { get; set; }

    public string Brand { get; set; }

    public DateTime Modified { get; set; }

    public bool? Published { get; set; }

    public DateTime Created { get; set; }

    public virtual ICollection<ArticleSeries> ArticleSeries { get; } = new List<ArticleSeries>();
}
