﻿using System;
using System.Collections.Generic;

namespace WebDB.Data.Models;

public partial class TestLeadBuilderBak
{
    public int Id { get; set; }

    public string PartNumber { get; set; }

    public int ConnectorOneId { get; set; }

    public int ConnectorTwoId { get; set; }

    public int WireId { get; set; }

    public string DiagramFileLink { get; set; }

    public int MinCentimeters { get; set; }

    public int MaxCentimeters { get; set; }

    public bool Published { get; set; }

    public DateTime Modified { get; set; }

    public DateTime Created { get; set; }
}
