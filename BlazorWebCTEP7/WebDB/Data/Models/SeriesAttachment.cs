﻿using System;
using System.Collections.Generic;

namespace WebDB.Data.Models;

public partial class SeriesAttachment
{
    public int Id { get; set; }

    public int SeriesId { get; set; }

    public string AttachmentFileLink { get; set; }

    public string Type { get; set; }

    public string LanguageCode { get; set; }

    public DateTime Modified { get; set; }

    public DateTime Created { get; set; }

    public virtual Series Series { get; set; }
}
