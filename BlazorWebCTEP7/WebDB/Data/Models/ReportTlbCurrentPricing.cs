﻿using System;
using System.Collections.Generic;

namespace WebDB.Data.Models;

public partial class ReportTlbCurrentPricing
{
    public string BasePartNumber { get; set; }

    public string Connector1 { get; set; }

    public string Connector2 { get; set; }

    public string Wire { get; set; }

    public decimal? Connector1CurrentCost { get; set; }

    public decimal? Connector2CurrentCost { get; set; }

    public decimal? WireCurrentCost { get; set; }
}
