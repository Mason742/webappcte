﻿using System;
using System.Collections.Generic;

namespace WebDB.Data.Models;

public partial class AOscilloscopeDifferentialCt
{
    public int? ProductId { get; set; }

    public string Accuracy { get; set; }

    public string Altitude { get; set; }

    public string Attenuation { get; set; }

    public string Bandwidth { get; set; }

    public string BnccableLength { get; set; }

    public string Dimension { get; set; }

    public string Iec { get; set; }

    public string InputImpedance { get; set; }

    public string InputLeadsLength { get; set; }

    public string InputVoltageAbsoluteMaximumRated { get; set; }

    public string InputVoltageMaximumCommonMode { get; set; }

    public string InputVoltageMaximumDifferential { get; set; }

    public string OperatingTemperature { get; set; }

    public string OutputVoltageSourceImpedance { get; set; }

    public string OutputVoltageSwing { get; set; }

    public string OutputVoltageTypicalNoise { get; set; }

    public string OutputVoltageTypicalOffset { get; set; }

    public string PollutionDegree { get; set; }

    public string RiseTime { get; set; }

    public string RoHs { get; set; }

    public string StorageTemperature { get; set; }

    public string Weight { get; set; }

    public string Humidity { get; set; }

    public string Temperature { get; set; }

    public string Type { get; set; }

    public string TypicalCmrr { get; set; }

    public string Power { get; set; }

    public string ArCmrr { get; set; }

    public string CableLength { get; set; }

    public string PowerConsumption { get; set; }

    public string PowerSupply { get; set; }

    public string Capacitance { get; set; }

    public string InputConnectorLength { get; set; }

    public string InputResistance { get; set; }

    public string Modifier { get; set; }

    public string AcCmrr { get; set; }

    public int Id { get; set; }

    public string Type1 { get; set; }

    public virtual Product Product { get; set; }
}
