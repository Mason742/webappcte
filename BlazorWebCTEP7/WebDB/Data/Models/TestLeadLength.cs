﻿using System;
using System.Collections.Generic;

namespace WebDB.Data.Models;

public partial class TestLeadLength
{
    public int Id { get; set; }

    public decimal Centimeters { get; set; }

    public bool? Published { get; set; }

    public DateTime Modified { get; set; }

    public DateTime Created { get; set; }
}
