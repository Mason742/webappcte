﻿using System;
using System.Collections.Generic;

namespace WebDB.Data.Models;

public partial class PressRelease
{
    public int Id { get; set; }

    public int SeriesId { get; set; }

    public string AlternateTitle { get; set; }

    public string EmailCopyFileLink { get; set; }

    public string AlternateSummary { get; set; }

    public string ImageFileLink { get; set; }

    public string Brand { get; set; }

    public DateTime Modified { get; set; }

    public DateTime Created { get; set; }

    public virtual Series Series { get; set; }
}
