﻿using System;
using System.Collections.Generic;

namespace WebDB.Data.Models;

public partial class TestLeadColor
{
    public int Id { get; set; }

    public int ColorId { get; set; }

    public string Name { get; set; }

    public string HexCode { get; set; }

    public bool? Published { get; set; }

    public DateTime Modified { get; set; }

    public DateTime Created { get; set; }
}
