﻿using System;
using System.Collections.Generic;

namespace WebDB.Data.Models;

public partial class ResourcesPressRelease
{
    public string ResourceType { get; set; }

    public string UrlLink { get; set; }

    public string Title { get; set; }

    public string Description { get; set; }

    public string ThumbnailLink { get; set; }

    public string Brand { get; set; }

    public bool? Published { get; set; }

    public bool? NewTab { get; set; }

    public DateTime Created { get; set; }
}
