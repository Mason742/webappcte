﻿using System;
using System.Collections.Generic;

namespace WebDB.Data.Models
{
    public partial class TypeInformationTab
    {
        public int? ProductId { get; set; }
        public string Model { get; set; }
        public string Type3 { get; set; }
        public string MainCategory { get; set; }
        public string SubCategory { get; set; }
        public string Type { get; set; }
        public string Type2 { get; set; }
    }
}
