﻿using System;
using System.Collections.Generic;

namespace WebDB.Data.Models;

public partial class AKitsCt
{
    public int? ProductId { get; set; }

    public string ConnectorGrade { get; set; }

    public string Impedance { get; set; }

    public string RoHs { get; set; }

    public string Temperature { get; set; }

    public string Type { get; set; }

    public string Bandwidth { get; set; }

    public string MaxCurrent { get; set; }

    public string Length { get; set; }

    public string Amperage { get; set; }

    public string MaxVoltage { get; set; }

    public int Id { get; set; }

    public string Type1 { get; set; }

    public virtual Product Product { get; set; }
}
