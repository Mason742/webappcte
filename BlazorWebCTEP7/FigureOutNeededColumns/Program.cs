﻿// See https://aka.ms/new-console-template for more information
using Microsoft.EntityFrameworkCore;
using System.Reflection;
using WebDB.Data.Context;
using WebDB.Data.Models;

Console.WriteLine("Hello, World!");




List<SeriesCategory> seriesCategories = new List<SeriesCategory>();
List<OldProductBak> oldProducts = new List<OldProductBak>();

Dictionary<string, HashSet<string>> tableAttributesByCategory = new Dictionary<string, HashSet<string>>();

using (var context = new DefaultDbContext())
{
    seriesCategories = context.SeriesCategories.AsNoTracking().Include(x => x.Category).Include(y => y.Series).ToList();
    oldProducts = context.OldProductBaks.ToList();
}
foreach (SeriesCategory category in seriesCategories.Where(x => x.Series.Brand == "CT"))
{
    foreach (OldProductBak product in oldProducts)
    {
        if (category.SeriesId == product.ProductFamilyId)
        {
            List<string> productAttributes = new List<string>();

            Type type = product.GetType();
            // Get the properties of the object
            PropertyInfo[] properties = type.GetProperties();

            // Iterate through the properties and check if they are empty
            foreach (var property in properties)
            {
                object value = property.GetValue(product);
                if (value != null && value?.ToString() != "" && !string.IsNullOrWhiteSpace(value?.ToString()))
                {
                    // Get the PersonGroup of the object
                    //string personGroup = (string)type.GetProperty("PersonGroup").GetValue(product);

                    // If the PersonGroup doesn't exist in the dictionary, add it
                    if (!tableAttributesByCategory.ContainsKey($"{category.Category.TableName}-{category.CategoryId}"))
                    {
                        tableAttributesByCategory[$"{category.Category.TableName}-{category.CategoryId}"] = new HashSet<string>();
                    }
                    // Add the empty property to the list for the PersonGroup
                    tableAttributesByCategory[$"{category.Category.TableName}-{category.CategoryId}"].Add(property!.Name);
                }
            }
        }

    }
}

// Print the empty properties for each PersonGroup
foreach (var group in tableAttributesByCategory)
{
    //Console.WriteLine($"tableAttributesByCategory for {group.Key}:");
    //foreach (var property in group.Value)
    //{
    //    Console.WriteLine(property);
    //}
    //Console.WriteLine();
    group.Value.Remove("Brand");
    group.Value.Remove("Published");
    group.Value.Remove("Created");
    group.Value.Remove("Model");
    group.Value.Remove("ProductFamilyId");
    Console.WriteLine($"/*SQL select for {group.Key}*/");
    var join = string.Join(",\n", group.Value);
    join = join.Replace("Id", "Product.Id as ProductId");
    Console.WriteLine($"SELECT {join}  \n" +
        $"INTO {group.Key.Substring(0, group.Key.IndexOf('-'))} \n" +
        "FROM OldProductBak \n" +
        $"LEFT JOIN SeriesCategory ON SeriesCategory.SeriesId = OldProductBak.ProductFamilyId \n" +
        $"LEFT JOIN Product ON Product.PartNumber = OldProductBak.Model \n" +
        $"WHERE SeriesCategory.CategoryId = {group.Key.Substring(group.Key.IndexOf('-') + 1)} \n" +
        $"ALTER TABLE {group.Key.Substring(0, group.Key.IndexOf('-'))} ADD Id INT IDENTITY(1,1) NOT NULL; \n" +
        $"ALTER TABLE {group.Key.Substring(0, group.Key.IndexOf('-'))} ADD CONSTRAINT FK_{group.Key.Substring(0, group.Key.IndexOf('-'))}_ProductId FOREIGN KEY (ProductId) REFERENCES Product(Id);"
        );

    Console.WriteLine();
    Console.WriteLine();
}