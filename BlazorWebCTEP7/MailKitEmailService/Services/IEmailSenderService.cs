﻿using EmailSenderService.Models;
using MimeKit;

namespace EmailSenderService.Services
{
    public interface IEmailSenderService
    {
        MailKitEmailSenderOptions Options { get; set; }

        Task Execute(MimeMessage mimeMessage);
        Task Execute(string to, string subject, string message);
        Task SendEmailAsync(MimeMessage mimeMessage);
        Task SendEmailAsync(string email, string subject, string message);
    }
}