﻿using MailKit.Security;

namespace EmailSenderService.Models;

public class MailKitEmailSenderOptions
{
    public MailKitEmailSenderOptions()
    {
        Host_SecureSocketOptions = SecureSocketOptions.Auto;
    }
    public MailKitEmailSenderOptions(bool defaultOptions = false)
    {
        if (defaultOptions)
        {
            Host_Address = "smtp.gmail.com";
            Host_Port = 465;
            Host_Username = "caltestelectronicsinfo@gmail.com";
            Host_Password = "hzoevnorrourmsan";
            Sender_EMail = "caltestelectronicsinfo@gmail.com";
            Sender_Name = "Mason Channer";
            Host_SecureSocketOptions = SecureSocketOptions.Auto;
        }
    }

    public string Host_Address { get; set; }

    public int Host_Port { get; set; }

    public string Host_Username { get; set; }

    public string Host_Password { get; set; }

    public SecureSocketOptions Host_SecureSocketOptions { get; set; }

    public string Sender_EMail { get; set; }

    public string Sender_Name { get; set; }
}
