﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using SolomonDB.Data.Models;

namespace SolomonDB.Data.Context;

public partial class SolomonDbContext : DbContext
{
    public SolomonDbContext()
    {
    }

    public SolomonDbContext(DbContextOptions<SolomonDbContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Customer> Customers { get; set; }

    public virtual DbSet<Inventory> Inventories { get; set; }

    public virtual DbSet<InventoryAdg> InventoryAdgs { get; set; }

    public virtual DbSet<ItemXref> ItemXrefs { get; set; }

    public virtual DbSet<MasonIntroductoryDate> MasonIntroductoryDates { get; set; }

    public virtual DbSet<MasonKitPrice> MasonKitPrices { get; set; }

    public virtual DbSet<MasonKitsAndComponent> MasonKitsAndComponents { get; set; }

    public virtual DbSet<Potran> Potrans { get; set; }

    public virtual DbSet<SlsPrc> SlsPrcs { get; set; }

    public virtual DbSet<SlsPrcDet> SlsPrcDets { get; set; }

    public virtual DbSet<Snote> Snotes { get; set; }

    public virtual DbSet<VrxErpInventory> VrxErpInventories { get; set; }

    public virtual DbSet<VrxItemMaster> VrxItemMasters { get; set; }

    public virtual DbSet<VrxOrderswebCte> VrxOrderswebCtes { get; set; }

    public virtual DbSet<VrxTrackingNumber> VrxTrackingNumbers { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer("Data Source=s-sql-02;Initial Catalog=CTEAPP;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False;Trusted_Connection=True");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Customer>(entity =>
        {
            entity.HasKey(e => e.CustId).HasName("Customer0");

            entity.ToTable("Customer", tb =>
                {
                    tb.HasTrigger("ADG_TR_CustNameXref_Add");
                    tb.HasTrigger("ADG_TR_CustNameXref_Delete");
                });

            entity.HasIndex(e => e.Name, "Customer1");

            entity.HasIndex(e => new { e.Status, e.CustId }, "Customer2");

            entity.HasIndex(e => new { e.StmtCycleId, e.CustId }, "Customer3");

            entity.HasIndex(e => new { e.ArAcct, e.ArSub, e.CustId }, "Customer4");

            entity.HasIndex(e => new { e.ClassId, e.CustId }, "Customer5");

            entity.HasIndex(e => new { e.Phone, e.CustId }, "Customer6");

            entity.HasIndex(e => new { e.Zip, e.CustId }, "Customer7");

            entity.Property(e => e.CustId)
                .HasMaxLength(15)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.AccrRevAcct)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.AccrRevSub)
                .IsRequired()
                .HasMaxLength(24)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.AcctNbr)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.Addr1)
                .IsRequired()
                .HasMaxLength(60)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.Addr2)
                .IsRequired()
                .HasMaxLength(60)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.AgentId)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("AgentID");
            entity.Property(e => e.ArAcct)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.ArSub)
                .IsRequired()
                .HasMaxLength(24)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.Attn)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.BankId)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("BankID");
            entity.Property(e => e.BillAddr1)
                .IsRequired()
                .HasMaxLength(60)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.BillAddr2)
                .IsRequired()
                .HasMaxLength(60)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.BillAttn)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.BillCity)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.BillCountry)
                .IsRequired()
                .HasMaxLength(3)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.BillFax)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.BillName)
                .IsRequired()
                .HasMaxLength(60)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.BillPhone)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.BillSalut)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.BillState)
                .IsRequired()
                .HasMaxLength(3)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.BillZip)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.CardExpDate)
                .HasDefaultValueSql("('01/01/1900')")
                .HasColumnType("smalldatetime");
            entity.Property(e => e.CardHldrName)
                .IsRequired()
                .HasMaxLength(60)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.CardNbr)
                .IsRequired()
                .HasMaxLength(20)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.CardType)
                .IsRequired()
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.City)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.ClassId)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.Country)
                .IsRequired()
                .HasMaxLength(3)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.CrtdDateTime)
                .HasDefaultValueSql("(rtrim(CONVERT([varchar](30),CONVERT([smalldatetime],getdate(),(0)),(0))))")
                .HasColumnType("smalldatetime")
                .HasColumnName("Crtd_DateTime");
            entity.Property(e => e.CrtdProg)
                .IsRequired()
                .HasMaxLength(8)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("Crtd_Prog");
            entity.Property(e => e.CrtdUser)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("Crtd_User");
            entity.Property(e => e.CuryId)
                .IsRequired()
                .HasMaxLength(4)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.CuryPrcLvlRtTp)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.CuryRateType)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.DfltShipToId)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.DocPublishingFlag)
                .IsRequired()
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("('Y')")
                .IsFixedLength();
            entity.Property(e => e.EmailAddr)
                .IsRequired()
                .HasMaxLength(80)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("EMailAddr");
            entity.Property(e => e.Fax)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.LanguageId)
                .IsRequired()
                .HasMaxLength(4)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("LanguageID");
            entity.Property(e => e.LupdDateTime)
                .HasDefaultValueSql("('01/01/1900')")
                .HasColumnType("smalldatetime")
                .HasColumnName("LUpd_DateTime");
            entity.Property(e => e.LupdProg)
                .IsRequired()
                .HasMaxLength(8)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("LUpd_Prog");
            entity.Property(e => e.LupdUser)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("LUpd_User");
            entity.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(60)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.PerNbr)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.Phone)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.PmtMethod)
                .IsRequired()
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.PrcLvlId)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.PrePayAcct)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.PrePaySub)
                .IsRequired()
                .HasMaxLength(24)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.PriceClassId)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("PriceClassID");
            entity.Property(e => e.PrtMcstmt).HasColumnName("PrtMCStmt");
            entity.Property(e => e.S4future01)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("S4Future01");
            entity.Property(e => e.S4future02)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("S4Future02");
            entity.Property(e => e.S4future03).HasColumnName("S4Future03");
            entity.Property(e => e.S4future04).HasColumnName("S4Future04");
            entity.Property(e => e.S4future05).HasColumnName("S4Future05");
            entity.Property(e => e.S4future06).HasColumnName("S4Future06");
            entity.Property(e => e.S4future07)
                .HasDefaultValueSql("('01/01/1900')")
                .HasColumnType("smalldatetime")
                .HasColumnName("S4Future07");
            entity.Property(e => e.S4future08)
                .HasDefaultValueSql("('01/01/1900')")
                .HasColumnType("smalldatetime")
                .HasColumnName("S4Future08");
            entity.Property(e => e.S4future09).HasColumnName("S4Future09");
            entity.Property(e => e.S4future10).HasColumnName("S4Future10");
            entity.Property(e => e.S4future11)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("S4Future11");
            entity.Property(e => e.S4future12)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("S4Future12");
            entity.Property(e => e.Salut)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.SetupDate)
                .HasDefaultValueSql("('01/01/1900')")
                .HasColumnType("smalldatetime");
            entity.Property(e => e.ShipPctAct)
                .IsRequired()
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.Siccode1)
                .IsRequired()
                .HasMaxLength(4)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("SICCode1");
            entity.Property(e => e.Siccode2)
                .IsRequired()
                .HasMaxLength(4)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("SICCode2");
            entity.Property(e => e.SlsAcct)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.SlsSub)
                .IsRequired()
                .HasMaxLength(24)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.SlsperId)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.State)
                .IsRequired()
                .HasMaxLength(3)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.Status)
                .IsRequired()
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.StmtCycleId)
                .IsRequired()
                .HasMaxLength(2)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.StmtType)
                .IsRequired()
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.TaxDflt)
                .IsRequired()
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.TaxExemptNbr)
                .IsRequired()
                .HasMaxLength(15)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.TaxId00)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("TaxID00");
            entity.Property(e => e.TaxId01)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("TaxID01");
            entity.Property(e => e.TaxId02)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("TaxID02");
            entity.Property(e => e.TaxId03)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("TaxID03");
            entity.Property(e => e.TaxLocId)
                .IsRequired()
                .HasMaxLength(15)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.TaxRegNbr)
                .IsRequired()
                .HasMaxLength(15)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.Terms)
                .IsRequired()
                .HasMaxLength(2)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.Territory)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.Tstamp)
                .IsRequired()
                .IsRowVersion()
                .IsConcurrencyToken()
                .HasColumnName("tstamp");
            entity.Property(e => e.User1)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.User2)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.User5)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.User6)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.User7)
                .HasDefaultValueSql("('01/01/1900')")
                .HasColumnType("smalldatetime");
            entity.Property(e => e.User8)
                .HasDefaultValueSql("('01/01/1900')")
                .HasColumnType("smalldatetime");
            entity.Property(e => e.Zip)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
        });

        modelBuilder.Entity<Inventory>(entity =>
        {
            entity.HasKey(e => e.InvtId).HasName("Inventory0");

            entity.ToTable("Inventory", tb =>
                {
                    tb.HasTrigger("ADG_TR_InvtDescrXref_Add");
                    tb.HasTrigger("ADG_TR_InvtDescrXref_Delete");
                });

            entity.HasIndex(e => new { e.ClassId, e.InvtId }, "Inventory1");

            entity.HasIndex(e => new { e.Descr, e.InvtId }, "Inventory2");

            entity.HasIndex(e => new { e.TranStatusCode, e.InvtId }, "Inventory3");

            entity.Property(e => e.InvtId)
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("InvtID");
            entity.Property(e => e.Abccode)
                .IsRequired()
                .HasMaxLength(2)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("ABCCode");
            entity.Property(e => e.AutoPodropShip).HasColumnName("AutoPODropShip");
            entity.Property(e => e.AutoPopolicy)
                .IsRequired()
                .HasMaxLength(2)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("AutoPOPolicy");
            entity.Property(e => e.BmidirStdCost).HasColumnName("BMIDirStdCost");
            entity.Property(e => e.BmifovhStdCost).HasColumnName("BMIFOvhStdCost");
            entity.Property(e => e.BmilastCost).HasColumnName("BMILastCost");
            entity.Property(e => e.BmipdirStdCost).HasColumnName("BMIPDirStdCost");
            entity.Property(e => e.BmipfovhStdCost).HasColumnName("BMIPFOvhStdCost");
            entity.Property(e => e.BmipstdCost).HasColumnName("BMIPStdCost");
            entity.Property(e => e.BmipvovhStdCost).HasColumnName("BMIPVOvhStdCost");
            entity.Property(e => e.BmistdCost).HasColumnName("BMIStdCost");
            entity.Property(e => e.BmivovhStdCost).HasColumnName("BMIVOvhStdCost");
            entity.Property(e => e.Bolcode)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("BOLCode");
            entity.Property(e => e.Buyer)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.ChkOrdQty)
                .IsRequired()
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.ClassId)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("ClassID");
            entity.Property(e => e.Cogsacct)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("COGSAcct");
            entity.Property(e => e.Cogssub)
                .IsRequired()
                .HasMaxLength(24)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("COGSSub");
            entity.Property(e => e.Color)
                .IsRequired()
                .HasMaxLength(20)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.CountStatus)
                .IsRequired()
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("('A')")
                .IsFixedLength();
            entity.Property(e => e.CrtdDateTime)
                .HasDefaultValueSql("(rtrim(CONVERT([varchar](30),CONVERT([smalldatetime],getdate(),(0)),(0))))")
                .HasColumnType("smalldatetime")
                .HasColumnName("Crtd_DateTime");
            entity.Property(e => e.CrtdProg)
                .IsRequired()
                .HasMaxLength(8)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("Crtd_Prog");
            entity.Property(e => e.CrtdUser)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("Crtd_User");
            entity.Property(e => e.CycleId)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("CycleID");
            entity.Property(e => e.Descr)
                .IsRequired()
                .HasMaxLength(60)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.DfltPickLoc)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.DfltPounit)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("DfltPOUnit");
            entity.Property(e => e.DfltSalesAcct)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.DfltSalesSub)
                .IsRequired()
                .HasMaxLength(24)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.DfltShpnotInvAcct)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.DfltShpnotInvSub)
                .IsRequired()
                .HasMaxLength(24)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.DfltSite)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.DfltSounit)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("DfltSOUnit");
            entity.Property(e => e.DfltWhseLoc)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.DiscAcct)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.DiscPrc)
                .IsRequired()
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.DiscSub)
                .IsRequired()
                .HasMaxLength(31)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.Eoq).HasColumnName("EOQ");
            entity.Property(e => e.FovhStdCost).HasColumnName("FOvhStdCost");
            entity.Property(e => e.FrtAcct)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.FrtSub)
                .IsRequired()
                .HasMaxLength(24)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.GlclassId)
                .IsRequired()
                .HasMaxLength(4)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("GLClassID");
            entity.Property(e => e.InvtAcct)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.InvtSub)
                .IsRequired()
                .HasMaxLength(24)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.InvtType)
                .IsRequired()
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.IrcalcPolicy)
                .IsRequired()
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("IRCalcPolicy");
            entity.Property(e => e.IrdaysSupply).HasColumnName("IRDaysSupply");
            entity.Property(e => e.IrdemandId)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("IRDemandID");
            entity.Property(e => e.IrfutureDate)
                .HasDefaultValueSql("('01/01/1900')")
                .HasColumnType("smalldatetime")
                .HasColumnName("IRFutureDate");
            entity.Property(e => e.IrfuturePolicy)
                .IsRequired()
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("IRFuturePolicy");
            entity.Property(e => e.IrleadTimeId)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("IRLeadTimeID");
            entity.Property(e => e.IrlinePtQty).HasColumnName("IRLinePtQty");
            entity.Property(e => e.IrminOnHand).HasColumnName("IRMinOnHand");
            entity.Property(e => e.IrmodelInvtId)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("IRModelInvtID");
            entity.Property(e => e.IrrcycDays).HasColumnName("IRRCycDays");
            entity.Property(e => e.IrseasonEndDay).HasColumnName("IRSeasonEndDay");
            entity.Property(e => e.IrseasonEndMon).HasColumnName("IRSeasonEndMon");
            entity.Property(e => e.IrseasonStrtDay).HasColumnName("IRSeasonStrtDay");
            entity.Property(e => e.IrseasonStrtMon).HasColumnName("IRSeasonStrtMon");
            entity.Property(e => e.IrserviceLevel).HasColumnName("IRServiceLevel");
            entity.Property(e => e.IrsftyStkDays).HasColumnName("IRSftyStkDays");
            entity.Property(e => e.IrsftyStkPct).HasColumnName("IRSftyStkPct");
            entity.Property(e => e.IrsftyStkPolicy)
                .IsRequired()
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("IRSftyStkPolicy");
            entity.Property(e => e.IrsourceCode)
                .IsRequired()
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("IRSourceCode");
            entity.Property(e => e.IrtargetOrdMethod)
                .IsRequired()
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("IRTargetOrdMethod");
            entity.Property(e => e.IrtargetOrdReq).HasColumnName("IRTargetOrdReq");
            entity.Property(e => e.IrtransferSiteId)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("IRTransferSiteID");
            entity.Property(e => e.ItemCommClassId)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("ItemCommClassID");
            entity.Property(e => e.LastCountDate)
                .HasDefaultValueSql("('01/01/1900')")
                .HasColumnType("smalldatetime");
            entity.Property(e => e.LastSiteId)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("LastSiteID");
            entity.Property(e => e.LcvarianceAcct)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("LCVarianceAcct");
            entity.Property(e => e.LcvarianceSub)
                .IsRequired()
                .HasMaxLength(24)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("LCVarianceSub");
            entity.Property(e => e.LotSerFxdTyp)
                .IsRequired()
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.LotSerFxdVal)
                .IsRequired()
                .HasMaxLength(12)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.LotSerIssMthd)
                .IsRequired()
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.LotSerNumVal)
                .IsRequired()
                .HasMaxLength(25)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.LotSerTrack)
                .IsRequired()
                .HasMaxLength(2)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.LupdDateTime)
                .HasDefaultValueSql("('01/01/1900')")
                .HasColumnType("smalldatetime")
                .HasColumnName("LUpd_DateTime");
            entity.Property(e => e.LupdProg)
                .IsRequired()
                .HasMaxLength(8)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("LUpd_Prog");
            entity.Property(e => e.LupdUser)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("LUpd_User");
            entity.Property(e => e.MaterialType)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.MfgClassId)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("MfgClassID");
            entity.Property(e => e.MoveClass)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.Msds)
                .IsRequired()
                .HasMaxLength(24)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("MSDS");
            entity.Property(e => e.NoteId).HasColumnName("NoteID");
            entity.Property(e => e.Pack)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.PdirStdCost).HasColumnName("PDirStdCost");
            entity.Property(e => e.PerNbr)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.PfovhStdCost).HasColumnName("PFOvhStdCost");
            entity.Property(e => e.Ppvacct)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("PPVAcct");
            entity.Property(e => e.Ppvsub)
                .IsRequired()
                .HasMaxLength(24)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("PPVSub");
            entity.Property(e => e.PriceClassId)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("PriceClassID");
            entity.Property(e => e.ProdMgrId)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("ProdMgrID");
            entity.Property(e => e.ProductionUnit)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.PstdCost).HasColumnName("PStdCost");
            entity.Property(e => e.PstdCostDate)
                .HasDefaultValueSql("('01/01/1900')")
                .HasColumnType("smalldatetime")
                .HasColumnName("PStdCostDate");
            entity.Property(e => e.PvovhStdCost).HasColumnName("PVOvhStdCost");
            entity.Property(e => e.ReplMthd)
                .IsRequired()
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.S4future01)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("S4Future01");
            entity.Property(e => e.S4future02)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("S4Future02");
            entity.Property(e => e.S4future03).HasColumnName("S4Future03");
            entity.Property(e => e.S4future04).HasColumnName("S4Future04");
            entity.Property(e => e.S4future05).HasColumnName("S4Future05");
            entity.Property(e => e.S4future06).HasColumnName("S4Future06");
            entity.Property(e => e.S4future07)
                .HasDefaultValueSql("('01/01/1900')")
                .HasColumnType("smalldatetime")
                .HasColumnName("S4Future07");
            entity.Property(e => e.S4future08)
                .HasDefaultValueSql("('01/01/1900')")
                .HasColumnType("smalldatetime")
                .HasColumnName("S4Future08");
            entity.Property(e => e.S4future09).HasColumnName("S4Future09");
            entity.Property(e => e.S4future10).HasColumnName("S4Future10");
            entity.Property(e => e.S4future11)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("S4Future11");
            entity.Property(e => e.S4future12)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("S4Future12");
            entity.Property(e => e.S4future13)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("S4Future13");
            entity.Property(e => e.SerAssign)
                .IsRequired()
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.Size)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.Source)
                .IsRequired()
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.Status)
                .IsRequired()
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.StdCostDate)
                .HasDefaultValueSql("('01/01/1900')")
                .HasColumnType("smalldatetime");
            entity.Property(e => e.StkUnit)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.StkWtUnit)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.Style)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.Supplr1)
                .IsRequired()
                .HasMaxLength(15)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.Supplr2)
                .IsRequired()
                .HasMaxLength(15)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.SupplrItem1)
                .IsRequired()
                .HasMaxLength(20)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.SupplrItem2)
                .IsRequired()
                .HasMaxLength(20)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.TaskId)
                .IsRequired()
                .HasMaxLength(32)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("TaskID");
            entity.Property(e => e.TaxCat)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.TranStatusCode)
                .IsRequired()
                .HasMaxLength(2)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.Tstamp)
                .IsRequired()
                .IsRowVersion()
                .IsConcurrencyToken()
                .HasColumnName("tstamp");
            entity.Property(e => e.Upccode)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("UPCCode");
            entity.Property(e => e.User1)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.User2)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.User5)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.User6)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.User7)
                .HasDefaultValueSql("('01/01/1900')")
                .HasColumnType("smalldatetime");
            entity.Property(e => e.User8)
                .HasDefaultValueSql("('01/01/1900')")
                .HasColumnType("smalldatetime");
            entity.Property(e => e.ValMthd)
                .IsRequired()
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.VovhStdCost).HasColumnName("VOvhStdCost");
            entity.Property(e => e.Ytdusage).HasColumnName("YTDUsage");
        });

        modelBuilder.Entity<InventoryAdg>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("InventoryADG");

            entity.HasIndex(e => e.InvtId, "InventoryADG0")
                .IsUnique()
                .IsClustered();

            entity.Property(e => e.Bolclass)
                .IsRequired()
                .HasMaxLength(20)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("BOLClass");
            entity.Property(e => e.CategoryCode)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.CountryOrig)
                .IsRequired()
                .HasMaxLength(20)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.CrtdDateTime)
                .HasDefaultValueSql("(rtrim(CONVERT([varchar](30),CONVERT([smalldatetime],getdate(),(0)),(0))))")
                .HasColumnType("smalldatetime")
                .HasColumnName("Crtd_DateTime");
            entity.Property(e => e.CrtdProg)
                .IsRequired()
                .HasMaxLength(8)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("Crtd_Prog");
            entity.Property(e => e.CrtdUser)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("Crtd_User");
            entity.Property(e => e.DensityUom)
                .IsRequired()
                .HasMaxLength(5)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("DensityUOM");
            entity.Property(e => e.DepthUom)
                .IsRequired()
                .HasMaxLength(5)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("DepthUOM");
            entity.Property(e => e.DiameterUom)
                .IsRequired()
                .HasMaxLength(5)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("DiameterUOM");
            entity.Property(e => e.GaugeUom)
                .IsRequired()
                .HasMaxLength(5)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("GaugeUOM");
            entity.Property(e => e.HeightUom)
                .IsRequired()
                .HasMaxLength(5)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("HeightUOM");
            entity.Property(e => e.InvtId)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("InvtID");
            entity.Property(e => e.LenUom)
                .IsRequired()
                .HasMaxLength(5)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("LenUOM");
            entity.Property(e => e.LupdDateTime)
                .HasDefaultValueSql("('01/01/1900')")
                .HasColumnType("smalldatetime")
                .HasColumnName("LUpd_DateTime");
            entity.Property(e => e.LupdProg)
                .IsRequired()
                .HasMaxLength(8)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("LUpd_Prog");
            entity.Property(e => e.LupdUser)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("LUpd_User");
            entity.Property(e => e.NoteId).HasColumnName("NoteID");
            entity.Property(e => e.Omcogsacct)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("OMCOGSAcct");
            entity.Property(e => e.Omcogssub)
                .IsRequired()
                .HasMaxLength(31)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("OMCOGSSub");
            entity.Property(e => e.OmsalesAcct)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("OMSalesAcct");
            entity.Property(e => e.OmsalesSub)
                .IsRequired()
                .HasMaxLength(31)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("OMSalesSub");
            entity.Property(e => e.PackMethod)
                .IsRequired()
                .HasMaxLength(2)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.PackUnitMultDiv)
                .IsRequired()
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.PackUom)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("PackUOM");
            entity.Property(e => e.ProdLineId)
                .IsRequired()
                .HasMaxLength(4)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("ProdLineID");
            entity.Property(e => e.RoyaltyCode)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.S4future01)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("S4Future01");
            entity.Property(e => e.S4future02)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("S4Future02");
            entity.Property(e => e.S4future03).HasColumnName("S4Future03");
            entity.Property(e => e.S4future04).HasColumnName("S4Future04");
            entity.Property(e => e.S4future05).HasColumnName("S4Future05");
            entity.Property(e => e.S4future06).HasColumnName("S4Future06");
            entity.Property(e => e.S4future07)
                .HasDefaultValueSql("('01/01/1900')")
                .HasColumnType("smalldatetime")
                .HasColumnName("S4Future07");
            entity.Property(e => e.S4future08)
                .HasDefaultValueSql("('01/01/1900')")
                .HasColumnType("smalldatetime")
                .HasColumnName("S4Future08");
            entity.Property(e => e.S4future09).HasColumnName("S4Future09");
            entity.Property(e => e.S4future10).HasColumnName("S4Future10");
            entity.Property(e => e.S4future11)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("S4Future11");
            entity.Property(e => e.S4future12)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("S4Future12");
            entity.Property(e => e.Scheight).HasColumnName("SCHeight");
            entity.Property(e => e.ScheightUom)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("SCHeightUOM");
            entity.Property(e => e.Sclen).HasColumnName("SCLen");
            entity.Property(e => e.SclenUom)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("SCLenUOM");
            entity.Property(e => e.Scvolume).HasColumnName("SCVolume");
            entity.Property(e => e.ScvolumeUom)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("SCVolumeUOM");
            entity.Property(e => e.Scweight).HasColumnName("SCWeight");
            entity.Property(e => e.ScweightUom)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("SCWeightUOM");
            entity.Property(e => e.Scwidth).HasColumnName("SCWidth");
            entity.Property(e => e.ScwidthUom)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("SCWidthUOM");
            entity.Property(e => e.Style)
                .IsRequired()
                .HasMaxLength(20)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.Tstamp)
                .IsRequired()
                .IsRowVersion()
                .IsConcurrencyToken()
                .HasColumnName("tstamp");
            entity.Property(e => e.User1)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.User10)
                .HasDefaultValueSql("('01/01/1900')")
                .HasColumnType("smalldatetime");
            entity.Property(e => e.User2)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.User3)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.User4)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.User7)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.User8)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.User9)
                .HasDefaultValueSql("('01/01/1900')")
                .HasColumnType("smalldatetime");
            entity.Property(e => e.VolUom)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("VolUOM");
            entity.Property(e => e.WeightUom)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("WeightUOM");
            entity.Property(e => e.WidthUom)
                .IsRequired()
                .HasMaxLength(5)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("WidthUOM");
        });

        modelBuilder.Entity<ItemXref>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("ItemXRef");

            entity.HasIndex(e => new { e.InvtId, e.AltIdtype, e.EntityId, e.AlternateId }, "ItemXRef0")
                .IsUnique()
                .IsClustered();

            entity.HasIndex(e => new { e.EntityId, e.AltIdtype, e.InvtId }, "ItemXRef1");

            entity.HasIndex(e => new { e.AlternateId, e.AltIdtype, e.EntityId }, "ItemXRef2");

            entity.Property(e => e.AltIdtype)
                .IsRequired()
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("AltIDType");
            entity.Property(e => e.AlternateId)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("AlternateID");
            entity.Property(e => e.CrtdDateTime)
                .HasDefaultValueSql("(rtrim(CONVERT([varchar](30),CONVERT([smalldatetime],getdate(),(0)),(0))))")
                .HasColumnType("smalldatetime")
                .HasColumnName("Crtd_DateTime");
            entity.Property(e => e.CrtdProg)
                .IsRequired()
                .HasMaxLength(8)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("Crtd_Prog");
            entity.Property(e => e.CrtdUser)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("Crtd_User");
            entity.Property(e => e.Descr)
                .IsRequired()
                .HasMaxLength(60)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.EntityId)
                .IsRequired()
                .HasMaxLength(15)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("EntityID");
            entity.Property(e => e.InvtId)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("InvtID");
            entity.Property(e => e.LupdDateTime)
                .HasDefaultValueSql("('01/01/1900')")
                .HasColumnType("smalldatetime")
                .HasColumnName("LUpd_DateTime");
            entity.Property(e => e.LupdProg)
                .IsRequired()
                .HasMaxLength(8)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("LUpd_Prog");
            entity.Property(e => e.LupdUser)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("LUpd_User");
            entity.Property(e => e.NoteId).HasColumnName("NoteID");
            entity.Property(e => e.S4future01)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("S4Future01");
            entity.Property(e => e.S4future02)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("S4Future02");
            entity.Property(e => e.S4future03).HasColumnName("S4Future03");
            entity.Property(e => e.S4future04).HasColumnName("S4Future04");
            entity.Property(e => e.S4future05).HasColumnName("S4Future05");
            entity.Property(e => e.S4future06).HasColumnName("S4Future06");
            entity.Property(e => e.S4future07)
                .HasDefaultValueSql("('01/01/1900')")
                .HasColumnType("smalldatetime")
                .HasColumnName("S4Future07");
            entity.Property(e => e.S4future08)
                .HasDefaultValueSql("('01/01/1900')")
                .HasColumnType("smalldatetime")
                .HasColumnName("S4Future08");
            entity.Property(e => e.S4future09).HasColumnName("S4Future09");
            entity.Property(e => e.S4future10).HasColumnName("S4Future10");
            entity.Property(e => e.S4future11)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("S4Future11");
            entity.Property(e => e.S4future12)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("S4Future12");
            entity.Property(e => e.Tstamp)
                .IsRequired()
                .IsRowVersion()
                .IsConcurrencyToken()
                .HasColumnName("tstamp");
            entity.Property(e => e.Unit)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.User1)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.User2)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.User5)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.User6)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.User7)
                .HasDefaultValueSql("('01/01/1900')")
                .HasColumnType("smalldatetime");
            entity.Property(e => e.User8)
                .HasDefaultValueSql("('01/01/1900')")
                .HasColumnType("smalldatetime");
        });

        modelBuilder.Entity<MasonIntroductoryDate>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("Mason_IntroductoryDate");

            entity.Property(e => e.FirstReceivedDate).HasColumnType("smalldatetime");
            entity.Property(e => e.IntroductoryDate).HasColumnType("smalldatetime");
            entity.Property(e => e.InventoryCreatedDate).HasColumnType("smalldatetime");
            entity.Property(e => e.InvtId)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .IsFixedLength()
                .HasColumnName("InvtID");
            entity.Property(e => e.KitCreatedDate).HasColumnType("smalldatetime");
        });

        modelBuilder.Entity<MasonKitPrice>(entity =>
        {
            entity.HasKey(e => e.InvtId);

            entity.ToTable("Mason_KitPrice");

            entity.Property(e => e.InvtId)
                .HasMaxLength(30)
                .IsUnicode(false)
                .IsFixedLength()
                .HasColumnName("InvtID");
            entity.Property(e => e.Dprice).HasColumnName("DPrice");
            entity.Property(e => e.Note)
                .IsRequired()
                .HasMaxLength(255)
                .IsUnicode(false);
        });

        modelBuilder.Entity<MasonKitsAndComponent>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("Mason_KitsAndComponents");

            entity.Property(e => e.CompDesc)
                .HasMaxLength(60)
                .IsUnicode(false);
            entity.Property(e => e.CompHandling)
                .HasMaxLength(10)
                .IsUnicode(false);
            entity.Property(e => e.CompId)
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasColumnName("CompID");
            entity.Property(e => e.CompMsrp).HasColumnName("CompMSRP");
            entity.Property(e => e.KitDesc)
                .HasMaxLength(60)
                .IsUnicode(false);
            entity.Property(e => e.KitHandling)
                .HasMaxLength(10)
                .IsUnicode(false);
            entity.Property(e => e.KitId)
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasColumnName("KitID");
            entity.Property(e => e.KitMsrp).HasColumnName("KitMSRP");
        });

        modelBuilder.Entity<Potran>(entity =>
        {
            entity.HasKey(e => new { e.RcptNbr, e.LineRef }).HasName("POTran0");

            entity.ToTable("POTran");

            entity.HasIndex(e => new { e.Ponbr, e.PolineNbr }, "POTran1");

            entity.HasIndex(e => new { e.ReasonCd, e.InvtId }, "POTran2");

            entity.HasIndex(e => new { e.RcptNbr, e.LineRef, e.LineNbr }, "POTran3");

            entity.HasIndex(e => new { e.InvtId, e.SiteId, e.PurchaseType, e.TranType, e.WhseLoc, e.LineRef, e.RcptNbr, e.CnvFact, e.UnitMultDiv, e.Qty }, "POTran4");

            entity.HasIndex(e => new { e.PerPost, e.PcStatus }, "potran_stran");

            entity.Property(e => e.RcptNbr)
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.LineRef)
                .HasMaxLength(5)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.Acct)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.AltIdtype)
                .IsRequired()
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("AltIDType");
            entity.Property(e => e.AlternateId)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("AlternateID");
            entity.Property(e => e.AplineId).HasColumnName("APLineID");
            entity.Property(e => e.AplineRef)
                .IsRequired()
                .HasMaxLength(5)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("APLineRef");
            entity.Property(e => e.BatNbr)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.BmicuryId)
                .IsRequired()
                .HasMaxLength(4)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("BMICuryID");
            entity.Property(e => e.BmieffDate)
                .HasDefaultValueSql("('01/01/1900')")
                .HasColumnType("smalldatetime")
                .HasColumnName("BMIEffDate");
            entity.Property(e => e.BmiextCost).HasColumnName("BMIExtCost");
            entity.Property(e => e.BmimultDiv)
                .IsRequired()
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("BMIMultDiv");
            entity.Property(e => e.Bmirate).HasColumnName("BMIRate");
            entity.Property(e => e.BmirtTp)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("BMIRtTp");
            entity.Property(e => e.BmitranAmt).HasColumnName("BMITranAmt");
            entity.Property(e => e.BmiunitCost).HasColumnName("BMIUnitCost");
            entity.Property(e => e.BmiunitPrice).HasColumnName("BMIUnitPrice");
            entity.Property(e => e.BomlineRef)
                .IsRequired()
                .HasMaxLength(5)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("BOMLineRef");
            entity.Property(e => e.Bomsequence).HasColumnName("BOMSequence");
            entity.Property(e => e.CpnyId)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("CpnyID");
            entity.Property(e => e.CrtdDateTime)
                .HasDefaultValueSql("(rtrim(CONVERT([varchar](30),CONVERT([smalldatetime],getdate(),(0)),(0))))")
                .HasColumnType("smalldatetime")
                .HasColumnName("Crtd_DateTime");
            entity.Property(e => e.CrtdProg)
                .IsRequired()
                .HasMaxLength(8)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("Crtd_Prog");
            entity.Property(e => e.CrtdUser)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("Crtd_User");
            entity.Property(e => e.CuryId)
                .IsRequired()
                .HasMaxLength(4)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("CuryID");
            entity.Property(e => e.CuryMultDiv)
                .IsRequired()
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.DrCr)
                .IsRequired()
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.InvtId)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("InvtID");
            entity.Property(e => e.JrnlType)
                .IsRequired()
                .HasMaxLength(3)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.LaborClassCd)
                .IsRequired()
                .HasMaxLength(4)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("Labor_Class_Cd");
            entity.Property(e => e.LineId).HasColumnName("LineID");
            entity.Property(e => e.LupdDateTime)
                .HasDefaultValueSql("('01/01/1900')")
                .HasColumnType("smalldatetime")
                .HasColumnName("LUpd_DateTime");
            entity.Property(e => e.LupdProg)
                .IsRequired()
                .HasMaxLength(8)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("LUpd_Prog");
            entity.Property(e => e.LupdUser)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("LUpd_User");
            entity.Property(e => e.NoteId).HasColumnName("NoteID");
            entity.Property(e => e.OrigRcptDate)
                .HasDefaultValueSql("('01/01/1900')")
                .HasColumnType("smalldatetime");
            entity.Property(e => e.OrigRcptNbr)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.OrigRetRcptNbr)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.PcFlag)
                .IsRequired()
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("PC_Flag");
            entity.Property(e => e.PcId)
                .IsRequired()
                .HasMaxLength(20)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("PC_ID");
            entity.Property(e => e.PcStatus)
                .IsRequired()
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("PC_Status");
            entity.Property(e => e.PerEnt)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.PerPost)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.PolineId).HasColumnName("POLineID");
            entity.Property(e => e.PolineNbr).HasColumnName("POLineNbr");
            entity.Property(e => e.PolineRef)
                .IsRequired()
                .HasMaxLength(5)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("POLIneRef");
            entity.Property(e => e.Ponbr)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("PONbr");
            entity.Property(e => e.Pooriginal)
                .IsRequired()
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("POOriginal");
            entity.Property(e => e.ProjectId)
                .IsRequired()
                .HasMaxLength(16)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("ProjectID");
            entity.Property(e => e.PurchaseType)
                .IsRequired()
                .HasMaxLength(2)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.RcptDate)
                .HasDefaultValueSql("('01/01/1900')")
                .HasColumnType("smalldatetime");
            entity.Property(e => e.RcptLineRefOrig)
                .IsRequired()
                .HasMaxLength(5)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.RcptMultDiv)
                .IsRequired()
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.RcptNbrOrig)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.RcptUnitDescr)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.ReasonCd)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.Refnbr)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.S4future01)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("S4Future01");
            entity.Property(e => e.S4future02)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("S4Future02");
            entity.Property(e => e.S4future03).HasColumnName("S4Future03");
            entity.Property(e => e.S4future04).HasColumnName("S4Future04");
            entity.Property(e => e.S4future05).HasColumnName("S4Future05");
            entity.Property(e => e.S4future06).HasColumnName("S4Future06");
            entity.Property(e => e.S4future07)
                .HasDefaultValueSql("('01/01/1900')")
                .HasColumnType("smalldatetime")
                .HasColumnName("S4Future07");
            entity.Property(e => e.S4future08)
                .HasDefaultValueSql("('01/01/1900')")
                .HasColumnType("smalldatetime")
                .HasColumnName("S4Future08");
            entity.Property(e => e.S4future09).HasColumnName("S4Future09");
            entity.Property(e => e.S4future10).HasColumnName("S4Future10");
            entity.Property(e => e.S4future11)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("S4Future11");
            entity.Property(e => e.S4future12)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("S4Future12");
            entity.Property(e => e.ServiceCallId)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("ServiceCallID");
            entity.Property(e => e.SiteId)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("SiteID");
            entity.Property(e => e.SolineId).HasColumnName("SOLineID");
            entity.Property(e => e.SolineRef)
                .IsRequired()
                .HasMaxLength(5)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("SOLineRef");
            entity.Property(e => e.SoordNbr)
                .IsRequired()
                .HasMaxLength(15)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("SOOrdNbr");
            entity.Property(e => e.SotypeId)
                .IsRequired()
                .HasMaxLength(4)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("SOTypeID");
            entity.Property(e => e.SpecificCostId)
                .IsRequired()
                .HasMaxLength(25)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("SpecificCostID");
            entity.Property(e => e.Sub)
                .IsRequired()
                .HasMaxLength(24)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.SvcContractId)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("SvcContractID");
            entity.Property(e => e.TaskId)
                .IsRequired()
                .HasMaxLength(32)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("TaskID");
            entity.Property(e => e.TaxCat)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.TaxIddflt)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("TaxIDDflt");
            entity.Property(e => e.TranDate)
                .HasDefaultValueSql("('01/01/1900')")
                .HasColumnType("smalldatetime");
            entity.Property(e => e.TranDesc)
                .IsRequired()
                .HasMaxLength(60)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.TranType)
                .IsRequired()
                .HasMaxLength(2)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.Tstamp)
                .IsRequired()
                .IsRowVersion()
                .IsConcurrencyToken()
                .HasColumnName("tstamp");
            entity.Property(e => e.UnitDescr)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.UnitMultDiv)
                .IsRequired()
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.User1)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.User2)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.User5)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.User6)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.User7)
                .HasDefaultValueSql("('01/01/1900')")
                .HasColumnType("smalldatetime");
            entity.Property(e => e.User8)
                .HasDefaultValueSql("('01/01/1900')")
                .HasColumnType("smalldatetime");
            entity.Property(e => e.VendId)
                .IsRequired()
                .HasMaxLength(15)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.VouchStage)
                .IsRequired()
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.WhseLoc)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.WipCogsAcct)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("WIP_COGS_Acct");
            entity.Property(e => e.WipCogsSub)
                .IsRequired()
                .HasMaxLength(24)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("WIP_COGS_Sub");
            entity.Property(e => e.WobomRef)
                .IsRequired()
                .HasMaxLength(5)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("WOBomRef");
            entity.Property(e => e.WocostType)
                .IsRequired()
                .HasMaxLength(2)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("WOCostType");
            entity.Property(e => e.Wonbr)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("WONbr");
            entity.Property(e => e.WostepNbr)
                .IsRequired()
                .HasMaxLength(5)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("WOStepNbr");
        });

        modelBuilder.Entity<SlsPrc>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("SlsPrc");

            entity.HasIndex(e => e.SlsPrcId, "SlsPrc0")
                .IsUnique()
                .IsClustered();

            entity.HasIndex(e => new { e.PriceCat, e.DiscPrcTyp, e.SelectFld1, e.SelectFld2, e.CuryId, e.SiteId }, "SlsPrc1").IsUnique();

            entity.HasIndex(e => e.CatalogNbr, "SlsPrc2");

            entity.HasIndex(e => new { e.PriceCat, e.SelectFld1, e.SelectFld2, e.CuryId, e.SiteId, e.CatalogNbr }, "SlsPrc3");

            entity.Property(e => e.CatalogNbr)
                .IsRequired()
                .HasMaxLength(15)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.ContractNbr)
                .IsRequired()
                .HasMaxLength(25)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.CrtdDateTime)
                .HasDefaultValueSql("(rtrim(CONVERT([varchar](30),CONVERT([smalldatetime],getdate(),(0)),(0))))")
                .HasColumnType("smalldatetime")
                .HasColumnName("Crtd_DateTime");
            entity.Property(e => e.CrtdProg)
                .IsRequired()
                .HasMaxLength(8)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("Crtd_Prog");
            entity.Property(e => e.CrtdUser)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("Crtd_User");
            entity.Property(e => e.CuryId)
                .IsRequired()
                .HasMaxLength(4)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("CuryID");
            entity.Property(e => e.CustClassId)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("CustClassID");
            entity.Property(e => e.CustId)
                .IsRequired()
                .HasMaxLength(15)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("CustID");
            entity.Property(e => e.DetCntr)
                .IsRequired()
                .HasMaxLength(5)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.DiscPrcMthd)
                .IsRequired()
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.DiscPrcTyp)
                .IsRequired()
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.InvtId)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("InvtID");
            entity.Property(e => e.LupdDateTime)
                .HasDefaultValueSql("('01/01/1900')")
                .HasColumnType("smalldatetime")
                .HasColumnName("LUpd_DateTime");
            entity.Property(e => e.LupdProg)
                .IsRequired()
                .HasMaxLength(8)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("LUpd_Prog");
            entity.Property(e => e.LupdUser)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("LUpd_User");
            entity.Property(e => e.NoteId).HasColumnName("NoteID");
            entity.Property(e => e.PrcLvlId)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("PrcLvlID");
            entity.Property(e => e.PriceCat)
                .IsRequired()
                .HasMaxLength(2)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.PriceClassId)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("PriceClassID");
            entity.Property(e => e.S4future01)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("S4Future01");
            entity.Property(e => e.S4future02)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("S4Future02");
            entity.Property(e => e.S4future03).HasColumnName("S4Future03");
            entity.Property(e => e.S4future04).HasColumnName("S4Future04");
            entity.Property(e => e.S4future05).HasColumnName("S4Future05");
            entity.Property(e => e.S4future06).HasColumnName("S4Future06");
            entity.Property(e => e.S4future07)
                .HasDefaultValueSql("('01/01/1900')")
                .HasColumnType("smalldatetime")
                .HasColumnName("S4Future07");
            entity.Property(e => e.S4future08)
                .HasDefaultValueSql("('01/01/1900')")
                .HasColumnType("smalldatetime")
                .HasColumnName("S4Future08");
            entity.Property(e => e.S4future09).HasColumnName("S4Future09");
            entity.Property(e => e.S4future10).HasColumnName("S4Future10");
            entity.Property(e => e.S4future11)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("S4Future11");
            entity.Property(e => e.S4future12)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("S4Future12");
            entity.Property(e => e.SelectFld1)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.SelectFld2)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.SiteId)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("SiteID");
            entity.Property(e => e.SlsPrcId)
                .IsRequired()
                .HasMaxLength(15)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("SlsPrcID");
            entity.Property(e => e.Tstamp)
                .IsRequired()
                .IsRowVersion()
                .IsConcurrencyToken()
                .HasColumnName("tstamp");
            entity.Property(e => e.User1)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.User2)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.User5)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.User6)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.User7)
                .HasDefaultValueSql("('01/01/1900')")
                .HasColumnType("smalldatetime");
            entity.Property(e => e.User8)
                .HasDefaultValueSql("('01/01/1900')")
                .HasColumnType("smalldatetime");
        });

        modelBuilder.Entity<SlsPrcDet>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("SlsPrcDet");

            entity.HasIndex(e => new { e.SlsPrcId, e.DetRef }, "SlsPrcDet0")
                .IsUnique()
                .IsClustered();

            entity.Property(e => e.CrtdDateTime)
                .HasDefaultValueSql("(rtrim(CONVERT([varchar](30),CONVERT([smalldatetime],getdate(),(0)),(0))))")
                .HasColumnType("smalldatetime")
                .HasColumnName("Crtd_DateTime");
            entity.Property(e => e.CrtdProg)
                .IsRequired()
                .HasMaxLength(8)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("Crtd_Prog");
            entity.Property(e => e.CrtdUser)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("Crtd_User");
            entity.Property(e => e.DetRef)
                .IsRequired()
                .HasMaxLength(5)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.EndDate)
                .HasDefaultValueSql("('01/01/1900')")
                .HasColumnType("smalldatetime");
            entity.Property(e => e.LupdDateTime)
                .HasDefaultValueSql("('01/01/1900')")
                .HasColumnType("smalldatetime")
                .HasColumnName("LUpd_DateTime");
            entity.Property(e => e.LupdProg)
                .IsRequired()
                .HasMaxLength(8)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("LUpd_Prog");
            entity.Property(e => e.LupdUser)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("LUpd_User");
            entity.Property(e => e.NoteId).HasColumnName("NoteID");
            entity.Property(e => e.PrcLvlId)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("PrcLvlID");
            entity.Property(e => e.S4future01)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("S4Future01");
            entity.Property(e => e.S4future02)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("S4Future02");
            entity.Property(e => e.S4future03).HasColumnName("S4Future03");
            entity.Property(e => e.S4future04).HasColumnName("S4Future04");
            entity.Property(e => e.S4future05).HasColumnName("S4Future05");
            entity.Property(e => e.S4future06).HasColumnName("S4Future06");
            entity.Property(e => e.S4future07)
                .HasDefaultValueSql("('01/01/1900')")
                .HasColumnType("smalldatetime")
                .HasColumnName("S4Future07");
            entity.Property(e => e.S4future08)
                .HasDefaultValueSql("('01/01/1900')")
                .HasColumnType("smalldatetime")
                .HasColumnName("S4Future08");
            entity.Property(e => e.S4future09).HasColumnName("S4Future09");
            entity.Property(e => e.S4future10).HasColumnName("S4Future10");
            entity.Property(e => e.S4future11)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("S4Future11");
            entity.Property(e => e.S4future12)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("S4Future12");
            entity.Property(e => e.SlsPrcId)
                .IsRequired()
                .HasMaxLength(15)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength()
                .HasColumnName("SlsPrcID");
            entity.Property(e => e.SlsUnit)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.StartDate)
                .HasDefaultValueSql("('01/01/1900')")
                .HasColumnType("smalldatetime");
            entity.Property(e => e.Tstamp)
                .IsRequired()
                .IsRowVersion()
                .IsConcurrencyToken()
                .HasColumnName("tstamp");
            entity.Property(e => e.User1)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.User2)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.User5)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.User6)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("(' ')")
                .IsFixedLength();
            entity.Property(e => e.User7)
                .HasDefaultValueSql("('01/01/1900')")
                .HasColumnType("smalldatetime");
            entity.Property(e => e.User8)
                .HasDefaultValueSql("('01/01/1900')")
                .HasColumnType("smalldatetime");
        });

        modelBuilder.Entity<Snote>(entity =>
        {
            entity.HasKey(e => e.NId).HasName("SNote0");

            entity.ToTable("Snote");

            entity.Property(e => e.NId).HasColumnName("nID");
            entity.Property(e => e.DtRevisedDate)
                .HasColumnType("smalldatetime")
                .HasColumnName("dtRevisedDate");
            entity.Property(e => e.SLevelName)
                .IsRequired()
                .HasMaxLength(20)
                .IsUnicode(false)
                .IsFixedLength()
                .HasColumnName("sLevelName");
            entity.Property(e => e.SNoteText)
                .IsRequired()
                .HasColumnType("text")
                .HasColumnName("sNoteText");
            entity.Property(e => e.STableName)
                .IsRequired()
                .HasMaxLength(20)
                .IsUnicode(false)
                .IsFixedLength()
                .HasColumnName("sTableName");
            entity.Property(e => e.Tstamp)
                .IsRequired()
                .IsRowVersion()
                .IsConcurrencyToken()
                .HasColumnName("tstamp");
        });

        modelBuilder.Entity<VrxErpInventory>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("VRX_ErpInventory");

            entity.Property(e => e.Brand)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.CountryOfOrigin)
                .HasMaxLength(20)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.Description)
                .IsRequired()
                .HasMaxLength(60)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.HarmonizedCode)
                .HasMaxLength(20)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.InventoryId)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.MasterPartNumber)
                .IsRequired()
                .HasMaxLength(24)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.PackUom)
                .HasMaxLength(6)
                .IsUnicode(false)
                .IsFixedLength()
                .HasColumnName("PackUOM");
            entity.Property(e => e.SpecialHandling)
                .HasMaxLength(10)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.TransactionStatus)
                .IsRequired()
                .HasMaxLength(2)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.Upccode)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .IsFixedLength()
                .HasColumnName("UPCCode");
            entity.Property(e => e.WarrantyId)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .IsFixedLength();
        });

        modelBuilder.Entity<VrxItemMaster>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("VRX_ItemMaster");

            entity.Property(e => e.Abccode)
                .IsRequired()
                .HasMaxLength(2)
                .IsUnicode(false)
                .IsFixedLength()
                .HasColumnName("ABCCode");
            entity.Property(e => e.BkQtyOnPo).HasColumnName("BkQtyOnPO");
            entity.Property(e => e.BkqtyAvail)
                .HasMaxLength(23)
                .IsUnicode(false)
                .HasColumnName("BKQtyAvail");
            entity.Property(e => e.CalTestMasterPart)
                .IsRequired()
                .HasMaxLength(24)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.CalTestPriceListType)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.ChkOrdQty)
                .IsRequired()
                .HasMaxLength(1)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.ClassId)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false)
                .IsFixedLength()
                .HasColumnName("ClassID");
            entity.Property(e => e.CountryOrig)
                .IsRequired()
                .HasMaxLength(20)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.CtdistyStockItem)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .IsFixedLength()
                .HasColumnName("CTDistyStockItem");
            entity.Property(e => e.Ctproduct)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .IsFixedLength()
                .HasColumnName("CTProduct");
            entity.Property(e => e.Ctproduct1).HasColumnName("CTProduct#");
            entity.Property(e => e.CurrentActiveProd)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.CycleId)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .IsFixedLength()
                .HasColumnName("CycleID");
            entity.Property(e => e.Descr)
                .IsRequired()
                .HasMaxLength(60)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.DfltPounit)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false)
                .IsFixedLength()
                .HasColumnName("DfltPOUnit");
            entity.Property(e => e.DfltSounit)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false)
                .IsFixedLength()
                .HasColumnName("DfltSOUnit");
            entity.Property(e => e.Eoq).HasColumnName("EOQ");
            entity.Property(e => e.FirstRecDateCalc).HasColumnType("smalldatetime");
            entity.Property(e => e.FirstRecDateManual).HasColumnType("smalldatetime");
            entity.Property(e => e.HarmonizeCode)
                .IsRequired()
                .HasMaxLength(20)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.InvtId)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .IsFixedLength()
                .HasColumnName("InvtID");
            entity.Property(e => e.InvtType)
                .IsRequired()
                .HasMaxLength(1)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.LastIntranDate)
                .HasColumnType("smalldatetime")
                .HasColumnName("LastINTranDate");
            entity.Property(e => e.LastShipDate).HasColumnType("smalldatetime");
            entity.Property(e => e.LotPolicy)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.LotSerTrack)
                .IsRequired()
                .HasMaxLength(2)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.MaterialType)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.Name)
                .HasMaxLength(60)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.Pkgtype)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .IsFixedLength()
                .HasColumnName("PKGTYPE");
            entity.Property(e => e.PriceClassId)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false)
                .IsFixedLength()
                .HasColumnName("PriceClassID");
            entity.Property(e => e.ProdLineId)
                .IsRequired()
                .HasMaxLength(4)
                .IsUnicode(false)
                .IsFixedLength()
                .HasColumnName("ProdLineID");
            entity.Property(e => e.Scheight).HasColumnName("SCHeight");
            entity.Property(e => e.Sclen).HasColumnName("SCLen");
            entity.Property(e => e.Scweight).HasColumnName("SCWeight");
            entity.Property(e => e.Scwidth).HasColumnName("SCWidth");
            entity.Property(e => e.Source)
                .IsRequired()
                .HasMaxLength(1)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.SpecialHandling)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.StkUnit)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.Supplr1)
                .IsRequired()
                .HasMaxLength(15)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.Supplr2)
                .IsRequired()
                .HasMaxLength(15)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.TaxCat)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.TranStatusCode)
                .IsRequired()
                .HasMaxLength(2)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.Upccode)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .IsFixedLength()
                .HasColumnName("UPCCode");
            entity.Property(e => e.UspriceList)
                .IsRequired()
                .HasMaxLength(5)
                .IsUnicode(false)
                .IsFixedLength()
                .HasColumnName("USPriceList");
            entity.Property(e => e.ValMthd)
                .IsRequired()
                .HasMaxLength(1)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.Warranty)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.Website)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e._2weekavg).HasColumnName("2weekavg");
        });

        modelBuilder.Entity<VrxOrderswebCte>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("VRX_ORDERSWEB_CTE");

            entity.Property(e => e.AlternateId)
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasColumnName("AlternateID");
            entity.Property(e => e.BillName)
                .HasMaxLength(60)
                .IsUnicode(false);
            entity.Property(e => e.CurySlsPrice).HasColumnType("decimal(15, 2)");
            entity.Property(e => e.CuryTotMerch).HasColumnType("decimal(15, 2)");
            entity.Property(e => e.CuryTotOrd).HasColumnType("decimal(15, 2)");
            entity.Property(e => e.CustId)
                .HasMaxLength(15)
                .IsUnicode(false)
                .HasColumnName("CustID");
            entity.Property(e => e.CustOrdNbr)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.Eta)
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasColumnName("ETA");
            entity.Property(e => e.ExtPrice).HasColumnType("decimal(15, 2)");
            entity.Property(e => e.InvtId)
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasColumnName("InvtID");
            entity.Property(e => e.OrdDate)
                .HasMaxLength(10)
                .IsUnicode(false);
            entity.Property(e => e.OrdNbr)
                .HasMaxLength(15)
                .IsUnicode(false);
            entity.Property(e => e.QtyOrd)
                .HasMaxLength(23)
                .IsUnicode(false);
            entity.Property(e => e.QtyShip)
                .HasMaxLength(23)
                .IsUnicode(false);
            entity.Property(e => e.RevisedEtaDate)
                .HasColumnType("smalldatetime")
                .HasColumnName("Revised ETA Date");
            entity.Property(e => e.ShipVia)
                .HasMaxLength(15)
                .IsUnicode(false);
            entity.Property(e => e.Status)
                .HasMaxLength(1)
                .IsUnicode(false);
        });

        modelBuilder.Entity<VrxTrackingNumber>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("VRX_TrackingNumbers");

            entity.Property(e => e.InvtId)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .IsFixedLength()
                .HasColumnName("InvtID");
            entity.Property(e => e.OrdNbr)
                .IsRequired()
                .HasMaxLength(15)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.TrackingNbr)
                .HasMaxLength(20)
                .IsUnicode(false)
                .IsFixedLength();
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
