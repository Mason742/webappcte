﻿using System;
using System.Collections.Generic;

namespace SolomonDB.Data.Models;

public partial class MasonKitPrice
{
    public string InvtId { get; set; }

    public double Dprice { get; set; }

    public double NewPrice { get; set; }

    public string Note { get; set; }
}
