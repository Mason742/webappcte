﻿using System;
using System.Collections.Generic;

namespace SolomonDB.Data.Models;

public partial class SlsPrc
{
    public string CatalogNbr { get; set; }

    public string ContractNbr { get; set; }

    public double ContractQty { get; set; }

    public DateTime CrtdDateTime { get; set; }

    public string CrtdProg { get; set; }

    public string CrtdUser { get; set; }

    public string CuryId { get; set; }

    public string CustClassId { get; set; }

    public string CustId { get; set; }

    public string DetCntr { get; set; }

    public string DiscPrcMthd { get; set; }

    public string DiscPrcTyp { get; set; }

    public string InvtId { get; set; }

    public DateTime LupdDateTime { get; set; }

    public string LupdProg { get; set; }

    public string LupdUser { get; set; }

    public double MaxQty { get; set; }

    public double MinQty { get; set; }

    public int NoteId { get; set; }

    public string PrcLvlId { get; set; }

    public string PriceCat { get; set; }

    public string PriceClassId { get; set; }

    public string S4future01 { get; set; }

    public string S4future02 { get; set; }

    public double S4future03 { get; set; }

    public double S4future04 { get; set; }

    public double S4future05 { get; set; }

    public double S4future06 { get; set; }

    public DateTime S4future07 { get; set; }

    public DateTime S4future08 { get; set; }

    public int S4future09 { get; set; }

    public int S4future10 { get; set; }

    public string S4future11 { get; set; }

    public string S4future12 { get; set; }

    public string SelectFld1 { get; set; }

    public string SelectFld2 { get; set; }

    public string SiteId { get; set; }

    public string SlsPrcId { get; set; }

    public string User1 { get; set; }

    public string User2 { get; set; }

    public double User3 { get; set; }

    public double User4 { get; set; }

    public string User5 { get; set; }

    public string User6 { get; set; }

    public DateTime User7 { get; set; }

    public DateTime User8 { get; set; }

    public byte[] Tstamp { get; set; }
}
