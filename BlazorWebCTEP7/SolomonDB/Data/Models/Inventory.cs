﻿using System;
using System.Collections.Generic;

namespace SolomonDB.Data.Models;

public partial class Inventory
{
    public string Abccode { get; set; }

    public short ApprovedVendor { get; set; }

    public short AutoPodropShip { get; set; }

    public string AutoPopolicy { get; set; }

    public double BmidirStdCost { get; set; }

    public double BmifovhStdCost { get; set; }

    public double BmilastCost { get; set; }

    public double BmipdirStdCost { get; set; }

    public double BmipfovhStdCost { get; set; }

    public double BmipstdCost { get; set; }

    public double BmipvovhStdCost { get; set; }

    public double BmistdCost { get; set; }

    public double BmivovhStdCost { get; set; }

    public string Bolcode { get; set; }

    public string Buyer { get; set; }

    public string ChkOrdQty { get; set; }

    public string ClassId { get; set; }

    public string Cogsacct { get; set; }

    public string Cogssub { get; set; }

    public string Color { get; set; }

    public string CountStatus { get; set; }

    public DateTime CrtdDateTime { get; set; }

    public string CrtdProg { get; set; }

    public string CrtdUser { get; set; }

    public double CuryListPrice { get; set; }

    public double CuryMinPrice { get; set; }

    public short CustomFtr { get; set; }

    public string CycleId { get; set; }

    public string Descr { get; set; }

    public string DfltPickLoc { get; set; }

    public string DfltPounit { get; set; }

    public string DfltSalesAcct { get; set; }

    public string DfltSalesSub { get; set; }

    public string DfltShpnotInvAcct { get; set; }

    public string DfltShpnotInvSub { get; set; }

    public string DfltSite { get; set; }

    public string DfltSounit { get; set; }

    public string DfltWhseLoc { get; set; }

    public double DirStdCost { get; set; }

    public string DiscAcct { get; set; }

    public string DiscPrc { get; set; }

    public string DiscSub { get; set; }

    public double Eoq { get; set; }

    public short ExplInvoice { get; set; }

    public short ExplOrder { get; set; }

    public short ExplPackSlip { get; set; }

    public short ExplPickList { get; set; }

    public short ExplShipping { get; set; }

    public double FovhStdCost { get; set; }

    public string FrtAcct { get; set; }

    public string FrtSub { get; set; }

    public string GlclassId { get; set; }

    public string InvtAcct { get; set; }

    public string InvtId { get; set; }

    public string InvtSub { get; set; }

    public string InvtType { get; set; }

    public string IrcalcPolicy { get; set; }

    public double IrdaysSupply { get; set; }

    public string IrdemandId { get; set; }

    public DateTime IrfutureDate { get; set; }

    public string IrfuturePolicy { get; set; }

    public string IrleadTimeId { get; set; }

    public double IrlinePtQty { get; set; }

    public double IrminOnHand { get; set; }

    public string IrmodelInvtId { get; set; }

    public short IrrcycDays { get; set; }

    public short IrseasonEndDay { get; set; }

    public short IrseasonEndMon { get; set; }

    public short IrseasonStrtDay { get; set; }

    public short IrseasonStrtMon { get; set; }

    public double IrserviceLevel { get; set; }

    public double IrsftyStkDays { get; set; }

    public double IrsftyStkPct { get; set; }

    public string IrsftyStkPolicy { get; set; }

    public string IrsourceCode { get; set; }

    public string IrtargetOrdMethod { get; set; }

    public double IrtargetOrdReq { get; set; }

    public string IrtransferSiteId { get; set; }

    public string ItemCommClassId { get; set; }

    public short Kit { get; set; }

    public double LastBookQty { get; set; }

    public double LastCost { get; set; }

    public DateTime LastCountDate { get; set; }

    public string LastSiteId { get; set; }

    public double LastStdCost { get; set; }

    public double LastVarAmt { get; set; }

    public double LastVarPct { get; set; }

    public double LastVarQty { get; set; }

    public string LcvarianceAcct { get; set; }

    public string LcvarianceSub { get; set; }

    public double LeadTime { get; set; }

    public short LinkSpecId { get; set; }

    public short LotSerFxdLen { get; set; }

    public string LotSerFxdTyp { get; set; }

    public string LotSerFxdVal { get; set; }

    public string LotSerIssMthd { get; set; }

    public short LotSerNumLen { get; set; }

    public string LotSerNumVal { get; set; }

    public string LotSerTrack { get; set; }

    public DateTime LupdDateTime { get; set; }

    public string LupdProg { get; set; }

    public string LupdUser { get; set; }

    public string MaterialType { get; set; }

    public double MaxOnHand { get; set; }

    public string MfgClassId { get; set; }

    public double MfgLeadTime { get; set; }

    public double MinGrossProfit { get; set; }

    public string MoveClass { get; set; }

    public string Msds { get; set; }

    public int NoteId { get; set; }

    public string Pack { get; set; }

    public double PdirStdCost { get; set; }

    public string PerNbr { get; set; }

    public double PfovhStdCost { get; set; }

    public string Ppvacct { get; set; }

    public string Ppvsub { get; set; }

    public string PriceClassId { get; set; }

    public string ProdMgrId { get; set; }

    public string ProductionUnit { get; set; }

    public double PstdCost { get; set; }

    public DateTime PstdCostDate { get; set; }

    public double PvovhStdCost { get; set; }

    public double ReordPt { get; set; }

    public double ReOrdPtCalc { get; set; }

    public double ReordQty { get; set; }

    public double ReOrdQtyCalc { get; set; }

    public string ReplMthd { get; set; }

    public short RollupCost { get; set; }

    public short RollupPrice { get; set; }

    public short RvsdPrc { get; set; }

    public string S4future01 { get; set; }

    public string S4future02 { get; set; }

    public double S4future03 { get; set; }

    public double S4future04 { get; set; }

    public double S4future05 { get; set; }

    public double S4future06 { get; set; }

    public DateTime S4future07 { get; set; }

    public DateTime S4future08 { get; set; }

    public int S4future09 { get; set; }

    public int S4future10 { get; set; }

    public string S4future11 { get; set; }

    public string S4future12 { get; set; }

    public string S4future13 { get; set; }

    public double SafetyStk { get; set; }

    public double SafetyStkCalc { get; set; }

    public short Selected { get; set; }

    public string SerAssign { get; set; }

    public short Service { get; set; }

    public short ShelfLife { get; set; }

    public string Size { get; set; }

    public string Source { get; set; }

    public string Status { get; set; }

    public double StdCost { get; set; }

    public DateTime StdCostDate { get; set; }

    public double StkBasePrc { get; set; }

    public short StkItem { get; set; }

    public double StkRvsdPrc { get; set; }

    public double StkTaxBasisPrc { get; set; }

    public string StkUnit { get; set; }

    public double StkVol { get; set; }

    public double StkWt { get; set; }

    public string StkWtUnit { get; set; }

    public string Style { get; set; }

    public string Supplr1 { get; set; }

    public string Supplr2 { get; set; }

    public string SupplrItem1 { get; set; }

    public string SupplrItem2 { get; set; }

    public string TaskId { get; set; }

    public string TaxCat { get; set; }

    public string TranStatusCode { get; set; }

    public double Turns { get; set; }

    public string Upccode { get; set; }

    public double UsageRate { get; set; }

    public string User1 { get; set; }

    public string User2 { get; set; }

    public double User3 { get; set; }

    public double User4 { get; set; }

    public string User5 { get; set; }

    public string User6 { get; set; }

    public DateTime User7 { get; set; }

    public DateTime User8 { get; set; }

    public string ValMthd { get; set; }

    public double VovhStdCost { get; set; }

    public short WarrantyDays { get; set; }

    public double Ytdusage { get; set; }

    public byte[] Tstamp { get; set; }
}
