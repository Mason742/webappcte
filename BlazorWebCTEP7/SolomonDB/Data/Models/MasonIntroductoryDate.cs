﻿using System;
using System.Collections.Generic;

namespace SolomonDB.Data.Models;

public partial class MasonIntroductoryDate
{
    public string InvtId { get; set; }

    public DateTime? FirstReceivedDate { get; set; }

    public DateTime? KitCreatedDate { get; set; }

    public DateTime InventoryCreatedDate { get; set; }

    public DateTime IntroductoryDate { get; set; }
}
