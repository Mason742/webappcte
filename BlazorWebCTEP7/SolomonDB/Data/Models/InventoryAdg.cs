﻿using System;
using System.Collections.Generic;

namespace SolomonDB.Data.Models;

public partial class InventoryAdg
{
    public short AllowGenCont { get; set; }

    public double BatchSize { get; set; }

    public string Bolclass { get; set; }

    public string CategoryCode { get; set; }

    public string CountryOrig { get; set; }

    public DateTime CrtdDateTime { get; set; }

    public string CrtdProg { get; set; }

    public string CrtdUser { get; set; }

    public double Density { get; set; }

    public string DensityUom { get; set; }

    public double Depth { get; set; }

    public string DepthUom { get; set; }

    public double Diameter { get; set; }

    public string DiameterUom { get; set; }

    public double Gauge { get; set; }

    public string GaugeUom { get; set; }

    public double Height { get; set; }

    public string HeightUom { get; set; }

    public string InvtId { get; set; }

    public double Len { get; set; }

    public string LenUom { get; set; }

    public double ListPrice { get; set; }

    public DateTime LupdDateTime { get; set; }

    public string LupdProg { get; set; }

    public string LupdUser { get; set; }

    public double MinPrice { get; set; }

    public int NoteId { get; set; }

    public string Omcogsacct { get; set; }

    public string Omcogssub { get; set; }

    public string OmsalesAcct { get; set; }

    public string OmsalesSub { get; set; }

    public short Pack { get; set; }

    public double PackCnvFact { get; set; }

    public string PackMethod { get; set; }

    public short PackSize { get; set; }

    public string PackUnitMultDiv { get; set; }

    public string PackUom { get; set; }

    public string ProdLineId { get; set; }

    public double RetailPrice { get; set; }

    public string RoyaltyCode { get; set; }

    public string S4future01 { get; set; }

    public string S4future02 { get; set; }

    public double S4future03 { get; set; }

    public double S4future04 { get; set; }

    public double S4future05 { get; set; }

    public double S4future06 { get; set; }

    public DateTime S4future07 { get; set; }

    public DateTime S4future08 { get; set; }

    public int S4future09 { get; set; }

    public int S4future10 { get; set; }

    public string S4future11 { get; set; }

    public string S4future12 { get; set; }

    public double Scheight { get; set; }

    public string ScheightUom { get; set; }

    public double Sclen { get; set; }

    public string SclenUom { get; set; }

    public double Scvolume { get; set; }

    public string ScvolumeUom { get; set; }

    public double Scweight { get; set; }

    public string ScweightUom { get; set; }

    public double Scwidth { get; set; }

    public string ScwidthUom { get; set; }

    public short StdCartonBreak { get; set; }

    public double StdGrossWt { get; set; }

    public double StdTareWt { get; set; }

    public string Style { get; set; }

    public string User1 { get; set; }

    public DateTime User10 { get; set; }

    public string User2 { get; set; }

    public string User3 { get; set; }

    public string User4 { get; set; }

    public double User5 { get; set; }

    public double User6 { get; set; }

    public string User7 { get; set; }

    public string User8 { get; set; }

    public DateTime User9 { get; set; }

    public double Volume { get; set; }

    public string VolUom { get; set; }

    public double Weight { get; set; }

    public string WeightUom { get; set; }

    public double Width { get; set; }

    public string WidthUom { get; set; }

    public byte[] Tstamp { get; set; }
}
