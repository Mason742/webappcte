﻿using System;
using System.Collections.Generic;

namespace SolomonDB.Data.Models;

public partial class Customer
{
    public string AccrRevAcct { get; set; }

    public string AccrRevSub { get; set; }

    public string AcctNbr { get; set; }

    public string Addr1 { get; set; }

    public string Addr2 { get; set; }

    public string AgentId { get; set; }

    public short ApplFinChrg { get; set; }

    public string ArAcct { get; set; }

    public string ArSub { get; set; }

    public string Attn { get; set; }

    public short AutoApply { get; set; }

    public string BankId { get; set; }

    public string BillAddr1 { get; set; }

    public string BillAddr2 { get; set; }

    public string BillAttn { get; set; }

    public string BillCity { get; set; }

    public string BillCountry { get; set; }

    public string BillFax { get; set; }

    public string BillName { get; set; }

    public string BillPhone { get; set; }

    public string BillSalut { get; set; }

    public string BillState { get; set; }

    public short BillThruProject { get; set; }

    public string BillZip { get; set; }

    public DateTime CardExpDate { get; set; }

    public string CardHldrName { get; set; }

    public string CardNbr { get; set; }

    public string CardType { get; set; }

    public string City { get; set; }

    public string ClassId { get; set; }

    public short ConsolInv { get; set; }

    public string Country { get; set; }

    public double CrLmt { get; set; }

    public DateTime CrtdDateTime { get; set; }

    public string CrtdProg { get; set; }

    public string CrtdUser { get; set; }

    public string CuryId { get; set; }

    public string CuryPrcLvlRtTp { get; set; }

    public string CuryRateType { get; set; }

    public short CustFillPriority { get; set; }

    public string CustId { get; set; }

    public string DfltShipToId { get; set; }

    public string DocPublishingFlag { get; set; }

    public short DunMsg { get; set; }

    public string EmailAddr { get; set; }

    public string Fax { get; set; }

    public short InvtSubst { get; set; }

    public string LanguageId { get; set; }

    public DateTime LupdDateTime { get; set; }

    public string LupdProg { get; set; }

    public string LupdUser { get; set; }

    public string Name { get; set; }

    public int NoteId { get; set; }

    public short OneDraft { get; set; }

    public string PerNbr { get; set; }

    public string Phone { get; set; }

    public string PmtMethod { get; set; }

    public string PrcLvlId { get; set; }

    public string PrePayAcct { get; set; }

    public string PrePaySub { get; set; }

    public string PriceClassId { get; set; }

    public short PrtMcstmt { get; set; }

    public short PrtStmt { get; set; }

    public string S4future01 { get; set; }

    public string S4future02 { get; set; }

    public double S4future03 { get; set; }

    public double S4future04 { get; set; }

    public double S4future05 { get; set; }

    public double S4future06 { get; set; }

    public DateTime S4future07 { get; set; }

    public DateTime S4future08 { get; set; }

    public int S4future09 { get; set; }

    public int S4future10 { get; set; }

    public string S4future11 { get; set; }

    public string S4future12 { get; set; }

    public string Salut { get; set; }

    public DateTime SetupDate { get; set; }

    public short ShipCmplt { get; set; }

    public string ShipPctAct { get; set; }

    public double ShipPctMax { get; set; }

    public string Siccode1 { get; set; }

    public string Siccode2 { get; set; }

    public short SingleInvoice { get; set; }

    public string SlsAcct { get; set; }

    public string SlsperId { get; set; }

    public string SlsSub { get; set; }

    public string State { get; set; }

    public string Status { get; set; }

    public string StmtCycleId { get; set; }

    public string StmtType { get; set; }

    public string TaxDflt { get; set; }

    public string TaxExemptNbr { get; set; }

    public string TaxId00 { get; set; }

    public string TaxId01 { get; set; }

    public string TaxId02 { get; set; }

    public string TaxId03 { get; set; }

    public string TaxLocId { get; set; }

    public string TaxRegNbr { get; set; }

    public string Terms { get; set; }

    public string Territory { get; set; }

    public double TradeDisc { get; set; }

    public string User1 { get; set; }

    public string User2 { get; set; }

    public double User3 { get; set; }

    public double User4 { get; set; }

    public string User5 { get; set; }

    public string User6 { get; set; }

    public DateTime User7 { get; set; }

    public DateTime User8 { get; set; }

    public string Zip { get; set; }

    public byte[] Tstamp { get; set; }
}
