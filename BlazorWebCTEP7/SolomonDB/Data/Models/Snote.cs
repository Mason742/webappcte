﻿using System;
using System.Collections.Generic;

namespace SolomonDB.Data.Models;

public partial class Snote
{
    public DateTime DtRevisedDate { get; set; }

    public int NId { get; set; }

    public string SLevelName { get; set; }

    public string STableName { get; set; }

    public string SNoteText { get; set; }

    public byte[] Tstamp { get; set; }
}
