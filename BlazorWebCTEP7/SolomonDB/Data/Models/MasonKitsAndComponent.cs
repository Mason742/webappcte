﻿using System;
using System.Collections.Generic;

namespace SolomonDB.Data.Models;

public partial class MasonKitsAndComponent
{
    public string KitId { get; set; }

    public double KitMsrp { get; set; }

    public string KitDesc { get; set; }

    public string KitHandling { get; set; }

    public string CompId { get; set; }

    public double CompMsrp { get; set; }

    public double CmpnentQty { get; set; }

    public string CompDesc { get; set; }

    public string CompHandling { get; set; }

    public double CompLastCost { get; set; }

    public double CompStdCost { get; set; }

    public int IsKit { get; set; }
}
