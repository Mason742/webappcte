﻿using System;
using System.Collections.Generic;

namespace SolomonDB.Data.Models;

public partial class VrxOrderswebCte
{
    public string CustId { get; set; }

    public string BillName { get; set; }

    public string OrdNbr { get; set; }

    public string Status { get; set; }

    public string CustOrdNbr { get; set; }

    public string OrdDate { get; set; }

    public decimal? CuryTotMerch { get; set; }

    public decimal? CuryTotOrd { get; set; }

    public string InvtId { get; set; }

    public string AlternateId { get; set; }

    public string QtyOrd { get; set; }

    public string QtyShip { get; set; }

    public decimal? CurySlsPrice { get; set; }

    public decimal? ExtPrice { get; set; }

    public string Eta { get; set; }

    public string ShipVia { get; set; }

    public DateTime? RevisedEtaDate { get; set; }
}
