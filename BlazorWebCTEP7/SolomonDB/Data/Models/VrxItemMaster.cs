﻿using System;
using System.Collections.Generic;

namespace SolomonDB.Data.Models;

public partial class VrxItemMaster
{
    public string InvtId { get; set; }

    public string Descr { get; set; }

    public string ClassId { get; set; }

    public string PriceClassId { get; set; }

    public string InvtType { get; set; }

    public string ValMthd { get; set; }

    public string LotSerTrack { get; set; }

    public string CalTestMasterPart { get; set; }

    public string MaterialType { get; set; }

    public string TaxCat { get; set; }

    public string TranStatusCode { get; set; }

    public short Kit { get; set; }

    public short StkItem { get; set; }

    public string ChkOrdQty { get; set; }

    public string StkUnit { get; set; }

    public string DfltPounit { get; set; }

    public string DfltSounit { get; set; }

    public string CalTestPriceListType { get; set; }

    public string Warranty { get; set; }

    public double LastCost { get; set; }

    public double StkBasePrc { get; set; }

    public string Ctproduct { get; set; }

    public double Ctproduct1 { get; set; }

    public string Supplr1 { get; set; }

    public string Name { get; set; }

    public string Supplr2 { get; set; }

    public string HarmonizeCode { get; set; }

    public string CountryOrig { get; set; }

    public double ReordQty { get; set; }

    public double ReordPt { get; set; }

    public double Eoq { get; set; }

    public string CurrentActiveProd { get; set; }

    public string UspriceList { get; set; }

    public string CtdistyStockItem { get; set; }

    public double NewStockBasePrice { get; set; }

    public double HistoricalPrice { get; set; }

    public string Pkgtype { get; set; }

    public double? QtySoldLast24Mo { get; set; }

    public double? AmtSoldLast24Mo { get; set; }

    public double? QtyOnHand { get; set; }

    public double? QtyRetLast24Mo { get; set; }

    public int? CustCountLast24Mo { get; set; }

    public string ProdLineId { get; set; }

    public string Website { get; set; }

    public double SafetyStk { get; set; }

    public string Source { get; set; }

    public string SpecialHandling { get; set; }

    public double LeadTime { get; set; }

    public double? QtyAvail { get; set; }

    public double? QtyAllAvail { get; set; }

    public DateTime? LastShipDate { get; set; }

    public double Length { get; set; }

    public double Width { get; set; }

    public double Height { get; set; }

    public double Weight { get; set; }

    public double Sclen { get; set; }

    public double Scwidth { get; set; }

    public double Scheight { get; set; }

    public double Scweight { get; set; }

    public string CycleId { get; set; }

    public double RepairPrice { get; set; }

    public double EvalCharge { get; set; }

    public double AcctStdCost { get; set; }

    public string Upccode { get; set; }

    public int CalcTrend { get; set; }

    public string Abccode { get; set; }

    public double MfgLeadTime { get; set; }

    public double? _2weekavg { get; set; }

    public double? Prior2weekavg { get; set; }

    public double? RecomendedSafetyStock { get; set; }

    public short Pack { get; set; }

    public string LotPolicy { get; set; }

    public double LotSize { get; set; }

    public DateTime? LastIntranDate { get; set; }

    public DateTime? FirstRecDateCalc { get; set; }

    public DateTime FirstRecDateManual { get; set; }

    public string BkqtyAvail { get; set; }

    public double? BkQtyOnPo { get; set; }
}
