﻿using System;
using System.Collections.Generic;

namespace SolomonDB.Data.Models;

public partial class Potran
{
    public string Acct { get; set; }

    public short AcctDist { get; set; }

    public double AddlCost { get; set; }

    public double AddlCostPct { get; set; }

    public double AddlCostVouch { get; set; }

    public string AlternateId { get; set; }

    public string AltIdtype { get; set; }

    public short AplineId { get; set; }

    public string AplineRef { get; set; }

    public string BatNbr { get; set; }

    public string BmicuryId { get; set; }

    public DateTime BmieffDate { get; set; }

    public double BmiextCost { get; set; }

    public string BmimultDiv { get; set; }

    public double Bmirate { get; set; }

    public string BmirtTp { get; set; }

    public double BmitranAmt { get; set; }

    public double BmiunitCost { get; set; }

    public double BmiunitPrice { get; set; }

    public string BomlineRef { get; set; }

    public short Bomsequence { get; set; }

    public double CnvFact { get; set; }

    public double CostVouched { get; set; }

    public string CpnyId { get; set; }

    public DateTime CrtdDateTime { get; set; }

    public string CrtdProg { get; set; }

    public string CrtdUser { get; set; }

    public double CuryAddlCost { get; set; }

    public double CuryAddlCostVouch { get; set; }

    public double CuryCostVouched { get; set; }

    public double CuryExtCost { get; set; }

    public string CuryId { get; set; }

    public string CuryMultDiv { get; set; }

    public double CuryRate { get; set; }

    public double CuryTranAmt { get; set; }

    public double CuryUnitCost { get; set; }

    public string DrCr { get; set; }

    public double ExtCost { get; set; }

    public double ExtWeight { get; set; }

    public short FlatRateLineNbr { get; set; }

    public string InvtId { get; set; }

    public string JrnlType { get; set; }

    public string LaborClassCd { get; set; }

    public int LineId { get; set; }

    public short LineNbr { get; set; }

    public string LineRef { get; set; }

    public DateTime LupdDateTime { get; set; }

    public string LupdProg { get; set; }

    public string LupdUser { get; set; }

    public int NoteId { get; set; }

    public DateTime OrigRcptDate { get; set; }

    public string OrigRcptNbr { get; set; }

    public string OrigRetRcptNbr { get; set; }

    public string PcFlag { get; set; }

    public string PcId { get; set; }

    public string PcStatus { get; set; }

    public string PerEnt { get; set; }

    public string PerPost { get; set; }

    public int PolineId { get; set; }

    public short PolineNbr { get; set; }

    public string PolineRef { get; set; }

    public string Ponbr { get; set; }

    public string Pooriginal { get; set; }

    public string ProjectId { get; set; }

    public string PurchaseType { get; set; }

    public double Qty { get; set; }

    public double QtyVouched { get; set; }

    public double RcptConvFact { get; set; }

    public DateTime RcptDate { get; set; }

    public string RcptLineRefOrig { get; set; }

    public string RcptMultDiv { get; set; }

    public string RcptNbr { get; set; }

    public string RcptNbrOrig { get; set; }

    public double RcptQty { get; set; }

    public string RcptUnitDescr { get; set; }

    public string ReasonCd { get; set; }

    public string Refnbr { get; set; }

    public string S4future01 { get; set; }

    public string S4future02 { get; set; }

    public double S4future03 { get; set; }

    public double S4future04 { get; set; }

    public double S4future05 { get; set; }

    public double S4future06 { get; set; }

    public DateTime S4future07 { get; set; }

    public DateTime S4future08 { get; set; }

    public int S4future09 { get; set; }

    public int S4future10 { get; set; }

    public string S4future11 { get; set; }

    public string S4future12 { get; set; }

    public string ServiceCallId { get; set; }

    public string SiteId { get; set; }

    public int SolineId { get; set; }

    public string SolineRef { get; set; }

    public string SoordNbr { get; set; }

    public string SotypeId { get; set; }

    public string SpecificCostId { get; set; }

    public short StepNbr { get; set; }

    public string Sub { get; set; }

    public string SvcContractId { get; set; }

    public short SvcLineNbr { get; set; }

    public string TaskId { get; set; }

    public string TaxCat { get; set; }

    public string TaxIddflt { get; set; }

    public double TranAmt { get; set; }

    public DateTime TranDate { get; set; }

    public string TranDesc { get; set; }

    public string TranType { get; set; }

    public double UnitCost { get; set; }

    public string UnitDescr { get; set; }

    public string UnitMultDiv { get; set; }

    public double UnitWeight { get; set; }

    public string User1 { get; set; }

    public string User2 { get; set; }

    public double User3 { get; set; }

    public double User4 { get; set; }

    public string User5 { get; set; }

    public string User6 { get; set; }

    public DateTime User7 { get; set; }

    public DateTime User8 { get; set; }

    public string VendId { get; set; }

    public string VouchStage { get; set; }

    public string WhseLoc { get; set; }

    public string WipCogsAcct { get; set; }

    public string WipCogsSub { get; set; }

    public string WobomRef { get; set; }

    public string WocostType { get; set; }

    public string Wonbr { get; set; }

    public string WostepNbr { get; set; }

    public byte[] Tstamp { get; set; }
}
