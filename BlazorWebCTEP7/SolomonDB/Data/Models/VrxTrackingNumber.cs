﻿using System;
using System.Collections.Generic;

namespace SolomonDB.Data.Models;

public partial class VrxTrackingNumber
{
    public string InvtId { get; set; }

    public string OrdNbr { get; set; }

    public string TrackingNbr { get; set; }
}
