﻿using System;
using System.Collections.Generic;

namespace SolomonDB.Data.Models;

public partial class VrxErpInventory
{
    public string InventoryId { get; set; }

    public string TransactionStatus { get; set; }

    public string Description { get; set; }

    public int Discontinued { get; set; }

    public string SpecialHandling { get; set; }

    public string Upccode { get; set; }

    public string MasterPartNumber { get; set; }

    public string WarrantyId { get; set; }

    public double Msrp { get; set; }

    public double PoLeadTime { get; set; }

    public double ManufacturerLeadTime { get; set; }

    public string HarmonizedCode { get; set; }

    public string CountryOfOrigin { get; set; }

    public double RepairPrice { get; set; }

    public double EvaluationCharge { get; set; }

    public double CalibrationCharge { get; set; }

    public double? ShippingCost { get; set; }

    public double? QuantityAvailable { get; set; }

    public short? PacksPerCarton { get; set; }

    public short? PackSize { get; set; }

    public string PackUom { get; set; }

    public string Brand { get; set; }
}
