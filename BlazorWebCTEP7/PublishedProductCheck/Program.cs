﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using WebDB.Data.Context;
using WebDB.Data.Models;

namespace PublishedProductCheck
{
    class Program
    {
        static void Main()
        {
            List<Product> products = new();
            List<Series> series = new();
            using (CTEDbContext db = new())
            {
                products = db.Products.AsNoTracking().ToList();
                series = db.Series.ToList();

                foreach (var s in series)
                {
                    bool debugPublish = false;
                    foreach (var product in products.Where(x => x.SeriesId == s.Id))
                    {
                        if (product.Published == true)
                        {
                            Console.WriteLine(s.Name + ": should be published because of " + product.PartNumber.Trim());
                            s.Published = true;
                            debugPublish = true;
                            break;
                        }
                        else
                        {
                            s.Published = false;
                            debugPublish = false;
                        }
                    }
                    if (!products.Where(x => x.SeriesId == s.Id).Any())
                    {
                        s.Published = false;
                        debugPublish = false;
                    }

                    if(debugPublish == false)
                    {
                        Console.WriteLine("NOT Published because "+s.Name + ": does not have subproducts.");
                    }
                }

                db.SaveChanges();
            }
            Console.WriteLine("Exit");
        }
    }
}
