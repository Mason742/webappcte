﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EPPlusExcelService.Helpers
{
    public static class WorksheetHelper
    {
        public static int GetColumnByName(this ExcelWorksheet ws, string columnName)
        {
            if (ws == null) throw new ArgumentNullException(nameof(ws));
            return ws.Cells["0:0"].First(c => c.Value.ToString() == columnName).Start.Column;
        }
        //compare differences with Matthew !!!
        public static int GetColumnByNameV2(this ExcelWorksheet ws, string columnName)
        {
            if (ws == null) throw new ArgumentNullException(nameof(ws));
            ExcelRangeBase cells = ws.Cells["1:1"].FirstOrDefault(c => (c?.Value ?? string.Empty).ToString().ToLower() == columnName.ToLower());
            if (cells == null) throw new NullReferenceException($"No column named {columnName} found in ImportTable. \n Add or rename column.");
            return cells.Start.Column;
        }

        public static string ToStringOrEmptyWhenNull(this object value)
        {
            return value == null ? "" : value.ToString();
        }

        public static bool IsLastRowEmpty(this ExcelWorksheet worksheet)
        {
            var empties = new List<bool>();

            for (int i = 1; i <= worksheet.Dimension.End.Column; i++)
            {
                bool rowEmpty = worksheet.Cells[worksheet.Dimension.End.Row, i].Value == null;
                empties.Add(rowEmpty);
            }

            return empties.All(e => e);
        }

        public static int TrimLastEmptyRows(this ExcelWorksheet worksheet)
        {
            int deleted = 0;
            while (worksheet.IsLastRowEmpty())
            {
                worksheet.DeleteRow(worksheet.Dimension.End.Row);
                deleted++;
            }
            return deleted;
        }



    }

}
