﻿using EPPlusExcelService.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDB.Data.Models;

namespace EPPlusExcelService.Services
{
    public class ExcelService
    {
        //public static List<ProductCategory> ImportProductCategoriesToSQL(Stream excelFileStream)
        //{
        //    List<ProductCategory> productList = new List<ProductCategory>();
        //    using (ExcelPackage excelPackage = new ExcelPackage(excelFileStream))
        //    {
        //        ExcelWorksheet workSheet = excelPackage.Workbook.Worksheets[0];
        //        int totalRows = workSheet.Dimension.Rows;

        //        for (int i = 2; i <= totalRows; i++)
        //        {
        //            var p = new ProductCategory();
        //            p.Id = int.Parse(workSheet.Cells[i, WorksheetHelper.GetColumnByName(workSheet, nameof(p.Id))].Value.ToStringOrEmptyWhenNull());
        //            p.ProductFamilyId = int.Parse(workSheet.Cells[i, WorksheetHelper.GetColumnByName(workSheet, nameof(p.ProductFamilyId))].Value.ToStringOrEmptyWhenNull());
        //            p.MainCategory = workSheet.Cells[i, WorksheetHelper.GetColumnByName(workSheet, nameof(p.MainCategory))].Value.ToStringOrEmptyWhenNull();
        //            p.SubCategory = workSheet.Cells[i, WorksheetHelper.GetColumnByName(workSheet, nameof(p.SubCategory))].Value.ToStringOrEmptyWhenNull();
        //            p.SubSubCategory = workSheet.Cells[i, WorksheetHelper.GetColumnByName(workSheet, nameof(p.SubSubCategory))].Value.ToStringOrEmptyWhenNull();
        //            p.Brand = workSheet.Cells[i, WorksheetHelper.GetColumnByName(workSheet, nameof(p.Brand))].Value.ToStringOrEmptyWhenNull();
        //            p.Created = DateTime.Now;
        //            productList.Add(p);
        //        }

        //    }
        //    return productList;

        //    //var dataTable = ExcelPackageToDataTable(excelPackage);
        //    //if (destinationTableName.Equals("ProductFilter"))
        //    //{
        //    //    dataTable.Columns.Remove("ProductFilterId");
        //    //    dataTable.Columns.Remove("Product");
        //    //    dataTable.Columns.Remove("Family");
        //    //    dataTable.Columns.Remove("Title");
        //    //    dataTable.Columns.Remove("Summary");
        //    //    dataTable.Columns.Remove("MainCategory");
        //    //    dataTable.Columns.Remove("SubCategory");
        //    //    dataTable.Columns.Remove("SubSubCategory");
        //    //    dataTable.Columns.Remove("Brand");
        //    //    dataTable.Columns.Remove("MainCategoryId");
        //    //    dataTable.Columns.Remove("SubCategoryId");
        //    //    dataTable.Columns.Remove("SubSubCategoryId");
        //    //}
        //    //InsertDataIntoSQLServerUsingSQLBulkCopy(dataTable, destinationTableName);
        //}

        //public static List<Product> ImportProductsToSQL(Stream excelFileStream)
        //{
        //    List<Product> productList = new List<Product>();
        //    using (ExcelPackage excelPackage = new ExcelPackage(excelFileStream))
        //    {

        //        ExcelWorksheet workSheet = excelPackage.Workbook.Worksheets[0];
        //        int totalRows = workSheet.Dimension.Rows;

        //        for (int i = 2; i <= totalRows; i++)
        //        {
        //            var p = new Product();
        //            p.Id = int.Parse(workSheet.Cells[i, WorksheetHelper.GetColumnByName(workSheet, nameof(p.Id))].Value.ToStringOrEmptyWhenNull());
        //            p.Model = workSheet.Cells[i, WorksheetHelper.GetColumnByName(workSheet, nameof(p.Model))].Value.ToStringOrEmptyWhenNull();
        //            p.Accuracy = workSheet.Cells[i, WorksheetHelper.GetColumnByName(workSheet, nameof(p.Accuracy))].Value.ToStringOrEmptyWhenNull();
        //            p.Amperage = workSheet.Cells[i, WorksheetHelper.GetColumnByName(workSheet, nameof(p.Amperage))].Value.ToStringOrEmptyWhenNull();
        //            p.Attenuation = workSheet.Cells[i, WorksheetHelper.GetColumnByName(workSheet, nameof(p.Attenuation))].Value.ToStringOrEmptyWhenNull();
        //            p.Bandwidth = workSheet.Cells[i, WorksheetHelper.GetColumnByName(workSheet, nameof(p.Bandwidth))].Value.ToStringOrEmptyWhenNull();
        //            p.BodyMaterial = workSheet.Cells[i, WorksheetHelper.GetColumnByName(workSheet, nameof(p.BodyMaterial))].Value.ToStringOrEmptyWhenNull();
        //            p.Brand = workSheet.Cells[i, WorksheetHelper.GetColumnByName(workSheet, nameof(p.Brand))].Value.ToStringOrEmptyWhenNull();
        //            p.CableLength = workSheet.Cells[i, WorksheetHelper.GetColumnByName(workSheet, nameof(p.CableLength))].Value.ToStringOrEmptyWhenNull();
        //            p.CableType = workSheet.Cells[i, WorksheetHelper.GetColumnByName(workSheet, nameof(p.CableType))].Value.ToStringOrEmptyWhenNull();
        //            p.Color = workSheet.Cells[i, WorksheetHelper.GetColumnByName(workSheet, nameof(p.Color))].Value.ToStringOrEmptyWhenNull();
        //            p.ConnectorGrade = workSheet.Cells[i, WorksheetHelper.GetColumnByName(workSheet, nameof(p.ConnectorGrade))].Value.ToStringOrEmptyWhenNull();
        //            p.ContactMaterial = workSheet.Cells[i, WorksheetHelper.GetColumnByName(workSheet, nameof(p.ContactMaterial))].Value.ToStringOrEmptyWhenNull();
        //            p.Created = DateTime.Now; // workSheet.Cells[i, WorksheetHelper.GetColumnByName(workSheet, nameof(p.Created)].Value;
        //            p.Frequency = workSheet.Cells[i, WorksheetHelper.GetColumnByName(workSheet, nameof(p.Frequency))].Value.ToStringOrEmptyWhenNull();
        //            p.FrequencyRange = workSheet.Cells[i, WorksheetHelper.GetColumnByName(workSheet, nameof(p.FrequencyRange))].Value.ToStringOrEmptyWhenNull();
        //            p.GroundBarrelDiameter = workSheet.Cells[i, WorksheetHelper.GetColumnByName(workSheet, nameof(p.GroundBarrelDiameter))].Value.ToStringOrEmptyWhenNull();
        //            p.Iec = workSheet.Cells[i, WorksheetHelper.GetColumnByName(workSheet, nameof(p.Iec))].Value.ToStringOrEmptyWhenNull();
        //            p.Impedance = workSheet.Cells[i, WorksheetHelper.GetColumnByName(workSheet, nameof(p.Impedance))].Value.ToStringOrEmptyWhenNull();
        //            p.InputPower = workSheet.Cells[i, WorksheetHelper.GetColumnByName(workSheet, nameof(p.InputPower))].Value.ToStringOrEmptyWhenNull();
        //            p.InsertionLoss = workSheet.Cells[i, WorksheetHelper.GetColumnByName(workSheet, nameof(p.InsertionLoss))].Value.ToStringOrEmptyWhenNull();
        //            p.Length = workSheet.Cells[i, WorksheetHelper.GetColumnByName(workSheet, nameof(p.Length))].Value.ToStringOrEmptyWhenNull();
        //            p.MaxVoltage = workSheet.Cells[i, WorksheetHelper.GetColumnByName(workSheet, nameof(p.MaxVoltage))].Value.ToStringOrEmptyWhenNull();
        //            p.Modifier = workSheet.Cells[i, WorksheetHelper.GetColumnByName(workSheet, nameof(p.Modifier))].Value.ToStringOrEmptyWhenNull();
        //            p.RoHs = workSheet.Cells[i, WorksheetHelper.GetColumnByName(workSheet, nameof(p.RoHs))].Value.ToStringOrEmptyWhenNull();
        //            p.Published = true; // workSheet.Cells[i, WorksheetHelper.GetColumnByName(workSheet, nameof(p.Published))].Value.ToStringOrEmptyWhenNull();
        //            p.Series = workSheet.Cells[i, WorksheetHelper.GetColumnByName(workSheet, nameof(p.Series))].Value.ToStringOrEmptyWhenNull();
        //            p.Temperature = workSheet.Cells[i, WorksheetHelper.GetColumnByName(workSheet, nameof(p.Temperature))].Value.ToStringOrEmptyWhenNull();
        //            p.Vswr = workSheet.Cells[i, WorksheetHelper.GetColumnByName(workSheet, nameof(p.Vswr))].Value.ToStringOrEmptyWhenNull();
        //            productList.Add(p);
        //        }

        //    }
        //    return productList;

        //    //var dataTable = ExcelPackageToDataTable(excelPackage);
        //    //if (destinationTableName.Equals("ProductFilter"))
        //    //{
        //    //    dataTable.Columns.Remove("ProductFilterId");
        //    //    dataTable.Columns.Remove("Product");
        //    //    dataTable.Columns.Remove("Family");
        //    //    dataTable.Columns.Remove("Title");
        //    //    dataTable.Columns.Remove("Summary");
        //    //    dataTable.Columns.Remove("MainCategory");
        //    //    dataTable.Columns.Remove("SubCategory");
        //    //    dataTable.Columns.Remove("SubSubCategory");
        //    //    dataTable.Columns.Remove("Brand");
        //    //    dataTable.Columns.Remove("MainCategoryId");
        //    //    dataTable.Columns.Remove("SubCategoryId");
        //    //    dataTable.Columns.Remove("SubSubCategoryId");
        //    //}
        //    //InsertDataIntoSQLServerUsingSQLBulkCopy(dataTable, destinationTableName);
        //}


    }

}
