﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TransactionImportToolKit;

namespace SalesPriceTransactionImportToolKit
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void btnSalesPriceToolkit_Click(object sender, EventArgs e)
        {
            Form1 form1 = new Form1();
            form1.OpenForm<Form1>();
        }

        private void btnXrefToolkit_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            form2.OpenForm<Form2>();
        }

        private void btnKitPrices_Click(object sender, EventArgs e)
        {
            CalculateKitPriceForm calculateKitPriceForm = new CalculateKitPriceForm();
            calculateKitPriceForm.OpenForm<CalculateKitPriceForm>();
        }
    }
    public static class FormExtensions
    {
        private static Dictionary<Type, Form> openForms = new Dictionary<Type, Form>();

        public static void OpenForm<T>(this Form form) where T : Form, new()
        {
            Type type = typeof(T);
            Form openForm;
            if (!openForms.TryGetValue(type, out openForm) || openForm.IsDisposed)
            {
                openForm = new T();
                openForms[type] = openForm;
                openForm.Show();
            }
            else
            {
                openForm.BringToFront();
            }
        }
    }
}
