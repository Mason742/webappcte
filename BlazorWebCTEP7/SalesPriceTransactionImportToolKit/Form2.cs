﻿using Microsoft.EntityFrameworkCore;
using OfficeOpenXml;
using SolomonDB.Data.Context;
using SolomonDB.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OfficeOpenXml.DataValidation;
using OfficeOpenXml.Table;
using EPPlusExcelService.Helpers;



namespace SalesPriceTransactionImportToolKit
{
    public partial class Form2 : Form
    {
        public static readonly string ProjectDirectory = @"C:\Projects\SalesPriceTransactionImportToolKit\Xref\";
        public static readonly string TI_FileName = "TI_ImportFile.csv";
        public static readonly string TemplateFileName = "Template_ExportFile.xlsx";
        public static readonly string TI_FilePath = ProjectDirectory + TI_FileName;
        public static readonly string TemplateFilePath = ProjectDirectory + TemplateFileName;


        public List<string> VendorList;
        public List<string> CurrencyList;


        public Form2()
        {
            InitializeComponent();

            VendorList = new List<string>() { "NO DB Found" };

            Directory.CreateDirectory(ProjectDirectory);
            File.Copy(@"Resources\XRef.ctl", ProjectDirectory + "XRef.ctl", true);
            File.Copy(@"Resources\XRef.LOG", ProjectDirectory + "XRef.LOG", true);
        }


        private void btnGenerateXrefExoport_Click(object sender, EventArgs e)
        {
            btnGenerateXrefExoport.Enabled = false;
            try
            {
                using (var solomonDB = new SolomonDbContext())
                {
                    List<Xref> xrefs = new List<Xref>();
                    string vendor = cbVendorID.Text.Trim();
                    xrefs = solomonDB.ItemXrefs.Where(x => x.AltIdtype == "V" && x.EntityId.Trim() == vendor).Select(x => new Xref
                    {
                        VendorId = x.EntityId.Trim(),
                        InvtId = x.InvtId.Trim(),
                        AlternateId = x.AlternateId.Trim(),
                    }).ToList();
                    var s = GenerateTemplateCSV(xrefs);
                    var p = new Process
                    {
                        StartInfo = new ProcessStartInfo(s)
                        {
                            UseShellExecute = true,
                        }
                    };
                    p.Start();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed to Generate Template \n Message: " + ex);
            }
            btnGenerateXrefExoport.Enabled = true;
        }

        private static string GenerateTemplateCSV(List<Xref> join)
        {
            if (join is null)
            {
                throw new ArgumentNullException(nameof(join));
            }
            FileInfo f = new(TemplateFilePath);
            if (f.Exists) f.Delete();
            using (ExcelPackage excelPackage = new(f))
            {
                ExcelWorksheet ws = excelPackage.Workbook.Worksheets.Add("Import");
                ws.Cells[1, 1].Value = "InvtID";
                ws.Cells[1, 2].Value = "VendorID";
                ws.Cells[1, 3].Value = "AlternateID";
                ws.Cells[2, 1].LoadFromCollection(join);

                //create a range for the table
                ExcelRange range = ws.Cells[1, 1, ws.Dimension.End.Row, ws.Dimension.End.Column];

                //add a table to the range
                ExcelTable tab = ws.Tables.Add(range, "ImportTable");

                //format the table
                tab.TableStyle = TableStyles.Medium2;

                //AddListValidationValues(excelPackage);
                excelPackage.SaveAs(f);
            }
            return f.FullName;
        }

        private void btnGenerateXrefImport_Click(object sender, EventArgs e)
        {
            btnGenerateXrefImport.Enabled = false;
            openFileDialog1 = new OpenFileDialog()
            {
                FileName = "Select a xlsx file",
                Filter = "Excel files (*.xlsx)|*.xlsx",
                Title = "TI_File.xlsx"
            };
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    var filePath = openFileDialog1.FileName;
                    FileInfo fileInfo = new(filePath);
                    File.WriteAllText(TI_FilePath, GenerateTIDataFile(fileInfo), Encoding.ASCII);
                    var p = new Process
                    {
                        StartInfo = new ProcessStartInfo(TI_FilePath)
                        {
                            UseShellExecute = true,
                        }
                    };
                    p.Start();

                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Generating TI data file Error:\n\nError message: {ex.Message}\n\n" +
                    $"Details:\n\n{ex.StackTrace}");
                }
            }
            btnGenerateXrefImport.Enabled = true;
        }

        public class Xref
        {
            public string InvtId { get; set; }
            public string VendorId { get; set; }
            public string AlternateId { get; set; }
            public string NewAlternateId { get; set; }
            public string Action { get; set; } = "Update";
        }

        private string GenerateTIDataFile(FileInfo fileInfo)
        {
            List<Xref> xrefs = new();

            ExcelPackage package = new(fileInfo);

            ExcelWorksheet worksheet = package.Workbook.Worksheets.Where(x => x.Name.Contains("Import")).FirstOrDefault();
            if (worksheet == null)
            {
                throw new NullReferenceException("Excel Sheet named Import not found. \n Rename sheet to Import.");
            }

            // delete any empty rows possible added by the user
            var emptyRows = worksheet.TrimLastEmptyRows();
            if (emptyRows > 0)
            {
                MessageBox.Show($"{emptyRows} empty rows automatically deleted.", "Notice");
            }

            // get number of rows and columns in the ImportTable
            var table = worksheet.Tables.Where(x => x.Name == "ImportTable").FirstOrDefault();
            if (table == null)
            {
                throw new NullReferenceException("Excel ImportTable not found. \n Rename table to ImportTable.");
            }
            int columns = table.WorkSheet.Dimension.Columns;
            int rows = table.WorkSheet.Dimension.Rows;

            int header = 1;

            // loop through the worksheet rows and columns
            for (int i = 1 + header; i <= rows; i++)
            {
                Xref xref = new()
                {
                    InvtId = worksheet.Cells[i, worksheet.GetColumnByNameV2("InvtId")].Value.ToString().Trim(),
                    VendorId = worksheet.Cells[i, worksheet.GetColumnByNameV2("VendorId")].Value.ToString().Trim(),
                    AlternateId = worksheet.Cells[i, worksheet.GetColumnByNameV2("AlternateId")].Value.ToString().Trim()
                };
                xrefs.Add(xref);
            }

            //check for duplicates because user error and grouping can produce unexpected results.
            //CheckForDuplicates(salesPrices);

            string values = "";
            var distinctXrefs = xrefs.GroupBy(x => new { x.VendorId, x.InvtId, x.AlternateId }).Select(x => x.First()).ToList();
            foreach (var xref in distinctXrefs)
            {
                values += $"Item,{xref.InvtId}\n";
                values += $"CrossReference,V,{xref.VendorId},{xref.AlternateId}\n";
                if (string.IsNullOrEmpty(values))
                {
                    throw new Exception("No values to print found");
                }
            }
            return values;
        }

        private bool UpdateXRefTable(FileInfo fileInfo)
        {
            List<Xref> xrefs = new();

            ExcelPackage package = new(fileInfo);

            ExcelWorksheet worksheet = package.Workbook.Worksheets.Where(x => x.Name.Contains("Import")).FirstOrDefault();
            if (worksheet == null)
            {
                throw new NullReferenceException("Excel Sheet named Import not found. \n Rename sheet to Import.");
            }

            // delete any empty rows possible added by the user
            var emptyRows = worksheet.TrimLastEmptyRows();
            if (emptyRows > 0)
            {
                MessageBox.Show($"{emptyRows} empty rows automatically deleted.", "Notice");
            }

            // get number of rows and columns in the ImportTable
            var table = worksheet.Tables.Where(x => x.Name == "ImportTable").FirstOrDefault();
            if (table == null)
            {
                throw new NullReferenceException("Excel ImportTable not found. \n Rename table to ImportTable.");
            }
            int columns = table.WorkSheet.Dimension.Columns;
            int rows = table.WorkSheet.Dimension.Rows;

            int header = 1;

            // loop through the worksheet rows and columns
            for (int i = 1 + header; i <= rows; i++)
            {
                Xref xref = new()
                {
                    InvtId = worksheet.Cells[i, worksheet.GetColumnByNameV2("InvtId")].Value.ToString().Trim(),
                    VendorId = worksheet.Cells[i, worksheet.GetColumnByNameV2("VendorId")].Value.ToString().Trim(),
                    AlternateId = worksheet.Cells[i, worksheet.GetColumnByNameV2("AlternateId")].Value.ToString().Trim(),
                    NewAlternateId = worksheet.Cells[i, worksheet.GetColumnByNameV2("NewAlternateId")].Value.ToString().Trim(),
                    Action = worksheet.Cells[i, worksheet.GetColumnByNameV2("Action")].Value.ToString().Trim()
                };
                xrefs.Add(xref);
            }

            //check for duplicates because user error and grouping can produce unexpected results.
            //CheckForDuplicates(salesPrices);
            List<ItemXref> items = new List<ItemXref>();
            using (var sl = new SolomonDbContext())
            {
                items = sl.ItemXrefs.Where(x => x.AltIdtype == "V" && !string.IsNullOrWhiteSpace(x.InvtId)).ToList();

                var distinctXrefs = xrefs.GroupBy(x => new { x.VendorId, x.InvtId, x.AlternateId, x.NewAlternateId, x.Action }).Select(x => x.First()).ToList();
                foreach (var xref in distinctXrefs)
                {
                    var thing = items.FirstOrDefault(i => i.AlternateId.Trim() == xref.AlternateId.Trim() && i.InvtId.Trim() == xref.InvtId && i.EntityId.Trim() == xref.VendorId);
                    if (thing != null)
                    {
                        string newAlternateId = xref.NewAlternateId?.Trim().ToUpper() ?? thing.AlternateId;
                        if (xref.Action == "Update")
                        {
                            //Console.WriteLine($"Updating: {xref.InvtId},{xref.AlternateId}->{newAlternateId}");
                            string updateSql = $"UPDATE ItemXref SET AlternateId = '{newAlternateId}' WHERE AltIdType = 'V' AND AlternateId = '{thing.AlternateId}' AND InvtId = '{thing.InvtId}' AND EntityId = '{thing.EntityId}'";
                            sl.Database.ExecuteSqlRaw(updateSql);
                        }
                        else if (xref.Action == "Delete")
                        {
                            //Console.WriteLine($"Deleting: {xref.InvtId},{xref.AlternateId}");
                            string deleteSql = $"DELETE FROM ItemXref WHERE AltIdType = 'V' AND AlternateId = '{thing.AlternateId}' AND InvtId = '{thing.InvtId}' AND EntityId = '{thing.EntityId}'";
                            sl.Database.ExecuteSqlRaw(deleteSql);
                        }

                    }
                }
                return true;
            }
        }


        private async void Form2_Load(object sender, EventArgs e)
        {
            try
            {
                using (var db = new SolomonDbContext())
                {
                    VendorList = await db.ItemXrefs.Where(x => x.AltIdtype == "V").Select(x => x.EntityId).Distinct().ToListAsync();
                }
                cbVendorID.DataSource = VendorList;

            }
            catch (Exception ex)
            {
                MessageBox.Show("No DB connection: \n" + ex.Message);
            }
        }

        private void btnOpenProjectDirectory_Click(object sender, EventArgs e)
        {
            Process.Start("explorer.exe", ProjectDirectory);
        }

        private void btnDirectlyUpdateXRef_Click(object sender, EventArgs e)
        {
            btnDirectlyUpdateXRef.Enabled = false;
            openFileDialog1 = new OpenFileDialog()
            {
                FileName = "Select a xlsx file",
                Filter = "Excel files (*.xlsx)|*.xlsx",
                Title = "TI_File.xlsx"
            };
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    var filePath = openFileDialog1.FileName;
                    FileInfo fileInfo = new(filePath);
                    UpdateXRefTable(fileInfo);
                    MessageBox.Show("Completed Successully");
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Generating TI data file Error:\n\nError message: {ex.Message}\n\n" +
                    $"Details:\n\n{ex.StackTrace}");
                }
            }
            btnDirectlyUpdateXRef.Enabled = true;
        }

    }
}
