using EPPlusExcelService.Helpers;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml;
using OfficeOpenXml.DataValidation;
using OfficeOpenXml.Table;
using SolomonDB;
using SolomonDB.Data.Context;
using System.Diagnostics;
using System.Security;
using System.Text;

namespace SalesPriceTransactionImportToolKit
{
    public partial class Form1 : Form
    {
        public static readonly string ProjectDirectory = @"C:\Projects\SalesPriceTransactionImportToolKit\SalesPrice\";
        public static readonly string TI_FileName = "TI_ImportFile.csv";
        public static readonly string TemplateFileName = "Template_ExportFile.xlsx";
        public static readonly string TI_FilePath = ProjectDirectory + TI_FileName;
        public static readonly string TemplateFilePath = ProjectDirectory + TemplateFileName;


        public List<string> CustomerList;
        public List<string> CurrencyList;

        public Form1()
        {
            CustomerList = new List<string>() { "NO DB Found" };
            CurrencyList = new List<string>() { "NO DB Found" };

            Directory.CreateDirectory(ProjectDirectory);
            File.Copy(@"Resources\4038000.ctl", ProjectDirectory + "4038000.ctl", true);
            File.Copy(@"Resources\salesprice.LOG", ProjectDirectory + "salesprice.LOG", true);
            InitializeComponent();
        }

        private void BtnExcelTemplateExport_Click(object sender, EventArgs e)
        {
            btnExcelTemplateExport.Enabled = false;
            try
            {
                using (var solomonDB = new SolomonDbContext())
                {
                    List<SalesPrice> join = null;
                    if (cbCurrencyID.Text == "*" && cbCustomerID.Text == "*")
                    {
                        join = (from a in solomonDB.SlsPrcs
                                join b in solomonDB.SlsPrcDets on a.SlsPrcId equals b.SlsPrcId
                                where a.PriceCat == "IC"
                                select new SalesPrice
                                {
                                    CatalogNbr = a.CatalogNbr.Trim(),
                                    CuryId = a.CuryId.Trim(),
                                    DiscPrcTyp = a.DiscPrcTyp.Trim(),
                                    DiscPrcMthd = a.DiscPrcMthd.Trim(),
                                    InvtId = a.InvtId.Trim(),
                                    CustId = a.CustId.Trim(),
                                    SiteId = a.SiteId.Trim(),
                                    MinQty = a.MinQty.ToString().Trim(),
                                    MaxQty = a.MaxQty.ToString().Trim(),
                                    SalesUnit = b.SlsUnit.Trim(),
                                    QtyBreak = b.QtyBreak.ToString().Trim(),
                                    Percent = b.DiscPct.ToString().Trim(),
                                    Price = b.DiscPrice.ToString().Trim(),
                                }).ToList();
                    }
                    else if (cbCurrencyID.Text == "*")
                    {
                        join = (from a in solomonDB.SlsPrcs
                                join b in solomonDB.SlsPrcDets on a.SlsPrcId equals b.SlsPrcId
                                where a.CustId.Trim() == cbCustomerID.Text.Trim() && a.PriceCat == "IC"
                                select new SalesPrice
                                {
                                    CatalogNbr = a.CatalogNbr.Trim(),
                                    CuryId = a.CuryId.Trim(),
                                    DiscPrcTyp = a.DiscPrcTyp.Trim(),
                                    DiscPrcMthd = a.DiscPrcMthd.Trim(),
                                    InvtId = a.InvtId.Trim(),
                                    CustId = a.CustId.Trim(),
                                    SiteId = a.SiteId.Trim(),
                                    MinQty = a.MinQty.ToString().Trim(),
                                    MaxQty = a.MaxQty.ToString().Trim(),
                                    SalesUnit = b.SlsUnit.Trim(),
                                    QtyBreak = b.QtyBreak.ToString().Trim(),
                                    Percent = b.DiscPct.ToString().Trim(),
                                    Price = b.DiscPrice.ToString().Trim(),
                                }).ToList();
                    }
                    else if (cbCustomerID.Text == "*")
                    {
                        join = (from a in solomonDB.SlsPrcs
                                join b in solomonDB.SlsPrcDets on a.SlsPrcId equals b.SlsPrcId
                                where a.CuryId.Trim() == cbCurrencyID.Text.Trim() && a.PriceCat == "IC"
                                select new SalesPrice
                                {
                                    CatalogNbr = a.CatalogNbr.Trim(),
                                    CuryId = a.CuryId.Trim(),
                                    DiscPrcTyp = a.DiscPrcTyp.Trim(),
                                    DiscPrcMthd = a.DiscPrcMthd.Trim(),
                                    InvtId = a.InvtId.Trim(),
                                    CustId = a.CustId.Trim(),
                                    SiteId = a.SiteId.Trim(),
                                    MinQty = a.MinQty.ToString().Trim(),
                                    MaxQty = a.MaxQty.ToString().Trim(),
                                    SalesUnit = b.SlsUnit.Trim(),
                                    QtyBreak = b.QtyBreak.ToString().Trim(),
                                    Percent = b.DiscPct.ToString().Trim(),
                                    Price = b.DiscPrice.ToString().Trim(),
                                }).ToList();
                    }
                    else
                    {
                        join = (from a in solomonDB.SlsPrcs
                                join b in solomonDB.SlsPrcDets on a.SlsPrcId equals b.SlsPrcId
                                where a.CuryId.Trim() == cbCurrencyID.Text.Trim() && a.CustId.Trim() == cbCustomerID.Text.Trim() && a.PriceCat == "IC"
                                select new SalesPrice
                                {
                                    CatalogNbr = a.CatalogNbr.Trim(),
                                    CuryId = a.CuryId.Trim(),
                                    DiscPrcTyp = a.DiscPrcTyp.Trim(),
                                    DiscPrcMthd = a.DiscPrcMthd.Trim(),
                                    InvtId = a.InvtId.Trim(),
                                    CustId = a.CustId.Trim(),
                                    SiteId = a.SiteId.Trim(),
                                    MinQty = a.MinQty.ToString().Trim(),
                                    MaxQty = a.MaxQty.ToString().Trim(),
                                    SalesUnit = b.SlsUnit.Trim(),
                                    QtyBreak = b.QtyBreak.ToString().Trim(),
                                    Percent = b.DiscPct.ToString().Trim(),
                                    Price = b.DiscPrice.ToString().Trim(),
                                }).ToList();
                    }

                    if (join == null)
                    {
                        throw new NotImplementedException("No Price List found. Check Currency and Customer ID. Check Sales Price screen.");
                    }
                    var s = GenerateTemplateCSV(join);
                    var p = new Process
                    {
                        StartInfo = new ProcessStartInfo(s)
                        {
                            UseShellExecute = true,
                        }
                    };
                    p.Start();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed to Generate Template \n Message: " + ex);
            }
            btnExcelTemplateExport.Enabled = true;
        }

        private static string GenerateTemplateCSV(List<SalesPrice> join)
        {
            if (join is null)
            {
                throw new ArgumentNullException(nameof(join));
            }
            FileInfo f = new(TemplateFilePath);
            if (f.Exists) f.Delete();
            using (ExcelPackage excelPackage = new(f))
            {
                ExcelWorksheet ws = excelPackage.Workbook.Worksheets.Add("Import");
                ws.Cells[1, 1].Value = "Price Plan ID";
                ws.Cells[1, 2].Value = "Currency ID";
                ws.Cells[1, 3].Value = "Price Type";
                ws.Cells[1, 4].Value = "Disc Method";
                ws.Cells[1, 5].Value = "Inventory ID";
                ws.Cells[1, 6].Value = "Customer ID";
                ws.Cells[1, 7].Value = "Site ID";
                ws.Cells[1, 8].Value = "Min Qty";
                ws.Cells[1, 9].Value = "Max Qty";
                ws.Cells[1, 10].Value = "Sales Unit";
                ws.Cells[1, 11].Value = "Qty Break";
                ws.Cells[1, 12].Value = "Percent";
                ws.Cells[1, 13].Value = "Price";
                ws.Cells[2, 1].LoadFromCollection(join);

                ws.Cells[1, 2].AddComment("Values are USD or EUR", "Mason Channer");
                ws.Cells[1, 3].AddComment("Values are P;Promotion,S;Standard", "Mason Channer");
                ws.Cells[1, 4].AddComment("Values are F;Flat Price, R;Price Discount, P;Percent Discount", "Mason Channer");
                ws.Cells[1, 10].AddComment("Values are EA, FT, METER", "Mason Channer");
                ws.Cells[1, 12].AddComment("Values are whole number only, do not put % sign. 50 equals 50%", "Mason Channer");
                ws.Cells[1, 13].AddComment("Values are mixed number only, do not put $ sign.", "Mason Channer");

                //create a range for the table
                ExcelRange range = ws.Cells[1, 1, ws.Dimension.End.Row, ws.Dimension.End.Column];

                //add a table to the range
                ExcelTable tab = ws.Tables.Add(range, "ImportTable");

                //format the table
                tab.TableStyle = TableStyles.Medium2;

                AddListValidationValues(excelPackage);
                excelPackage.SaveAs(f);
            }
            return f.FullName;
        }

        public class SalesPrice
        {
            public string CatalogNbr { get; set; }
            public string CuryId { get; set; }
            public string DiscPrcTyp { get; set; }
            public string DiscPrcMthd { get; set; }
            public string InvtId { get; set; }
            public string CustId { get; set; }
            public string SiteId { get; set; }
            public string MinQty { get; set; }
            public string MaxQty { get; set; }
            public string SalesUnit { get; set; }
            public string QtyBreak { get; set; }
            public string Percent { get; set; }
            public string Price { get; set; }
        }

        private void BtnGenerateTIDataFile_Click(object sender, EventArgs e)
        {
            btnGenerateTIDataFile.Enabled = false;
            openFileDialog1 = new OpenFileDialog()
            {
                FileName = "Select a xlsx file",
                Filter = "Excel files (*.xlsx)|*.xlsx",
                Title = "TI_File.xlsx"
            };
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    var filePath = openFileDialog1.FileName;
                    FileInfo fileInfo = new(filePath);
                    File.WriteAllText(TI_FilePath, GenerateTIDataFile(fileInfo), Encoding.ASCII);
                    var p = new Process
                    {
                        StartInfo = new ProcessStartInfo(TI_FilePath)
                        {
                            UseShellExecute = true,
                        }
                    };
                    p.Start();

                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Generating TI data file Error:\n\nError message: {ex.Message}\n\n" +
                    $"Details:\n\n{ex.StackTrace}");
                }
            }
            btnGenerateTIDataFile.Enabled = true;
        }

        private string GenerateTIDataFile(FileInfo fileInfo)
        {
            List<SalesPrice> salesPrices = new();

            ExcelPackage package = new(fileInfo);

            ExcelWorksheet worksheet = package.Workbook.Worksheets.Where(x => x.Name == "Import").FirstOrDefault();
            if (worksheet == null)
            {
                throw new NullReferenceException("Excel Sheet named Import not found. \n Rename sheet to Import.");
            }

            // delete any empty rows possible added by the user
            var emptyRows = worksheet.TrimLastEmptyRows();
            if (emptyRows > 0)
            {
                MessageBox.Show($"{emptyRows} empty rows automatically deleted.", "Notice");
            }

            // get number of rows and columns in the ImportTable
            var table = worksheet.Tables.Where(x => x.Name == "ImportTable").FirstOrDefault();
            if (table == null)
            {
                throw new NullReferenceException("Excel ImportTable not found. \n Rename table to ImportTable.");
            }
            int columns = table.WorkSheet.Dimension.Columns;
            int rows = table.WorkSheet.Dimension.Rows;

            int header = 1;

            // loop through the worksheet rows and columns
            for (int i = 1 + header; i <= rows; i++)
            {
                SalesPrice salesPrice = new()
                {
                    CatalogNbr = worksheet.Cells[i, worksheet.GetColumnByNameV2("Price Plan ID")].Value.ToString(),
                    CuryId = worksheet.Cells[i, worksheet.GetColumnByNameV2("Currency ID")].Value.ToString(),
                    DiscPrcTyp = worksheet.Cells[i, worksheet.GetColumnByNameV2("Price Type")].Value.ToString(),
                    DiscPrcMthd = worksheet.Cells[i, worksheet.GetColumnByNameV2("Disc Method")].Value.ToString(),
                    InvtId = worksheet.Cells[i, worksheet.GetColumnByNameV2("Inventory ID")].Value.ToString(),
                    CustId = worksheet.Cells[i, worksheet.GetColumnByNameV2("Customer ID")].Value.ToString(),
                    SiteId = worksheet.Cells[i, worksheet.GetColumnByNameV2("Site ID")].Value.ToString(),
                    MinQty = (worksheet.Cells[i, worksheet.GetColumnByNameV2("Min Qty")].Value == null ? "0" : worksheet.Cells[i, worksheet.GetColumnByNameV2("Min Qty")].Value.ToString()),
                    MaxQty = (worksheet.Cells[i, worksheet.GetColumnByNameV2("Max Qty")].Value == null ? "0" : worksheet.Cells[i, worksheet.GetColumnByNameV2("Max Qty")].Value.ToString()),
                    SalesUnit = worksheet.Cells[i, worksheet.GetColumnByNameV2("Sales Unit")].Value.ToString(),
                    QtyBreak = worksheet.Cells[i, worksheet.GetColumnByNameV2("Qty Break")].Value.ToString()
                };
                if (salesPrice.DiscPrcMthd == "P")
                {
                    salesPrice.Percent = (worksheet.Cells[i, worksheet.GetColumnByNameV2("Percent")].Value ?? "").ToString();
                    salesPrice.Price = "";

                }
                else if (salesPrice.DiscPrcMthd == "F" || salesPrice.DiscPrcMthd == "R")
                {
                    salesPrice.Percent = "";
                    salesPrice.Price = (worksheet.Cells[i, worksheet.GetColumnByNameV2("Price")].Value ?? "").ToString();
                }
                salesPrices.Add(salesPrice);
            }

            //check for duplicates because user error and grouping can produce unexpected results.
            CheckForDuplicates(salesPrices);

            string values = "";
            var distinctSalePrice = salesPrices.GroupBy(x => new { x.CustId, x.InvtId, x.SiteId, x.CuryId }).Select(x => x.First()).ToList();
            foreach (var Selection in distinctSalePrice)
            {
                if (Selection.InvtId == "CT2704")
                    Console.WriteLine("hi");
                values += $"Selection,{Selection.CuryId},{Selection.SiteId},IC,{Selection.InvtId},{Selection.CustId},\n";
                foreach (var SlsPrc in salesPrices.GroupBy(x => new { x.CustId, x.InvtId, x.DiscPrcMthd, x.DiscPrcTyp, x.SiteId, x.CuryId })
                                                  .Select(x => x.First())
                                                  .Where(x => x.InvtId == Selection.InvtId && x.CustId == Selection.CustId && x.SiteId == Selection.SiteId && x.CuryId == Selection.CuryId)
                                                  .ToList())
                {
                    values += $"SlsPrc,{SlsPrc.CatalogNbr},{SlsPrc.DiscPrcTyp},{SlsPrc.DiscPrcMthd},{SlsPrc.SiteId},{SlsPrc.MinQty},{SlsPrc.MaxQty},\n";
                    foreach (var SlsPrcDet in salesPrices
                                               .Where(x => x.InvtId == SlsPrc.InvtId
                                               && x.CustId == SlsPrc.CustId
                                               && x.DiscPrcMthd == SlsPrc.DiscPrcMthd
                                               && x.DiscPrcTyp == SlsPrc.DiscPrcTyp
                                               && x.SiteId == SlsPrc.SiteId
                                               && x.CuryId == SlsPrc.CuryId))
                    {
                        values += $"SlsPrcDet,{SlsPrcDet.SalesUnit},{SlsPrcDet.QtyBreak},{SlsPrcDet.Percent},{SlsPrcDet.Price},\n";
                    }
                }
                if (string.IsNullOrEmpty(values))
                {
                    throw new Exception("No values to print found");
                }
            }
            return values;
        }

        private static void CheckForDuplicates(List<SalesPrice> salesPrices)
        {
            var duplicates = salesPrices.GroupBy(x => new { x.CatalogNbr, x.CuryId, x.DiscPrcTyp, x.DiscPrcMthd, x.InvtId, x.CustId, x.SiteId, x.MinQty, x.MaxQty, x.SalesUnit, x.QtyBreak })
              .Where(g => g.Count() > 1)
              .Select(y => y.Key)
              .ToList();
            var duplicates2 = salesPrices.Where(x => x.DiscPrcTyp == "S")
                .GroupBy(x => new { x.CatalogNbr, x.CuryId, x.DiscPrcTyp, x.InvtId, x.CustId, x.SiteId, x.MinQty, x.MaxQty, x.SalesUnit, x.QtyBreak })
                .Where(g => g.Count() > 1)
                .Select(y => y.Key)
                .ToList();

            var duplicates3 = salesPrices.Where(x => x.DiscPrcTyp == "P")
                .GroupBy(x => new { x.CatalogNbr, x.CuryId, x.DiscPrcTyp, x.InvtId, x.CustId, x.SiteId, x.MinQty, x.MaxQty, x.SalesUnit, x.QtyBreak })
                .Where(g => g.Count() > 1)
                .Select(y => y.Key)
                .ToList();

            if (duplicates.Count > 0)
            {
                string joined = string.Join(",", duplicates);
                throw new NotImplementedException("Duplicates found:\nRemove the duplcates to continue.\n" + joined);
            }
            if (duplicates2.Count > 0)
            {
                string joined = string.Join(",", duplicates2);
                throw new NotImplementedException("Duplicates found:\nYou can't have multiple standard pricings for same InvtID. Remove the duplcates to continue.\n" + joined);
            }
            if (duplicates3.Count > 0)
            {
                string joined = string.Join(",", duplicates3);
                throw new NotImplementedException("Duplicates found:\nYou can't have multiple promotional pricings for same InvtID. Remove the duplcates to continue.\n" + joined);
            }


        }

        private static void AddListValidationValues(ExcelPackage package)
        {
            var sheet = package.Workbook.Worksheets[0];

            using (var db = new SolomonDbContext())
            {
                List<string> priceTypes = db.SlsPrcs.Select(s => s.DiscPrcTyp).Distinct().ToList();
                List<string> discountMethods = db.SlsPrcs.Select(s => s.DiscPrcMthd).Distinct().ToList();
                List<string> salesUnits = db.SlsPrcDets.Select(sd => sd.SlsUnit).Distinct().ToList();
                List<string> curyIds = db.SlsPrcs.Select(s => s.CuryId.Trim()).Distinct().ToList();

                // add a validation and set values
                var validation4 = sheet.DataValidations.AddListValidation("B:B");
                validation4.ShowErrorMessage = true;
                validation4.ErrorStyle = ExcelDataValidationWarningStyle.warning;
                validation4.ErrorTitle = "An invalid value was entered";
                validation4.Error = "Select a value from the list";
                foreach (var ci in curyIds)
                {
                    validation4.Formula.Values.Add(ci.Trim());
                }
                Console.WriteLine("Added sheet for list validation with values");

                // add a validation and set values
                var validation = sheet.DataValidations.AddListValidation("C:C");
                validation.ShowErrorMessage = true;
                validation.ErrorStyle = ExcelDataValidationWarningStyle.warning;
                validation.ErrorTitle = "An invalid value was entered";
                validation.Error = "Select a value from the list";
                foreach (var pt in priceTypes)
                {
                    validation.Formula.Values.Add(pt.Trim());
                }
                Console.WriteLine("Added sheet for list validation with values");


                // add a validation and set values
                var validation2 = sheet.DataValidations.AddListValidation("D:D");
                validation2.ShowErrorMessage = true;
                validation2.ErrorStyle = ExcelDataValidationWarningStyle.warning;
                validation2.ErrorTitle = "An invalid value was entered";
                validation2.Error = "Select a value from the list";
                foreach (var dm in discountMethods)
                {
                    validation2.Formula.Values.Add(dm.Trim());
                }
                Console.WriteLine("Added sheet for list validation with values");

                // add a validation and set values
                var validation3 = sheet.DataValidations.AddListValidation("J:J");
                validation3.ShowErrorMessage = true;
                validation3.ErrorStyle = ExcelDataValidationWarningStyle.warning;
                validation3.ErrorTitle = "An invalid value was entered";
                validation3.Error = "Select a value from the list";
                foreach (var su in salesUnits)
                {
                    validation3.Formula.Values.Add(su.Trim());
                }
                Console.WriteLine("Added sheet for list validation with values");
            }
        }

        private async void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                using (var db = new SolomonDbContext())
                {
                    CustomerList = await db.SlsPrcs.Select(s => s.CustId.Trim()).Distinct().ToListAsync();
                    CurrencyList = await db.SlsPrcs.Select(s => s.CuryId.Trim()).Distinct().ToListAsync();
                }
                CustomerList = CustomerList.Prepend("*").ToList();
                CurrencyList = CurrencyList.Prepend("*").ToList();
                cbCustomerID.DataSource = CustomerList;
                cbCurrencyID.DataSource = CurrencyList;

            }
            catch (Exception ex)
            {
                MessageBox.Show("No DB connection: \n" + ex.Message);
            }
        }

        private void BtnOpenProjectDirectory_Click(object sender, EventArgs e)
        {
            Process.Start("explorer.exe", ProjectDirectory);
        }

    }

}