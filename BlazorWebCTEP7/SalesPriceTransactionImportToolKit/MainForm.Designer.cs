﻿namespace SalesPriceTransactionImportToolKit
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            btnXrefToolkit = new Button();
            btnSalesPriceToolkit = new Button();
            btnKitPrices = new Button();
            SuspendLayout();
            // 
            // btnXrefToolkit
            // 
            btnXrefToolkit.Location = new Point(197, 104);
            btnXrefToolkit.Name = "btnXrefToolkit";
            btnXrefToolkit.Size = new Size(107, 99);
            btnXrefToolkit.TabIndex = 3;
            btnXrefToolkit.Text = "ItemXRef Toolkit";
            btnXrefToolkit.UseVisualStyleBackColor = true;
            btnXrefToolkit.Click += btnXrefToolkit_Click;
            // 
            // btnSalesPriceToolkit
            // 
            btnSalesPriceToolkit.Location = new Point(58, 104);
            btnSalesPriceToolkit.Name = "btnSalesPriceToolkit";
            btnSalesPriceToolkit.Size = new Size(107, 99);
            btnSalesPriceToolkit.TabIndex = 2;
            btnSalesPriceToolkit.Text = "Sales Price Toolkit";
            btnSalesPriceToolkit.UseVisualStyleBackColor = true;
            btnSalesPriceToolkit.Click += btnSalesPriceToolkit_Click;
            // 
            // btnKitPrices
            // 
            btnKitPrices.Location = new Point(341, 104);
            btnKitPrices.Name = "btnKitPrices";
            btnKitPrices.Size = new Size(107, 99);
            btnKitPrices.TabIndex = 4;
            btnKitPrices.Text = "Calculate Kit Prices";
            btnKitPrices.UseVisualStyleBackColor = true;
            btnKitPrices.Click += btnKitPrices_Click;
            // 
            // MainForm
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 451);
            Controls.Add(btnKitPrices);
            Controls.Add(btnXrefToolkit);
            Controls.Add(btnSalesPriceToolkit);
            Name = "MainForm";
            Text = "Transaction Import Toolkit";
            ResumeLayout(false);
        }

        #endregion

        private Button btnXrefToolkit;
        private Button btnSalesPriceToolkit;
        private Button btnKitPrices;
    }
}