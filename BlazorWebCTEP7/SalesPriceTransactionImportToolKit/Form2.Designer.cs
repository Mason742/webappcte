﻿namespace SalesPriceTransactionImportToolKit
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            btnGenerateXrefExoport = new Button();
            btnGenerateXrefImport = new Button();
            cbVendorID = new ComboBox();
            label3 = new Label();
            textBox1 = new TextBox();
            openFileDialog1 = new OpenFileDialog();
            btnOpenProjectDirectory = new Button();
            btnDirectlyUpdateXRef = new Button();
            SuspendLayout();
            // 
            // btnGenerateXrefExoport
            // 
            btnGenerateXrefExoport.Location = new Point(47, 85);
            btnGenerateXrefExoport.Name = "btnGenerateXrefExoport";
            btnGenerateXrefExoport.Size = new Size(130, 72);
            btnGenerateXrefExoport.TabIndex = 0;
            btnGenerateXrefExoport.Text = "Generate XRef Export";
            btnGenerateXrefExoport.UseVisualStyleBackColor = true;
            btnGenerateXrefExoport.Click += btnGenerateXrefExoport_Click;
            // 
            // btnGenerateXrefImport
            // 
            btnGenerateXrefImport.Location = new Point(219, 85);
            btnGenerateXrefImport.Name = "btnGenerateXrefImport";
            btnGenerateXrefImport.Size = new Size(149, 72);
            btnGenerateXrefImport.TabIndex = 1;
            btnGenerateXrefImport.Text = "Generate Transaction Import Data File";
            btnGenerateXrefImport.UseVisualStyleBackColor = true;
            btnGenerateXrefImport.Click += btnGenerateXrefImport_Click;
            // 
            // cbVendorID
            // 
            cbVendorID.FormattingEnabled = true;
            cbVendorID.Location = new Point(138, 29);
            cbVendorID.Margin = new Padding(3, 4, 3, 4);
            cbVendorID.Name = "cbVendorID";
            cbVendorID.Size = new Size(138, 28);
            cbVendorID.TabIndex = 13;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new Point(47, 32);
            label3.Name = "label3";
            label3.Size = new Size(75, 20);
            label3.TabIndex = 12;
            label3.Text = "Vendor ID";
            // 
            // textBox1
            // 
            textBox1.Location = new Point(47, 177);
            textBox1.Margin = new Padding(3, 4, 3, 4);
            textBox1.Multiline = true;
            textBox1.Name = "textBox1";
            textBox1.ReadOnly = true;
            textBox1.Size = new Size(346, 156);
            textBox1.TabIndex = 14;
            textBox1.Text = resources.GetString("textBox1.Text");
            // 
            // openFileDialog1
            // 
            openFileDialog1.FileName = "openFileDialog1";
            // 
            // btnOpenProjectDirectory
            // 
            btnOpenProjectDirectory.Location = new Point(413, 85);
            btnOpenProjectDirectory.Margin = new Padding(3, 4, 3, 4);
            btnOpenProjectDirectory.Name = "btnOpenProjectDirectory";
            btnOpenProjectDirectory.Size = new Size(171, 72);
            btnOpenProjectDirectory.TabIndex = 15;
            btnOpenProjectDirectory.Text = "Open Project Directory";
            btnOpenProjectDirectory.UseVisualStyleBackColor = true;
            btnOpenProjectDirectory.Click += btnOpenProjectDirectory_Click;
            // 
            // btnDirectlyUpdateXRef
            // 
            btnDirectlyUpdateXRef.Location = new Point(413, 263);
            btnDirectlyUpdateXRef.Name = "btnDirectlyUpdateXRef";
            btnDirectlyUpdateXRef.Size = new Size(171, 72);
            btnDirectlyUpdateXRef.TabIndex = 16;
            btnDirectlyUpdateXRef.Text = "Directly Update XRef Table (SQL Script)";
            btnDirectlyUpdateXRef.UseVisualStyleBackColor = true;
            btnDirectlyUpdateXRef.Click += btnDirectlyUpdateXRef_Click;
            // 
            // Form2
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 451);
            Controls.Add(btnDirectlyUpdateXRef);
            Controls.Add(btnOpenProjectDirectory);
            Controls.Add(textBox1);
            Controls.Add(cbVendorID);
            Controls.Add(label3);
            Controls.Add(btnGenerateXrefImport);
            Controls.Add(btnGenerateXrefExoport);
            Name = "Form2";
            Text = "Item Cross References Toolkit";
            Load += Form2_Load;
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Button btnGenerateXrefExoport;
        private Button btnGenerateXrefImport;
        private ComboBox cbVendorID;
        private Label label3;
        private TextBox textBox1;
        private OpenFileDialog openFileDialog1;
        private Button btnOpenProjectDirectory;
        private Button btnDirectlyUpdateXRef;
    }
}