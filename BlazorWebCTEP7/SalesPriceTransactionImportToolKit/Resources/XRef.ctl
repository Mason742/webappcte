'ControlMacroType: VBAComplete
' VBComponent: ThisScreen, ComponentType: 100 **********************************************************
'10380 Control Macro
Sub ProcessImportLine( LevelNumber%, Retval% )
  select Case LevelNumber
    case TI_Start

      call AliasConstant( "Level0", "Item" )

      call AliasConstant( "Level1", "CrossReference" )

    case 0 ' 0 of 1

            'Level 0 if of Type N
            ' cinvtidH is a key field for level 0
            ' Field mask is UUUUUUUUUUUUUUUUUUUU
            ' NOTE  -- This field is a required field
            serr = SetObjectValue("cinvtidH", ImportField(1))
        Case 1 ' 1 of 1

            'Level 1 if of Type D
            ' ctype_Renamed is a key field for level 1
            ' ctype_Renamed is an Combo Box
            ' Values are C;Customer Part Number,V;Vendor Part Number,M;Manufacturer Part Number,S;Substitute,K;Stock Keeping Unit (SKU),U;Universal Product Code (UPC),E;European Article Number (EAN),I;Military Spec Number,D;National Drug Code,P;Print/Drawing Number,B;Int'l Std Book Nbr (ISBN),G;Global Cross-Reference,O;Obsolete
            ' NOTE  -- This field is a required field

            serr = SetObjectValue("ctype_Renamed", ImportField(1))

            ' cEntityID is a key field for level 1
            ' Field mask is MMMMMMMMMMMMMMM
            ' NOTE  -- This field is a required field
            serr = SetObjectValue( "cEntityID", ImportField(2) )

      ' cAlternateID is a key field for level 1
      ' Field mask is UUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
      ' NOTE  -- This field is a required field
      serr = SetObjectValue( "cAlternateID", ImportField(3) )

      ' cInvtId is a key field for level 1
      ' Field mask is XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      serr = SetObjectValue( "xuser1", ImportField(4) )

            ' Field mask is XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
            serr = SetObjectValue("cDescr", ImportField(5))


        Case TI_Finish

  End Select
End Sub
' The following shows the correct syntax to
'  PRESS a button
' These object names are specific to this screen.
'      Button object name TemplateFormBtnPaste, Caption Paste
'      serr = SetObjectValue( "TemplateFormBtnPaste", "PRESS" )

'      Button object name TemplateFormBtnSave, Caption Save
'      serr = SetObjectValue( "TemplateFormBtnSave", "PRESS" )

'      Button object name TemplateFormBtnDelete, Caption Delete
'      serr = SetObjectValue( "TemplateFormBtnDelete", "PRESS" )

'      Button object name TemplateFormBtnClose, Caption Close
'      serr = SetObjectValue( "TemplateFormBtnClose", "PRESS" )

'      Button object name cmdFindItem, Caption 
'      serr = SetObjectValue( "cmdFindItem", "PRESS" )

' End VBComponent **********************************************************
