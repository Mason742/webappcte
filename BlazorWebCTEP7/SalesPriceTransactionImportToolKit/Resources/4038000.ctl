'ControlMacroType: VBAComplete
' VBComponent: ThisScreen, ComponentType: 100 **********************************************************
'40380 Control Macro
Sub ProcessImportLine( LevelNumber%, Retval% )
  select Case LevelNumber
    case TI_Start

      call AliasConstant( "Level0", "Selection" )

      call AliasConstant( "Level1", "SlsPrc" )

      call AliasConstant( "Level2", "SlsPrcDet" )

    case 0 ' 0 of 2

    'Level 0 if of Type N
      ' Field mask is UUUU
      ' NOTE  -- This field is a required field
      serr = SetObjectValue( "cCuryID", ImportField(1) )

      ' Field mask is MMMMMMMMMM
      serr = SetObjectValue( "cSiteID", ImportField(2) )

      ' cPriceCat is an Combo Box
      ' Values are CU;Customer,CC;Customer Price Class,IT;Inventory Item,IL;Invt Item and Cust Price Class,IC;Invt Item and Customer,PC;Invt Price Class,PL;Invt Price Class and Cust Price Class,LC;Invt Price Class and Customer
      ' NOTE  -- This field is a required field
      serr = SetObjectValue( "cPriceCat", ImportField(3) )

      ' Field mask is MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
      serr = SetObjectValue( "cInvtID", ImportField(4) )

      ' Field mask is MMMMMMMMMMMMMMM
      serr = SetObjectValue( "cCustID", ImportField(5) )
serr = SetObjectValue( "cmdRefresh", "PRESS" )
     


    case 1 ' 1 of 2

    'Level 1 if of Type D
      ' Field mask is UUUUUUUUUUUUUUU
      serr = SetObjectValue( "cCatalogNbr_0", ImportField(1) )

      ' cDiscPrcTyp_0 is an Combo Box
      ' Values are P;Promotion,S;Standard
      ' NOTE  -- This field is a required field
      serr = SetObjectValue( "cDiscPrcTyp_0", ImportField(2) )

      ' cDiscPrcMthd_0 is an Combo Box
      ' Values are F;Flat Price,R;Price Discount,P;Percent Discount,M;Percent Markup,K;Price Markup
      ' NOTE  -- This field is a required field
      serr = SetObjectValue( "cDiscPrcMthd_0", ImportField(3) )

      ' Field mask is UUUUUU
      ' NOTE  -- This field is a required field
      serr = SetObjectValue( "cSiteID_0", ImportField(4) )

      serr = SetObjectValue( "cMinQty_0", ImportField(5) )

      serr = SetObjectValue( "cMaxQty_0", ImportField(6) )


    case 2 ' 2 of 2

    'Level 2 if of Type D
      ' Field mask is UUUUUU
      ' NOTE  -- This field is a required field
      serr = SetObjectValue( "cSlsUnit_1", ImportField(1) )

      ' NOTE  -- This field is a required field
      serr = SetObjectValue( "cQtyBreak_1", ImportField(2) )

      serr = SetObjectValue( "cDiscPct_1", ImportField(3) )

      serr = SetObjectValue( "cDiscPrice_1", ImportField(4) )

    case TI_Finish

  End Select
End Sub
' The following shows the correct syntax to
'  PRESS a button
' These object names are specific to this screen.
'      Button object name cmdCopyOK, Caption OK
'      serr = SetObjectValue( "cmdCopyOK", "PRESS" )

'      Button object name cmdCopyCancel, Caption Cancel
'      serr = SetObjectValue( "cmdCopyCancel", "PRESS" )

'      Button object name cmdMassCopyOK, Caption OK
'      serr = SetObjectValue( "cmdMassCopyOK", "PRESS" )

'      Button object name cmdMassCopyCancel, Caption Cancel
'      serr = SetObjectValue( "cmdMassCopyCancel", "PRESS" )

'      Button object name TemplateFormBtnPaste, Caption Paste
'      serr = SetObjectValue( "TemplateFormBtnPaste", "PRESS" )

'      Button object name TemplateFormBtnSave, Caption Save
'      serr = SetObjectValue( "TemplateFormBtnSave", "PRESS" )

'      Button object name TemplateFormBtnDelete, Caption Delete
'      serr = SetObjectValue( "TemplateFormBtnDelete", "PRESS" )

'      Button object name TemplateFormBtnClose, Caption Close
'      serr = SetObjectValue( "TemplateFormBtnClose", "PRESS" )

'      Button object name cmdRateChangeOK, Caption OK
'      serr = SetObjectValue( "cmdRateChangeOK", "PRESS" )

'      Button object name cmdRateChangeCancel, Caption Cancel
'      serr = SetObjectValue( "cmdRateChangeCancel", "PRESS" )

'      Button object name cmdRefresh, Caption &Refresh
'      serr = SetObjectValue( "cmdRefresh", "PRESS" )

'      Button object name cmdCopy, Caption Cop&y...
'      serr = SetObjectValue( "cmdCopy", "PRESS" )

'      Button object name cmdMassCopy, Caption Ma&ss Copy...
'      serr = SetObjectValue( "cmdMassCopy", "PRESS" )

'      Button object name cmdRateChange, Caption R&ate Change...
'      serr = SetObjectValue( "cmdRateChange", "PRESS" )

' End VBComponent **********************************************************
