﻿using SolomonDB.Data.Models;

namespace SalesPriceTransactionImportToolKit
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            btnExcelTemplateExport = new Button();
            label1 = new Label();
            label3 = new Label();
            btnGenerateTIDataFile = new Button();
            openFileDialog1 = new OpenFileDialog();
            cbCurrencyID = new ComboBox();
            customerBindingSource = new BindingSource(components);
            cbCustomerID = new ComboBox();
            textBox1 = new TextBox();
            btnOpenProjectDirectory = new Button();
            ((System.ComponentModel.ISupportInitialize)customerBindingSource).BeginInit();
            SuspendLayout();
            // 
            // btnExcelTemplateExport
            // 
            btnExcelTemplateExport.Location = new Point(57, 126);
            btnExcelTemplateExport.Margin = new Padding(3, 2, 3, 2);
            btnExcelTemplateExport.Name = "btnExcelTemplateExport";
            btnExcelTemplateExport.Size = new Size(156, 40);
            btnExcelTemplateExport.TabIndex = 0;
            btnExcelTemplateExport.Text = "Generate Excel Export";
            btnExcelTemplateExport.UseVisualStyleBackColor = true;
            btnExcelTemplateExport.Click += BtnExcelTemplateExport_Click;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(57, 9);
            label1.Name = "label1";
            label1.Size = new Size(69, 15);
            label1.TabIndex = 6;
            label1.Text = "Currency ID";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new Point(193, 9);
            label3.Name = "label3";
            label3.Size = new Size(73, 15);
            label3.TabIndex = 8;
            label3.Text = "Customer ID";
            // 
            // btnGenerateTIDataFile
            // 
            btnGenerateTIDataFile.Location = new Point(274, 126);
            btnGenerateTIDataFile.Name = "btnGenerateTIDataFile";
            btnGenerateTIDataFile.Size = new Size(150, 40);
            btnGenerateTIDataFile.TabIndex = 9;
            btnGenerateTIDataFile.Text = "Generate Transaction Import Data File";
            btnGenerateTIDataFile.UseVisualStyleBackColor = true;
            btnGenerateTIDataFile.Click += BtnGenerateTIDataFile_Click;
            // 
            // openFileDialog1
            // 
            openFileDialog1.FileName = "openFileDialog1";
            // 
            // cbCurrencyID
            // 
            cbCurrencyID.FormattingEnabled = true;
            cbCurrencyID.Location = new Point(57, 42);
            cbCurrencyID.Name = "cbCurrencyID";
            cbCurrencyID.Size = new Size(121, 23);
            cbCurrencyID.TabIndex = 10;
            // 
            // customerBindingSource
            // 
            customerBindingSource.DataSource = typeof(Customer);
            // 
            // cbCustomerID
            // 
            cbCustomerID.FormattingEnabled = true;
            cbCustomerID.Location = new Point(193, 42);
            cbCustomerID.Name = "cbCustomerID";
            cbCustomerID.Size = new Size(121, 23);
            cbCustomerID.TabIndex = 11;
            // 
            // textBox1
            // 
            textBox1.Location = new Point(57, 186);
            textBox1.Multiline = true;
            textBox1.Name = "textBox1";
            textBox1.ReadOnly = true;
            textBox1.Size = new Size(303, 118);
            textBox1.TabIndex = 12;
            textBox1.Text = resources.GetString("textBox1.Text");
            // 
            // btnOpenProjectDirectory
            // 
            btnOpenProjectDirectory.Location = new Point(476, 126);
            btnOpenProjectDirectory.Name = "btnOpenProjectDirectory";
            btnOpenProjectDirectory.Size = new Size(150, 40);
            btnOpenProjectDirectory.TabIndex = 13;
            btnOpenProjectDirectory.Text = "Open Project Directory";
            btnOpenProjectDirectory.UseVisualStyleBackColor = true;
            btnOpenProjectDirectory.Click += BtnOpenProjectDirectory_Click;
            // 
            // Form1
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(700, 338);
            Controls.Add(btnOpenProjectDirectory);
            Controls.Add(textBox1);
            Controls.Add(cbCustomerID);
            Controls.Add(cbCurrencyID);
            Controls.Add(btnGenerateTIDataFile);
            Controls.Add(label3);
            Controls.Add(label1);
            Controls.Add(btnExcelTemplateExport);
            Margin = new Padding(3, 2, 3, 2);
            Name = "Form1";
            Text = "Sales Price Toolkit";
            Load += Form1_Load;
            ((System.ComponentModel.ISupportInitialize)customerBindingSource).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Button btnExcelTemplateExport;
        private Label label1;
        private Label label3;
        private Button btnGenerateTIDataFile;
        private OpenFileDialog openFileDialog1;
        private ComboBox cbCurrencyID;
        private BindingSource customerBindingSource;
        private ComboBox cbCustomerID;
        private TextBox textBox1;
        private Button btnOpenProjectDirectory;
    }
}