﻿using Microsoft.EntityFrameworkCore;
using OfficeOpenXml;
using SolomonDB.Data.Context;
using SolomonDB.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EPPlusExcelService.Helpers;
using static SalesPriceTransactionImportToolKit.Form2;

namespace TransactionImportToolKit
{
    public partial class CalculateKitPriceForm : Form
    {
        public CalculateKitPriceForm()
        {
            InitializeComponent();
        }
        private void btnUpdateKitPrices_Click(object sender, EventArgs e)
        {
            btnUpdateKitPrices.Enabled = false;
            openFileDialog1 = new OpenFileDialog()
            {
                FileName = "Select a xlsx file",
                Filter = "Excel files (*.xlsx)|*.xlsx",
                Title = "CTE_PriceList.xlsx"
            };
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    var filePath = openFileDialog1.FileName;
                    FileInfo fileInfo = new(filePath);
                    UpdateKitsTable(fileInfo);
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Generating TI data file Error:\n\nError message: {ex.Message}\n\n" +
                    $"Details:\n\n{ex.StackTrace}");
                }
            }
            MessageBox.Show("Completed");
            btnUpdateKitPrices.Enabled = true;
        }

        public record PriceListItem
        {
            public string InvtID { get; set; }
            public double Price { get; set; }
        }



        private void UpdateKitsTable(FileInfo fileInfo)
        {
            using (var sl = new SolomonDbContext())
            {
                ExcelPackage package = new(fileInfo);
                var worksheet = package.Workbook.Worksheets.FirstOrDefault(x => x.Name.Contains("CTE_PriceList"));

                if (worksheet == null)
                {
                    throw new NullReferenceException("Excel Sheet named CTE_PriceList not found. \n Rename sheet to CTE_PriceList.");
                }

                var emptyRows = worksheet.TrimLastEmptyRows();

                if (emptyRows > 0)
                {
                    MessageBox.Show($"{emptyRows} empty rows automatically deleted.", "Notice");
                }

                DialogResult dialogResult = MessageBox.Show("Are you sure you want to calculate kit prices? Doing so will delete the current kit calculations.", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (dialogResult == DialogResult.No)
                {
                    MessageBox.Show("Cancelled");
                    return;
                }

                List<PriceListItem> priceListItems = new List<PriceListItem>();
                var table = worksheet.Tables.FirstOrDefault(x => x.Name == "Table_CTE_PriceList");

                if (table == null)
                {
                    throw new NullReferenceException("Excel Table_CTE_PriceList not found. \n Rename table to Table_CTE_PriceList.");
                }

                int rows = table.WorkSheet.Dimension.Rows;
                int header = 1;
                var invtIdColumn = worksheet.GetColumnByNameV2("InvtID");
                var preKitValueColumn = worksheet.GetColumnByNameV2("PreKitValue");

                for (int i = 1 + header; i <= rows; i++)
                {
                    PriceListItem item = new()
                    {
                        InvtID = worksheet.Cells[i, invtIdColumn].Value.ToString().Trim(),
                        Price = (double)worksheet.Cells[i, preKitValueColumn].Value,
                    };
                    priceListItems.Add(item);
                }

                string deleteSql = $"TRUNCATE TABLE Mason_KitPrice";
                sl.Database.ExecuteSqlRaw(deleteSql);

                List<MasonKitsAndComponent> kitsAndComponents = sl.MasonKitsAndComponents.ToList();
                List<string> kits = kitsAndComponents.DistinctBy(x => x.KitId).Select(x => x.KitId).ToList();
                List<MasonKitPrice> kitsPrices = new List<MasonKitPrice>();

                foreach (string k in kits)
                {
                    var components = kitsAndComponents.Where(x => x.KitId == k).ToList();
                    var result = GetKitPricev2(components, kitsAndComponents, priceListItems);
                    kitsPrices.Add(new MasonKitPrice
                    {
                        InvtId = k.Trim(),
                        Dprice = priceListItems.FirstOrDefault(x=>x.InvtID==k)?.Price ?? 0,
                        NewPrice = result.Item1,
                        Note = result.Item2
                    });
                }

                sl.MasonKitPrices.AddRange(kitsPrices);
                sl.SaveChanges();
            }
        }

        private (double, string) GetKitPricev2(List<MasonKitsAndComponent> components, List<MasonKitsAndComponent> kitsAndComponents, List<PriceListItem> priceListItems)
        {
            double price = 0;
            string note = "Pass";

            foreach (MasonKitsAndComponent c in components)
            {
                double componentPrice = 0;
                if (c.IsKit == 0)
                {
                    componentPrice = priceListItems.FirstOrDefault(x => x.InvtID == c.CompId)?.Price ?? 0;
                    if (componentPrice == 0)
                    {
                        if(c.CompMsrp > 0)
                        {
                            componentPrice = c.CompMsrp;
                        }else if(c.CompLastCost > 0)
                        {
                            componentPrice = c.CompLastCost;
                        }else if(c.CompStdCost > 0)
                        {
                            componentPrice= c.CompStdCost;
                        }
                        else
                        {
                        //failed to find cost
                        note = "Fail";
                        }
                    }
                    componentPrice *= c.CmpnentQty;
                }
                else
                {
                    List<MasonKitsAndComponent> childComponents = GetComponentsByKitId(c.CompId, kitsAndComponents);
                    var (childPrice, childNote) = GetKitPricev2(childComponents, kitsAndComponents, priceListItems);
                    if (childNote == "Fail")
                    {
                        note = "Fail";
                    }
                    componentPrice = c.CmpnentQty * childPrice;
                }

                price += componentPrice;
            }

            return (price, note);
        }

        private List<MasonKitsAndComponent> GetComponentsByKitId(string kitId, List<MasonKitsAndComponent> kitsAndComponents)
        {
            return kitsAndComponents.Where(x => x.KitId == kitId).ToList();
        }

        //private string GetKitNote(List<MasonKitsAndComponent> components, List<PriceListItem> priceListItems)
        //{
        //    foreach (MasonKitsAndComponent c in components)
        //    {
        //        var price = priceListItems.FirstOrDefault(x => x.InvtID == c.CompId)?.Price;
        //        if (price == null || price == 0)
        //        {
        //            return "Fail:Component has no MSRP";
        //        }
        //    }
        //    return "Pass";
        //}
    }
}
