﻿namespace TransactionImportToolKit
{
    partial class CalculateKitPriceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            btnUpdateKitPrices = new Button();
            textBox1 = new TextBox();
            openFileDialog1 = new OpenFileDialog();
            SuspendLayout();
            // 
            // btnUpdateKitPrices
            // 
            btnUpdateKitPrices.Location = new Point(100, 93);
            btnUpdateKitPrices.Name = "btnUpdateKitPrices";
            btnUpdateKitPrices.Size = new Size(149, 72);
            btnUpdateKitPrices.TabIndex = 2;
            btnUpdateKitPrices.Text = "Update Kit Prices";
            btnUpdateKitPrices.UseVisualStyleBackColor = true;
            btnUpdateKitPrices.Click += btnUpdateKitPrices_Click;
            // 
            // textBox1
            // 
            textBox1.Location = new Point(100, 187);
            textBox1.Margin = new Padding(3, 4, 3, 4);
            textBox1.Multiline = true;
            textBox1.Name = "textBox1";
            textBox1.ReadOnly = true;
            textBox1.Size = new Size(346, 156);
            textBox1.TabIndex = 15;
            textBox1.Text = "1. Select the CTE_PriceList.xlsx\r\n2. Click Yes on the prompt update kit prices\r\n";
            // 
            // openFileDialog1
            // 
            openFileDialog1.FileName = "openFileDialog1";
            // 
            // CalculateKitPriceForm
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(textBox1);
            Controls.Add(btnUpdateKitPrices);
            Name = "CalculateKitPriceForm";
            Text = "CalculateKitPriceForm";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Button btnUpdateKitPrices;
        private TextBox textBox1;
        private OpenFileDialog openFileDialog1;
    }
}