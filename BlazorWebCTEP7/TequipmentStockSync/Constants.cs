﻿using EmailSenderService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TequipmentStockSync
{
    internal class Constants
    {
        public const string ApiKey = "hapikey=cccf72f9-081e-40c3-b92c-edf6f3ded038";
        public const string GlobalDirectory = @"C:\Projects\TequipmentStockSync\";
        public const string ProductPriceFlatFileCT = GlobalDirectory + @"out\product_quantity_cte.csv";
        public const string ProductPriceFlatFileGS = GlobalDirectory + @"out\product_quantity_gs.csv";

    }
}
