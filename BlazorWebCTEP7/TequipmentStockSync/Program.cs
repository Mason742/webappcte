﻿using MailKit.Net.Smtp;
using MimeKit;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using WebDB.Data.Context;
using WebDB.Data.Models;
using EmailSenderService;
using EmailSenderService.Services;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using EmailSenderService.Models;

using TequipmentStockSync;
List<ErpInventory> erpInventories = new();
List<BkGsStock> bKGsStock = new();

try
{
    using (DefaultDbContext db = new())
    {
        erpInventories = await db.ErpInventories.OrderBy(x => x.PartNumber).Where(x => x.SpecialHandling.Trim().Equals("CATALOG") || x.SpecialHandling.Trim().Equals("SPECIALORD")).ToListAsync();
        //bK_GS_Stock = await db.BkGsStocks.ToListAsync();
    }

    foreach (var bkStock in bKGsStock)
    {
        string term = bkStock.PartNumber.Replace("GS-", "");
        bool found1 = erpInventories.Any(x => x.PartNumber.Trim().Equals(bkStock.PartNumber));
        bool found2 = erpInventories.Any(x => x.PartNumber.Trim().Equals(term));
        bool found3 = erpInventories.Any(x => x.PartNumber.Trim().Replace("-", "").Equals(term));
        if (found1 || found2 || found3)
        {
            if (found1)
            {
                erpInventories.Where(x => x.PartNumber.Trim().Equals(bkStock.PartNumber)).FirstOrDefault().Quantity += bkStock.Quantity;
            }
            else if (found2)
            {
                erpInventories.Where(x => x.PartNumber.Trim().Equals(term)).FirstOrDefault().Quantity += bkStock.Quantity;
            }
            else if (found3)
            {
                erpInventories.Where(x => x.PartNumber.Trim().Replace("-", "").Equals(term)).FirstOrDefault().Quantity += bkStock.Quantity;
            }
        }
    }

    string outputFile = "\"Product Model\",Quantity,Date\n";
    foreach (var pp in erpInventories.Where(x => x.Brand.Trim().Equals("CT")))
    {
        outputFile += pp.PartNumber.Trim() + "," + pp.Quantity + ",\"" + pp.Created.ToString("yyyy-MM-dd HH:mm:ss") + "\"\n";
    }
    File.WriteAllText(Constants.ProductPriceFlatFileCT, outputFile);

    outputFile = "\"Product Model\",Quantity,Date\n";
    foreach (var pp in erpInventories.Where(x => x.Brand.Trim().Equals("GS")))
    {
        outputFile += pp.PartNumber.Trim() + "," + pp.Quantity + ",\"" + pp.Created.ToString("yyyy-MM-dd HH:mm:ss") + "\"\n";
    }
    File.WriteAllText(Constants.ProductPriceFlatFileGS, outputFile);

    MimeMessage mimeMessage = CreateEmail();

    MailKitEmailSenderService mailKitEmailService = new(new MailKitEmailSenderOptions(defaultOptions: true));

    await mailKitEmailService.SendEmailAsync(mimeMessage);
    Console.WriteLine("Email sent: " + mimeMessage.To);
}
catch (Exception ex)
{
    Console.WriteLine(ex.Message);
}
Console.WriteLine("Closing...");


static MimeMessage CreateEmail()
{
    //create mailing list
    InternetAddressList list = new()
            {
                new MailboxAddress("Tequipment", "vendors@iwhtech.com")
            };
    //list.Add(new MailboxAddress("Tequipment", "mchanner@caltestelectronics.com"));

    MimeMessage message = new();
    message.From.Add(new MailboxAddress("info", "caltestelectronicsinfo@gmail.com"));
    message.To.AddRange(list);

    //create message
    var bodyBuilder = new BodyBuilder();

    message.Subject = "CTE/GS: Quantity Product Stock (" + DateTime.Now.ToString("M-d-yyyy") + ")";
    bodyBuilder.TextBody = "Attached are the product stock quantities for Cal Test Electronics and Global Specialties (" + DateTime.Now.ToString("M-d-yyyy") + ")";
    bodyBuilder.Attachments.Add(Constants.ProductPriceFlatFileCT);
    bodyBuilder.Attachments.Add(Constants.ProductPriceFlatFileGS);

    message.Body = bodyBuilder.ToMessageBody();
    return message;
}
