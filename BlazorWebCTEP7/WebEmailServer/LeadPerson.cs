﻿using BlazorWebWasmCTE.Shared.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static CTECMS.Data.CMSDbContext;

namespace WebEmailServer
{
    public class LeadPerson
    {
        public string Title { get; set; } = "Web Master";
        public string Email { get; set; } = "webforms@caltestelectronics.com";
        public string MicrosoftTeamsChannelName { get; set; } = "Web Forms";
        public string MicrosoftTeamsEmail { get; set; } = "43f51f9e.caltestelectronics.com@amer.teams.ms";
        //anthony owner of all tickets
        public string HubSpotOwnerId { get; set; } = "64511761"; //"65835033";



        public LeadPerson GetLeadPerson(WebFormEmail wF)
        {
            LeadPerson leadPerson = new LeadPerson();

            if (wF.Reason.Equals("Contact_Sales"))
            {
                leadPerson.Title = "Sales";
                leadPerson.Email = "sales@caltestelectronics.com";
                //Rachel
                leadPerson.HubSpotOwnerId = "70217528";
            }
            else if (wF.Reason.Equals("Contact_TechSupport"))
            {
                leadPerson.Title = "Tech Support";
                leadPerson.Email = "techsupport@caltestelectronics.com";
                //Marco
                leadPerson.HubSpotOwnerId = "65835033";
            }
            else if (wF.Reason.Equals("Contact_Other"))
            {
                leadPerson.Title = "Anthony";
                leadPerson.Email = "anthony@caltestelectronics.com";
                //Anthony
                leadPerson.HubSpotOwnerId = "70248610";
            }
            else if (wF.Reason.Equals("TestLeadBuilder_Email"))
            {
                leadPerson.Title = "Marco";
                leadPerson.Email = "marco@caltestelectronics.com";
                //Marco
                leadPerson.HubSpotOwnerId = "65835033";
            }
            else if (wF.Reason.Equals("TestLeadBuilder_Quote"))
            {
                leadPerson.Title = "Marco";
                leadPerson.Email = "marco@caltestelectronics.com";
                //Marco
                leadPerson.HubSpotOwnerId = "65835033";
            }
            else if (wF.Reason.Equals("Service_Repair"))
            {
                leadPerson.Title = "Service";
                leadPerson.Email = "service@caltestelectronics.com";
                //Marco
                leadPerson.HubSpotOwnerId = "70248610";
            }
            else if (wF.Reason.Equals("Service_Calibration"))
            {
                leadPerson.Title = "Service";
                leadPerson.Email = "service@caltestelectronics.com";
                //Marco
                leadPerson.HubSpotOwnerId = "70248610";
            }
            return leadPerson;
        }

    }
}
