﻿using BlazorWebWasmCTE.Shared.Data;
using Microsoft.CodeAnalysis.CSharp;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using static CTECMS.Data.CMSDbContext;

namespace WebEmailServer
{
    public class HubSpotService
    {
        public const string apiKey = "hapikey=cccf72f9-081e-40c3-b92c-edf6f3ded038";
        public static ContactResponse.SearchResponse.Response SearchContact(string email, RestClient client)
        {
            var contactRequest = new RestRequest("contacts/search?" + apiKey, Method.POST);
            contactRequest.AddHeader("Content-Type", "application/json");
            contactRequest.AddParameter("application/json", "{\"filterGroups\":[{\"filters\":[{\"value\":\"" + email + "\",\"propertyName\":\"email\",\"operator\":\"EQ\"}]}],\"sorts\":[\"email\"],\"properties\":[\"email\",\"id\"],\"limit\":1,\"after\":0}", ParameterType.RequestBody);
            IRestResponse contactJsonResponse = client.Execute(contactRequest);
            ContactResponse.SearchResponse.Response contactSearchResponse = JsonConvert.DeserializeObject<ContactResponse.SearchResponse.Response>(contactJsonResponse.Content);
            return contactSearchResponse;
        }

        public  static ContactResponse.CreateResponse.Response CreateContact(WebFormEmail webForm, RestClient client)
        {
            var contactCreateRequest = new RestRequest("contacts?" + apiKey, Method.POST);
            contactCreateRequest.AddHeader("accept", "application/json");
            contactCreateRequest.AddHeader("content-type", "application/json");
            contactCreateRequest.AddParameter("application/json", "{\"properties\":{\"country\":\"" + webForm.Country + "\",\"state\":\"" + webForm.State + "\",\"city\":\"" + webForm.City + "\",\"company\":\"" + webForm.Company + "\",\"email\":\"" + webForm.Email + "\",\"firstname\":\"" + webForm.FirstName + "\",\"lastname\":\"" + webForm.LastName + "\",\"phone\":\"" + webForm.Phone + "\"}}", ParameterType.RequestBody);
            IRestResponse createJsonResponse = client.Execute(contactCreateRequest);
            ContactResponse.CreateResponse.Response createContactResponse = JsonConvert.DeserializeObject<ContactResponse.CreateResponse.Response>(createJsonResponse.Content);
            return createContactResponse;
        }

        public static ContactResponse.UpdateResponse.Response UpdateContact(WebFormEmail webForm, RestClient client, string contactId)
        {
            var contactUpdateRequest = new RestRequest("contacts/" + contactId + "?" + apiKey, Method.PATCH);
            contactUpdateRequest.AddHeader("accept", "application/json");
            contactUpdateRequest.AddHeader("content-type", "application/json");
            contactUpdateRequest.AddParameter("application/json", "{\"properties\":{\"pr_subscriber\":\"" + webForm.NewsletterSignup.ToString().ToLower() + "\"}}", ParameterType.RequestBody);
            IRestResponse updateContactJsonResponse = client.Execute(contactUpdateRequest);
            ContactResponse.UpdateResponse.Response updateContactResponse = JsonConvert.DeserializeObject<ContactResponse.UpdateResponse.Response>(updateContactJsonResponse.Content);
            return updateContactResponse;
        }

        public static TicketResponse.CreateResponse CreateTicket(WebFormEmail webForm, RestClient client, string ticketOwnerId)
        {
            //webForm.Message = SymbolDisplay.FormatLiteral(webForm.Message, false);
            string temp = webForm.Message;
            webForm.Message = SyntaxFactory.LiteralExpression(SyntaxKind.StringLiteralExpression, SyntaxFactory.Literal(webForm.Message)).ToString();
            //webForm.Message = HttpUtility. .HtmlEncode(webForm.Message);
            webForm.Message = webForm.Message.Trim('"');
            string ticketDescription = PrintProperties(webForm);
            webForm.Message = temp;
            var ticketRequest = new RestRequest("tickets?" + apiKey, Method.POST);
            ticketRequest.AddHeader("accept", "application/json");
            ticketRequest.AddHeader("content-type", "application/json");
            if (webForm.Reason == "Contact_Sales" || webForm.Reason == "TestLeadBuilder_Quote" || webForm.Reason == "TestLeadBuilder_Email")
            {
                ticketRequest.AddParameter("application/json", "{\"properties\":{\"hs_pipeline\":1809558,\"hs_pipeline_stage\":6354307,\"hs_ticket_priority\":\"MEDIUM\",\"hubspot_owner_id\":\"" + ticketOwnerId + "\",\"subject\":\"" + webForm.Reason.Split("_")[0] + " - " + webForm.Reason.Split("_")[1].ToUpper() + "\",\"content\":\"" + ticketDescription + "\",\"source_type\":\"WebForm\",\"hs_ticket_category\":\"" + webForm.Reason + "\"}}", ParameterType.RequestBody);
            }
            else
            {
                ticketRequest.AddParameter("application/json", "{\"properties\":{\"hs_pipeline\":0,\"hs_pipeline_stage\":1,\"hs_ticket_priority\":\"MEDIUM\",\"hubspot_owner_id\":\"" + ticketOwnerId + "\",\"subject\":\"" + webForm.Reason.Split("_")[0] + " - " + webForm.Reason.Split("_")[1].ToUpper() + "\",\"content\":\"" + ticketDescription + "\",\"source_type\":\"WebForm\",\"hs_ticket_category\":\"" + webForm.Reason + "\"}}", ParameterType.RequestBody);
            }
            IRestResponse ticketJsonResponse = client.Execute(ticketRequest);
            TicketResponse.CreateResponse ticketCreateResponse = JsonConvert.DeserializeObject<TicketResponse.CreateResponse>(ticketJsonResponse.Content);
            return ticketCreateResponse;
        }

        public static CompanyResponse.SearchResponse.Response SearchCompany(string companyName, RestClient client)
        {
            //1. check if contact exists
            var searchCompanyRequest = new RestRequest("companies/search?" + apiKey, Method.POST);
            searchCompanyRequest.AddHeader("Content-Type", "application/json");
            searchCompanyRequest.AddParameter("application/json", "{\"filterGroups\":[{\"filters\":[{\"value\":\"" + companyName + "\",\"propertyName\":\"name\",\"operator\":\"EQ\"}]}],\"sorts\":[\"name\"],\"properties\":[\"name\",\"id\"],\"limit\":1,\"after\":0}", ParameterType.RequestBody);
            IRestResponse searchCompanyJsonResponse = client.Execute(searchCompanyRequest);
            CompanyResponse.SearchResponse.Response searchCompanyResponse = JsonConvert.DeserializeObject<CompanyResponse.SearchResponse.Response>(searchCompanyJsonResponse.Content);
            return searchCompanyResponse;
        }

        public static bool CreateCompanyAssociation(string companyId, string contactId, RestClient client)
        {
            string associationUrl = "/companies/" + companyId + "/associations/contacts/" + contactId + "/company_to_contact?" + apiKey;
            var request = new RestRequest(associationUrl, Method.PUT);
            request.AddHeader("accept", "application/json");
            IRestResponse associationJsonReponse = client.Execute(request);
            return associationJsonReponse.IsSuccessful;
        }

        public static bool CreateCompanyTicketAssociation(string companyId, string ticketId, RestClient client)
        {
            string associationUrl = "/companies/" + companyId + "/associations/tickets/" + ticketId + "/company_to_ticket?" + apiKey;
            var request = new RestRequest(associationUrl, Method.PUT);
            request.AddHeader("accept", "application/json");
            IRestResponse associationJsonReponse = client.Execute(request);
            return associationJsonReponse.IsSuccessful;
        }

        public static bool CreateTicketAssociation(string ticketId, string contactId, RestClient client)
        {
            string associationUrl = "/tickets/" + ticketId + "/associations/contacts/" + contactId + "/ticket_to_contact?" + apiKey;
            var request = new RestRequest(associationUrl, Method.PUT);
            request.AddHeader("accept", "application/json");
            IRestResponse associationJsonReponse = client.Execute(request);
            return associationJsonReponse.IsSuccessful;
        }

        public static string PrintProperties(object obj)
        {
            string content = "";
            if (obj == null) return "";
            Type objType = obj.GetType();
            PropertyInfo[] properties = objType.GetProperties();

            foreach (PropertyInfo property in properties)
            {
                object propValue = property.GetValue(obj, null);
                if (property.Name.Equals("Receipt") && !string.IsNullOrWhiteSpace((string)propValue))
                {
                    content += property.Name + ": Image attached to e-mail or contact e-mail log in HubSpot.\\n";
                }
                else if (property.Name.Equals("Message"))
                {
                    content += property.Name + ": " + propValue + "\\n";
                    content = propValue + " \\n\\n ============ \\n\\n" + content + "\\n";
                }
                else
                {
                    content += property.Name + ": " + propValue + "\\n";
                }
            }
            return content;
        }
    }
}
