﻿using BlazorWebWasmCTE.Shared.Data;
using CTECMS.Data;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using static CTECMS.Data.CMSDbContext;

namespace WebEmailServer
{
    public class Program
    {
        public static List<TestLeadBuilder> TestLeadBuilders { get; set; }
        public static List<TestLeadConnector> TestLeadConnectors { get; set; }
        public static List<TestLeadWire> TestLeadWires { get; set; }
        static void Main(string[] args)
        {
            TestLeadBuilders = new List<TestLeadBuilder>();
            TestLeadConnectors = new List<TestLeadConnector>();
            TestLeadWires = new List<TestLeadWire>();

            using (CMSDbContext db = new CMSDbContext())
            {
                TestLeadBuilders = db.TestLeadBuilder.ToList();
                TestLeadConnectors = db.TestLeadConnector.ToList();
                TestLeadWires = db.TestLeadWire.ToList();
            }

            RestClient client = new RestClient("https://api.hubapi.com/crm/v3/objects/");
            client.Timeout = -1;

            //webForms (repair, contact, tlb)
            List<WebFormEmail> webForms = new List<WebFormEmail>();
            using (CMSDbContext db = new CMSDbContext())
            {
                webForms = db.WebForm.Where(x => x.Sent == false).ToList();
                foreach (var wF in webForms)
                {
                    LeadPerson leadPerson = new LeadPerson();
                    leadPerson = leadPerson.GetLeadPerson(wF);
                    try
                    {
                        HubspotIntegration(client, wF, leadPerson);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Failed to log " + wF.Reason + " in HubSpot. " + e.Message);
                    }
                }
                db.WebForm.UpdateRange(webForms);
                db.SaveChanges();
            }

            List<NewsletterSignup> newsletterSignups = new List<NewsletterSignup>();
            using (CMSDbContext db = new CMSDbContext())
            {
                newsletterSignups = db.NewsletterSignup.ToList();
                foreach (var ns in newsletterSignups)
                {
                    WebFormEmail webForm = new WebFormEmail { FirstName = ns.FirstName, LastName = ns.LastName, Email = ns.Email, NewsletterSignup = true };
                    HubspotIntegration(client, webForm, null, false);
                    db.NewsletterSignup.RemoveRange(ns);
                }
                db.SaveChanges();
            }
        }

        private static void HubspotIntegration(RestClient client, WebFormEmail webForm, LeadPerson leadPerson, bool createTicket = true)
        {
            string hubSpotContactId = null;
            //1. Check if contact exists to get contactID
            ContactResponse.SearchResponse.Response searchResponse = HubSpotService.SearchContact(webForm.Email, client);

            //2. Create contact, if it doesnt exists
            if (searchResponse.Results.Count() <= 0)
            {
                ContactResponse.CreateResponse.Response contactCreateResponse = HubSpotService.CreateContact(webForm, client);
                hubSpotContactId = contactCreateResponse.Id;
            }
            else
            {
                hubSpotContactId = searchResponse.Results.FirstOrDefault().Id;
            }

            //3. Update contact subscriber information
            ContactResponse.UpdateResponse.Response contactUpdatedResponse = HubSpotService.UpdateContact(webForm, client, hubSpotContactId);
            TicketResponse.CreateResponse ticketCreatedRepsonse = new TicketResponse.CreateResponse();
            if (createTicket)
            {
                //4. create ticket
                ticketCreatedRepsonse = HubSpotService.CreateTicket(webForm, client, leadPerson.HubSpotOwnerId);

                //5. associate ticket to contact
                bool associationCreated = HubSpotService.CreateTicketAssociation(ticketCreatedRepsonse.Id, hubSpotContactId, client);
            }

            //6. Check if company exists
            if (!string.IsNullOrWhiteSpace(webForm.Company))
            {
                CompanyResponse.SearchResponse.Response searchCompanyResponse = HubSpotService.SearchCompany(webForm.Company, client);
                if (searchCompanyResponse.Results.Count() > 0)
                {
                    //7. If company exists associate to contact
                    bool associationCreated = HubSpotService.CreateCompanyAssociation(searchCompanyResponse.Results.FirstOrDefault().Id, hubSpotContactId, client);

                    //8. Associate company to ticket
                    if (ticketCreatedRepsonse.Id != null)
                    {
                        bool companyTicketAssociationCreated = HubSpotService.CreateCompanyTicketAssociation(searchCompanyResponse.Results.FirstOrDefault().Id, ticketCreatedRepsonse.Id, client);
                    }
                }
            }

        }
    }
}
