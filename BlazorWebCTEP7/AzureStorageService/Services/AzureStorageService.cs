﻿using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StorageService.Constants;
using StorageService.Helpers;
using System.IO;
using Microsoft.Extensions.Logging;

namespace StorageService.Services
{
    public class AzureStorageService : IStorageService
    {
        private readonly BlobServiceClient _blobServiceClient;
        private readonly BlobContainerClient _webBlobContainerClient;
        private readonly ILogger<AzureStorageService> _logger;


        public AzureStorageService(ILogger<AzureStorageService> logger)
        {
            _blobServiceClient = new BlobServiceClient(AzureStorageConstants.ConnectionString);
            _webBlobContainerClient = _blobServiceClient.GetBlobContainerClient(BlobContainerClient.WebBlobContainerName);
            _logger = logger;
        }

        /// <summary>
        /// Creates blob in azure with path to local file
        /// </summary>
        /// <param name="sourceFilePath"></param>
        /// <param name="azureDestinationPath"></param>
        /// <returns></returns>
        public async Task<bool> UploadFileAsync(string sourceFilePath, string azureDestinationPath)
        {
            // Get a reference to a blob
            var blobClient = _webBlobContainerClient.GetBlobClient(azureDestinationPath + Path.GetFileName(sourceFilePath));
            try
            {

                BlobHttpHeaders blobHttpHeaders = new()
                {
                    //set MimeType
                    ContentType = sourceFilePath.GetMimeType()
                };

                // Upload file data
                await blobClient.UploadAsync(sourceFilePath, blobHttpHeaders);

                // Verify we uploaded some content
                BlobProperties properties = await blobClient.GetPropertiesAsync();

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("Unknown encountered on server. Message:'{0}' when writing an object", e.Message);
                return false;
            }
        }

        /// <summary>
        /// Creates blob in Azure with filestream
        /// </summary>
        /// <param name="sourceFileStream"></param>
        /// <param name="sourceFilename"></param>
        /// <param name="azureDestinationPath"></param>
        /// <returns></returns>
        public async Task<bool> UploadFileAsync(Stream sourceFileStream, string sourceFilename, string azureDestinationPath)
        {
            sourceFileStream.Position = 0;
            // Get a reference to a blob
            var blobClient = _webBlobContainerClient.GetBlobClient(azureDestinationPath + Path.GetFileName(sourceFilename));
            try
            {
                BlobHttpHeaders blobHttpHeaders = new()
                {
                    //set MimeType
                    ContentType = sourceFilename.GetMimeType()
                };

                // Upload file data
                await blobClient.UploadAsync(sourceFileStream, blobHttpHeaders);

                // Verify we uploaded some content
                BlobProperties properties = await blobClient.GetPropertiesAsync();

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("Unknown encountered on server. Message:'{0}' when writing an object", e.Message);
                return false;
            }

        }

        public async Task<bool> MoveFileAsync(string relativeSourceAzurePath, string relativeDestinationAzurePath, bool deleteSource = true)
        {
            try
            {
                var destinationBlobClient = _webBlobContainerClient.GetBlobClient(relativeDestinationAzurePath);
                var sourceBlobClient = _webBlobContainerClient.GetBlobClient(relativeSourceAzurePath);
                var sourceUri = sourceBlobClient.Uri;
                var copyOperation = await destinationBlobClient.StartCopyFromUriAsync(sourceUri);
                await copyOperation.WaitForCompletionAsync();

                if (deleteSource)
                {
                    await sourceBlobClient.DeleteAsync();
                }

                return true;
            }catch (Exception e)
            {
                Console.WriteLine("Unknown encountered on server. Message:'{0}' when moving an object", e.Message);
                return false;
            }
            
        }

        public async Task<bool> DeleteFileAsync(string azureFilePathUrl)
        {

            BlobClient bobClient = _webBlobContainerClient.GetBlobClient(azureFilePathUrl);
            try
            {
                if (await bobClient.ExistsAsync())
                {
                    await bobClient.DeleteAsync();
                    return true;
                }
                else
                {
                    // Log a warning message that the blob does not exist.
                    _logger.LogWarning("Blob at path {BlobPath} does not exist.", azureFilePathUrl);
                    return false;
                }
            }
            catch (Exception e)
            {
                _logger.LogError("Unknown encountered on server. Message:'{0}' when writing an object", e.Message);
                return false;
            }
        }

    }
}
