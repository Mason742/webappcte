﻿
namespace StorageService.Services
{
    public interface IStorageService
    {
        Task<bool> DeleteFileAsync(string azureFilePathUrl);
        Task<bool> MoveFileAsync(string relativeSourceAzurePath, string relativeDestinationAzurePath, bool deleteSource = true);
        Task<bool> UploadFileAsync(Stream sourceFileStream, string sourceFilename, string azureDestinationPath);
        Task<bool> UploadFileAsync(string sourceFilePath, string azureDestinationPath);
    }
}