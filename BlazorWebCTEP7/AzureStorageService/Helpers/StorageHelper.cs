﻿using Azure.Storage.Blobs;
using Microsoft.AspNetCore.StaticFiles;
using StorageService.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StorageService.Helpers
{
    internal static class StorageHelper
    {
        /// <summary>
        /// Returns MIME Type from file path string
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetMimeType(this string value)
        {
            var provider = new FileExtensionContentTypeProvider();
            if (!provider.TryGetContentType(value, out string contentType))
            {
                contentType = "application/octet-stream";
            }
            return contentType;
        }


    }
}
