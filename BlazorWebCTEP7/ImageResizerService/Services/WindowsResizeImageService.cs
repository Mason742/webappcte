﻿using ImageResizerService.Helpers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ImageResizerService.Services
{
    public class WindowsResizeImageService : IResizeImageService
    {
        public const string ProjectDirectory = @"C:\Projects\ImageResizerService\";
        public const string DefaultInDirectory = ProjectDirectory + @"in\";
        public const string DefaultOutDirectory = ProjectDirectory + @"out\";

        private readonly ImageCodecInfo jgpEncoder;
        private readonly System.Drawing.Imaging.Encoder myEncoder;
        private readonly EncoderParameters myEncoderParameters;
        private readonly EncoderParameters myLowEncoderParameters;

        public WindowsResizeImageService()
        {
            jgpEncoder = GetEncoder(ImageFormat.Jpeg);

            // Create an Encoder object based on the GUID
            // for the Quality parameter category
            myEncoder = System.Drawing.Imaging.Encoder.Quality;

            // Create an EncoderParameters object.
            // An EncoderParameters object has an array of EncoderParameter
            // objects. In this case, there is only one
            // EncoderParameter object in the array
            myEncoderParameters = new EncoderParameters(1);

            EncoderParameter myEncoderParameter = new(myEncoder, 100L);
            myEncoderParameters.Param[0] = myEncoderParameter;

            myLowEncoderParameters = new EncoderParameters(1);

            EncoderParameter myLowEncoderParameter = new(myEncoder, 80L);
            myLowEncoderParameters.Param[0] = myLowEncoderParameter;
        }

        /// <summary>
        /// Resize PNG and JPEG images to standard sizes: Thumbnail, Medium, Large and save them to the default directory or outputDirectory parameter
        /// </summary>
        /// <param name="filepath"></param>
        /// <param name="outputDirectory">Overrides default output directory with provided string parameter</param>
        public void ResizeImageToFile(string filepath, string outputDirectory = DefaultOutDirectory)
        {
            Bitmap sourceBitmap = new(filepath);

            var filename = Path.GetFileNameWithoutExtension(filepath);

            //Set transparent pixels to white color.
            sourceBitmap = Transparent2Color(sourceBitmap, Color.White);

            var bmpTmb = new Bitmap(sourceBitmap, GetSizeAdjustedToAspectRatio(sourceBitmap.Width, sourceBitmap.Height, 100, 100));

            var bmpMd = new Bitmap(sourceBitmap, GetSizeAdjustedToAspectRatio(sourceBitmap.Width, sourceBitmap.Height, 650, 650));

            var bmpLg = new Bitmap(sourceBitmap, GetSizeAdjustedToAspectRatio(sourceBitmap.Width, sourceBitmap.Height, 1280, 1280));

            bmpTmb.Save(outputDirectory + filename + "-tmb.jpg", jgpEncoder, myLowEncoderParameters);
            bmpMd.Save(outputDirectory + filename + "-md.jpg", jgpEncoder, myEncoderParameters);
            bmpLg.Save(outputDirectory + filename + "-lg.jpg", jgpEncoder, myEncoderParameters);

        }

        /// <summary>
        /// Resize PNG and JPEG images to standard sizes: Thumbnail, Medium, Large as MemoryStream
        /// </summary>
        /// <param name="memoryStream"></param>
        /// <returns>MemoryStream containing JPEG format</returns>
        public (MemoryStream Thumbnail, MemoryStream Medium, MemoryStream Large) ResizeImageToMemoryStream(Stream memoryStream)
        {
            Bitmap sourceBitmap = new(memoryStream);

            //Set transparent pixels to white color.
            sourceBitmap = Transparent2Color(sourceBitmap, Color.White);

            var bmpTmb = new Bitmap(sourceBitmap, GetSizeAdjustedToAspectRatio(sourceBitmap.Width, sourceBitmap.Height, 100, 100));

            var bmpMd = new Bitmap(sourceBitmap, GetSizeAdjustedToAspectRatio(sourceBitmap.Width, sourceBitmap.Height, 650, 650));

            var bmpLg = new Bitmap(sourceBitmap, GetSizeAdjustedToAspectRatio(sourceBitmap.Width, sourceBitmap.Height, 1280, 1280));

            var ms1 = new MemoryStream();
            var ms2 = new MemoryStream();
            var ms3 = new MemoryStream();

            //sourceBitmap.Save(outputDirectory + fname + "-jpg.jpg", jgpEncoder, myEncoderParameters);
            bmpTmb.Save(ms1, jgpEncoder, myLowEncoderParameters);
            bmpMd.Save(ms2, jgpEncoder, myEncoderParameters);
            bmpLg.Save(ms3, jgpEncoder, myEncoderParameters);

            return (Thumbnail: ms1, Medium: ms2, Large: ms3);
        }

        private static ImageCodecInfo GetEncoder(ImageFormat format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();
            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }
            return null;
        }

        private static Bitmap Transparent2Color(Bitmap bmp1, Color target)
        {
            Bitmap bmp2 = new(bmp1.Width, bmp1.Height);
            Rectangle rect = new(Point.Empty, bmp1.Size);
            using (Graphics G = Graphics.FromImage(bmp2))
            {
                G.Clear(target);
                G.DrawImageUnscaledAndClipped(bmp1, rect);
            }
            return bmp2;
        }

        public static Size GetSizeAdjustedToAspectRatio(int sourceWidth, int sourceHeight, int dWidth, int dHeight)
        {
            bool isLandscape = sourceWidth > sourceHeight;

            int newHeight;
            int newWidth;
            if (isLandscape)
            {
                newHeight = dWidth * sourceHeight / sourceWidth;
                newWidth = dWidth;
            }
            else
            {
                newWidth = dHeight * sourceWidth / sourceHeight;
                newHeight = dHeight;
            }

            return new Size(newWidth, newHeight);
        }
    }
}
