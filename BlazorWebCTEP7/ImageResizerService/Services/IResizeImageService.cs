﻿
namespace ImageResizerService.Services
{
    public interface IResizeImageService
    {
        void ResizeImageToFile(string filepath, string outputDirectory = "C:\\Projects\\ImageResizerService\\out\\");
        (MemoryStream Thumbnail, MemoryStream Medium, MemoryStream Large) ResizeImageToMemoryStream(Stream memoryStream);
    }
}