﻿// See https://aka.ms/new-console-template for more information
using ImageResizerService.Services;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;

Console.WriteLine("Hello, World!");

WindowsResizeImageService resizeImageService = new WindowsResizeImageService();
Stopwatch sw = new Stopwatch();
Dictionary<string, MemoryStream> memoryStreams = new Dictionary<string, MemoryStream>();
foreach (string filepath in Directory.GetFiles(WindowsResizeImageService.DefaultInDirectory))
{
    FileStream fs = new FileStream(filepath, FileMode.Open, FileAccess.Read);
    MemoryStream ms = new MemoryStream();
    fs.CopyTo(ms);
    memoryStreams.Add(filepath, ms);
}
sw.Restart();
foreach (var dictionaryItem in memoryStreams)
{
    var filepath = dictionaryItem.Key;

    (MemoryStream Thumbnail, MemoryStream Medium, MemoryStream Large) imageStreams = resizeImageService.ResizeImageToMemoryStream(dictionaryItem.Value);

    var filename = Path.GetFileNameWithoutExtension(filepath);
    var ext = Path.GetExtension(filepath);

    using (Image tmb = System.Drawing.Image.FromStream(imageStreams.Thumbnail))
    {
        tmb.Save(WindowsResizeImageService.DefaultOutDirectory + filename + "-tmb.jpg", ImageFormat.Jpeg);
    }
    using (Image md = System.Drawing.Image.FromStream(imageStreams.Medium))
    {
        md.Save(WindowsResizeImageService.DefaultOutDirectory + filename + "-md.jpg", ImageFormat.Jpeg);
    }
    using (Image lg = System.Drawing.Image.FromStream(imageStreams.Large))
    {
        lg.Save(WindowsResizeImageService.DefaultOutDirectory + filename + "-lg.jpg", ImageFormat.Jpeg);
    }
}

sw.Stop();
Console.WriteLine(sw.Elapsed);

sw.Restart();
foreach (string filepath in Directory.GetFiles(WindowsResizeImageService.DefaultInDirectory))
{
    resizeImageService.ResizeImageToFile(filepath);
}
sw.Stop();
Console.WriteLine(sw.Elapsed);

Console.ReadKey();