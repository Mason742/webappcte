﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageResizerService.Helpers
{
    public static class ImageExtensions
    {
        public static byte[] ToByteArray(this Image image, ImageFormat format)
        {
            using (MemoryStream ms = new())
            {
                if (OperatingSystem.IsWindows())
                {
                    image.Save(ms, format);
                    return ms.ToArray();
                }
            }
            throw new PlatformNotSupportedException();
        }
    }
}
