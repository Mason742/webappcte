﻿// See https://aka.ms/new-console-template for more information
using SolomonDB.Data.Context;
using SolomonDB.Data.Models;
using WebDB.Data.Context;
using Microsoft.EntityFrameworkCore;
using WebDB.Data.Models;
using Microsoft.Data.SqlClient.Server;
using System.Globalization;
using System.Text.RegularExpressions;

Console.WriteLine("Hello, World!");

//List<VrxErpInventory> inventory = new List<VrxErpInventory>();
//List<VrxOrderswebCte> orderswebCte = new List<VrxOrderswebCte>();
//List<VrxTrackingNumber> vrxtrackingNumbers = new List<VrxTrackingNumber>();
//using (var solomonDb = new SolomonDbContext())
//{
//    Console.WriteLine("Getting Solomon Data");
//    inventory = solomonDb.VrxErpInventories.ToList();
//    orderswebCte = solomonDb.VrxOrderswebCtes.ToList();
//    vrxtrackingNumbers = solomonDb.VrxTrackingNumbers.ToList();
//    Console.WriteLine($"SL Records, Inventory:{inventory.Count} PurchaseOrders:{orderswebCte.Count} TrackingNumbers:{vrxtrackingNumbers.Count}");

//}

//using (var ctedb = new DefaultDbContext())
//{
//    var created = DateTime.Now;

//    var erpInventories = inventory.Select(i => new ErpInventory
//    {
//        PartNumber = i.InventoryId.Trim(),
//        Price = (decimal)i.Msrp,
//        Quantity = (int)i.QuantityAvailable!,
//        MinimumOrderQuantity = (int)i.PacksPerCarton!,
//        LeadTime = (int)i.PoLeadTime,
//        SpecialHandling = i.SpecialHandling.Trim(),
//        Brand = i.Brand.Trim(),
//        Created = created
//    }).ToList();
//    Console.WriteLine($"erpInventories:{erpInventories.Count}");
//    if (erpInventories.Count > 0)
//    {
//        Console.WriteLine("Deleting Table");
//        await ctedb.Database.ExecuteSqlRawAsync("TRUNCATE TABLE ErpInventory");
//        ctedb.ErpInventories.AddRange(erpInventories);
//        Console.WriteLine("Saving changes");
//        await ctedb.SaveChangesAsync();
//        Console.WriteLine("Changes saved");
//    }

//    var orders = orderswebCte.Select(o => new PurchaseOrder
//    {
//        CustomerId = o.CustId.Trim(),
//        BillName = o.BillName.Trim(),
//        OrderNumber = o.OrdNbr.Trim(),
//        Status = o.Status.Trim(),
//        CustomerOrderNumber = o.CustOrdNbr.Trim(),
//        OrderDate = DateTime.ParseExact(o.OrdDate, "yyyy-MM-dd", CultureInfo.InvariantCulture),
//        TotalSale = (decimal)o.CuryTotOrd!,
//        Price = (decimal)o.CurySlsPrice!,
//        PartNumber = o.InvtId.Trim(),
//        AlternateModel = o.AlternateId.Trim(),
//        QuantityOrdered = int.Parse(o.QtyOrd),
//        QuantityShipped = int.Parse(o.QtyShip),
//        Eta = DateTime.ParseExact(o.Eta, "yyyy-MM-dd", CultureInfo.InvariantCulture),
//        RevisedEta = o.RevisedEtaDate,
//        ShippingMethod = o.ShipVia.Trim(),
//        Created = created
//    }).ToList();
//    Console.WriteLine($"PurchaseOrder:{orders.Count}");
//    if (orders.Count > 0)
//    {
//        Console.WriteLine("Deleting Table");
//        await ctedb.Database.ExecuteSqlRawAsync("TRUNCATE TABLE PurchaseOrder");
//        ctedb.PurchaseOrders.AddRange(orders);
//        Console.WriteLine("Saving changes");
//        await ctedb.SaveChangesAsync();
//        Console.WriteLine("Changes saved");
//    }

//    var trackingNumbers = vrxtrackingNumbers.Select(tn => new OrderTrackingNumber
//    {
//        PartNumber = tn.InvtId.Trim().ToUpper(),
//        OrderNumber = tn.OrdNbr.Trim(),
//        TrackingNumber = tn.TrackingNbr.Trim(),
//        Created = created
//    }).ToList();
//    Console.WriteLine($"OrderTrackingNumber:{trackingNumbers.Count}");
//    if (trackingNumbers.Count > 0)
//    {
//        Console.WriteLine("Deleting Table");
//        await ctedb.Database.ExecuteSqlRawAsync("TRUNCATE TABLE OrderTrackingNumber");
//        ctedb.OrderTrackingNumbers.AddRange(trackingNumbers);
//        Console.WriteLine("Saving changes");
//        await ctedb.SaveChangesAsync();
//        Console.WriteLine("Changes saved");
//    }


//    var products = ctedb.Products.ToList();
//    Console.WriteLine($"Iterating through {products.Count} products and updating Product table SL information");
//    foreach (var product in products)
//    {
//        if (inventory.Any(x => x.InventoryId.Trim() == product.PartNumber))
//        {
//            var erp = inventory.FirstOrDefault(x => x.InventoryId.Trim() == product.PartNumber);
//            product.Warranty = erp!.WarrantyId.Trim();
//            if (string.IsNullOrWhiteSpace(product.Description))
//            {
//                product.Description = erp.Description.Trim();
//            }
//            if (erp.Msrp != 0)
//            {
//                product.Msrp = (decimal)erp.Msrp;
//            }
//            product.TransactionStatus = erp.TransactionStatus.Trim();
//            product.SpecialHandling = erp.SpecialHandling.Trim();
//            product.UpcCode = erp.Upccode.Trim();
//            product.PoLeadTime = erp.PoLeadTime.ToString();
//            product.ManufacturerLeadTime = erp.ManufacturerLeadTime.ToString();
//            product.HarmonizedCode = erp.HarmonizedCode.Trim();
//            product.RepairPrice = (decimal)erp.RepairPrice;
//            product.EvaluationCharge = (decimal)erp.EvaluationCharge;
//            product.CalibrationPrice = (decimal)erp.CalibrationCharge;
//            product.ShippingCost = erp.ShippingCost.ToString();
//            product.MasterPartNumber = erp.MasterPartNumber.Trim();
//            product.PacksPerCarton = erp.PacksPerCarton.ToString();
//            product.PackSize = erp.PackSize.ToString();
//            product.PackUom = erp.PackUom.Trim();
//            product.CountryOfOrigin=erp.CountryOfOrigin.Trim();
//            product.Modified = created;
//            product.Discontinued = erp.Discontinued == 1 ? true : false;
//            if (product.Discontinued == true)
//            {
//                Console.WriteLine($"{product.PartNumber} -> discontinued");
//            }
//        }

//    }
//    Console.WriteLine("Saving changes");
//    await ctedb.SaveChangesAsync();
//    Console.WriteLine("Changes saved");

//    var series = ctedb.Series.Include(x => x.Products).ToList();
//    foreach (var s in series)
//    {
//        if (s.Products.All(x => x.Discontinued == true))
//        {
//            s.Discontinued = true;
//            Console.WriteLine($"{s.Name} -> completely discontinued");
//        }
//    }
//    Console.WriteLine("Saving changes");
//    await ctedb.SaveChangesAsync();
//    Console.WriteLine("Changes saved");
//}

//Console.WriteLine("Press Y to update DateFirstReceived in Inventory Items screen, or N to exit");
//string input = Console.ReadLine();

////var logFilePath = "log.txt";
////var logFile = File.CreateText(logFilePath);
////Console.SetOut(logFile);

//if (input.ToUpper() == "Y")
//{
//    // code to update DateFirstReceived in Inventory Items screen
//    await Update2();
//    Console.WriteLine("DateFirstReceived updated successfully");
//}
//else if (input.ToUpper() == "N")
//{
//    // code to close the application
//    Console.WriteLine("Application closed");
//    Environment.Exit(0); // this will exit the application with a success code
//}
//else
//{
//    // invalid input, show error message and close the application
//    Console.WriteLine("Invalid input, please try again");
//    Environment.Exit(1); // this will exit the application with an error code
//}
//async Task<Task> Update2()
//{
//    var webProducts = new List<Product>();
//    using (var webDb = new DefaultDbContext())
//    {
//        webProducts = await webDb.Products.ToListAsync();
//    }
//    using (var ctedb = new SolomonDbContext())
//    {
//        var inventoryData = await ctedb.Inventories.ToListAsync();
//        var masonData = await ctedb.MasonIntroductoryDates.ToListAsync();

//        foreach (var datum in masonData)
//        {
//            Console.Write($"{datum.InvtId.Trim()}: {datum.IntroductoryDate}");

//            if (datum.IntroductoryDate >= new DateTime(1900, 2, 1))
//            {
//                Console.WriteLine(" (has date)");
//                continue;
//            }

//            var inventoryItem = inventoryData.FirstOrDefault(i => i.InvtId.Trim().ToUpper() == datum.InvtId.Trim().ToUpper());
//            if (inventoryItem == null)
//            {
//                Console.WriteLine(" (inventory item not found)");
//                continue;
//            }

//            DateTime? introductoryDate = null;

//            if (datum.FirstReceivedDate != null)
//            {
//                Console.Write($" (used PO date: {datum.FirstReceivedDate})");
//                introductoryDate = datum.FirstReceivedDate.Value;
//            }
//            else if (datum.KitCreatedDate != null)
//            {
//                Console.Write($" (used Kit date: {datum.KitCreatedDate})");
//                introductoryDate = datum.KitCreatedDate.Value;
//            }
//            else
//            {
//                Console.Write($" (used created date: {datum.InventoryCreatedDate})");
//                introductoryDate = datum.InventoryCreatedDate;
//            }

//            var webProduct = webProducts.FirstOrDefault(i => i.PartNumber.Trim().ToUpper() == datum.InvtId.Trim().ToUpper());
//            if (webProduct != null)
//            {
//                Console.WriteLine(" (webProduct not found)");
//                introductoryDate = webProduct.Created;
//            }

//            if (introductoryDate != null)
//            {
//                inventoryItem.User7 = introductoryDate.Value;
//                await ctedb.SaveChangesAsync();
//                Console.Write(" (user7 updated)");
//            }

//            Console.WriteLine();
//        }
//    }
//    return Task.CompletedTask;
//}


using(var solomonDb = new SolomonDbContext())
{
    Regex regex = new Regex(@"CT[0-9]+-[0-9]-[0-9]+");
    List<string> possibleWires = new List<string>();

    var wires = solomonDb.VrxItemMasters
    .Where(x => x.Supplr1 == "C00058" && x.TranStatusCode == "AC" && x.MaterialType != "OEMP" && x.ProdLineId == "WIRE")
    .AsEnumerable()
    .Select(x => x.InvtId.Split('-').First())
    .Distinct()
    .ToList();

    possibleWires = solomonDb.VrxItemMasters.Where(x=>x.Supplr1=="C00058" && x.TranStatusCode == "AC" && x.MaterialType != "OEMP" && x.ProdLineId!="WIRE" ).ToList().SelectMany(input=> regex.Matches(input.InvtId)
        .Cast<Match>()
        .Select(m=>m.Value)
    ).ToList();
    possibleWires = possibleWires.Prepend("CT2002-0").ToList();
    possibleWires.RemoveAll(x => !wires.Any(y => x.StartsWith(y)));

    //prints out wires that should be labeled as wires
    Console.WriteLine(string.Join(", ", possibleWires)); // Outputs "CT2002B-10-1, CT123-45-6, CT456-78-9"
}