﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DistributorStockUpload
{
    internal class Constants
    {
        internal const string DownloadPath = @"C:\Projects\DistributorProductStockUpload\in\";
        internal const string UrlDownloadPath = DownloadPath + @"URL\";
        internal const string FtpDownloadPath = DownloadPath + @"FTP\";
        internal const string ApiDownloadPath = DownloadPath + @"API\";
        internal const string OutputPath = @"C:\Projects\DistributorProductStockUpload\out\";
        internal const string ArrowApiBaseUri = "https://api.arrow.com/itemservice/v4/en/search/list?req=";
        internal const string MouserApiBaseUri = "https://api.mouser.com/api/v2/search/partnumberandmanufacturer?apiKey=40a12c52-be1d-47b9-8970-0a787c4a170e";

    }
    
}
