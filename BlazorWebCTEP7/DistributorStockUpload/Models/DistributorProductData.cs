﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DistributorStockUpload.Models
{
    internal class DistributorProductData
    {
        public string Model { get; set; }
        public int Quantity { get; set; }
        public string Link { get; set; }
        public string Brand { get; set; }
        public string DistributorName { get; set; }
    }
}
