﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DistributorStockUpload.Models
{
    internal class FtpCredentials
    {
        internal string FtpUrl { get; set; }
        internal string RemoteFilename { get; set; }
        internal string DestinationFilename { get; set; }
        internal string Username { get; set; }
        internal string Password { get; set; }
    }
}
