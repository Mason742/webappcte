﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace DistributorStockUpload.Models
{
    internal class ArrowResponse
    {
        // Root myDeserializedClass = JsonSerializer.Deserialize<Root>(myJsonResponse);
        public class Root
        {
            [JsonPropertyName("itemserviceresult")]
            public Itemserviceresult Itemserviceresult { get; set; }
        }

        public class ServiceMetaData
        {
            [JsonPropertyName("version")]
            public string Version { get; set; }
        }

        public class Response
        {
            [JsonPropertyName("returnCode")]
            public string ReturnCode { get; set; }

            [JsonPropertyName("returnMsg")]
            public string ReturnMsg { get; set; }

            [JsonPropertyName("success")]
            public bool Success { get; set; }
        }

        public class ResponseSequence
        {
            [JsonPropertyName("transactionTime")]
            public string TransactionTime { get; set; }

            [JsonPropertyName("queryTime")]
            public string QueryTime { get; set; }

            [JsonPropertyName("dbTime")]
            public string DbTime { get; set; }

            [JsonPropertyName("totalItems")]
            public int TotalItems { get; set; }

            [JsonPropertyName("resources")]
            public List<object> Resources { get; set; }

            [JsonPropertyName("qq")]
            public int Qq { get; set; }
        }

        public class TransactionArea
        {
            [JsonPropertyName("response")]
            public Response Response { get; set; }

            [JsonPropertyName("responseSequence")]
            public ResponseSequence ResponseSequence { get; set; }
        }

        public class Manufacturer
        {
            [JsonPropertyName("mfrCd")]
            public string MfrCd { get; set; }

            [JsonPropertyName("mfrName")]
            public string MfrName { get; set; }
        }

        public class Resource
        {
            [JsonPropertyName("type")]
            public string Type { get; set; }

            [JsonPropertyName("uri")]
            public string Uri { get; set; }
        }

        public class Compliance
        {
            [JsonPropertyName("displayLabel")]
            public string DisplayLabel { get; set; }

            [JsonPropertyName("displayValue")]
            public string DisplayValue { get; set; }
        }

        public class EnvData
        {
            [JsonPropertyName("compliance")]
            public List<Compliance> Compliance { get; set; }
        }

        public class Availability
        {
            [JsonPropertyName("fohQty")]
            public int FohQty { get; set; }

            [JsonPropertyName("availabilityCd")]
            public string AvailabilityCd { get; set; }

            [JsonPropertyName("availabilityMessage")]
            public string AvailabilityMessage { get; set; }

            [JsonPropertyName("pipeline")]
            public List<object> Pipeline { get; set; }
        }

        public class ResaleList
        {
            [JsonPropertyName("displayPrice")]
            public string DisplayPrice { get; set; }

            [JsonPropertyName("price")]
            public double Price { get; set; }

            [JsonPropertyName("minQty")]
            public int MinQty { get; set; }

            [JsonPropertyName("maxQty")]
            public int MaxQty { get; set; }
        }

        public class Prices
        {
            [JsonPropertyName("resaleList")]
            public List<ResaleList> ResaleList { get; set; }
        }

        public class SourcePart
        {
            [JsonPropertyName("packSize")]
            public int PackSize { get; set; }

            [JsonPropertyName("minimumOrderQuantity")]
            public int MinimumOrderQuantity { get; set; }

            [JsonPropertyName("sourcePartId")]
            public string SourcePartId { get; set; }

            [JsonPropertyName("Availability")]
            public List<Availability> Availability { get; set; }

            [JsonPropertyName("customerSpecificPricing")]
            public List<object> CustomerSpecificPricing { get; set; }

            [JsonPropertyName("customerSpecificInventory")]
            public List<object> CustomerSpecificInventory { get; set; }

            [JsonPropertyName("dateCode")]
            public string DateCode { get; set; }

            [JsonPropertyName("resources")]
            public List<Resource> Resources { get; set; }

            [JsonPropertyName("inStock")]
            public bool InStock { get; set; }

            [JsonPropertyName("mfrLeadTime")]
            public int MfrLeadTime { get; set; }

            [JsonPropertyName("tariffFlag")]
            public bool TariffFlag { get; set; }

            [JsonPropertyName("shipsFrom")]
            public string ShipsFrom { get; set; }

            [JsonPropertyName("shipsIn")]
            public string ShipsIn { get; set; }

            [JsonPropertyName("arrowLeadTime")]
            public string ArrowLeadTime { get; set; }

            [JsonPropertyName("isNcnr")]
            public bool IsNcnr { get; set; }

            [JsonPropertyName("isNpi")]
            public bool IsNpi { get; set; }

            [JsonPropertyName("productCode")]
            public string ProductCode { get; set; }

            [JsonPropertyName("eccnCode")]
            public string EccnCode { get; set; }

            [JsonPropertyName("containerType")]
            public string ContainerType { get; set; }

            [JsonPropertyName("Prices")]
            public Prices Prices { get; set; }

            [JsonPropertyName("countryOfOrigin")]
            public string CountryOfOrigin { get; set; }
        }

        public class Source
        {
            [JsonPropertyName("currency")]
            public string Currency { get; set; }

            [JsonPropertyName("sourceCd")]
            public string SourceCd { get; set; }

            [JsonPropertyName("displayName")]
            public string DisplayName { get; set; }

            [JsonPropertyName("sourceParts")]
            public List<SourcePart> SourceParts { get; set; }
        }

        public class WebSite
        {
            [JsonPropertyName("code")]
            public string Code { get; set; }

            [JsonPropertyName("name")]
            public string Name { get; set; }

            [JsonPropertyName("sources")]
            public List<Source> Sources { get; set; }
        }

        public class InvOrg
        {
            [JsonPropertyName("webSites")]
            public List<WebSite> WebSites { get; set; }
        }

        public class PartList
        {
            [JsonPropertyName("itemId")]
            public int ItemId { get; set; }

            [JsonPropertyName("partNum")]
            public string PartNum { get; set; }

            [JsonPropertyName("manufacturer")]
            public Manufacturer Manufacturer { get; set; }

            [JsonPropertyName("desc")]
            public string Desc { get; set; }

            [JsonPropertyName("packageType")]
            public string PackageType { get; set; }

            [JsonPropertyName("resources")]
            public List<Resource> Resources { get; set; }

            [JsonPropertyName("EnvData")]
            public EnvData EnvData { get; set; }

            [JsonPropertyName("InvOrg")]
            public InvOrg InvOrg { get; set; }

            [JsonPropertyName("hasDatasheet")]
            public bool HasDatasheet { get; set; }

            [JsonPropertyName("categoryName")]
            public string CategoryName { get; set; }

            [JsonPropertyName("status")]
            public string Status { get; set; }
        }

        public class ResultList
        {
            [JsonPropertyName("resources")]
            public List<object> Resources { get; set; }

            [JsonPropertyName("PartList")]
            public List<PartList> PartList { get; set; }

            [JsonPropertyName("requestedPartNum")]
            public string RequestedPartNum { get; set; }

            [JsonPropertyName("numberFound")]
            public int NumberFound { get; set; }
        }

        public class Datum
        {
            [JsonPropertyName("partsRequested")]
            public int PartsRequested { get; set; }

            [JsonPropertyName("partsFound")]
            public int PartsFound { get; set; }

            [JsonPropertyName("partsNotFound")]
            public int PartsNotFound { get; set; }

            [JsonPropertyName("partsError")]
            public int PartsError { get; set; }

            [JsonPropertyName("resultList")]
            public List<ResultList> ResultList { get; set; }
        }

        public class Itemserviceresult
        {
            [JsonPropertyName("serviceMetaData")]
            public List<ServiceMetaData> ServiceMetaData { get; set; }

            [JsonPropertyName("transactionArea")]
            public List<TransactionArea> TransactionArea { get; set; }

            [JsonPropertyName("data")]
            public List<Datum> Data { get; set; }
        }


    }

}
