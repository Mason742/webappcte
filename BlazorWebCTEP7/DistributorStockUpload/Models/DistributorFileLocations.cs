﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DistributorStockUpload.Models
{
    internal static class DistributorFileLocations
    {

        internal static class UrlLocation
        {
            internal const string TequipmentCT = "https://www.iwhtech.com/InSynch/InventoryScript/ProductFeedCalTest.csv";
            internal const string TequipmentGS = "https://www.iwhtech.com/InSynch/InventoryScript/ProductFeedGlobalSpecialties.csv";
            internal const string SeframCT = "https://www.dropbox.com/s/tij1qhemekj2g2f/product_stock_qty_cte_sefram.csv?dl=1";
            internal const string TestEquipmentGS = "https://www.testequipmentdepot.com/global-specialties/inv/89734_1dfY/fotronic_gs.csv";

        }
        internal static class FtpLocation
        {

            internal static FtpCredentials DigikeyCT()
            {
                return new FtpCredentials
                {
                    FtpUrl = "ftp://ftp2.digikey.com/",
                    RemoteFilename = "BK_Precision_CAL_TEST.csv",
                    DestinationFilename = "DigikeyCT.csv",
                    Username = "Global_Specialties",
                    Password = "EWg073U"
                };
            }
            internal static FtpCredentials DigikeyGS()
            {
                return new FtpCredentials
                {
                    FtpUrl = "ftp://ftp2.digikey.com/",
                    RemoteFilename = "BK_Precision_GLOBAL_SPECIALTIES.csv",
                    DestinationFilename = "DigikeyGS.csv",
                    Username = "Global_Specialties",
                    Password = "EWg073U"
                };
            }
            internal static FtpCredentials NewarkCT()
            {
                return new FtpCredentials
                {
                    FtpUrl = "ftp://52.151.42.139/",
                    RemoteFilename = "Cal_Test.txt",
                    DestinationFilename = "NewarkCT.csv",
                    Username = "newark_ftp_user",
                    Password = "^Yz&2Z`az2=b+teM"
                };
            }
            internal static FtpCredentials NewarkGS()
            {
                return new FtpCredentials
                {
                    FtpUrl = "ftp://ftp2.digikey.com/",
                    RemoteFilename = "gloabl_specialties.txt",
                    DestinationFilename = "NewarkGS.csv",
                    Username = "newark_ftp_user",
                    Password = "^Yz&2Z`az2=b+teM"
                };
            }

        }
        internal static class ApiLocation
        {
            internal const string ArrowCTGS = "http://api.arrow.com/";
            internal const string MouserCTGS = "http://api.mouser.com/";
        }


    }
}
