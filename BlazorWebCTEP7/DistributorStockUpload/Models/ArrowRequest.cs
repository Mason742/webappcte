﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace DistributorStockUpload.Models
{
    internal class ArrowRequest
    {
        public ArrowRequest()
        {
            this.RequestResult = new Request();
        }

        [JsonPropertyName("request")]
        public Request RequestResult { get; set; }
        public class Part
        {
            [JsonPropertyName("partNum")]
            public string PartNum { get; set; }

            [JsonPropertyName("mfr")]
            public string Mfr { get; set; }
        }

        public class Request
        {
            [JsonPropertyName("login")]
            public string Login { get; set; } = "caltestelectronics";

            [JsonPropertyName("apikey")]
            public string Apikey { get; set; } = "f9921b1c8163c87cf1cfb7dee0704d8573da6aeb8d01d9fbae1ac55a75974bfc";

            [JsonPropertyName("remoteIp")]
            public string RemoteIp { get; set; } = "104.24.12.69";

            [JsonPropertyName("useExact")]
            public bool UseExact { get; set; } = true;

            [JsonPropertyName("parts")]
            public List<Part> Parts { get; set; } = new List<Part>();
        }
    }
}
