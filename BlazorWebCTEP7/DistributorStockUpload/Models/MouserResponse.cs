﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace DistributorStockUpload.Models
{
    internal class MouserResponse
    {
        // Root myDeserializedClass = JsonSerializer.Deserialize<Root>(myJsonResponse);
        // Root myDeserializedClass = JsonSerializer.Deserialize<Root>(myJsonResponse);
        public class Error
        {
            [JsonPropertyName("Id")]
            public int Id { get; set; }

            [JsonPropertyName("Code")]
            public string Code { get; set; }

            [JsonPropertyName("Message")]
            public string Message { get; set; }

            [JsonPropertyName("ResourceKey")]
            public string ResourceKey { get; set; }

            [JsonPropertyName("ResourceFormatString")]
            public string ResourceFormatString { get; set; }

            [JsonPropertyName("ResourceFormatString2")]
            public string ResourceFormatString2 { get; set; }

            [JsonPropertyName("PropertyName")]
            public string PropertyName { get; set; }
        }

        public class ProductAttribute
        {
            [JsonPropertyName("AttributeName")]
            public string AttributeName { get; set; }

            [JsonPropertyName("AttributeValue")]
            public string AttributeValue { get; set; }
        }

        public class PriceBreak
        {
            [JsonPropertyName("Quantity")]
            public int Quantity { get; set; }

            [JsonPropertyName("Price")]
            public string Price { get; set; }

            [JsonPropertyName("Currency")]
            public string Currency { get; set; }
        }

        public class AlternatePackaging
        {
            [JsonPropertyName("APMfrPN")]
            public string APMfrPN { get; set; }
        }

        public class UnitWeightKg
        {
            [JsonPropertyName("UnitWeight")]
            public decimal UnitWeight { get; set; }
        }

        public class StandardCost
        {
            [JsonPropertyName("Standardcost")]
            public int Standardcost { get; set; }
        }

        public class ProductCompliance
        {
            [JsonPropertyName("ComplianceName")]
            public string ComplianceName { get; set; }

            [JsonPropertyName("ComplianceValue")]
            public string ComplianceValue { get; set; }
        }

        public class Part
        {
            [JsonPropertyName("Availability")]
            public string Availability { get; set; } = "None";

            [JsonPropertyName("DataSheetUrl")]
            public string DataSheetUrl { get; set; }

            [JsonPropertyName("Description")]
            public string Description { get; set; }

            [JsonPropertyName("FactoryStock")]
            public string FactoryStock { get; set; }

            [JsonPropertyName("ImagePath")]
            public string ImagePath { get; set; }

            [JsonPropertyName("Category")]
            public string Category { get; set; }

            [JsonPropertyName("LeadTime")]
            public string LeadTime { get; set; }

            [JsonPropertyName("LifecycleStatus")]
            public string LifecycleStatus { get; set; }

            [JsonPropertyName("Manufacturer")]
            public string Manufacturer { get; set; }

            [JsonPropertyName("ManufacturerPartNumber")]
            public string ManufacturerPartNumber { get; set; }

            [JsonPropertyName("Min")]
            public string Min { get; set; }

            [JsonPropertyName("Mult")]
            public string Mult { get; set; }

            [JsonPropertyName("MouserPartNumber")]
            public string MouserPartNumber { get; set; }

            [JsonPropertyName("ProductAttributes")]
            public List<ProductAttribute> ProductAttributes { get; set; }

            [JsonPropertyName("PriceBreaks")]
            public List<PriceBreak> PriceBreaks { get; set; }

            [JsonPropertyName("AlternatePackagings")]
            public List<AlternatePackaging> AlternatePackagings { get; set; }

            [JsonPropertyName("ProductDetailUrl")]
            public string ProductDetailUrl { get; set; }

            [JsonPropertyName("Reeling")]
            public bool Reeling { get; set; }

            [JsonPropertyName("ROHSStatus")]
            public string ROHSStatus { get; set; }

            [JsonPropertyName("SuggestedReplacement")]
            public string SuggestedReplacement { get; set; }

            [JsonPropertyName("MultiSimBlue")]
            public int MultiSimBlue { get; set; }

            [JsonPropertyName("UnitWeightKg")]
            public UnitWeightKg UnitWeightKg { get; set; }

            [JsonPropertyName("StandardCost")]
            public StandardCost StandardCost { get; set; }

            [JsonPropertyName("IsDiscontinued")]
            public string IsDiscontinued { get; set; }

            [JsonPropertyName("RTM")]
            public string RTM { get; set; }

            [JsonPropertyName("MouserProductCategory")]
            public string MouserProductCategory { get; set; }

            [JsonPropertyName("IPCCode")]
            public string IPCCode { get; set; }

            [JsonPropertyName("SField")]
            public string SField { get; set; }

            [JsonPropertyName("VNum")]
            public string VNum { get; set; }

            [JsonPropertyName("ActualMfrName")]
            public string ActualMfrName { get; set; }

            [JsonPropertyName("AvailableOnOrder")]
            public string AvailableOnOrder { get; set; }

            [JsonPropertyName("InfoMessages")]
            public List<string> InfoMessages { get; set; }

            [JsonPropertyName("SalesMaximumOrderQty")]
            public string SalesMaximumOrderQty { get; set; }

            [JsonPropertyName("RestrictionMessage")]
            public string RestrictionMessage { get; set; }

            [JsonPropertyName("PID")]
            public string PID { get; set; }

            [JsonPropertyName("ProductCompliance")]
            public List<ProductCompliance> ProductCompliance { get; set; }
        }

        public class SearchResults
        {
            [JsonPropertyName("NumberOfResult")]
            public int NumberOfResult { get; set; }

            [JsonPropertyName("Parts")]
            public List<Part> Parts { get; set; } = new List<Part>();
        }

        [JsonPropertyName("Errors")]
        public List<Error> Errors { get; set; }

        [JsonPropertyName("SearchResults")]
        public SearchResults SearchResult { get; set; } = new SearchResults();



    }
}
