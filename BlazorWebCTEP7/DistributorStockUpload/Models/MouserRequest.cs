﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace DistributorStockUpload.Models
{
    internal class MouserRequest
    {
        // Root myDeserializedClass = JsonSerializer.Deserialize<Root>(myJsonResponse);
        public MouserRequest()
        {
            this.SearchByPartMfrNameRequestResult = new SearchByPartMfrNameRequest();
        }
        [JsonPropertyName("SearchByPartMfrNameRequest")]
        public SearchByPartMfrNameRequest SearchByPartMfrNameRequestResult { get; set; }

        [JsonIgnore]
        public string Brand { get; set; }

        public class SearchByPartMfrNameRequest
        {
            [JsonPropertyName("manufacturerName")]
            public string ManufacturerName { get; set; }

            [JsonPropertyName("mouserPartNumber")]
            public string MouserPartNumber { get; set; }

            [JsonPropertyName("partSearchOptions")]
            public string PartSearchOptions { get; set; } = "Exact";
        }

    }
}
