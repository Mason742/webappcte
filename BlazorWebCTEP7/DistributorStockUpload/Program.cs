﻿// See https://aka.ms/new-console-template for more information
using DistributorStockUpload;
using DistributorStockUpload.Models;
using System.Linq;
using System.Reflection;
using DistributorStockUpload.Helpers;
using System.Net;
using WebDB.Data.Context;
using WebDB.Data.Models;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Net.Http.Json;
using System.Text;
using System.Text.RegularExpressions;

Console.WriteLine("Hello, World!");

ProjectInitialSetup();

HttpClient httpClient = new();
List<Task> tasks = new();


var urlTask = Task.Run(async () =>
{
    try
    {
        Console.WriteLine("Downloading URL Files... : ");
        await DownloadUrlFiles(httpClient);
        Console.WriteLine("Uploading URL Files... : ");
        await UploadFiles(Constants.UrlDownloadPath);
        Console.WriteLine("URL Files Done.");
    }
    catch (Exception ex)
    {
        Console.WriteLine("Failed to do urlTask: " + ex.Message);
    }
});
tasks.Add(urlTask);



var ftpTask = Task.Run(async () =>
{
    try
    {

        Console.WriteLine("Downloading FTP Files... : ");
        await DownloadFtpFiles(httpClient);
        Console.WriteLine("Uploading FTP Files... : ");
        await UploadFiles(Constants.FtpDownloadPath);
        Console.WriteLine("FTP Files Done.");
    }
    catch (Exception ex)
    {
        Console.WriteLine("Failed to do ftpTask: " + ex.Message);
    }
});
tasks.Add(ftpTask);




var apiTask = Task.Run(async () =>
{
    try
    {
        Console.WriteLine("Downloading API Files... : ");
        var data = await DownloadApiFiles(httpClient);
        Console.WriteLine("Writing API Data to Files");
        await CreateApiFile(data);
        Console.WriteLine("Downloading API Files... : ");
        await UploadFiles(Constants.ApiDownloadPath);
        Console.WriteLine("API Files Done.");
    }
    catch (Exception ex)
    {
        Console.WriteLine("Failed to do apiTask: " + ex.Message);
    }
});
tasks.Add(apiTask);

await Task.WhenAll(tasks);

Console.WriteLine("Done.");


Task CreateApiFile(List<List<DistributorProductData>> listOfData)
{
    foreach (var data in listOfData)
    {
        string outputFile = "model,quantity,link\n";
        foreach (var d in data)
        {
            outputFile += $"{d.Model},{d.Quantity},{d.Link}\n";
        }
        if (data.Count <= 0)
        {
            continue;
        }
        string fileName = data.FirstOrDefault().DistributorName;
        File.WriteAllText(Constants.ApiDownloadPath + fileName, outputFile);
    }
    return Task.CompletedTask;
}



void ProjectInitialSetup()
{
    Directory.CreateDirectory(Constants.DownloadPath);
    Directory.CreateDirectory(Constants.OutputPath);
    Directory.CreateDirectory(Constants.UrlDownloadPath);
    Directory.CreateDirectory(Constants.FtpDownloadPath);
    Directory.CreateDirectory(Constants.ApiDownloadPath);
}


Task UploadFiles(string directoryPath)
{
    List<DistributorStock> distributorStocks = new();
    HashSet<string> distributorCustomerIds = new();
    foreach (var file in Directory.GetFiles(directoryPath))
    {
        var dataFromFile = File.ReadAllLines(file).Skip(1).Select(ds => DistributorStock.FromCsv(ds)).ToList();
        var distributorCustomerId = GetDistributorId(Path.GetFileNameWithoutExtension(file));
        dataFromFile.ForEach(ds => ds.CustomerId = distributorCustomerId);
        distributorCustomerIds.Add(distributorCustomerId);
        distributorStocks.AddRange(dataFromFile);
    }
    using (var context = new DefaultDbContext())
    {
        foreach (var dci in distributorCustomerIds)
        {
            context.DistributorStocks.RemoveRange(context.DistributorStocks.Where(x => x.CustomerId == dci));
        }
        context.DistributorStocks.AddRange(distributorStocks);
        context.SaveChanges();
    }
    return Task.CompletedTask;
}

async Task DownloadUrlFiles(HttpClient httpClient)
{
    var response1 = await httpClient.GetAsync(DistributorFileLocations.UrlLocation.TequipmentCT);
    using (var fs = new FileStream(Constants.UrlDownloadPath + nameof(DistributorFileLocations.UrlLocation.TequipmentCT) + ".csv", FileMode.Create))
    {
        await response1.Content.CopyToAsync(fs);
    }

    var response2 = await httpClient.GetAsync(DistributorFileLocations.UrlLocation.TequipmentGS);
    using (var fs = new FileStream(Constants.UrlDownloadPath + nameof(DistributorFileLocations.UrlLocation.TequipmentGS) + ".csv", FileMode.Create))
    {
        await response2.Content.CopyToAsync(fs);
    }

    var response3 = await httpClient.GetAsync(DistributorFileLocations.UrlLocation.TestEquipmentGS);
    using (var fs = new FileStream(Constants.UrlDownloadPath + nameof(DistributorFileLocations.UrlLocation.TestEquipmentGS) + ".csv", FileMode.Create))
    {
        await response3.Content.CopyToAsync(fs);
    }
}
async Task DownloadFtpFiles(HttpClient httpClient)
{
    //CT Files
    CopyFtpFile("ftp://ftp2.digikey.com/", "BK_Precision_CAL_TEST.csv", "DigikeyCT.csv", "Global_Specialties", "EWg073U");
    //GS Files
    CopyFtpFile("ftp://ftp2.digikey.com/", "BK_Precision_GLOBAL_SPECIALTIES.csv", "DigikeyGS.csv", "Global_Specialties", "EWg073U");

    //CT Files
    CopyFtpFile("ftp://52.151.42.139/", "Cal_Test.txt", "NewarkCT.csv", "newark_ftp_user", "^Yz&2Z`az2=b+teM");
    //GS Files
    CopyFtpFile("ftp://52.151.42.139/", "gloabl_specialties.txt", "NewarkGS.csv", "newark_ftp_user", "^Yz&2Z`az2=b+teM");
    await ConvertNewarkFtpFileFromTsvToCsv();
}

async Task<List<List<DistributorProductData>>> DownloadApiFiles(HttpClient httpClient)
{
    //get product price data
    List<ErpInventory> erpInventories;
    using (var context = new DefaultDbContext())
    {
        erpInventories = context.ErpInventories.ToList();
    }

    List<List<DistributorProductData>> distributorProductDataLists = new();
    var arrowTask = Task.Run(async () =>
      {
          var data = await GetArrowApiData(httpClient, erpInventories);
          data.ForEach(x => x.DistributorName = "ArrowCTGS");
          distributorProductDataLists.Add(data);
      });

    var mouserTask = Task.Run(async () =>
    {
        try
        {
            var data = await GetMouserApiData(httpClient, erpInventories);
            data.ForEach(x => x.DistributorName = "MouserCTGS");
            distributorProductDataLists.Add(data);

        }catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
    });

    await Task.WhenAll(arrowTask, mouserTask);

    return distributorProductDataLists;
}

async Task<List<DistributorProductData>> GetMouserApiData(HttpClient httpClient, List<ErpInventory> erpInventories)
{
    httpClient.DefaultRequestHeaders.Accept.Clear();
    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
    List<MouserRequest> mouserRequests = new();
    List<ErpInventory> ctProductPrices = erpInventories.Where(x => x.Brand.Trim().Equals("CT", StringComparison.OrdinalIgnoreCase)).ToList();
    for (int i = 0; i < ctProductPrices.Count; i += 50)
    {
        MouserRequest mouserRequest = new();
        mouserRequest.Brand = "CT";
        var batchesCtOf50 = ctProductPrices.Skip(i).Take(50);
        mouserRequest.SearchByPartMfrNameRequestResult = new MouserRequest.SearchByPartMfrNameRequest
        {
            ManufacturerName = "Cal Test Electronics",
            MouserPartNumber = string.Join("|", batchesCtOf50.Select(x => $"510-{x.PartNumber.Trim()}"))
        };
        mouserRequests.Add(mouserRequest);
    }

    List<ErpInventory> gsProductPrices = erpInventories.Where(x => x.Brand.Trim().Equals("GS", StringComparison.OrdinalIgnoreCase)).ToList();
    for (int i = 0; i < gsProductPrices.Count; i += 50)
    {
        MouserRequest mouserRequest = new();
        mouserRequest.Brand = "GS";
        var batchesGsOf50 = gsProductPrices.Skip(i).Take(50);
        mouserRequest.SearchByPartMfrNameRequestResult = new MouserRequest.SearchByPartMfrNameRequest
        {
            ManufacturerName = "Global Specialties",
            MouserPartNumber = string.Join("|", batchesGsOf50.Select(x => $"510-{x.PartNumber.Trim()}"))
        };
        mouserRequests.Add(mouserRequest);
    }

    List<DistributorProductData> distributorProductDatas = new();

    for (int i = 0; i < mouserRequests.Count; i++)
    {
        string json = JsonSerializer.Serialize(mouserRequests[i]);
        var httpContent = new StringContent(json, Encoding.UTF8, "application/json");
        var response = await httpClient.PostAsync(Constants.MouserApiBaseUri, httpContent);
        string resultContent = await response.Content.ReadAsStringAsync();
        if (resultContent == null || string.IsNullOrWhiteSpace(resultContent))
        {
            continue;
        }
        MouserResponse mouserResponse = JsonSerializer.Deserialize<MouserResponse>(resultContent);
        if (mouserResponse != null)
        {
            int qty = 0;
            try
            {
                foreach (var parts in mouserResponse.SearchResult.Parts)
                {
                    if (parts.Availability != "None" && !parts.Availability.Contains("On Order"))
                    {
                        string qtyString = parts.Availability.Trim();

                        // Here we call Regex.Match.
                        Match match = Regex.Match(qtyString, @"([0-9]+)",
                            RegexOptions.IgnoreCase);

                        // Here we check the Match instance.
                        if (match.Success)
                        {
                            // Finally, we get the Group value and display it.
                            string key = match.Groups[1].Value;
                            //Console.WriteLine(key);
                            _ = int.TryParse(key, out qty);
                        }
                    }
                    else
                    {
                        qty = 0;
                    }
                    if (parts.ManufacturerPartNumber == null)
                    {
                        continue;
                    }
                    var model = parts.ManufacturerPartNumber.Trim().Replace("510-", "");
                    var brand = mouserRequests[i].Brand;
                    var link = parts.ProductDetailUrl;
                    distributorProductDatas.Add(new DistributorProductData { Model = model, Quantity = qty, Link = link, Brand = brand });
                    Console.WriteLine($"{model},{brand},{qty},{link}");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed to:" + ex.Message);
            }
        }
        if (i % 25 == 0)
        {
            //sleep for 65 seconds after 25 calls just to be safe, because cant make for than 30 calls a minute to Mouser Api. https://www.mouser.com/api-search/
            var delayAmount = TimeSpan.FromSeconds(65);
            Console.WriteLine("MouserAPI, Waiting: " + DateTime.Now.Add(delayAmount));
            await Task.Delay(delayAmount);
        }
    }
    return distributorProductDatas;
}

async Task<List<DistributorProductData>> GetArrowApiData(HttpClient httpClient, List<ErpInventory> erpInventories)
{
    httpClient.DefaultRequestHeaders.Accept.Clear();
    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
    List<ArrowRequest> arrowRequests = new();

    for (int i = 0; i < erpInventories.Count; i += 25)
    {
        ArrowRequest arrowRequest = new();
        var batchesOf10 = erpInventories.Skip(i).Take(25);
        foreach (var productPrice in batchesOf10)
        {
            arrowRequest.RequestResult.Parts.Add(new ArrowRequest.Part { PartNum = productPrice.PartNumber.Trim(), Mfr = productPrice.Brand.Trim().Equals("CT", StringComparison.OrdinalIgnoreCase) ? "CALTEST" : "GLBLSPCL" });
            if (batchesOf10.LastOrDefault() == productPrice)
            {
                arrowRequests.Add(arrowRequest);
            }
        }
    }

    List<DistributorProductData> distributorProductData = new();

    foreach (var arrowRequest in arrowRequests)
    {
        string requestUri = Constants.ArrowApiBaseUri + JsonSerializer.Serialize(arrowRequest);
        if (arrowRequests.First() != arrowRequest)
        {
            Console.WriteLine("ArrowAPI, Waiting: " + DateTime.Now.AddSeconds(1));
            await Task.Delay(TimeSpan.FromSeconds(1));
        }
        var response = await httpClient.GetFromJsonAsync<ArrowResponse.Root>(requestUri);

        if (response == null)
        {
            continue;
        }
        if (response.Itemserviceresult == null)
        {
            continue;
        }
        if (response.Itemserviceresult.Data[0].PartsFound == 0)
        {
            continue;
        }
        if (response.Itemserviceresult.Data[0].PartsFound > 10)
        {
            Console.WriteLine();
        }

        foreach (var resultList in response.Itemserviceresult.Data[0].ResultList)
        {
            foreach (var part in resultList.PartList)
            {
                try
                {
                    string link = part.InvOrg.WebSites[0].Sources[0].SourceParts[0].Resources.FirstOrDefault(x => x.Type == "detail").Uri;
                    var qty = part.InvOrg.WebSites[0].Sources[0].SourceParts[0].Availability[0].FohQty;
                    var brand = part.Manufacturer.MfrCd.Equals("CALTEST", StringComparison.OrdinalIgnoreCase) ? "CT" : "GS";
                    distributorProductData.Add(new DistributorProductData { Model = part.PartNum, Link = link, Brand = brand, Quantity = qty });
                    Console.WriteLine($"{part.PartNum},{brand},{qty},{link}");

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }
    }
    return distributorProductData;
}

static bool CopyFtpFile(string fTPPath, string fileName, string newFileName, string userName, string password)
{
    try
    {
#pragma warning disable SYSLIB0014 // Type or member is obsolete
        //Ftp is obsolete but not possible with HTTP client
        FtpWebRequest request = (FtpWebRequest)WebRequest.Create(fTPPath + fileName);
#pragma warning restore SYSLIB0014 // Type or member is obsolete
        request.Method = WebRequestMethods.Ftp.DownloadFile;

        request.Credentials = new NetworkCredential(userName, password);
        FtpWebResponse response = (FtpWebResponse)request.GetResponse();
        Stream responseStream = response.GetResponseStream();
        using (Stream s = File.Create(Constants.FtpDownloadPath + newFileName))
        {
            responseStream.CopyTo(s);
        }
        responseStream.Close();
        return true;
    }
    catch
    {
        return false;
    }
}

async Task ConvertNewarkFtpFileFromTsvToCsv()
{
    string text = await File.ReadAllTextAsync(Constants.FtpDownloadPath + "NewarkCT.csv");
    text = text.Replace(",", "");
    File.WriteAllText(Constants.FtpDownloadPath + "NewarkCT.csv", text);

    text = await File.ReadAllTextAsync(Constants.FtpDownloadPath + "NewarkGS.csv");
    text = text.Replace(",", "");
    File.WriteAllText(Constants.FtpDownloadPath + "NewarkGS.csv", text);

    var lines = await File.ReadAllLinesAsync(Constants.FtpDownloadPath + "NewarkCT.csv");
    var csv = lines.Select(row => string.Join(",", row.Split('\t')));
    File.WriteAllLines(Constants.FtpDownloadPath + "NewarkCT.csv", csv);

    lines = await File.ReadAllLinesAsync(Constants.FtpDownloadPath + "NewarkGS.csv");
    csv = lines.Select(row => string.Join(",", row.Split('\t')));
    File.WriteAllLines(Constants.FtpDownloadPath + "NewarkGS.csv", csv);
}

static string GetDistributorId(string fileName)
{
    string customerId = fileName switch
    {
        "ArrowCT" or "ArrowGS" or "ArrowCTGS" => "C000958",
        "DigikeyGS" or "DigikeyCT" => "C000825",
        "TestEquipmentGS" => "G000197",
        "SeframCT" => "C000353",
        "TequipmentGS" or "TequipmentCT" => "C000249",
        "NewarkGS" or "NewarkCT" => "G000069",
        "MouserCT" or "MouserGS" or "MouserCTGS" => "G000066",
        _ => string.Empty,
    };
    return customerId;
}