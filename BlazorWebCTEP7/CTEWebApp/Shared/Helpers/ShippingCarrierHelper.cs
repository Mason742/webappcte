﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTEWebApp.Shared.Helpers
{
    public static class ShippingCarrierHelper
    {

        public static string DetermineCarrierByTrackingNumber(string trackingNumber)
        {
            if (trackingNumber.Length == 18 && trackingNumber.StartsWith("1Z"))
            {
                return "UPS";
            }
            else if (trackingNumber.Length == 12 && (trackingNumber.StartsWith("12") || trackingNumber.StartsWith("92") || trackingNumber.StartsWith("16")))
            {
                return "FedEx";
            }
            else if (trackingNumber.Length == 22 && (trackingNumber.StartsWith("EJ") || trackingNumber.StartsWith("EA") || trackingNumber.StartsWith("EC")))
            {
                return "USPS";
            }
            else
            {
                return "Unknown";
            }
        }

        public static string DetermineCarrierUrlByTrackingNumber(string trackingNumber)
        {
            if (trackingNumber.Length == 18 && trackingNumber.StartsWith("1Z"))
            {
                return "http://wwwapps.ups.com/WebTracking/track?track=yes&trackNums=" + trackingNumber;
            }
            else if (trackingNumber.Length >= 12 && (trackingNumber.StartsWith("12") || trackingNumber.StartsWith("92") || trackingNumber.StartsWith("16")))
            {
                return "https://www.fedex.com/apps/fedextrack/?tracknumbers=" + trackingNumber;
            }
            else if (trackingNumber.Length == 22 && (trackingNumber.StartsWith("EJ") || trackingNumber.StartsWith("EA") || trackingNumber.StartsWith("EC")))
            {
                return "https://tools.usps.com/go/TrackConfirmAction?tLabels=" + trackingNumber;
            }
            else
            {
                return "https://www.google.com/search?q=" + trackingNumber;
            }
        }
    }
}
