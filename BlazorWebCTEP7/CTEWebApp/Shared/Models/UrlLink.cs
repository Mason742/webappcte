﻿namespace CTEWebApp.Shared.Models;

public class UrlLink
{
    public string Url { get; set; }
}
