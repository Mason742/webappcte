﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTEWebApp.Shared.Models
{

    public class TestLeadBuilderEmailFormDTO
    {
        [Required(ErrorMessage = "Field should not be empty")]
        public string FirstName { get; set; }
        
        [Required(ErrorMessage = "Field should not be empty")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Field should not be empty")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string Email { get; set; }

        public TestLeadBuilderResult TestLeadBuilderResult { get; set; }    
    }

}
