﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTEWebApp.Shared.Models
{
    public static class HsTicketCategories
    {
        public const string ContactSales = "Contact_Sales";
        public const string ContactTechSupport = "Contact_TechSupport";
        public const string ContactOther = "Contact_Other";
        public const string ServiceCalibration = "Service_Calibration";
        public const string ServiceRepair = "Service_Repair";
        public const string ServiceRepairAndCalibration = "Service_RepairAndCalibration";
        public const string TestLeadBuilderQuote = "TestLeadBuilder_Quote";
    }
}
