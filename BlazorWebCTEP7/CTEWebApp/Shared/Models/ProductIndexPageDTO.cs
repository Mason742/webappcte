﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDB.Data.Models;

namespace CTEWebApp.Shared.Models
{
    public class ProductIndexPageDTO
    {
        public int TotalCount { get; set; }
        public List<Category> Caterogies { get; set; }
        public class Category
        {
            public string PartNumber { get; set; }
            public string GroupName { get; set; } 
            public string TableName { get; set; } 
            public string DisplayName { get; set; } 
            public string ImageFileLink { get; set; } 
            public string TypeName { get; set; }
            public int ProductCount { get; set; } = 0;
            public string[] ProductsFound { get; set; } 


        }

    }
}
