﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDB.Data.Models;

namespace CTEWebApp.Shared.Models
{
    public class ArticlePageDTO
    {
        public string Title { get; set; }
        public string BodyHtml { get; set; }
        public DateTime Created { get; set; }
        public List<string> RelatedSeriesNames { get; set; }
    }

}
