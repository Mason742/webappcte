﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTEWebApp.Shared.Models
{
    public class User
    {
        public string Id { get; set; }
       
        [Required(ErrorMessage = "Field should not be empty")]
        public string GivenName { get; set; }
       
        [Required(ErrorMessage = "Field should not be empty")]
        public string Surname { get; set; }
        public string Email { get; set; }
        public string DisplayName { get; set; }
        public string Role { get; set; }
       
        [Required(ErrorMessage = "Field should not be empty")]
        public string CompanyName { get; set; }
       
        [Required(ErrorMessage = "Field should not be empty")]
        public string CustomerId { get; set; }
    }

}
