﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTEWebApp.Shared.Models
{
    public partial class PurchaseOrderSummaryDTO
    {
        public string CustomerId { get; set; } = null!;
        public string OrderNumber { get; set; } = null!;
        public string CustomerOrderNumber { get; set; } = null!;
        public DateTime OrderDate { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal TotalSale { get; set; }
        public string ShippingMethod { get; set; } = null!;
        public string Status { get; set; } = null!;
        public DateTime Created { get; set; }
    }
}
