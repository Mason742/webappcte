﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTEWebApp.Shared.Models
{
    public class ConfiguratorGenerator
    {
        public ConfiguratorGenerator()
        {
            MinCentimeter = 10;
            MaxCentimeter = 10000;
        }
        public int?[] Connector1Ids { get; set; }
        public int?[] Connector2Ids { get; set; }
        public int?[] WireIds { get; set; }
        public string PartNumber { get; set; }
        public int MinCentimeter { get; set; }
        public int MaxCentimeter { get; set; }
    }
}
