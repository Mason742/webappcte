﻿using CTEWebApp.Shared.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTEWebApp.Shared.Models.DTOs
{
    public class AvTestLeadAssembliesCtDTO : CompiledSelectors<AvTestLeadAssembliesCtDTO>
    {
        public string ProductId { get; set; }
        public string ConductorArea { get; set; }
        public string Iec { get; set; }
        public string JacketMaterial { get; set; }
        public string Length { get; set; }
        public string MaxCurrent { get; set; }
        public string MaxResistance { get; set; }
        public string Od { get; set; }
        public string RoHs { get; set; }
        public string Stranding { get; set; }
        public string Temperature { get; set; }
        public string WireGauge { get; set; }
        public string Color { get; set; }
        public string JacketOd { get; set; }
        public string Ulrating { get; set; }
        public string MaxVoltage { get; set; }
        public string Type { get; set; }
        public string Type1 { get; set; }

        public string CableType { get; set; }
        public string CableLength { get; set; }
        public string Impedance { get; set; }
        public string VoltageRating { get; set; }
        public string ContactMaterial { get; set; }
        public string InsulationMaterial { get; set; }
        public string Ul94flameRating { get; set; }


        public string Warranty { get; set; }
        public string SpecialHandling { get; set; }
        public string UpcCode { get; set; }
        public string HarmonizedCode { get; set; }
        public string CountryOfOrigin { get; set; }
        public string Discontinued { get; set; }
        public string RoHsCompliant { get; set; }

    }
}
