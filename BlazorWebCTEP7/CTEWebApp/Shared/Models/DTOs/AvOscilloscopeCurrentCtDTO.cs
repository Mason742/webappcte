﻿using CTEWebApp.Shared.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTEWebApp.Shared.Models.DTOs
{
    public class AvOscilloscopeCurrentCtDTO : CompiledSelectors<AvOscilloscopeCurrentCtDTO>
    {
        public string ProductId { get; set; }
        public string Bandwidth { get; set; }
        public string BatteryLife { get; set; }
        public string CurrentDcac { get; set; }
        public string DcmeasurementAccuracy { get; set; }
        public string Dimension { get; set; }
        public string Humidity { get; set; }
        public string MaxCableDiameter { get; set; }
        public string MaxFloatingVoltage { get; set; }
        public string MaxVoltage { get; set; }
        public string MeasurementRanges { get; set; }
        public string Modifier { get; set; }
        public string RiseFallTime { get; set; }
        public string RoHs { get; set; }
        public string Temperature { get; set; }
        public string Type { get; set; }
        public string Weight { get; set; }
        public string Impedance { get; set; }
        public string MaxCurrent { get; set; }


        public string Warranty { get; set; }
        public string SpecialHandling { get; set; }
        public string UpcCode { get; set; }
        public string HarmonizedCode { get; set; }
        public string CountryOfOrigin { get; set; }
        public string Discontinued { get; set; }
        public string RoHsCompliant { get; set; }

    }
}
