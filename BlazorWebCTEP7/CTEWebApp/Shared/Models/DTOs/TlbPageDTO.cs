﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDB.Data.Models;

namespace CTEWebApp.Shared.Models.DTOs
{
    public class TlbPageDTO
    {
        public List<string> StandardTestLeadWires { get; set; }
        public List<TestLeadBuilderDTO> TestLeadBuilders { get; set; }
        public List<TestLeadConnectorDTO> TestLeadConnectors { get; set; }
        public List<TestLeadConnectorDTO> TestLeadConnectors2 { get; set; }
        public List<TestLeadWireDTO> TestLeadWires { get; set; }
        public List<decimal> TestLeadLengths { get; set; }
        public List<TestLeadColorDTO> TestLeadColors { get; set; }


        public class TestLeadBuilderDTO
        {
            public string PartNumber { get; set; }
            public int ConnectorOneId { get; set; }
            public int ConnectorTwoId { get; set; }
            public int WireId { get; set; }
            public string DiagramFileLink { get; set; }
            public int MinCentimeters { get; set; }
            public int MaxCentimeters { get; set; }
        }

        public class TestLeadConnectorDTO
        {
            public int Id { get; set; }
            public string Type { get; set; }
            public decimal Cost { get; set; }
            public string ImageFileLink { get; set; }
            public string Information { get; set; }
            public int Rating { get; set; }
        }

        public class TestLeadWireDTO
        {
            public int Id { get; set; }
            public string PartNumber { get; set; }
            public string Description { get; set; }
            public int MaxCurrent { get; set; }
            public string Material { get; set; }
            public decimal ConductorAreaMm2 { get; set; }
            public int Gauge { get; set; }
            public int Strands { get; set; }
            public decimal StrandDiameterMm { get; set; }
            public decimal LeadodMm { get; set; }
            public decimal LeadodIn { get; set; }
            public decimal CostPerCentimeter { get; set; }

        }

        public class TestLeadColorDTO
        {
            public int ColorId { get; set; }
            public string Name { get; set; }
            public string HexCode { get; set; }

        }
    }
}
