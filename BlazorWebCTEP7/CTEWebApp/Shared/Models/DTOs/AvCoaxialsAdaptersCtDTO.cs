﻿using CTEWebApp.Shared.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTEWebApp.Shared.Models.DTOs
{
    public class AvCoaxialsAdaptersCtDTO : CompiledSelectors<AvCoaxialsAdaptersCtDTO>
    {
        public string ProductId { get; set; }
        public string ConnectorGrade { get; set; }
        public string FrequencyRange { get; set; }
        public string Impedance { get; set; }
        public string InsertionLoss { get; set; }
        public string RoHs { get; set; }
        public string Temperature { get; set; }
        public string Vswr { get; set; }
        public string MaxCurrent { get; set; }
        public string MaxVoltage { get; set; }
        public string Iec { get; set; }
        public string MaxResistance { get; set; }
        public string Type { get; set; }
        public string Type1 { get; set; }

        public string CableLength { get; set; }
        public string ConductorArea { get; set; }
        public string JacketMaterial { get; set; }
        public string Stranding { get; set; }
        public string WireGauge { get; set; }
        public string Color { get; set; }
        public string CenterContactResistance { get; set; }
        public string WorkingVoltage { get; set; }
        public string Frequency { get; set; }



        public string Warranty { get; set; }
        public string SpecialHandling { get; set; }
        public string UpcCode { get; set; }
        public string HarmonizedCode { get; set; }
        public string CountryOfOrigin { get; set; }
        public string Discontinued { get; set; }
        public string RoHsCompliant { get; set; }

    }
}
