﻿using CTEWebApp.Shared.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTEWebApp.Shared.Models.DTOs
{
    public class AvProbeAccessoriesCtDTO : CompiledSelectors<AvProbeAccessoriesCtDTO>
    {
        public string ProductId { get; set; }
        public string Iec { get; set; }
        public string MaxCurrent { get; set; }
        public string MaxResistance { get; set; }
        public string RoHs { get; set; }
        public string Temperature { get; set; }
        public string Color { get; set; }
        public string InterruptingRating { get; set; }
        public string MaxVoltage { get; set; }
        public string Type { get; set; }
        public string Type1 { get; set; }

        public string Impedance { get; set; }
        public string Length { get; set; }
        public string InputPower { get; set; }
        public string OutputPower { get; set; }
        public string Modifier { get; set; }
        public string Accuracy { get; set; }
        public string AveragePower { get; set; }
        public string Bandwidth { get; set; }
        public string MeterImpedance { get; set; }
        public string ContactMaterial { get; set; }
        public string InsulationMaterial { get; set; }
        public string Dimension { get; set; }
        public string Voltage { get; set; }
        public string Ulrating { get; set; }
        public string JacketMaterial { get; set; }
        public string Od { get; set; }
        public string Stranding { get; set; }
        public string TipDimension { get; set; }
        public string WireGauge { get; set; }


        public string Warranty { get; set; }
        public string SpecialHandling { get; set; }
        public string UpcCode { get; set; }
        public string HarmonizedCode { get; set; }
        public string CountryOfOrigin { get; set; }
        public string Discontinued { get; set; }
        public string RoHsCompliant { get; set; }

    }
}
