﻿using CTEWebApp.Shared.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTEWebApp.Shared.Models.DTOs
{
    public class AvCoaxialsTerminatorsCtDTO : CompiledSelectors<AvCoaxialsTerminatorsCtDTO>
    {
        public string ProductId { get; set; }
        public string Accuracy { get; set; }
        public string AveragePower { get; set; }
        public string ConnectorGrade { get; set; }
        public string FrequencyRange { get; set; }
        public string Impedance { get; set; }
        public string InputVoltage { get; set; }
        public string RoHs { get; set; }
        public string Temperature { get; set; }
        public string Vswr { get; set; }
        public string Ztolerance { get; set; }
        public string Length { get; set; }

        public string Type { get; set; }

        public string Warranty { get; set; }
        public string SpecialHandling { get; set; }
        public string UpcCode { get; set; }
        public string HarmonizedCode { get; set; }
        public string CountryOfOrigin { get; set; }
        public string Discontinued { get; set; }
        public string RoHsCompliant { get; set; }

    }
}
