﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTEWebApp.Shared.Models.DTOs
{
    public class AvBindingPostCtDTO : CompiledSelectors<AvBindingPostCtDTO>
    {
        public string ProductId { get; set; }
        public string Color { get; set; }
        public string Iec { get; set; }
        public string MaxCurrent { get; set; }
        public string MaxResistance { get; set; }
        public string MaxVoltage { get; set; }
        public string RoHs { get; set; }
        public string Temperature { get; set; }
        public string BodyMaterial { get; set; }
        public string PollutionDegree { get; set; }
        public string Warranty { get; set; }
        public string UpcCode { get; set; }
        public string HarmonizedCode { get; set; }
        public string CountryOfOrigin { get; set; }
        public string MasterPartNumber { get; set; }
        public string Discontinued { get; set; }
        public string RoHsCompliant { get; set; }
        public string Type { get; set; }


    }
}
