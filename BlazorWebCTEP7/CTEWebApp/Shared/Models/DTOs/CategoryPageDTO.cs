﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTEWebApp.Shared.Models.DTOs
{
    public class CategoryPageDTO<T>
    {
        public JumbotronContent CategoryJumbotron { get; set; }
        public List<T> Data { get; set; }
        
    }
    public class JumbotronContent
    {
        public string Title { get; set; }
        public string Subtitle { get; set; }
        public string ImageLink { get; set; }
    }

}
