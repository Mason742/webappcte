﻿using CTEWebApp.Shared.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTEWebApp.Shared.Models.DTOs
{
    public class AvTestLeadWireCtDTO : CompiledSelectors<AvTestLeadWireCtDTO>
    {
        public string ProductId { get; set; }
        public string Color { get; set; }
        public string ConductorArea { get; set; }
        public string Iec { get; set; }
        public string JacketMaterial { get; set; }
        public string Length { get; set; }
        public string MaxCurrent { get; set; }
        public string Od { get; set; }
        public string RoHs { get; set; }
        public string Stranding { get; set; }
        public string Temperature { get; set; }
        public string WireGauge { get; set; }
        public string Type { get; set; }


        public string Warranty { get; set; }
        public string SpecialHandling { get; set; }
        public string UpcCode { get; set; }
        public string HarmonizedCode { get; set; }
        public string CountryOfOrigin { get; set; }
        public string Discontinued { get; set; }
        public string RoHsCompliant { get; set; }

    }
}
