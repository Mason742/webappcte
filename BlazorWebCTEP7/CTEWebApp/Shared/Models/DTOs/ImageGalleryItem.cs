﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTEWebApp.Shared.Models.DTOs
{
    public class ImageGalleryItem
    {
        public string Thumbnail { get; set; }
        public string Medium { get; set; }
        public string Large { get; set; }
        public string Original { get; set; }
        public int Position { get; set; }

        public string GetThumbnail()
        {
            if (Thumbnail != null)
            {
                return Thumbnail;
            }
            if (Medium != null)
            {
                return Medium;
            }
            if (Large != null)
            {
                return Large;
            }
            if (Original != null)
            {
                return Original;
            }
            return null;
        }

        public string GetMedium()
        {
            if (Medium != null)
            {
                return Medium;
            }
            if (Large != null)
            {
                return Large;
            }
            if (Original != null)
            {
                return Original;
            }
            return null;
        }

        public string GetLarge()
        {
            if (Large != null)
            {
                return Large;
            }
            if (Original != null)
            {
                return Original;
            }
            return null;
        }
    }
}
