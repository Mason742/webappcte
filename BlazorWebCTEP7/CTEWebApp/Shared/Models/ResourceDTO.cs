﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTEWebApp.Shared.Models
{
    public class ResourceDTO
    {
        public string ResourceType { get; set; }
        public string UrlLink { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ThumbnailLink { get; set; }
        public bool OpenNewTab { get; set; }
        public DateTime CreatedDate { get; set; }

    }
}
