﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTEWebApp.Shared.Models
{
    public class StockDTO
    {
        public string Model { get; set; }
        public int Quantity { get; set; }
    }
}
