﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDB.Data.Models;

namespace CTEWebApp.Shared.Models
{
    public class WhereToBuyPageDTO
    {
        public List<AuthorizedDistributorDTO> authorizedDistributorDTOs { get; set; }
        public List<string> whereToBuySearchList{ get; set; }
    }
}
