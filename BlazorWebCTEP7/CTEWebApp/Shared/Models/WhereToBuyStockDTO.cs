﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTEWebApp.Shared.Models
{
    public class WhereToBuyStockDTO
    {
        public string PartNumber { get; set; }
        public string DistributorName { get; set; }
        public int Quantity { get; set; }
        public string WebsiteLink { get; set; }

    }
}
