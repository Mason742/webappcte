﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTEWebApp.Shared.Models
{
    public class CompanyInfoDTO
    {
        public string CompanyName { get; set; }
        public string CustomerId { get; set; }

    }
}
