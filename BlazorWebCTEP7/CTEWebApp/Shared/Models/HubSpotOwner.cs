﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTEWebApp.Shared.Models
{
    public class HubSpotOwner
    {
        public string Title { get; set; } = "Web Master";
        public string Email { get; set; } = "webforms@caltestelectronics.com";
        public string MicrosoftTeamsChannelName { get; set; } = "Web Forms";
        public string MicrosoftTeamsEmail { get; set; } = "43f51f9e.caltestelectronics.com@amer.teams.ms";
        
        //anthony default owner of all tickets
        public string HubSpotOwnerId { get; set; } = "70248610"; //"65835033";
        public string HubSpotTicketCategory { get; set; }
        public string FormattedTicketCategory { get; set; }
        public HubSpotOwner(string hsTicketCategory)
        {
            if (hsTicketCategory.Equals(HsTicketCategories.ContactSales))
            {
                //Mason
                this.HubSpotOwnerId = "64511761";
            }
            else if (hsTicketCategory.Equals(HsTicketCategories.ContactTechSupport))
            {
                //Gabriel
                this.HubSpotOwnerId = "81297048";
            }
            else if (hsTicketCategory.Equals(HsTicketCategories.ContactOther))
            {
                //Anthony
                this.HubSpotOwnerId = "70248610";
            }
            else if (hsTicketCategory.Equals(HsTicketCategories.TestLeadBuilderQuote))
            {
                //Mason
                this.HubSpotOwnerId = "64511761";
            }
            else if (hsTicketCategory.Equals(HsTicketCategories.ServiceRepair))
            {
                //Anthony
                this.HubSpotOwnerId = "70248610";
            }
            else if (hsTicketCategory.Equals(HsTicketCategories.ServiceCalibration))
            {
                //Anthony
                this.HubSpotOwnerId = "70248610";
            }
            HubSpotTicketCategory = hsTicketCategory;
            FormattedTicketCategory = HubSpotTicketCategory.Replace("_", " ");
            ////mason for testing !!!
            //this.HubSpotOwnerId = "64511761";
        }


    }


}
