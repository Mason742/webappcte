﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTEWebApp.Shared.Models
{
    public class ArticleListPageDTO
    {
        public List<ArticleListItemDTO> ArticleListItemDTOs { get; set; }
        public int TotalCount { get; set; }
    }

    public class ArticleListItemDTO
    {
        public string Title { get; set; }
        public string Summary { get; set; }
        public string Url { get; set; }
        public DateTime Created { get; set; }
    }
}
