﻿using CTEWebApp.Shared.Helpers;
using CTEWebApp.Shared.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;

namespace CTEWebApp.Server.Controllers.WebControllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ResponseCache(CacheProfileName = "Default6Hours")]
    public class ResourceController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly ILogger<ResourceController> _logger;
        private readonly IMemoryCache _memoryCache;
        public ResourceController(ApplicationDbContext context, ILogger<ResourceController> logger, IMemoryCache memoryCache)
        {
            _context = context;
            _logger = logger;
            _memoryCache = memoryCache;
        }

        [HttpGet("PageData/{brand}")]
        public async Task<ActionResult<ResourcePageDTO>> PageData(string brand, string SearchParameter, string SortParameter, string TypeParameter, int PageParameter)
        {
            SortParameter = "Relevancy";
            string cacheId = $"{nameof(ResourceController)}-cache-{brand}-{SortParameter}";

            //!!! implement server side sort logic, currently fallback on client side sort .
            //!!! talk to Matthew about Expressions

            if (_memoryCache.TryGetValue(cacheId, out List<ResourceDTO> cachedRecords))
            {
                _logger.LogInformation($"using {cacheId} cache");
            }
            else
            {
                _logger.LogInformation($"setting {cacheId} cache");
                cachedRecords = new List<ResourceDTO>();
                cachedRecords = await _context.ResourcesViews.Where(x => x.Brand.Contains(brand)).AsNoTracking()
                .OrderByDescending(x => x.Published)
                .ThenBy(x => x.ResourceType == "Group" ? 1 : x.ResourceType == "Category" ? 2 : x.ResourceType == "Product" ? 3 : x.ResourceType == "Article" ? 4 : x.ResourceType == "Page" ? 5 : x.ResourceType == "Company Document" ? 6 : x.ResourceType == "Marketing Release" ? 7 : 8)
                .ThenByDescending(x => x.Created)
                .ThenBy(x => x.Title)
                .Select(x => new ResourceDTO
                {
                    ResourceType = x.ResourceType,
                    Title = x.Title,
                    Description = x.Description,
                    ThumbnailLink = x.ThumbnailLink,
                    UrlLink = x.UrlLink,
                    OpenNewTab = (bool)x.NewTab,
                    CreatedDate = x.Created,
                }).ToListAsync();
                _memoryCache.Set(cacheId, cachedRecords, new MemoryCacheEntryOptions()
        .SetAbsoluteExpiration(TimeSpan.FromHours(24)));
            }

            //make a copy of cached list to query upon
            List<ResourceDTO> query = new List<ResourceDTO>(cachedRecords);
            if (SearchParameter != null)
            {
                char[] delimiters = { ',', ' ' };
                string[] searchParameters = SearchParameter.Split(delimiters);
                query = query.Where(y => searchParameters.All(x => (y.Title?.Contains(x, StringComparison.OrdinalIgnoreCase) ?? false) || (y.Description?.Contains(x, StringComparison.OrdinalIgnoreCase) ?? false))).ToList();
            }

            var q2 = from item in query
                     group item by item.ResourceType into g
                     select new { Key = g.Key, Count = g.Count() };
            if (TypeParameter != null)
            {
                query = query.Where(x => x.ResourceType == TypeParameter).ToList();
            }

            var pageDTO = new ResourcePageDTO();
            var pagedList = PagedList<ResourceDTO>.ToPagedList(query.AsQueryable(), PageParameter, 25);
            pageDTO.Records = pagedList; // .Skip(PageParameter == 1 ? 0 : PageParameter * 20).Take(20).ToList();
            pageDTO.CurrentPage = pagedList.CurrentPage;
            pageDTO.TotalPages = pagedList.TotalPages;
            pageDTO.PageSize = pagedList.PageSize;
            pageDTO.TotalCount = pagedList.TotalCount;
            pageDTO.HasPrevious = pagedList.HasPrevious;
            pageDTO.HasNext = pagedList.HasNext;


            pageDTO.Types = q2.OrderByDescending(x => x.Count).ToDictionary(x => x.Key, x => x.Count);
            return Ok(pageDTO);
        }

        [HttpGet("QuickSearch/{brand}")]
        public async Task<ActionResult<List<ResourceDTO>>> QuickSearch(string brand, string SearchParameter)
        {
            string SortParameter = "Relevancy";
            string cacheId = $"{nameof(ResourceController)}-cache-{brand}-{SortParameter}";

            //!!! implement server side sort logic, currently fallback on client side sort .
            //!!! talk to Matthew about Expressions

            if (_memoryCache.TryGetValue(cacheId, out List<ResourceDTO> cachedRecords))
            {
                _logger.LogInformation($"using {cacheId} cache");
            }
            else
            {
                _logger.LogInformation($"setting {cacheId} cache");
                cachedRecords = new List<ResourceDTO>();
                cachedRecords = await _context.ResourcesViews.Where(x => x.Brand.Contains(brand)).AsNoTracking()
                .OrderByDescending(x => x.Published)
                .ThenBy(x => x.ResourceType == "Group" ? 1 : x.ResourceType == "Category" ? 2 : x.ResourceType == "Product" ? 3 : x.ResourceType == "Article" ? 4 : x.ResourceType == "Page" ? 5 : x.ResourceType == "Company Document" ? 6 : x.ResourceType == "Marketing Release" ? 7 : 8)
                .ThenByDescending(x => x.Created)
                .ThenBy(x => x.Title)
                .Select(x => new ResourceDTO
                {
                    ResourceType = x.ResourceType,
                    Title = x.Title,
                    Description = x.Description,
                    ThumbnailLink = x.ThumbnailLink,
                    UrlLink = x.UrlLink,
                    OpenNewTab = (bool)x.NewTab,
                    CreatedDate = x.Created,
                }).ToListAsync();
                _memoryCache.Set(cacheId, cachedRecords, new MemoryCacheEntryOptions()
        .SetAbsoluteExpiration(TimeSpan.FromHours(24)));
            }

            //make a copy of cached list to query upon
            List<ResourceDTO> query = new List<ResourceDTO>(cachedRecords);
            if (SearchParameter != null)
            {
                char[] delimiters = { ',', ' ' };
                string[] searchParameters = SearchParameter.Split(delimiters);
                query = query.Where(y => searchParameters.All(x => (y.Title?.Contains(x, StringComparison.OrdinalIgnoreCase) ?? false) || (y.Description?.Contains(x, StringComparison.OrdinalIgnoreCase) ?? false))).ToList();
            }

            query = query.Take(5).ToList();
            return query;
        }

    }
}
