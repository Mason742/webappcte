﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;

namespace CTEWebApp.Server.Controllers.WebControllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ResponseCache(CacheProfileName = "Default6Hours")]
    public class WhereToBuyController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly ILogger<WhereToBuyController> _logger;
        private readonly IMemoryCache _memoryCache;
        public WhereToBuyController(ApplicationDbContext context, ILogger<WhereToBuyController> logger, IMemoryCache memoryCache)
        {
            _context = context;
            _logger = logger;
            _memoryCache = memoryCache;
        }

        [HttpGet("PageData/{brand}")]
        public async Task<ActionResult<WhereToBuyPageDTO>> PageData(string brand)
        {
            if (_memoryCache.TryGetValue(nameof(WhereToBuyPageDTO) + brand, out WhereToBuyPageDTO data))
            {
                _logger.LogInformation("Using PageData cache");
                return data;
            }
            else
            {
                data = new WhereToBuyPageDTO();
                data.authorizedDistributorDTOs = await _context.AuthorizedDistributors
                    .Where(x => x.Brand == brand || x.Brand == "CT,GS")
                    .Select(y => new AuthorizedDistributorDTO
                    {
                        Country = y.Country,
                        LogoLink = y.LogoImageFileLink,
                        PhoneNumber = y.Phone,
                        Website = (brand == "CT" ? y.WebsiteUrl : y.WebsiteUrlGs)
                    })
                    .ToListAsync();
                data.whereToBuySearchList = await (from distributorStock in _context.DistributorStocks
                                                   join erpInventory in _context.ErpInventories on distributorStock.PartNumber equals erpInventory.PartNumber
                                                   orderby erpInventory.PartNumber ascending
                                                   select erpInventory.PartNumber.Trim()).Distinct().ToListAsync();
                _memoryCache.Set(nameof(WhereToBuyPageDTO) + brand, data, new MemoryCacheEntryOptions()
        .SetAbsoluteExpiration(TimeSpan.FromHours(6)));
                return Ok(data);
            }
        }

        [HttpGet("StockSearch")]
        public async Task<ActionResult<List<string>>> StockSearch(string searchParameter)
        {
            if (_memoryCache.TryGetValue("WTB_StockSearch", out List<WhereToBuyStockDTO> data))
            {
                _logger.LogInformation("Using StockSearch Cache");
            }
            else
            {
                data = await (from distributorStock in _context.DistributorStocks
                              join erpInventory in _context.ErpInventories on distributorStock.PartNumber equals erpInventory.PartNumber
                              join authorizedDistributor in _context.AuthorizedDistributors on distributorStock.CustomerId equals authorizedDistributor.CustomerId
                              group new { distributorStock, erpInventory, authorizedDistributor } by new
                              {
                                  erpInventory.PartNumber,
                                  authorizedDistributor.Name,
                                  distributorStock.Quantity,
                                  distributorStock.WebsiteUrl,
                              } into view
                              orderby view.Key.Quantity, view.Key.Name descending
                              select new WhereToBuyStockDTO { PartNumber = view.Key.PartNumber.Trim(), DistributorName = view.Key.Name, Quantity = view.Key.Quantity, WebsiteLink = view.Key.WebsiteUrl }).ToListAsync();
                _memoryCache.Set("WTB_StockSearch", data, new MemoryCacheEntryOptions()
        .SetAbsoluteExpiration(TimeSpan.FromHours(6)));
                _logger.LogInformation($"Setting StockSearch cache {data.Count}");
            }
            return Ok(data.Where(x => x.PartNumber == searchParameter).ToList());
        }

    }
}
