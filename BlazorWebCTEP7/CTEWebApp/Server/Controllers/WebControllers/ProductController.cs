﻿using CTEWebApp.Server.Helpers;
using HubSpotService.Services;
using EmailSenderService.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.OData.Query;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System.Dynamic;
using System.Linq;
namespace CTEWebApp.Server.Controllers;

[Route("api/[controller]")]
[ApiController]

public class ProductController : ControllerBase
{
    private readonly ApplicationDbContext _context;
    private readonly ILogger<ProductController> _logger;
    private readonly IMemoryCache _memoryCache;
    public ProductController(ApplicationDbContext context, ILogger<ProductController> logger, IMemoryCache memoryCache)
    {
        _context = context;
        _logger = logger;
        _memoryCache = memoryCache;
    }


    [HttpGet("PageData/{partNumber}")]
    public async Task<ActionResult<ProductPageDTO>> GetProductPageData(string partNumber)
    {
        Series series = new Series();
        if (_memoryCache.TryGetValue($"product-series-cache", out List<Series> seriesList))
        {
            _logger.LogInformation("using product-series-cache");
        }
        else
        {
            _logger.LogInformation("setting product-series-cache");

            seriesList = new List<Series>();
            seriesList = await _context.Series.AsNoTracking()
            .Include(x => x.SeriesAttachments)
            .Include(x => x.SeriesImages)
            .Include(x => x.Products).ThenInclude(x => x.ProductImages)
            .Include(x => x.ReplacementSeries)
            .Include(x => x.SeriesCategories).ThenInclude(x => x.Category).ThenInclude(x => x.CategoryGroups).ThenInclude(x => x.Group)
            .Include(x => x.SeriesRelatedSeries)
            .ToListAsync();

            _memoryCache.Set("product-series-cache", seriesList, new MemoryCacheEntryOptions()
        .SetAbsoluteExpiration(TimeSpan.FromHours(24)));
        }
        series = seriesList.FirstOrDefault(x => x.Name == partNumber || x.Products.Any(y => y.PartNumber == partNumber));

        if (series is null)
        {
            return NotFound();
        }

        ProductPageDTO dto = new();

        dto.SelectedPartNumber = series.Products.FirstOrDefault(p => p.PartNumber == partNumber)?.PartNumber;

        //if the part number couldnt be found from the products list then select the first partnumber.
        //probably because the partnumber parameter is a series number
        if (dto.SelectedPartNumber == null)
        {
            dto.SelectedPartNumber = series.Products?.FirstOrDefault()?.PartNumber;
            //also set partNumber for rest of api
            partNumber = dto.SelectedPartNumber;
        }
        dto.SeriesName = series.Name;
        dto.Title = series.Title;
        dto.SpecificationsLegacy = series.SpecificationsLegacy;
        dto.Overview = series.Description;
        dto.New = series.Products.FirstOrDefault(p => p.PartNumber == partNumber).Created > DateTime.Now.AddDays(-365);
        dto.Discontinued = series.Discontinued || series.Products.FirstOrDefault(p => p.PartNumber == partNumber).Discontinued;
        dto.GroupName = series.SeriesCategories.FirstOrDefault()?.Category.CategoryGroups.FirstOrDefault()?.Group.Name;
        dto.CategoryName = series.SeriesCategories.FirstOrDefault()?.Category.DisplayName;
        dto.CategoryTableName = series.SeriesCategories.FirstOrDefault()?.Category.TableName;
        dto.Products = series.Products.Where(x=>x.Discontinued==false).Select(p => new ProductPageDTO.ProductDTO
        {
            Catalog = true, //!!! need db backing field
            Description = p.Description,
            PartNumber = p.PartNumber,
            Msrp = p.Msrp,
            Warranty = p.Warranty,
        }).ToList();
        dto.Documents = series.SeriesAttachments //!!! software could be its own table or add description nullable field for all attachments
            .Where(sa => sa.Type != "Video" && sa.Type != "Software")
            .Select(d => new ProductPageDTO.Attachment { Type = d.Type, AttachmentFileLink = d.AttachmentFileLink })
            .ToList();
        dto.Softwares = series.SeriesAttachments
            .Where(sa => sa.Type == "Software")
            .Select(s => new ProductPageDTO.Attachment { Type = s.Type, AttachmentFileLink = s.AttachmentFileLink })
            .ToList();
        dto.Videos = series.SeriesAttachments
            .Where(sa => sa.Type == "Video")
            .Select(v => new ProductPageDTO.Attachment { Type = v.Type, AttachmentFileLink = v.AttachmentFileLink })
            .ToList();

        if (series.ReplacementSeries != null)
        {
            dto.ReplacementSeries = new()
            {
                Name = series.ReplacementSeries?.Name,
                Title = series.ReplacementSeries?.Title,
                MainImage = series
                        .ReplacementSeries
                        .SeriesImages
                        .FirstOrDefault(x => x.Position == 1)
                        ?.ImageFileLinkTmb ?? series.ReplacementSeries.Products.FirstOrDefault()?.ProductImages.FirstOrDefault(x => x.Position == 1)?.ImageFileLinkTmb
            };
        }


        if (_memoryCache.TryGetValue($"product-controller-images-cache", out List<VImageFileLinkTmb> images))
        {
            _logger.LogInformation("using product-controller-images-cache");
        }
        else
        {
            images = new();
            images = await _context.VImageFileLinkTmbs.ToListAsync();
            _memoryCache.Set("product-controller-images-cache", images, new MemoryCacheEntryOptions()
        .SetAbsoluteExpiration(TimeSpan.FromHours(24)));
            _logger.LogInformation("setting product-controller-images-cache");
        }

        dto.Images = new();
        //get all product images by partnumber currently viewing
        var productId = series.Products
                .FirstOrDefault(p => p.PartNumber == partNumber).Id;

        dto.Images
            .AddRange(images
            .Where(x =>
            x.ProductId == productId
            ).OrderBy(x => x.Position)
            .Select(si => new ImageGalleryItem
            {
                Original = si.ImageFileLink,
                Large = si.ImageFileLinkLg,
                Medium = si.ImageFileLinkMd,
                Thumbnail = si.ImageFileLinkTmb,
                Position = (int)si.Position
            }).ToList());

        dto.MainImage = images
            .Where(x =>
            x.ProductId == productId
            ).OrderBy(x => x.Position)
            .Select(mi => new ImageGalleryItem
            {
                Original = mi.ImageFileLink,
                Large = mi.ImageFileLinkLg,
                Medium = mi.ImageFileLinkMd,
                Thumbnail = mi.ImageFileLinkTmb,
                Position = (int)mi.Position
            }).
            Where(y => y.Position == 1)
            .FirstOrDefault();

        //get all series images
        dto.Images.AddRange(series.SeriesImages
            .OrderBy(x => x.Position)
            .Select(si => new ImageGalleryItem
            {
                Original = si.ImageFileLink,
                Large = si.ImageFileLinkLg,
                Medium = si.ImageFileLinkMd,
                Thumbnail = si.ImageFileLinkTmb
            }).ToList());

        dto.Warranty = series.Products?.FirstOrDefault(p => p.PartNumber == partNumber).Warranty;
        dto.Msrp = series.Products?.FirstOrDefault(p => p.PartNumber == partNumber).Msrp;

        dto.RelatedProducts = series.SeriesRelatedSeries.Select(sr => new ProductPageDTO.RelatedProductDTO
        {
            ProductName = sr.Series.Name,
            MainImage = sr.Series.Products.FirstOrDefault()?.ProductImages.FirstOrDefault(x => x.Position == 1)?.ImageFileLinkTmb,
            Title = sr.Series.Title
        }).ToList();

        return Ok(dto);
    }
}