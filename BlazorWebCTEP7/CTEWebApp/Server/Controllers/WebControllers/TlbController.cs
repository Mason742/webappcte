﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;

namespace CTEWebApp.Server.Controllers.WebControllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ResponseCache(CacheProfileName = "Default6Hours")]
    public class TlbController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly ILogger<TlbController> _logger;
        private readonly IMemoryCache _memoryCache;
        public TlbController(ApplicationDbContext context, ILogger<TlbController> logger, IMemoryCache memoryCache)
        {
            _context = context;
            _logger = logger;
            _memoryCache = memoryCache;
        }

        [HttpGet("PageData")]
        public async Task<ActionResult<TlbPageDTO>> PageData()
        {
            if (_memoryCache.TryGetValue(nameof(TlbPageDTO) + "-cache", out TlbPageDTO data))
            {
                _logger.LogInformation("Using PageData cache");
                return Ok(data);
            }
            else
            {
                data = new TlbPageDTO();
                //set tlb page data
                data = new()
                {
                    StandardTestLeadWires = _context.Products.Select(x => x.PartNumber).ToList(),
                    TestLeadBuilders = await _context.TestLeadBuilders.Select(tlb => new TlbPageDTO.TestLeadBuilderDTO
                    {
                        ConnectorOneId = tlb.ConnectorOneId,
                        ConnectorTwoId = tlb.ConnectorTwoId,
                        DiagramFileLink = tlb.DiagramFileLink,
                        MaxCentimeters = tlb.MaxCentimeters,
                        MinCentimeters = tlb.MinCentimeters,
                        PartNumber = tlb.PartNumber,
                        WireId = tlb.WireId,

                    }).ToListAsync(),
                    TestLeadConnectors = await _context.TestLeadConnectors.Select(conn1 => new TlbPageDTO.TestLeadConnectorDTO
                    {
                        Id = conn1.Id,
                        Cost = conn1.Cost,
                        ImageFileLink = conn1.ImageFileLink,
                        Information = conn1.Information,
                        Rating = conn1.Rating,
                        Type = conn1.Type,
                    }).ToListAsync(),
                    TestLeadConnectors2 = await _context.TestLeadConnectors.Select(conn2 => new TlbPageDTO.TestLeadConnectorDTO
                    {
                        Id = conn2.Id,
                        Cost = conn2.Cost,
                        ImageFileLink = conn2.ImageFileLink,
                        Information = conn2.Information,
                        Rating = conn2.Rating,
                        Type = conn2.Type,
                    }).ToListAsync(),
                    TestLeadColors = await _context.TestLeadColors.Select(color => new TlbPageDTO.TestLeadColorDTO
                    {
                        ColorId = color.ColorId,
                        HexCode = color.HexCode,
                        Name = color.Name,
                    }).ToListAsync(),
                    TestLeadLengths = await _context.TestLeadLengths.Select(length => length.Centimeters).ToListAsync(),
                    TestLeadWires = await _context.TestLeadWires.Select(wire => new TlbPageDTO.TestLeadWireDTO
                    {
                        Id = wire.Id,
                        PartNumber = wire.PartNumber,
                        ConductorAreaMm2 = wire.ConductorAreaMm2,
                        CostPerCentimeter = wire.CostPerCentimeter,
                        Description = wire.Description,
                        Gauge = wire.Gauge,
                        LeadodIn = wire.LeadodIn,
                        LeadodMm = wire.LeadodMm,
                        Material = wire.Material,
                        MaxCurrent = wire.MaxCurrent,
                        StrandDiameterMm = wire.StrandDiameterMm,
                        Strands = wire.Strands,
                    }).ToListAsync(),
                };

                _memoryCache.Set(nameof(TlbPageDTO) + "-cache", data, new MemoryCacheEntryOptions()
        .SetAbsoluteExpiration(TimeSpan.FromHours(24)));
                return Ok(data);
            }
            
        }
    }
}
