﻿using CTEWebApp.Server.Helpers;
using CTEWebApp.Shared.Helpers;
using EmailSenderService.Services;
using HubSpotService.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CTEWebApp.Server.Controllers.WebControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TlbQuoteFormController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly ILogger<TlbQuoteFormController> _logger;
        private readonly IEmailSenderService _emailSender;
        private readonly CrmService _crmService;
        public TlbQuoteFormController(ApplicationDbContext context, ILogger<TlbQuoteFormController> logger, IEmailSenderService emailSender, CrmService crmService)
        {
            _context = context;
            _logger = logger;
            _emailSender = emailSender;
            _crmService = crmService;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] TestLeadBuilderQuoteFormDTO data)
        {
            if (data == null)
            {
                return BadRequest("No data sent");
            }

            if (string.IsNullOrWhiteSpace(data.Email) || string.IsNullOrWhiteSpace(data.FirstName) || string.IsNullOrWhiteSpace(data.LastName))
            {
                return BadRequest("Form not filled out");
            }

            TestLeadBuilderQuoteForm testLeadBuilderQuoteForm = new TestLeadBuilderQuoteForm();
            testLeadBuilderQuoteForm.Brand = data.Brand;
            testLeadBuilderQuoteForm.PartNumber = data.TestLeadBuilderResult.PartNumber;
            testLeadBuilderQuoteForm.Quantity = data.Quantity;

            testLeadBuilderQuoteForm.FirstName = data.FirstName;
            testLeadBuilderQuoteForm.LastName = data.LastName;
            testLeadBuilderQuoteForm.Email = data.Email;
            testLeadBuilderQuoteForm.Company = data.Company;
            testLeadBuilderQuoteForm.Phone = data.Phone;
            testLeadBuilderQuoteForm.PhoneExt = data.PhoneExt;
            testLeadBuilderQuoteForm.Address1 = data.Address1;
            testLeadBuilderQuoteForm.Address2 = data.Address2;

           

            testLeadBuilderQuoteForm.City = data.City;
            testLeadBuilderQuoteForm.StateOrProvince = data.StateOrProvince;
            testLeadBuilderQuoteForm.PostalCode = data.PostalCode;
            testLeadBuilderQuoteForm.Country = data.Country;
            testLeadBuilderQuoteForm.Message = data.Message;

            HubSpotOwner hubSpotOwner = new HubSpotOwner(HsTicketCategories.TestLeadBuilderQuote);

            //save to db
            try
            {
                _context.TestLeadBuilderQuoteForms.Add(testLeadBuilderQuoteForm);
                await _context.SaveChangesAsync();

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Failed to save contact form to DB.");
            }

            //then try to send email
            try
            {
                var email = WebFormEmailGenerator.GenerateTestLeadBuilderQuoteFormEmail(data, hubSpotOwner);
                await _emailSender.SendEmailAsync(email);
                testLeadBuilderQuoteForm.EmailConfirmationSent = true;
                await _context.SaveChangesAsync();

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Failed to email contact form receipt to customer.");
            }

            //then try to save to hubspot
            try
            {
                await _crmService.HubspotIntegration(testLeadBuilderQuoteForm.Email, hubSpotOwner, testLeadBuilderQuoteForm);
                testLeadBuilderQuoteForm.HubSpotTicketCreated = true;
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                await _emailSender.SendEmailAsync("mchanner@caltestelectronics.com", "Failed to log to hubspot.", (data.FirstName + " " + data.LastName + "\n" + data.Email + "\n" + data.Message + "\n\n" + ex.Message));

                _logger.LogError(ex.Message, "Failed to log to hubspot.");
            }

            if (testLeadBuilderQuoteForm.EmailConfirmationSent && testLeadBuilderQuoteForm.HubSpotTicketCreated)
            {
                return Ok(data);
            }
            return BadRequest(data);
        }



    }
}
