﻿using CTEWebApp.Server.Helpers;
using HubSpotService.Services;
using EmailSenderService.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.OData.Query;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System.Dynamic;
using System.Linq;
namespace CTEWebApp.Server.Controllers;

[Route("api/[controller]")]
[ApiController]
[ResponseCache(CacheProfileName = "Default6Hours")]
public class WebController : ControllerBase
{
    private readonly ApplicationDbContext _context;
    private readonly ILogger<WebController> _logger;
    private readonly IEmailSenderService _emailSender;
    private readonly IMemoryCache _memoryCache;
    private readonly CrmService _hubSpotService;
    public WebController(ApplicationDbContext context, ILogger<WebController> logger, IEmailSenderService emailSender, CrmService hubSpotService, IMemoryCache memoryCache)
    {
        _context = context;
        _logger = logger;
        _emailSender = emailSender;
        _hubSpotService = hubSpotService;
        _memoryCache = memoryCache;
    }

    [HttpGet("ListOfGroups/{brand}")]
    public async Task<ActionResult<List<string>>> GetGroups(string brand)
    {
        if (_memoryCache.TryGetValue("list-of-groups-cache", out List<Group> data))
        {
            _logger.LogInformation("Using PageData cache");
        }
        else
        {
            data = new List<Group>();
            data = await _context.Groups.ToListAsync();
            _memoryCache.Set("list-of-groups-cache", data, new MemoryCacheEntryOptions()
        .SetAbsoluteExpiration(TimeSpan.FromHours(24)));
        }
        var query = data.Where(x => x.Brand == brand).Select(x => x.Name).ToList();
        return Ok(query);
    }

    [HttpGet("Articles/{url}")]
    public async Task<ActionResult<ArticlePageDTO>> GetArticles(string url)
    {

        if (_memoryCache.TryGetValue("article-cache", out List<Article> data))
        {
            _logger.LogInformation("using article cache");
        }
        else
        {
            _logger.LogInformation("setting article cache");
            data = new List<Article>();
            data = await _context.Articles.ToListAsync();
            _memoryCache.Set("article-cache", data, new MemoryCacheEntryOptions()
        .SetAbsoluteExpiration(TimeSpan.FromHours(24)));
        }

        var article = data.FirstOrDefault(x => x.Url == url);
        ArticlePageDTO articlePageDTO = new()
        {
            Title = article.Title,
            BodyHtml = article.BodyHtml,
            Created = article.Created,
            RelatedSeriesNames = await _context.ArticleSeries
                                                    .Join(_context.Series,
                                                    articleSeries => articleSeries.SeriesId,
                                                    series => series.Id,
                                                    (articleSeries, series) => new { articleSeries, series })
                                                    .Where(z => z.articleSeries.ArticleId == article.Id)
                                                    .Select(z => z.series.Name).ToListAsync()
        };
        return Ok(articlePageDTO);
    }

}