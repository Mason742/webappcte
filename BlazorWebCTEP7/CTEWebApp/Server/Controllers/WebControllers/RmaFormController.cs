﻿using CTEWebApp.Server.Helpers;
using CTEWebApp.Shared.Helpers;
using EmailSenderService.Services;
using HubSpotService.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StorageService.Services;

namespace CTEWebApp.Server.Controllers.WebControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RmaFormController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly ILogger<RmaFormController> _logger;
        private readonly IEmailSenderService _emailSender;
        private readonly CrmService _crmService;
        private readonly IStorageService _storageService;

        public RmaFormController(ApplicationDbContext context, ILogger<RmaFormController> logger, IEmailSenderService emailSender, CrmService crmService, IStorageService storageService)
        {
            _context = context;
            _logger = logger;
            _emailSender = emailSender;
            _crmService = crmService;
            _storageService = storageService;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] RmaFormDTO data)
        {
            string hsTicketCategory = string.Empty;

            if (data.CalibrationServiceChoice == null || data.RepairOptionChoice == null)
            {
                return BadRequest("Must choose service options");
            }

            if (data.CalibrationServiceChoice != "None" && data.RepairOptionChoice != "None")
            {
                hsTicketCategory = HsTicketCategories.ServiceRepairAndCalibration;
            }
            else if (data.RepairOptionChoice != "None")
            {
                hsTicketCategory = HsTicketCategories.ServiceRepair;
            }
            else if (data.CalibrationServiceChoice != "None")
            {
                hsTicketCategory = HsTicketCategories.ServiceCalibration;
            }

            if (data == null)
            {
                return BadRequest("No data sent");
            }

            if (string.IsNullOrWhiteSpace(data.Brand) || string.IsNullOrWhiteSpace(data.Email) || string.IsNullOrWhiteSpace(data.FirstName) || string.IsNullOrWhiteSpace(data.LastName))
            {
                return BadRequest("Form not filled out");
            }

            RmaForm rmaForm = new()
            {
                FirstName = data.FirstName,
                LastName = data.LastName,
                Email = data.Email,
                Company = data.Company,
                Phone = data.Phone,
                PhoneExt = data.PhoneExt,
                Address1 = data.Address1,
                Address2 = data.Address2,
                City = data.City,
                StateOrProvince = data.StateOrProvince,
                PostalCode = data.PostalCode,
                Country = data.Country,
                PartNumber = data.PartNumber,
                RepairOptionChoice = data.RepairOptionChoice,
                CalibrationServiceChoice = data.CalibrationServiceChoice,
                SerialNumber = data.SerialNumber,
                Quantity = data.Quantity,
                ProofOfPurchaseFileLink = data.ProofOfPurchaseLink,
                RmaProductImageFileLink = data.RmaProductImageLink,
                ProblemDescription = data.ProblemDescription,

                Brand = BrandHelper.GetIdById(data.Brand)
            };

            HubSpotOwner hubSpotOwner = new(hsTicketCategory);

            //save to db
            try
            {
                _context.RmaForms.Add(rmaForm);
                //save form to get id
                await _context.SaveChangesAsync();

                if (rmaForm.ProofOfPurchaseFileLink != null)
                {
                    string relativeSourceAzurePath = FileUploadHelper.GetRelativeUri(rmaForm.ProofOfPurchaseFileLink);
                    var filename = Path.GetFileName(relativeSourceAzurePath);
                    string relativeDestinationPath = FileUploadHelper.GetProductionDirectory(typeof(RmaForm)) + rmaForm.Id + "/" + filename;
                    await _storageService.MoveFileAsync(relativeSourceAzurePath, relativeDestinationPath, true);
                    var destinationUrl = FileUploadHelper.AppendCustomDomainUrl(relativeDestinationPath);
                    rmaForm.ProofOfPurchaseFileLink = destinationUrl;
                    await _context.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Failed to save contact form to DB.");
            }

            //then try to send email
            try
            {
                var email = WebFormEmailGenerator.GenerateSupportEmail(rmaForm, hubSpotOwner);
                await _emailSender.SendEmailAsync(email);
                rmaForm.EmailConfirmationSent = true;
                await _context.SaveChangesAsync();

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Failed to email contact form receipt to customer.");
            }

            //then try to save to hubspot
            try
            {
                await _crmService.HubspotIntegration(rmaForm.Email, hubSpotOwner, rmaForm);
                rmaForm.HubSpotTicketCreated = true;
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                await _emailSender.SendEmailAsync("mchanner@caltestelectronics.com", "Failed to log to hubspot.", (data.FirstName + " " + data.LastName + "\n" + data.Email + "\n" + data.ProblemDescription + "\n\n" + ex.Message));
                _logger.LogError(ex, "Failed to log to hubspot.");
            }

            if (rmaForm.EmailConfirmationSent && rmaForm.HubSpotTicketCreated)
            {
                return Ok(data);
            }
            return BadRequest(data);
        }



    }
}
