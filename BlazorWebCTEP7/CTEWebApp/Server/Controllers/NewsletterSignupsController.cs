﻿using CTEWebApp.Server.Controllers.AdminCMS;
using Microsoft.AspNetCore.Mvc;

namespace CTEWebApp.Server.Controllers;

[Route("api/[controller]")]
[ApiController]
public class NewsletterSignupsController : GenericBaseApiController<NewsletterSignup>
{
    public NewsletterSignupsController(ApplicationDbContext context, ILogger<GenericBaseApiController<NewsletterSignup>> logger) : base(context, logger)
    {
    }
}
