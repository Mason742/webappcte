﻿using EmailSenderService.Services;
using Microsoft.AspNetCore.Mvc;
using StorageService.Services;

namespace CTEWebApp.Server.Controllers.AdminCMS
{
    public class TestLeadConnectorController : GenericBaseODataController<TestLeadConnector>
    {
        public TestLeadConnectorController(ApplicationDbContext context, ILogger<GenericBaseODataController<TestLeadConnector>> logger, IEmailSenderService emailSenderService, IStorageService storageService) : base(context, logger, emailSenderService, storageService)
        {
        }
    }
}
