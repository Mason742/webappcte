﻿using EmailSenderService.Services;
using Microsoft.AspNetCore.Mvc;

namespace CTEWebApp.Server.Controllers.AdminCMS
{
    public class ContactFormController : GenericBaseODataController<ContactForm>
    {
        public ContactFormController(ApplicationDbContext context, ILogger<GenericBaseODataController<ContactForm>> logger, IEmailSenderService emailSenderService) : base(context, logger, emailSenderService)
        {
        }
    }
}
