﻿using EmailSenderService.Services;
using Microsoft.AspNetCore.Mvc;

namespace CTEWebApp.Server.Controllers.AdminCMS
{
    public class RmaFormController : GenericBaseODataController<RmaForm>
    {
        public RmaFormController(ApplicationDbContext context, ILogger<GenericBaseODataController<RmaForm>> logger, IEmailSenderService emailSenderService) : base(context, logger, emailSenderService)
        {
        }
    }
}
