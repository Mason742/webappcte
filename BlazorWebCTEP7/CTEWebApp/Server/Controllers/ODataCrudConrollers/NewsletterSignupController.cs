﻿using EmailSenderService.Services;
using Microsoft.AspNetCore.Mvc;

namespace CTEWebApp.Server.Controllers.AdminCMS
{
    public class NewsletterSignupController : GenericBaseODataController<NewsletterSignup>
    {
        public NewsletterSignupController(ApplicationDbContext context, ILogger<GenericBaseODataController<NewsletterSignup>> logger, IEmailSenderService emailSenderService) : base(context, logger, emailSenderService)
        { }

        [HttpGet("api/Test222")]
        public IActionResult Test()
        {
            return Ok();
        }
    }
}
