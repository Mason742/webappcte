﻿using EmailSenderService.Services;
using Microsoft.AspNetCore.Mvc;

namespace CTEWebApp.Server.Controllers.AdminCMS
{
    public class SeriesCategoryController : GenericBaseODataController<SeriesCategory>
    {
        public SeriesCategoryController(ApplicationDbContext context, ILogger<GenericBaseODataController<SeriesCategory>> logger, IEmailSenderService emailSenderService) : base(context, logger, emailSenderService)
        {
        }
    }
}
