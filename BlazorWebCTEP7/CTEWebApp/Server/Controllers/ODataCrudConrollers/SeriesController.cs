﻿using EmailSenderService.Services;
using Microsoft.AspNetCore.Mvc;

namespace CTEWebApp.Server.Controllers.AdminCMS
{
    public class SeriesController : GenericBaseODataController<Series>
    {
        public SeriesController(ApplicationDbContext context, ILogger<GenericBaseODataController<Series>> logger, IEmailSenderService emailSenderService) : base(context, logger, emailSenderService)
        {
        }
    }
}
