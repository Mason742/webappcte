﻿using EmailSenderService.Services;
using Microsoft.AspNetCore.Mvc;
using StorageService.Services;

namespace CTEWebApp.Server.Controllers.AdminCMS
{
    public class SeriesImageController : GenericBaseODataController<SeriesImage>
    {
        public SeriesImageController(ApplicationDbContext context, ILogger<GenericBaseODataController<SeriesImage>> logger, IEmailSenderService emailSenderService, IStorageService storageService) : base(context, logger, emailSenderService, storageService)
        {
        }
    }
}
