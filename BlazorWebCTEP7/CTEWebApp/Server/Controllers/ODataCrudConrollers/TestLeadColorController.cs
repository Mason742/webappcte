﻿using EmailSenderService.Services;
using Microsoft.AspNetCore.Mvc;

namespace CTEWebApp.Server.Controllers.AdminCMS
{
    public class TestLeadColorController : GenericBaseODataController<TestLeadColor>
    {
        public TestLeadColorController(ApplicationDbContext context, ILogger<GenericBaseODataController<TestLeadColor>> logger, IEmailSenderService emailSenderService) : base(context, logger, emailSenderService)
        {
        }
    }
}
