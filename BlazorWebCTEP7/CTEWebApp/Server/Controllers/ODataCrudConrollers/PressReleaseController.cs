﻿using EmailSenderService.Services;
using Microsoft.AspNetCore.Mvc;
using StorageService.Services;

namespace CTEWebApp.Server.Controllers.AdminCMS
{
    public class PressReleaseController : GenericBaseODataController<PressRelease>
    {
        public PressReleaseController(ApplicationDbContext context, ILogger<GenericBaseODataController<PressRelease>> logger, IEmailSenderService emailSenderService, IStorageService storageService) : base(context, logger, emailSenderService, storageService)
        {
        }
    }
}
