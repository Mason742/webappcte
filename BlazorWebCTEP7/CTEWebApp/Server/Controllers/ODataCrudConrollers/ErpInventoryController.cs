﻿using EmailSenderService.Services;
using Microsoft.AspNetCore.Mvc;

namespace CTEWebApp.Server.Controllers.AdminCMS
{
    public class ErpInventoryController : GenericBaseODataController<ErpInventory>
    {
        public ErpInventoryController(ApplicationDbContext context, ILogger<GenericBaseODataController<ErpInventory>> logger, IEmailSenderService emailSenderService) : base(context, logger, emailSenderService)
        {
        }
    }
}
