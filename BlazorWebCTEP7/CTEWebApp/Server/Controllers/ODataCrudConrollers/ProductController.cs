﻿using EmailSenderService.Services;
using Microsoft.AspNetCore.Mvc;

namespace CTEWebApp.Server.Controllers.AdminCMS
{
    public class ProductController : GenericBaseODataController<Product>
    {
        public ProductController(ApplicationDbContext context, ILogger<GenericBaseODataController<Product>> logger, IEmailSenderService emailSenderService) : base(context, logger, emailSenderService)
        {
        }
    }
}
