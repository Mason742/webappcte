﻿using EmailSenderService.Services;
using Microsoft.AspNetCore.Mvc;

namespace CTEWebApp.Server.Controllers.AdminCMS
{
    public class CategoryGroupController : GenericBaseODataController<CategoryGroup>
    {
        public CategoryGroupController(ApplicationDbContext context, ILogger<GenericBaseODataController<CategoryGroup>> logger, IEmailSenderService emailSenderService) : base(context, logger, emailSenderService)
        {
        }
    }
}
