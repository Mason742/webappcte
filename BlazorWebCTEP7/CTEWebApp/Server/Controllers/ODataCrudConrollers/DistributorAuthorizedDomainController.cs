﻿using EmailSenderService.Services;
using Microsoft.AspNetCore.Mvc;

namespace CTEWebApp.Server.Controllers.AdminCMS
{
    public class DistributorAuthorizedDomainController : GenericBaseODataController<DistributorAuthorizedDomain>
    {
        public DistributorAuthorizedDomainController(ApplicationDbContext context, ILogger<GenericBaseODataController<DistributorAuthorizedDomain>> logger, IEmailSenderService emailSenderService) : base(context, logger, emailSenderService)
        {
        }
    }
}
