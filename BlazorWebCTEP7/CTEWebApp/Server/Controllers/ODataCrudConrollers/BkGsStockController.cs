﻿using EmailSenderService.Services;
using Microsoft.AspNetCore.Mvc;

namespace CTEWebApp.Server.Controllers.AdminCMS
{
    public class BkGsStockController : GenericBaseODataController<BkGsStock>
    {
        public BkGsStockController(ApplicationDbContext context, ILogger<GenericBaseODataController<BkGsStock>> logger, IEmailSenderService emailSenderService) : base(context, logger, emailSenderService)
        {
        }
    }
}
