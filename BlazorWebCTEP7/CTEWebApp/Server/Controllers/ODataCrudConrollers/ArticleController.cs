﻿using EmailSenderService.Services;
using Microsoft.AspNetCore.Mvc;

namespace CTEWebApp.Server.Controllers.AdminCMS
{
    public class ArticleController : GenericBaseODataController<Article>
    {
        public ArticleController(ApplicationDbContext context, ILogger<GenericBaseODataController<Article>> logger, IEmailSenderService emailSenderService) : base(context, logger, emailSenderService)
        {
        }
    }
}
