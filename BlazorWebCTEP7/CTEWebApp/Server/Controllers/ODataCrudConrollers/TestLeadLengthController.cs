﻿using EmailSenderService.Services;
using Microsoft.AspNetCore.Mvc;

namespace CTEWebApp.Server.Controllers.AdminCMS
{
    public class TestLeadLengthController : GenericBaseODataController<TestLeadLength>
    {
        public TestLeadLengthController(ApplicationDbContext context, ILogger<GenericBaseODataController<TestLeadLength>> logger, IEmailSenderService emailSenderService) : base(context, logger, emailSenderService)
        {
        }
    }
}
