﻿using EmailSenderService.Services;
using Microsoft.AspNetCore.Mvc;

namespace CTEWebApp.Server.Controllers.AdminCMS
{
    public class DistributorStockController : GenericBaseODataController<DistributorStock>
    {
        public DistributorStockController(ApplicationDbContext context, ILogger<GenericBaseODataController<DistributorStock>> logger, IEmailSenderService emailSenderService) : base(context, logger, emailSenderService)
        {
        }
    }
}
