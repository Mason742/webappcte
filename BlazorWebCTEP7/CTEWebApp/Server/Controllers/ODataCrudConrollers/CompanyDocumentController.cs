﻿using EmailSenderService.Services;
using Microsoft.AspNetCore.Mvc;
using StorageService.Services;

namespace CTEWebApp.Server.Controllers.AdminCMS
{
    public class CompanyDocumentController : GenericBaseODataController<CompanyDocument>
    {
        public CompanyDocumentController(ApplicationDbContext context, ILogger<GenericBaseODataController<CompanyDocument>> logger, IEmailSenderService emailSenderService, IStorageService storageService) : base(context, logger, emailSenderService, storageService)
        {
        }
    }
}
