﻿using EmailSenderService.Services;
using Microsoft.AspNetCore.Mvc;
using StorageService.Services;

namespace CTEWebApp.Server.Controllers.AdminCMS
{
    public class HomepageController : GenericBaseODataController<Homepage>
    {
        public HomepageController(ApplicationDbContext context, ILogger<GenericBaseODataController<Homepage>> logger, IEmailSenderService emailSenderService, IStorageService storageService) : base(context, logger, emailSenderService, storageService)
        {
        }
    }
}
