﻿using EmailSenderService.Services;
using Microsoft.AspNetCore.Mvc;

namespace CTEWebApp.Server.Controllers.AdminCMS
{
    public class AuthorizedDistributorController : GenericBaseODataController<AuthorizedDistributor>
    {
        public AuthorizedDistributorController(ApplicationDbContext context, ILogger<GenericBaseODataController<AuthorizedDistributor>> logger, IEmailSenderService emailSenderService) : base(context, logger, emailSenderService)
        {
        }
    }
}
