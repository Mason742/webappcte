﻿using EmailSenderService.Services;
using Microsoft.AspNetCore.Mvc;
using StorageService.Services;

namespace CTEWebApp.Server.Controllers.AdminCMS
{
    public class ProductImageController : GenericBaseODataController<ProductImage>
    {
        public ProductImageController(ApplicationDbContext context, ILogger<GenericBaseODataController<ProductImage>> logger, IEmailSenderService emailSenderService, IStorageService storageService) : base(context, logger, emailSenderService, storageService)
        {
        }
    }
}
