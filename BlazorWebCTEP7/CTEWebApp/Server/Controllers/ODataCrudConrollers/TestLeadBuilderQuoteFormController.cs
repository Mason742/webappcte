﻿using EmailSenderService.Services;
using Microsoft.AspNetCore.Mvc;

namespace CTEWebApp.Server.Controllers.AdminCMS
{
    public class TestLeadBuilderQuoteFormController : GenericBaseODataController<TestLeadBuilderQuoteForm>
    {
        public TestLeadBuilderQuoteFormController(ApplicationDbContext context, ILogger<GenericBaseODataController<TestLeadBuilderQuoteForm>> logger, IEmailSenderService emailSenderService) : base(context, logger, emailSenderService)
        {
        }
    }
}
