﻿using EmailSenderService.Services;
using Microsoft.AspNetCore.Mvc;
using StorageService.Services;

namespace CTEWebApp.Server.Controllers.AdminCMS
{
    public class SeriesAttachmentController : GenericBaseODataController<SeriesAttachment>
    {
        public SeriesAttachmentController(ApplicationDbContext context, ILogger<GenericBaseODataController<SeriesAttachment>> logger, IEmailSenderService emailSenderService, IStorageService storageService) : base(context, logger, emailSenderService, storageService)
        {
        }
    }
}
