﻿using EmailSenderService.Services;
using Microsoft.AspNetCore.Mvc;

namespace CTEWebApp.Server.Controllers.AdminCMS
{
    public class BrandCrossController : GenericBaseODataController<BrandCross>
    {
        public BrandCrossController(ApplicationDbContext context, ILogger<GenericBaseODataController<BrandCross>> logger, IEmailSenderService emailSenderService) : base(context, logger, emailSenderService)
        {
        }
    }
}
