﻿using CTEWebApp.Server.Helpers;
using HubSpotService.Services;
using EmailSenderService.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.OData.Query;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using System.Dynamic;
using System.Linq;
using WebDB.Data.Models;

namespace CTEWebApp.Server.Controllers;

[Route("api/[controller]")]
[Authorize(Roles = "Administrator,Distributor")]
[ApiController]
[ResponseCache(CacheProfileName = "Default6Hours")]
public class DistributorAccessController : ControllerBase
{
    private readonly ApplicationDbContext _context;
    private readonly ILogger<DistributorAccessController> _logger;
    private readonly IEmailSenderService _emailSender;
    private readonly IMemoryCache _memoryCache;
    public DistributorAccessController(ApplicationDbContext context, ILogger<DistributorAccessController> logger, IEmailSenderService emailSender, IMemoryCache memoryCache)
    {
        _context = context;
        _logger = logger;
        _emailSender = emailSender;
        _memoryCache = memoryCache;
    }

    [HttpGet("ErpInventories")]
    public async Task<ActionResult<List<ErpInventory>>> GetErpInventories()
    {
        if (_memoryCache.TryGetValue("erpInventories-cache", out List<ErpInventory> data))
        {
            _logger.LogInformation("using erpInventories cache");
        }
        else
        {
            _logger.LogInformation("setting erpInventories cache");
            data = await _context.ErpInventories.AsNoTracking().ToListAsync();
            _memoryCache.Set("erpInventories-cache", data, new MemoryCacheEntryOptions()
        .SetAbsoluteExpiration(TimeSpan.FromHours(6)));
        }
        return Ok(data);
    }

    [HttpGet("CompanyList")]
    [Authorize(Roles = "Administrator")]
    public async Task<ActionResult> GetCustomerList()
    {
        if (_memoryCache.TryGetValue("CompanyInfoDTO-cache", out List<CompanyInfoDTO> data))
        {
            _logger.LogInformation("using CompanyInfoDTO cache");
        }
        else
        {
            _logger.LogInformation("setting CompanyInfoDTO cache");
            data = await _context.ViewOrderTrackingNumberAggregates.Select(x => new CompanyInfoDTO { CompanyName = $"{x.BillName} - {x.CustomerId}", CustomerId = x.CustomerId }).Distinct().ToListAsync();
            data.Insert(0, new CompanyInfoDTO { CompanyName = "Cal Test Electronics", CustomerId = "TEST" });
            _memoryCache.Set("CompanyInfoDTO-cache", data, new MemoryCacheEntryOptions()
        .SetAbsoluteExpiration(TimeSpan.FromHours(24)));
        }
        return Ok(data);
    }

    [HttpGet("PurchaseOrderSummaries/{clientRequestedCustomerId}")]
    public async Task<ActionResult<List<PurchaseOrderSummaryDTO>>> GetPurchaseOrderSummaries(string clientRequestedCustomerId)
    {


        //do not trust client sent customerId unless Administrator.
        var customerId = User.Claims.FirstOrDefault(x => x.Type == "extension_CustomerId").Value;
        if (User.IsInRole("Administrator"))
        {
            customerId = clientRequestedCustomerId;
        }

        if (_memoryCache.TryGetValue("PurchaseOrderSummaries-cache", out List<PurchaseOrderSummaryDTO> data))
        {
            _logger.LogInformation("using PurchaseOrderSummaries cache");
        }
        else
        {
            _logger.LogInformation("setting PurchaseOrderSummaries cache");
            data = await
                        _context.ViewOrderTrackingNumberAggregates
                          .AsNoTracking()
                          .GroupBy(x => new GroupKey
                          {
                              CustomerId = x.CustomerId,
                              OrderNumber = x.OrderNumber,
                              CustomerOrderNumber = x.CustomerOrderNumber,
                              OrderDate = x.OrderDate,
                              ShippingMethod = x.ShippingMethod,
                              Status = x.Status,
                              Created = x.Created
                          })
                          .Select(g => new PurchaseOrderSummaryDTO
                          {
                              CustomerId = g.Key.CustomerId,
                              OrderNumber = g.Key.OrderNumber,
                              CustomerOrderNumber = g.Key.CustomerOrderNumber,
                              OrderDate = g.Key.OrderDate,
                              TotalSale = g.FirstOrDefault().TotalSale,
                              ShippingMethod = g.Key.ShippingMethod,
                              Status = g.Key.Status,
                              Created = g.Key.Created,
                          }).ToListAsync();
            _memoryCache.Set("PurchaseOrderSummaries-cache", data, new MemoryCacheEntryOptions()
        .SetAbsoluteExpiration(TimeSpan.FromHours(4)));
        }
        data = data.Where(x => x.CustomerId == customerId).ToList();
        return Ok(data);
    }

    [HttpGet("PurchaseOrders/{clientRequestedCustomerId}/{orderNumber}")]
    public async Task<ActionResult<List<ViewOrderTrackingNumberAggregate>>> GetPurchaseOrders(string clientRequestedCustomerId, string orderNumber)
    {
        //do not trust client sent customerId unless Administrator.
        var customerId = User.Claims.FirstOrDefault(x => x.Type == "extension_CustomerId").Value;
        if (User.IsInRole("Administrator"))
        {
            customerId = clientRequestedCustomerId;
        }

        if (_memoryCache.TryGetValue("PurchaseOrders-cache", out List<ViewOrderTrackingNumberAggregate> data))
        {
            _logger.LogInformation("using PurchaseOrders cache");
        }
        else
        {
            _logger.LogInformation("setting PurchaseOrders cache");
            data = await
                       _context.ViewOrderTrackingNumberAggregates
                         .AsNoTracking()
                         .ToListAsync();
            _memoryCache.Set("PurchaseOrders-cache", data, new MemoryCacheEntryOptions()
        .SetAbsoluteExpiration(TimeSpan.FromHours(4)));
        }
        data = data.Where(x => x.CustomerId == customerId && x.OrderNumber == orderNumber).ToList();
        return Ok(data);
    }

    class GroupKey
    {
        public string CustomerId { get; set; }
        public string OrderNumber { get; set; }
        public string CustomerOrderNumber { get; set; }
        public DateTime OrderDate { get; set; }
        public string ShippingMethod { get; set; }
        public string Status { get; set; }
        public DateTime Created { get; set; }
    }


    [ApiExplorerSettings(IgnoreApi = false)]
    [HttpGet("ExcelExport_PurchaseOrders/{clientRequestedCustomerId}")]
    public async Task<IActionResult> Get(string clientRequestedCustomerId)
    {
        //do not trust client sent customerId unless Administrator.
        var customerId = User.Claims.FirstOrDefault(x => x.Type == "extension_CustomerId").Value;
        if (User.IsInRole("Administrator"))
        {
            customerId = clientRequestedCustomerId;
        }

        if (_memoryCache.TryGetValue("ExcelExport_PurchaseOrders-cache", out List<ViewOrderTrackingNumberAggregate> data))
        {
            _logger.LogInformation("using ExcelExport_PurchaseOrders cache");
        }
        else
        {
            _logger.LogInformation("setting ExcelExport_PurchaseOrders cache");
            data = await _context.ViewOrderTrackingNumberAggregates.AsNoTracking().ToListAsync();
            _memoryCache.Set("ExcelExport_PurchaseOrders-cache", data, new MemoryCacheEntryOptions()
        .SetAbsoluteExpiration(TimeSpan.FromHours(4)));
        }

        data = data.Where(x => x.CustomerId == customerId).ToList();

        // do logic to create csv in memoryStream
        MemoryStream memoryStream;
        using (var package = new ExcelPackage())
        {
            ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Orders");
            worksheet.Cells["A1"].LoadFromCollection(data, true);

            //Format columns as date
            worksheet.Cells[2, 7, worksheet.Dimension.End.Row, 7].Style.Numberformat.Format = "MM/dd/yyyy";
            worksheet.Cells[2, 14, worksheet.Dimension.End.Row, 14].Style.Numberformat.Format = "MM/dd/yyyy";
            worksheet.Cells[2, 15, worksheet.Dimension.End.Row, 15].Style.Numberformat.Format = "MM/dd/yyyy";
            worksheet.Cells[2, 17, worksheet.Dimension.End.Row, 17].Style.Numberformat.Format = "MM/dd/yyyy";

            //format columns as USD currency
            worksheet.Cells[2, 8, worksheet.Dimension.End.Row, 8].Style.Numberformat.Format = "$#,##0.00";
            worksheet.Cells[2, 9, worksheet.Dimension.End.Row, 9].Style.Numberformat.Format = "$#,##0.00";

            //create a range for the table
            ExcelRange range = worksheet.Cells[1, 1, worksheet.Dimension.End.Row, worksheet.Dimension.End.Column];

            //add a table to the range
            ExcelTable tab = worksheet.Tables.Add(range, "Table1");

            //format the table
            tab.TableStyle = TableStyles.Medium2;

            //Make all text fit the cells
            worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();

            //hide ID column after cells autofitted
            worksheet.Column(1).Hidden = true;

            //find shipping soon revised etas
            var query = from cell in worksheet.Cells[2, 15, worksheet.Dimension.End.Row, 15]
                        where (cell.Text?.ToString().Contains("01/01/1900") == true || cell.Text.ToString().Contains("01/02/1900") == true)
                        select cell;

            //loop through shipping soon and set the cell value to shipping soon.
            foreach (var cell in query)
            {
                cell.Value = cell.Text.ToString().Replace("01/01/1900", "Preparing to ship...");
                cell.Value = cell.Text.ToString().Replace("01/02/1900", "Preparing to ship...");

            }
            memoryStream = new MemoryStream(package.GetAsByteArray());
        }
        return File(memoryStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", $"Orders_{customerId}.xlsx");
    }
}