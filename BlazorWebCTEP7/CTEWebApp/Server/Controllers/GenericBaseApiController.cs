﻿using EmailSenderService.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace CTEWebApp.Server.Controllers.AdminCMS
{
    public class GenericBaseApiController<T> : ControllerBase where T : class,IEntityBase
    {
        protected readonly ApplicationDbContext _context;
        protected readonly ILogger<GenericBaseApiController<T>> _logger;
        protected readonly IEmailSenderService _emailSenderService;
        public GenericBaseApiController(ApplicationDbContext context, ILogger<GenericBaseApiController<T>> logger)
        {
            _context = context;
            _logger = logger;

        }
        public GenericBaseApiController(ApplicationDbContext context, ILogger<GenericBaseApiController<T>> logger, IEmailSenderService emailSenderService)
        {
            _context = context;
            _logger = logger;
            _emailSenderService = emailSenderService;

        }

        [HttpGet]
        public virtual async Task<ActionResult<IEnumerable<T>>> List()
        {
            var list = await _context.Set<T>().ToListAsync();
            var data = new { Items = list, list.Count };
            return Ok(data);
        }

        /// <summary>
        /// https://docs.microsoft.com/en-us/aspnet/core/web-api/action-return-types?view=aspnetcore-6.0#actionresultt-type
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public virtual async Task<ActionResult<T>> Detail(int id)
        {
            var entity = await _context.Set<T>().FindAsync(id);
            if (entity == null)
            {
                return NotFound();
            }
            return entity;
        }

        [HttpPost]
        public virtual async Task<ActionResult<T>> Create(T entity)
        {
            await _context.Set<T>().AddAsync(entity);
            await _context.SaveChangesAsync();

            return CreatedAtAction("Detail", new { id = entity.Id }, entity);
        }

        [HttpPut]
        public virtual async Task<IActionResult> Update(int id, T entity)
        {
            if (id != entity.Id)
            {
                return BadRequest();
            }
            if (!await EntityExists(id))
            {
                return NotFound();
            }

            _context.Entry(entity).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return NoContent();
        }

        [Authorize(Roles = "Administrator")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var entity = await _context.Set<T>().FindAsync(id);
            if (entity == null)
            {
                return NotFound();
            }
            _context.Set<T>().Remove(entity);
            await _context.SaveChangesAsync();
            return NoContent();
        }

        private Task<bool> EntityExists(int id)
        {
            return _context.Set<T>().AnyAsync(x => x.Id == id);
        }
    }
}