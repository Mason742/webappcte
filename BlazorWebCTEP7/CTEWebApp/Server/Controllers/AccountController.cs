﻿using EmailSenderService.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.OData.Routing.Controllers;
using System.Text.Encodings.Web;

namespace CTEWebApp.Server.Controllers;

[Authorize]
[ApiExplorerSettings(IgnoreApi = true)]
[Route("api/[controller]")]
[ApiController]
public class AccountController : ControllerBase
{
    private readonly ApplicationDbContext _context;
    private readonly IEmailSenderService _emailSender;

    public AccountController(ApplicationDbContext context, IEmailSenderService emailSender)
    {
        _context = context;
        _emailSender = emailSender;
    }

    [AllowAnonymous]
    [HttpPost("authenticate")]
    public async Task<IActionResult> Authenticate()
    {
        // Check HTTP basic authorization
        if (!Authorize(Request))
        {
            Console.WriteLine("HTTP basic authentication validation failed.");
            return new UnauthorizedResult();
        }

        // Get the request body
        string requestBody = await new StreamReader(Request.Body).ReadToEndAsync();
        dynamic data = JsonConvert.DeserializeObject(requestBody);

        // If input data is null, show block page
        if (data == null)
        {
            return new OkObjectResult(new ResponseContent("ShowBlockPage", "There was a problem with your request."));
        }

        // Print out the request body
        Console.WriteLine("Request body: " + requestBody);

        // Get the current user language 
        string language = (data.ui_locales == null || data.ui_locales.ToString() == "") ? "default" : data.ui_locales.ToString();
        Console.WriteLine($"Current language: {language}");

        // If email claim not found, show block page. Email is required and sent by default.
        if (data.email == null || data.email.ToString() == "" || data.email.ToString().Contains("@") == false)
        {
            return new OkObjectResult(new ResponseContent("ShowBlockPage", "Email name is mandatory."));
        }

        // Get domain of email address
        string domain = data.email.ToString().Split("@")[1].ToLower();

        // set default role to Visitor
        string role = "Visitor";

        //set default customerId to empty
        string customerId = null;

        //set default company name
        string companyName = "NA";

        // Check the domain in the allowed list
        var allowedDomains = _context.DistributorAuthorizedDomains.ToList();

        foreach (var allowedDomain in allowedDomains)
        {
            if (allowedDomain.Domain.ToLower() == domain)
            {
                customerId = allowedDomain.CustomerId;
                companyName = allowedDomain.CompanyName;
                if (domain == "caltestelectronics.com")
                {
                    role = "Administrator";
                    break;
                }
                role = "Distributor";
                break;
                //return new OkObjectResult(new ResponseContent("ShowBlockPage", $"You must have an email account from an authorized distributor to register."));
            }
        }
        //thread 1
        //call but dont await
        //send email to distributor
        _ = Task.Run(async () =>
           {
               //Thread2
               if (role == "Distributor")
               {
                   await _emailSender.SendEmailAsync(data.email.ToString(), "Account Pre-Authorization", $"Hi {data.givenName.ToString()} {data.surname.ToString()}, you were pre-authorized. Thank you, {companyName}, for being a partner.<br><br> <a href='{HtmlEncoder.Default.Encode("https://www.caltestelectronics.com/distributor-access")}'>Click here to login</a>.");
                   await _emailSender.SendEmailAsync("marco@caltestelectronics.com", "Account Pre-Authorization", $"Hi there, {data.givenName.ToString()} {data.surname.ToString()}, {data.email.ToString()}, {customerId}, {companyName} was pre-authorized. Notification was sent to them.<br><br> <a href='{HtmlEncoder.Default.Encode("https://www.caltestelectronics.com/administrator/AdminDistributorAccessUserViewPage")}'>Click here to manage users</a>.");
                   await _emailSender.SendEmailAsync("mchanner@caltestelectronics.com", "Account Pre-Authorization", $"Hi there, {data.givenName.ToString()} {data.surname.ToString()}, {data.email.ToString()}, {customerId}, {companyName} was pre-authorized. Notification was sent to them.<br><br> <a href='{HtmlEncoder.Default.Encode("https://www.caltestelectronics.com/administrator/AdminDistributorAccessUserViewPage")}'>Click here to manage users</a>.");
               }
               else if (role == "Administrator")
               {
                   await _emailSender.SendEmailAsync("mchanner@caltestelectronics.com", "Account Admin-Authorization", $"Hi there, {data.givenName.ToString()} {data.surname.ToString()}, {data.email.ToString()}, {customerId}, {companyName} was pre-authorized to be an Admin. Notification was sent to them.<br><br> <a href='{HtmlEncoder.Default.Encode("https://www.caltestelectronics.com/administrator/AdminDistributorAccessUserViewPage")}'>Click here to manage users</a>.");
               }
               else if (role == "Visitor")
               {
                   await _emailSender.SendEmailAsync(data.email.ToString(), "Account Authorization", $"Hi {data.givenName.ToString()} {data.surname.ToString()}, thank you, for registering. Please wait while we verify your distributor account. This can take a few days.");
                   await _emailSender.SendEmailAsync("marco@caltestelectronics.com", "Account Need-Authorization", $"Hi there, {data.givenName.ToString()} {data.surname.ToString()}, {data.email.ToString()}, {customerId}, {companyName} needs to be authorized. Either they used a public domain or are not on our Authorized Distributor List. Notification was sent to them.<br><br> <a href='{HtmlEncoder.Default.Encode("https://www.caltestelectronics.com/administrator/AdminDistributorAccessUserViewPage")}'>Click here to manage users</a>.");
                   await _emailSender.SendEmailAsync("mchanner@caltestelectronics.com", "Account Need-Authorization", $"Hi there, {data.givenName.ToString()} {data.surname.ToString()}, {data.email.ToString()}, {customerId}, {companyName} needs to be authorized. Either they used a public domain or are not on our Authorized Distributor List. Notification was sent to them.<br><br> <a href='{HtmlEncoder.Default.Encode("https://www.caltestelectronics.com/administrator/AdminDistributorAccessUserViewPage")}'>Click here to manage users</a>.");
               }
           });
      //thread ?

        if (data.givenName == null || data.givenName.ToString().Length < 3 || data.surname == null || data.surname.ToString().Length < 3)
        {
            return new BadRequestObjectResult(new ResponseContent("ValidationError", "Please provide a First Name or Last Name with at least three characters."));
        }

        string dName = data.givenName.ToString() + " " + data.surname.ToString();

        // Input validation passed successfully, return `Allow` response.
        // TO DO: Configure the claims you want to return
        return new OkObjectResult(new ResponseContent()
        {
            extension_6df63c96d879496db5c865162950daec_CustomerId = customerId,
            extension_6df63c96d879496db5c865162950daec_Role = role,
            companyName = companyName,
            displayName = dName
            // You can also return custom claims using extension properties.
            //extension_CustomClaim = "my custom claim response"
        });
    }
    public class ResponseContent
    {
        public const string ApiVersion = "1.0.0";

        public ResponseContent()
        {
            this.version = ApiVersion;
            this.action = "Continue";
        }

        public ResponseContent(string action, string userMessage)
        {
            this.version = ApiVersion;
            this.action = action;
            this.userMessage = userMessage;
            if (action == "ValidationError")
            {
                this.status = "400";
            }
        }

        public string version { get; }
        public string action { get; set; }


        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string userMessage { get; set; }


        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string status { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string extension_6df63c96d879496db5c865162950daec_CustomerId { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string extension_6df63c96d879496db5c865162950daec_Role { get; set; }


        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string companyName { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string displayName { get; set; }


        //[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        //public string extension_CustomClaim { get; set; }
    }

    private static bool Authorize(HttpRequest req)
    {
        // Get the environment's credentials 
        string username = "mchanner"; //System.Environment.GetEnvironmentVariable("BASIC_AUTH_USERNAME", EnvironmentVariableTarget.Process);
        string password = "M@s0n123"; //System.Environment.GetEnvironmentVariable("BASIC_AUTH_PASSWORD", EnvironmentVariableTarget.Process);

        // Returns authorized if the username is empty or not exists.
        if (string.IsNullOrEmpty(username))
        {
            Console.WriteLine("HTTP basic authentication is not set.");
            return true;
        }

        // Check if the HTTP Authorization header exist
        if (!req.Headers.ContainsKey("Authorization"))
        {
            Console.WriteLine("Missing HTTP basic authentication header.");
            return false;
        }

        // Read the authorization header
        var auth = req.Headers["Authorization"].ToString();

        // Ensure the type of the authorization header id `Basic`
        if (!auth.StartsWith("Basic "))
        {
            Console.WriteLine("HTTP basic authentication header must start with 'Basic '.");
            return false;
        }

        // Get the the HTTP basinc authorization credentials
        var cred = System.Text.UTF8Encoding.UTF8.GetString(Convert.FromBase64String(auth.Substring(6))).Split(':');

        // Evaluate the credentials and return the result
        return (cred[0] == username && cred[1] == password);
    }
}
