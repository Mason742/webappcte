﻿using CTEWebApp.Shared.Helpers;
using MimeKit;

namespace CTEWebApp.Server.Helpers;

public static class WebFormEmailGenerator
{
    private readonly static string HeaderCT = "<tr><td valign=\"top\" style=\"vertical-align: top;\"><a title=\"Cal Test Electronics\" target=\"_blank\" href=\"http://www.caltestelectronics.com\"><img src=\"https://as.caltestelectronics.com/public/images/newsletter/cte_header.jpg\" alt=\"Cal Test Electronics\" title=\"Cal Test Electronics\" border=\"0\" style=\"display: block; margin: 0;\" /></a></td></tr>";
    private readonly static string FooterCT = "<tr><td valign=\"top\" style=\"vertical-align: top;\"><a title=\"Cal Test Electronics\" target=\"_blank\" href=\"http://www.caltestelectronics.com\"><img src=\"https://as.caltestelectronics.com/public/images/newsletter/cte_footer.jpg\" alt=\"Global Specialties\" title=\"Cal Test Electronics\" border=\"0\" style=\"display: block; margin: 0;\" /></a></td></tr>";

    private readonly static string HeaderGS = "<tr><td valign=\"top\" style=\"vertical-align: top;\"><a title=\"Global Specialties\" target=\"_blank\" href=\"http://www.globalspecialties.com\"><img src=\"https://as.caltestelectronics.com/public/images/newsletter/gs_header.jpg\" alt=\"Global Specialties\" title=\"Global Specialties\" border=\"0\" style=\"display: block; margin: 0;\"/></a></td></tr>";
    private readonly static string FooterGS = "<tr><td valign=\"top\" style=\"vertical-align: top;\"><a title=\"Global Specialties\" target=\"_blank\" href=\"http://www.globalspecialties.com\"><img src=\"https://as.caltestelectronics.com/public/images/newsletter/gs_footer.jpg\" alt=\"Global Specialties\" title=\"Global Specialties\" border=\"0\" style=\"display: block; margin: 0;\"/></a></td></tr>";

    private readonly static string ProductCatalogs = "";
    
    //private readonly static string SocialMedia = "<tr><td style=\"height: 20px;color:#THEME_PLACEHOLDER_TEXTCOLOR;font-size:18px;\"><strong>SOCIAL MEDIA</strong></td></tr><tr><td style=\"height: 20px;\">&nbsp;</td></tr><tr> <td> <table border=\"0\" align=\"left\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#FFFFFF\" style=\"color:#646464;\"> <tr><td width=\"324\">CAL TEST ELECTRONICS</td><td width=\"324\">GLOBAL SPECIALTIES</td></tr></table> </td></tr><tr> <td style=\"height: 20px;\"> <table border=\"0\" align=\"left\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#FFFFFF\"> <tr> <td width=\"324\"> <table border=\"0\" align=\"left\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#FFFFFF\"> <tr><td style=\"height: 20px;\">&nbsp;</td></tr><tr> <td width=\"40\"> <a href=\"http://goo.gl/pk9aEd\" target=\"_blank\" title=\"Cal Test Electronics Facebook Page\"> <img src=\"https://as.caltestelectronics.com/public/images/newsletter/facebook.png\" alt=\"Cal Test Electronics Facebook Page\" title=\"Cal Test Electronics Facebook Page\" border=\"0\" style=\"display: block; margin: 0;\"/> </a> </td><td width=\"40\"> <a href=\"http://goo.gl/JnRdeE\" target=\"_blank\" title=\"Cal Test Electronics Twitter Page\"> <img src=\"https://caltestelectronics.com/public/images/newsletter/twitter.png\" alt=\"Cal Test Electronics Twitter Page\" title=\"Cal Test Electronics Twitter Page\" border=\"0\" style=\"display: block; margin: 0;\"/> </a> </td><td width=\"40\"> <a href=\"http://goo.gl/m6pKNz\" target=\"_blank\" title=\"Cal Test Electronics LinkedIn Page\"> <img src=\"https://caltestelectronics.com/public/images/newsletter/linkedin.png\" alt=\"Cal Test Electronics LinkedIn Page\" title=\"Cal Test Electronics LinkedIn Page\" border=\"0\" style=\"display: block; margin: 0;\"/> </a> </td><td width=\"40\"> <a href=\"http://goo.gl/OuvhnF\" target=\"_blank\" title=\"Cal Test Electronics YouTube Page\"> <img src=\"https://caltestelectronics.com/public/images/newsletter/youtube.png\" alt=\"Cal Test Electronics YouTube Page\" title=\"Cal Test Electronics YouTube Page\" border=\"0\" style=\"display: block; margin: 0;\"/> </a> </td></tr></table> </td><td> <table border=\"0\" align=\"right\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#FFFFFF\"> <tr><td style=\"height: 20px;\">&nbsp;</td></tr><tr> <td width=\"40\"> <a href=\"http://goo.gl/Hy7eHV\" target=\"_blank\" title=\"Global Specialites Facebook Page\"> <img src=\"https://caltestelectronics.com/public/images/newsletter/facebook.png\" alt=\"Global Specialites Facebook Page\" title=\"Global Specialites Facebook Page\" border=\"0\" style=\"display: block; margin: 0;\"/> </a> </td><td width=\"40\"> <a href=\"http://goo.gl/SrjtpB\" target=\"_blank\" title=\"Global Specialites Twitter Page\"> <img src=\"https://caltestelectronics.com/public/images/newsletter/twitter.png\" alt=\"Global Specialites Twitter Page\" title=\"Global Specialites Twitter Page\" border=\"0\" style=\"display: block; margin: 0;\"/> </a> </td><td width=\"40\"> <a href=\"http://goo.gl/mEu4gR\" target=\"_blank\" title=\"Global Specialites LinkedIn Page\"> <img src=\"https://caltestelectronics.com/public/images/newsletter/linkedin.png\" alt=\"Global Specialites LinkedIn Page\" title=\"Global Specialites LinkedIn Page\" border=\"0\" style=\"display: block; margin: 0;\"/> </a> </td><td width=\"40\"> <a href=\"http://goo.gl/YiuVJQ\" target=\"_blank\" title=\"Global Specialites YouTube Page\"> <img src=\"https://caltestelectronics.com/public/images/newsletter/youtube.png\" alt=\"Global Specialites YouTube Page\" title=\"Global Specialites YouTube Page\" border=\"0\" style=\"display: block; margin: 0;\"/> </a> </td></tr></table> </td></tr></table> </td></tr><tr><td style=\"height: 20px;\">&nbsp;</td></tr>";

    private readonly static string FromName = "No Reply - Cal Test Electronics";
    private readonly static string FromEmail = "noreply@caltestelectronics.com";

    public static MimeMessage GenerateContactEmail(ContactForm contactForm, HubSpotOwner hubSpotOwner)
    {


        var fullName = contactForm.FirstName + " " + contactForm.LastName;

        //create message
        var message = DefaultMimeMessage(hubSpotOwner, fullName, contactForm.Email);

        var bodyBuilder = new BodyBuilder();

        message.Subject = $"{hubSpotOwner.FormattedTicketCategory} - {BrandHelper.GetNameById(contactForm.Brand)}";

        var template = GetHeaderFooterBranding(contactForm.Brand);

        bodyBuilder.HtmlBody = "<table width=\"648\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"color:#646464;background-color:#FFFFFF;font-size:16px;line-height:20px;font-family:Verdana, Geneva, sans-serif;\"><tbody> "
            + template.Header
            + " <tr><td style=\"height: 20px;\">&nbsp;</td></tr><tr> <td style=\"height: 20px;font-size:20px;\"><strong>CONTACT FORM</strong></td></tr><tr> <td style=\"height: 10px;font-size:18px;\"><strong>"
            + hubSpotOwner.FormattedTicketCategory
            + "</tr><tr><td style=\"height: 20px;\">&nbsp;</td></tr><tr> <td style=\"height: 20px;font-size:20px;\"> <table border=\"0\" align=\"center\" cellpadding=\"10\" cellspacing=\"0\" bgcolor=\"#FFFFFF\" style=\"font-size:18px;color:#646464;border: 2px solid #EAEAEA;\"> <tr><td style=\"color:#THEME_PLACEHOLDER_TEXTCOLOR;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>NAME</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
            + (contactForm.FirstName + " " + contactForm.LastName) + "</td></tr><tr style=\"background-color:#FAFAFA;\"><td style=\"color:#THEME_PLACEHOLDER_TEXTCOLOR;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>E-MAIL</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
            + contactForm.Email + "</td></tr><tr><td style=\"color:#THEME_PLACEHOLDER_TEXTCOLOR;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>PHONE</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
            + contactForm.Phone + "</td></tr><tr style=\"background-color:#FAFAFA;\"><td style=\"color:#THEME_PLACEHOLDER_TEXTCOLOR;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>COMPANY</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
            + contactForm.Company + "</td></tr><tr style=\"background-color:#FAFAFA;\"><td style=\"color:#THEME_PLACEHOLDER_TEXTCOLOR;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>CITY</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
            + contactForm.City + "</td></tr><tr><td style=\"color:#THEME_PLACEHOLDER_TEXTCOLOR;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>STATE</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
            + contactForm.StateOrProvince + "</td></tr><tr><td style=\"color:#THEME_PLACEHOLDER_TEXTCOLOR;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>COUNTRY</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
            + contactForm.Country + "</td></tr><tr style=\"background-color:#FAFAFA;\"><td style=\"color:#THEME_PLACEHOLDER_TEXTCOLOR;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>SUBJECT</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
            + contactForm.ContactReasonChoice.Replace("_", " ") + "</td></tr><tr><td style=\"color:#THEME_PLACEHOLDER_TEXTCOLOR;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>MESSAGE</strong></td><td width=\"418\">"
            + contactForm.Message + "</td></tr></table></td></tr><tr><td style=\"height: 20px;\">&nbsp;</td></tr><tr><td style=\"height: 20px;\">Thank you for contacting us. Here is a copy of your request for your records. We will get back to you shortly.</td></tr><tr><td style=\"height: 20px;\">&nbsp;</td></tr>"
            + ProductCatalogs + "<tr><td style=\"height: 20px;\">&nbsp;</td></tr>"
            + template.Footer
            + " </tbody></table>";
        bodyBuilder.HtmlBody = SetHtmlBodyBranding(bodyBuilder.HtmlBody, contactForm.Brand);
        bodyBuilder.TextBody = "Thank you for contacting us. Here is a copy of your request for your records. We will get back to you shortly.";
        message.Body = bodyBuilder.ToMessageBody();

        return message;

    }

    public static (string Header, string Footer) GetHeaderFooterBranding(string brand)
    {
        if (brand == BrandHelper.CT.Id)
        {
            return (Header: HeaderCT, Footer: FooterCT);
        }
        else if (brand == BrandHelper.GS.Id)
        {
            return (Header: HeaderGS, Footer: FooterGS);
        }
        else if (brand == BrandHelper.BK.Id)
        {
            throw new NotImplementedException();
        }
        throw new NotImplementedException();
    }

    public static string SetHtmlBodyBranding(string htmlBody, string brand)
    {
        if (brand == BrandHelper.CT.Id)
        {
            return htmlBody.Replace("#THEME_PLACEHOLDER_TEXTCOLOR", "#D21F3D");
        }
        else if (brand == BrandHelper.GS.Id)
        {
            return htmlBody.Replace("#THEME_PLACEHOLDER_TEXTCOLOR", "#005DAB");
        }
        else if (brand == BrandHelper.BK.Id)
        {
            throw new NotImplementedException();
        }
        return htmlBody;
    }

    public static MimeMessage GenerateSupportEmail(RmaForm rmaForm, HubSpotOwner hubSpotOwner)
    {
        var fullName = rmaForm.FirstName + " " + rmaForm.LastName;

        //create message
        var message = DefaultMimeMessage(hubSpotOwner, fullName, rmaForm.Email);

        //create message
        var bodyBuilder = new BodyBuilder();

        message.Subject = $"{hubSpotOwner.FormattedTicketCategory} - {BrandHelper.GetNameById(rmaForm.Brand)}";

        var template = GetHeaderFooterBranding(rmaForm.Brand);


        bodyBuilder.HtmlBody = "<table width=\"648\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"color:#646464;background-color:#FFFFFF;font-size:16px;line-height:20px;font-family:Verdana, Geneva, sans-serif;\" > <tbody>"
            + template.Header + "<tr><td style=\"height: 20px;\">&nbsp;</td></tr><tr> <td style=\"height: 20px;font-size:20px;text-transform: uppercase;\"><strong>"
            + hubSpotOwner.FormattedTicketCategory + "</strong></td></tr><tr><td style=\"height: 10px;font-size:18px;\"><strong>"
            + hubSpotOwner.FormattedTicketCategory + "</strong></td></tr><tr><td style=\"height: 20px;\">&nbsp;</td></tr><tr> <td style=\"height: 20px;font-size:20px;\"> <table border=\"0\" align=\"center\" cellpadding=\"10\" cellspacing=\"0\" bgcolor=\"#FFFFFF\" style=\"font-size:18px;color:#646464;border: 2px solid #EAEAEA;\"> <tr><td style=\"color:#THEME_PLACEHOLDER_TEXTCOLOR;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>NAME</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
            + (rmaForm.FirstName + rmaForm.LastName) + "</td></tr><tr style=\"background-color:#FAFAFA;\"><td style=\"color:#THEME_PLACEHOLDER_TEXTCOLOR;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>COMPANY</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
            + rmaForm.Company + "</td></tr><tr><td style=\"color:#THEME_PLACEHOLDER_TEXTCOLOR;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>E-MAIL</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
            + rmaForm.Email + "</td></tr><tr style=\"background-color:#FAFAFA;\"><td style=\"color:#THEME_PLACEHOLDER_TEXTCOLOR;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>PHONE</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
            + rmaForm.Phone + "</td></tr><tr><td style=\"color:#THEME_PLACEHOLDER_TEXTCOLOR;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>SUBJECT</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
            + hubSpotOwner.FormattedTicketCategory + "</td></tr><tr style=\"background-color:#FAFAFA;\"><td style=\"color:#THEME_PLACEHOLDER_TEXTCOLOR;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>TYPE OF SERVICE</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
            + hubSpotOwner.FormattedTicketCategory + "</td></tr><tr><td style=\"color:#THEME_PLACEHOLDER_TEXTCOLOR;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>MODEL NUMBER</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
            + rmaForm.PartNumber + "</td></tr><tr style=\"background-color:#FAFAFA;\"><td style=\"color:#THEME_PLACEHOLDER_TEXTCOLOR;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>QUANTITY</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
            + rmaForm.Quantity + "</td></tr><tr><td style=\"color:#THEME_PLACEHOLDER_TEXTCOLOR;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>SERIAL NUMBER</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
            + rmaForm.SerialNumber + "</td></tr><tr style=\"background-color:#FAFAFA;\"><td style=\"color:#THEME_PLACEHOLDER_TEXTCOLOR;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>PRODUCT UNDER WARRANTY</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
            + /*rmaForm.HasWarranty*/"" + "</td></tr><tr style=\"background-color:#FAFAFA;\"><td style=\"color:#THEME_PLACEHOLDER_TEXTCOLOR;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>STREET</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
            + rmaForm.Address1 + "</td></tr><tr><td style=\"color:#THEME_PLACEHOLDER_TEXTCOLOR;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>CITY</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
            + rmaForm.City + "</td></tr><tr style=\"background-color:#FAFAFA;\"><td style=\"color:#THEME_PLACEHOLDER_TEXTCOLOR;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>STATE</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
            + rmaForm.StateOrProvince + "</td></tr><tr><td style=\"color:#THEME_PLACEHOLDER_TEXTCOLOR;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>ZIP CODE</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
            + rmaForm.PostalCode + "</td></tr><tr style=\"background-color:#FAFAFA;\"><td style=\"color:#THEME_PLACEHOLDER_TEXTCOLOR;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>COUNTRY</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
            + rmaForm.Country + "</td></tr><tr> <td style=\"color:#THEME_PLACEHOLDER_TEXTCOLOR;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>PROBLEM DESCRIPTION</strong></td><td style=\"border-bottom: 2px solid #EAEAEA;\" width=\"418\">"
            + rmaForm.ProblemDescription + "</td></tr>" + "<tr><td style=\"color: #THEME_PLACEHOLDER_TEXTCOLOR;border-bottom: 2px solid #EAEAEA; border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>RECEIPT</strong></td><td style=\"border-bottom: 2px solid #EAEAEA;\" width=\"418\"><a href=\""
            + rmaForm.ProofOfPurchaseFileLink + "\">"
            + Path.GetFileName(rmaForm.ProofOfPurchaseFileLink)
            + "</a></td>"
            + "</table> </td></tr><tr><td style=\"height: 20px;\">&nbsp;</td></tr><tr><td style=\"height: 20px;\">Thank you for contacting us. Here is a copy of your request for your records. We will get back to you shortly.</td></tr><tr><td style=\"height: 20px;\">&nbsp;</td></tr>"
            + ProductCatalogs + "<tr><td style=\"height: 20px;\">&nbsp;</td></tr>"
            + template.Footer + "</tbody></table>";

        bodyBuilder.HtmlBody = SetHtmlBodyBranding(bodyBuilder.HtmlBody, rmaForm.Brand);
        bodyBuilder.TextBody = "Thank you for contacting us. Here is a copy of your request for your records. We will get back to you shortly.";
        message.Body = bodyBuilder.ToMessageBody();
        return message;
    }

    private static MimeMessage DefaultMimeMessage(HubSpotOwner leadPerson, string toFullName, string toEmail)
    {


        //create mailing list
        InternetAddressList list = new()
        {
            new MailboxAddress(toFullName, toEmail)
        };

        //create message
        var message = new MimeMessage();
        message.From.Add(new MailboxAddress(FromName, FromEmail));
        message.To.AddRange(list);
        message.Bcc.Add(new MailboxAddress(leadPerson.MicrosoftTeamsChannelName, leadPerson.MicrosoftTeamsEmail));
        message.Bcc.Add(new MailboxAddress(leadPerson.Title, leadPerson.Email));
        message.Bcc.Add(new MailboxAddress("HubSpot Logger", "14566053@bcc.hubspot.com"));

        return message;
    }

    public static MimeMessage GenerateTestLeadBuilderEmailFormEmail(TestLeadBuilderEmailFormDTO form, HubSpotOwner hubSpotOwner, bool standalone = false)
    {

        //set fullname
        var fullname = (form.FirstName + " " + form.LastName);

        var message = DefaultMimeMessage(hubSpotOwner, fullname, form.Email);
        message.Subject = $"{hubSpotOwner.FormattedTicketCategory} - {BrandHelper.CT.Name}";

        var bodyBuilder = new BodyBuilder();


        if (standalone)
        {
            message.Subject = "E-mail Results - Cal Test Electronics";
            bodyBuilder.HtmlBody = "<table width=\"648\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"color:#646464;background-color:#FFFFFF;font-size:16px;line-height:20px;font-family:Verdana, Geneva, sans-serif;\"><tbody>"
                + HeaderCT + "<tr><td style=\"height: 20px;\">&nbsp;</td></tr><tr><td style=\"height: 20px;font-size:20px;\"><strong>TEST LEAD BUILDER RESULTS</strong></td></tr><tr><td style=\"height: 20px;\">&nbsp;</td></tr><tr><td style=\"height: 20px;\">"
                + "Below are the results of our Test Lead Builder, submitted from Digi-Key website." + "</td></tr><tr><td style=\"height: 20px;\">&nbsp;</td></tr><tr><td style=\"height: 20px;font-size:20px;\"><table border=\"0\" align=\"center\" cellpadding=\"10\" cellspacing=\"0\" bgcolor=\"#FFFFFF\" style=\"font-size:18px;color:#646464;border: 2px solid #EAEAEA;\"><tr><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong style=\"text-decoration:underline;\">CUSTOMER</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">&nbsp;</td></tr><tr style=\"background-color:#FAFAFA;\"><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>NAME</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + (form.FirstName + " " + form.LastName) + "</td></tr><tr><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>E-MAIL</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + form.Email + "</td><tr><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>MESSAGE</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + "" + "</td></tr><tr><td style=\"color:#D21F3D; border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\">&nbsp;</td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">&nbsp;</td></tr><tr><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong style=\"text-decoration:underline;\">CUSTOM PRODUCT</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">&nbsp;</td></tr><tr style=\"background-color:#FAFAFA;\"><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>PART #</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + form.TestLeadBuilderResult.PartNumber + "</td></tr><tr><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>MSRP PER UNIT</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + " " + "</td></tr><tr><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>LEAD TIME</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + form.TestLeadBuilderResult.LeadTime + " </td></tr><tr><td style=\"border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\">&nbsp;</td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">&nbsp;</td></tr><tr><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong style=\"text-decoration:underline;\">BUILDER SETTINGS</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">&nbsp;</td></tr><tr style=\"background-color:#FAFAFA;\"><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>CONNECTOR #1</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + form.TestLeadBuilderResult.ConnectorOne + "</td></tr><tr><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>CONNECTOR #2</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + form.TestLeadBuilderResult.ConnectorTwo + "</td></tr><tr style=\"background-color:#FAFAFA;\"><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>JACKET & GAUGE</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + (form.TestLeadBuilderResult.WireJacket + " (" + form.TestLeadBuilderResult.WireGauge + ")") + "</td></tr><tr><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>VOLTAGE & CURRENT</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + (form.TestLeadBuilderResult.Voltage + " - " + form.TestLeadBuilderResult.Current + " Amps") + "</td></tr><tr style=\"background-color:#FAFAFA;\"><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>LENGTH</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + form.TestLeadBuilderResult.Length + "</td></tr><tr><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>COLOR</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + form.TestLeadBuilderResult.Color + "</td></tr><tr><td style=\"height: 20px;\">&nbsp;</td></tr></tbody></table><tr><td style=\"height: 20px;\"><p>A diagram of the results is attached to this e-mail.</p></td></tr><tr> <td style=\"height: 20px;\"><p>For any questions you might have, please contact:</p><p> Sales Department <br/>Cal Test Electronics<br/><a href=\"mailto:sales@caltestelectronics.com\">sales@caltestelectronics.com</a><br/></p></td></tr><tr><td style=\"height: 20px;\">&nbsp;</td></tr>"
                + ProductCatalogs + "<tr><td style=\"height: 20px;\">&nbsp;</td></tr>"
                + FooterCT + "</tbody></table>";
        }
        else if (standalone == false)
        {
            message.Subject = "E-mail Results - Cal Test Electronics";
            bodyBuilder.HtmlBody = "<table width=\"648\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"color:#646464;background-color:#FFFFFF;font-size:16px;line-height:20px;font-family:Verdana, Geneva, sans-serif;\"><tbody>"
                + HeaderCT + "<tr><td style=\"height: 20px;\">&nbsp;</td></tr><tr><td style=\"height: 20px;font-size:20px;\"><strong>TEST LEAD BUILDER RESULTS</strong></td></tr><tr><td style=\"height: 20px;\">&nbsp;</td></tr><tr><td style=\"height: 20px;\">"
                + "Below are the results of our Test Lead Builder." + "</td></tr><tr><td style=\"height: 20px;\">&nbsp;</td></tr><tr><td style=\"height: 20px;font-size:20px;\"><table border=\"0\" align=\"center\" cellpadding=\"10\" cellspacing=\"0\" bgcolor=\"#FFFFFF\" style=\"font-size:18px;color:#646464;border: 2px solid #EAEAEA;\"><tr><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong style=\"text-decoration:underline;\">CUSTOMER</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">&nbsp;</td></tr><tr style=\"background-color:#FAFAFA;\"><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>NAME</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + (form.FirstName + " " + form.LastName) + "</td></tr><tr><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>E-MAIL</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + form.Email + "</td><tr><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>MESSAGE</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + "" + "</td></tr><tr><td style=\"color:#D21F3D; border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\">&nbsp;</td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">&nbsp;</td></tr><tr><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong style=\"text-decoration:underline;\">CUSTOM PRODUCT</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">&nbsp;</td></tr><tr style=\"background-color:#FAFAFA;\"><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>PART #</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + form.TestLeadBuilderResult.PartNumber + "</td></tr><tr><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>MSRP PER UNIT</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + form.TestLeadBuilderResult.MSRP + "</td></tr><tr><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>LEAD TIME</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + form.TestLeadBuilderResult.LeadTime + " </td></tr><tr><td style=\"border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\">&nbsp;</td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">&nbsp;</td></tr><tr><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong style=\"text-decoration:underline;\">BUILDER SETTINGS</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">&nbsp;</td></tr><tr style=\"background-color:#FAFAFA;\"><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>CONNECTOR #1</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + form.TestLeadBuilderResult.ConnectorOne + "</td></tr><tr><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>CONNECTOR #2</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + form.TestLeadBuilderResult.ConnectorTwo + "</td></tr><tr style=\"background-color:#FAFAFA;\"><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>JACKET & GAUGE</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + (form.TestLeadBuilderResult.WireJacket + " (" + form.TestLeadBuilderResult.WireGauge + ")") + "</td></tr><tr><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>VOLTAGE & CURRENT</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + (form.TestLeadBuilderResult.Voltage + " - " + form.TestLeadBuilderResult.Current + " Amps") + "</td></tr><tr style=\"background-color:#FAFAFA;\"><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>LENGTH</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + form.TestLeadBuilderResult.Length + "</td></tr><tr><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>COLOR</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + form.TestLeadBuilderResult.Color + "</td></tr><tr><td style=\"height: 20px;\">&nbsp;</td></tr></tbody></table><tr><td style=\"height: 20px;\"><p>A diagram of the results is attached to this e-mail.</p></td></tr><tr> <td style=\"height: 20px;\"><p>For any questions you might have, please contact:</p><p> Sales Department <br/>Cal Test Electronics<br/><a href=\"mailto:sales@caltestelectronics.com\">sales@caltestelectronics.com</a><br/></p></td></tr><tr><td style=\"height: 20px;\">&nbsp;</td></tr>"
                + ProductCatalogs + "<tr><td style=\"height: 20px;\">&nbsp;</td></tr>"
                + FooterCT + "</tbody></table>";
        }
        bodyBuilder.TextBody = "Thank you for contacting us. Here is a copy of your request for your records. We will get back to you shortly.";
        message.Body = bodyBuilder.ToMessageBody();
        return message;

    }


    public static MimeMessage GenerateTestLeadBuilderQuoteFormEmail(TestLeadBuilderQuoteFormDTO form, HubSpotOwner hubSpotOwner, bool standalone = false)
    {

        //set fullname
        var fullname = form.FirstName + " " + form.LastName;

        var message = DefaultMimeMessage(hubSpotOwner, fullname, form.Email);

        var bodyBuilder = new BodyBuilder();

        message.Subject = $"{hubSpotOwner.FormattedTicketCategory} - {BrandHelper.CT.Name}";


        if (standalone)
        {
            bodyBuilder.HtmlBody = "<table width=\"648\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"color:#646464;background-color:#FFFFFF;font-size:16px;line-height:20px;font-family:Verdana, Geneva, sans-serif;\"><tbody>"
                + HeaderCT + "<tr><td style=\"height: 20px;\">&nbsp;</td></tr><tr><td style=\"height: 20px;font-size:20px;\"><strong>TEST LEAD BUILDER RESULTS</strong></td></tr><tr><td style=\"height: 20px;\">&nbsp;</td></tr><tr><td style=\"height: 20px;\">"
                + "Below are the results of our Test Lead Builder, submitted from Digi-Key website." + "</td></tr><tr><td style=\"height: 20px;\">&nbsp;</td></tr><tr><td style=\"height: 20px;font-size:20px;\"><table border=\"0\" align=\"center\" cellpadding=\"10\" cellspacing=\"0\" bgcolor=\"#FFFFFF\" style=\"font-size:18px;color:#646464;border: 2px solid #EAEAEA;\"><tr><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong style=\"text-decoration:underline;\">CUSTOMER</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">&nbsp;</td></tr><tr style=\"background-color:#FAFAFA;\"><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>NAME</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + (form.FirstName + " " + form.LastName) + "</td></tr><tr><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>E-MAIL</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + form.Email + "</td></tr><tr style=\"background-color:#FAFAFA;\"><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>PHONE</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + form.Phone + "</td></tr><tr><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>COMPANY</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + form.Company + "</td></tr><tr><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>MESSAGE</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + form.Message + "</td></tr><tr><td style=\"color:#D21F3D; border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\">&nbsp;</td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">&nbsp;</td></tr><tr><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong style=\"text-decoration:underline;\">SHIPPING ADDRESS</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">&nbsp;</td></tr><tr style=\"background-color:#FAFAFA;\"><td style=\"color:#D21F3D; border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>STREET</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + form.Address1 + "</td></tr><tr><td style=\"color:#D21F3D; border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>CITY</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + form.City + "</td></tr><tr style=\"background-color:#FAFAFA;\"><td style=\"color:#D21F3D; border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>STATE</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + form.StateOrProvince + "</td></tr><tr><td style=\"color:#D21F3D; border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>ZIP CODE</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + form.PostalCode + "</td></tr><tr style=\"background-color:#FAFAFA;\"><td style=\"color:#D21F3D; border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>COUNTRY</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + form.Country + "</td></tr><tr><td style=\"color:#D21F3D; border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\">&nbsp;</td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">&nbsp;</td></tr><tr><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong style=\"text-decoration:underline;\">CUSTOM PRODUCT</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">&nbsp;</td></tr><tr style=\"background-color:#FAFAFA;\"><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>PART #</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + form.TestLeadBuilderResult.PartNumber + "</td></tr><tr><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>MSRP PER UNIT</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + " " + "</td></tr><tr><td style=\"color:#D21F3D; border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>QUANTITY REQUESTED</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + form.Quantity + " </td></tr><tr><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>LEAD TIME</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + form.TestLeadBuilderResult.LeadTime + " </td></tr><tr><td style=\"border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\">&nbsp;</td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">&nbsp;</td></tr><tr><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong style=\"text-decoration:underline;\">BUILDER SETTINGS</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">&nbsp;</td></tr><tr style=\"background-color:#FAFAFA;\"><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>CONNECTOR #1</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + form.TestLeadBuilderResult.ConnectorOne + "</td></tr><tr><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>CONNECTOR #2</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + form.TestLeadBuilderResult.ConnectorTwo + "</td></tr><tr style=\"background-color:#FAFAFA;\"><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>JACKET & GAUGE</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + (form.TestLeadBuilderResult.WireJacket + " (" + form.TestLeadBuilderResult.WireGauge + ")") + "</td></tr><tr><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>VOLTAGE & CURRENT</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + (form.TestLeadBuilderResult.Voltage + " - " + form.TestLeadBuilderResult.Current + " Amps") + "</td></tr><tr style=\"background-color:#FAFAFA;\"><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>LENGTH</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + form.TestLeadBuilderResult.Length + "</td></tr><tr><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>COLOR</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + form.TestLeadBuilderResult.Color + "</td></tr><tr><td style=\"height: 20px;\">&nbsp;</td></tr></tbody></table><tr><td style=\"height: 20px;\"><p>Your request has been forwarded to a Cal Test Authorized Distributor.</p><p>A diagram of the results is attached to this e-mail.</p></td></tr><tr> <td style=\"height: 20px;\"><p>For any questions you might have, please contact:</p><p> Sales Department <br/>Cal Test Electronics<br/><a href=\"mailto:sales@caltestelectronics.com\">sales@caltestelectronics.com</a><br/></p></td></tr><tr><td style=\"height: 20px;\">&nbsp;</td></tr>"
                + ProductCatalogs + "<tr><td style=\"height: 20px;\">&nbsp;</td></tr>"
                + FooterCT + "</tbody></table>";
        }
        else if (standalone == false)
        {
            bodyBuilder.HtmlBody = "<table width=\"648\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"color:#646464;background-color:#FFFFFF;font-size:16px;line-height:20px;font-family:Verdana, Geneva, sans-serif;\"><tbody>"
                + HeaderCT + "<tr><td style=\"height: 20px;\">&nbsp;</td></tr><tr><td style=\"height: 20px;font-size:20px;\"><strong>TEST LEAD BUILDER RESULTS</strong></td></tr><tr><td style=\"height: 20px;\">&nbsp;</td></tr><tr><td style=\"height: 20px;\">"
                + "Below are the results of our Test Lead Builder." + "</td></tr><tr><td style=\"height: 20px;\">&nbsp;</td></tr><tr><td style=\"height: 20px;font-size:20px;\"><table border=\"0\" align=\"center\" cellpadding=\"10\" cellspacing=\"0\" bgcolor=\"#FFFFFF\" style=\"font-size:18px;color:#646464;border: 2px solid #EAEAEA;\"><tr><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong style=\"text-decoration:underline;\">CUSTOMER</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">&nbsp;</td></tr><tr style=\"background-color:#FAFAFA;\"><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>NAME</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + (form.FirstName + " " + form.LastName) + "</td></tr><tr><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>E-MAIL</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + form.Email + "</td></tr><tr style=\"background-color:#FAFAFA;\"><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>PHONE</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + form.Phone + "</td></tr><tr><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>COMPANY</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + form.Company + "</td></tr><tr><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>MESSAGE</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + form.Message + "</td></tr><tr><td style=\"color:#D21F3D; border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\">&nbsp;</td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">&nbsp;</td></tr><tr><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong style=\"text-decoration:underline;\">SHIPPING ADDRESS</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">&nbsp;</td></tr><tr style=\"background-color:#FAFAFA;\"><td style=\"color:#D21F3D; border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>STREET</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + form.Address1 + "</td></tr><tr><td style=\"color:#D21F3D; border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>CITY</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + form.City + "</td></tr><tr style=\"background-color:#FAFAFA;\"><td style=\"color:#D21F3D; border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>STATE</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + form.StateOrProvince + "</td></tr><tr><td style=\"color:#D21F3D; border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>ZIP CODE</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + form.PostalCode + "</td></tr><tr style=\"background-color:#FAFAFA;\"><td style=\"color:#D21F3D; border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>COUNTRY</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + form.Country + "</td></tr><tr><td style=\"color:#D21F3D; border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\">&nbsp;</td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">&nbsp;</td></tr><tr><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong style=\"text-decoration:underline;\">CUSTOM PRODUCT</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">&nbsp;</td></tr><tr style=\"background-color:#FAFAFA;\"><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>PART #</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + form.TestLeadBuilderResult.PartNumber + "</td></tr><tr><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>MSRP PER UNIT</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + form.TestLeadBuilderResult.MSRP + "</td></tr><tr><td style=\"color:#D21F3D; border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>QUANTITY REQUESTED</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + form.Quantity + " </td></tr><tr><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>LEAD TIME</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + form.TestLeadBuilderResult.LeadTime + " </td></tr><tr><td style=\"border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\">&nbsp;</td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">&nbsp;</td></tr><tr><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong style=\"text-decoration:underline;\">BUILDER SETTINGS</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">&nbsp;</td></tr><tr style=\"background-color:#FAFAFA;\"><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>CONNECTOR #1</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + form.TestLeadBuilderResult.ConnectorOne + "</td></tr><tr><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>CONNECTOR #2</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + form.TestLeadBuilderResult.ConnectorTwo + "</td></tr><tr style=\"background-color:#FAFAFA;\"><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>JACKET & GAUGE</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + (form.TestLeadBuilderResult.WireJacket + " (" + form.TestLeadBuilderResult.WireGauge + ")") + "</td></tr><tr><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>VOLTAGE & CURRENT</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + (form.TestLeadBuilderResult.Voltage + " - " + form.TestLeadBuilderResult.Current + " Amps") + "</td></tr><tr style=\"background-color:#FAFAFA;\"><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>LENGTH</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + form.TestLeadBuilderResult.Length + "</td></tr><tr><td style=\"color:#D21F3D;border-bottom: 2px solid #EAEAEA;border-right: 2px solid #EAEAEA;\" width=\"250\"><strong>COLOR</strong></td><td width=\"418\" style=\"border-bottom: 2px solid #EAEAEA;\">"
                + form.TestLeadBuilderResult.Color + "</td></tr><tr><td style=\"height: 20px;\">&nbsp;</td></tr></tbody></table><tr><td style=\"height: 20px;\"><p>Your request has been forwarded to a Cal Test Authorized Distributor.</p><p>A diagram of the results is attached to this e-mail.</p></td></tr><tr> <td style=\"height: 20px;\"><p>For any questions you might have, please contact:</p><p> Sales Department <br/>Cal Test Electronics<br/><a href=\"mailto:sales@caltestelectronics.com\">sales@caltestelectronics.com</a><br/></p></td></tr><tr><td style=\"height: 20px;\">&nbsp;</td></tr>"
                + ProductCatalogs + "<tr><td style=\"height: 20px;\">&nbsp;</td></tr>"
                + FooterCT + "</tbody></table>";
        }
        bodyBuilder.TextBody = "Thank you for contacting us. Here is a copy of your request for your records. We will get back to you shortly.";
        message.Body = bodyBuilder.ToMessageBody();
        return message;
    }

}
