using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.OData;
using Microsoft.AspNetCore.OData.Batch;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Identity.Web;
using Microsoft.Graph;
using Azure.Identity;
using Microsoft.OpenApi.Models;
using EmailSenderService.Services;
using EmailSenderService.Models;
using System.Text.Json.Serialization;
using StorageService.Services;
using ImageResizerService.Services;
using HubSpotService.Services;
using CTEWebApp.Server.Logging.DbLoggerObjects;
using Microsoft.Extensions.Logging.AzureAppServices;
using Microsoft.AspNetCore.Mvc;

var builder = Microsoft.AspNetCore.Builder.WebApplication.CreateBuilder(args);


builder.Services.AddResponseCaching();

// Add services to the container.
Console.WriteLine(builder.Environment.EnvironmentName);
var connectionString = builder.Configuration.GetConnectionString("DefaultConnection");
Console.WriteLine(connectionString);

builder.Services.AddDbContext<ApplicationDbContext>(options =>
{
    options.UseSqlServer(connectionString);
    if (builder.Environment.IsDevelopment())
    {
        options.EnableSensitiveDataLogging();
    }
});


builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddMicrosoftIdentityWebApi(builder.Configuration.GetSection("AzureAdB2C"));

builder.Services.Configure<JwtBearerOptions>(JwtBearerDefaults.AuthenticationScheme, options =>
{
    options.TokenValidationParameters.RoleClaimType = "extension_Role";
});

builder.Services.Configure<IdentityOptions>(options =>
    options.ClaimsIdentity.UserIdClaimType = ClaimTypes.NameIdentifier);

builder.Services.AddControllersWithViews();
builder.Services.AddRazorPages();
builder.Services.AddMemoryCache();

var batchHandler = new DefaultODataBatchHandler();
builder.Services.AddControllers(options =>
{
    options.CacheProfiles.Add("Default30Seconds",
        new CacheProfile()
        {
            Duration = 30,
            VaryByQueryKeys = new[] { "*" }
        });
    options.CacheProfiles.Add("Default6Hours",
        new CacheProfile()
        {
            Duration = (int)TimeSpan.FromHours(6).TotalSeconds,
            VaryByQueryKeys = new[] { "*" }
        });
    options.CacheProfiles.Add("Default1Day",
        new CacheProfile()
        {
            Duration = (int)TimeSpan.FromDays(1).TotalSeconds,
            VaryByQueryKeys = new[] { "*" }
        });
}).AddJsonOptions(builder =>
{
    builder.JsonSerializerOptions.DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull;
})
.AddOData(options => options.Select().Filter().Count().OrderBy().SetMaxTop(1000).AddRouteComponents("odata", ApplicationDbContext.GetEdmModel(), batchHandler));

builder.Services.AddHttpClient();

builder.Services.AddHttpClient<CrmService>();

// Read application settings from appsettings.json (tenant ID, app ID, client secret, etc.)
// Initialize the client credential auth provider
var scopes = new[] { "https://graph.microsoft.com/.default" };
var clientSecretCredential = new ClientSecretCredential(builder.Configuration["AzureADGraph:TenantId"], builder.Configuration["AzureADGraph:AppId"], builder.Configuration["AzureADGraph:ClientSecret"]);

builder.Services.AddSingleton<GraphServiceClient>(sp =>
{
    return new GraphServiceClient(clientSecretCredential, scopes);
});

builder.Services.AddScoped<IStorageService, AzureStorageService>(ss =>
{
    return new AzureStorageService();
});

builder.Services.AddTransient<IResizeImageService, WindowsResizeImageService>();

builder.Services.AddTransient<IEmailSenderService, MailKitEmailSenderService>();

builder.Services.Configure<MailKitEmailSenderOptions>(options =>
{
    options.Host_Address = builder.Configuration["ExternalProviders:MailKit:SMTP:Address"];
    options.Host_Port = Convert.ToInt32(builder.Configuration["ExternalProviders:MailKit:SMTP:Port"]);
    options.Host_Username = builder.Configuration["ExternalProviders:MailKit:SMTP:Account"];
    options.Host_Password = builder.Configuration["ExternalProviders:MailKit:SMTP:Password"];
    options.Sender_EMail = builder.Configuration["ExternalProviders:MailKit:SMTP:SenderEmail"];
    options.Sender_Name = builder.Configuration["ExternalProviders:MailKit:SMTP:SenderName"];
});

if (builder.Environment.IsDevelopment())
{
    builder.Services.AddDatabaseDeveloperPageExceptionFilter();
}

builder.Services.AddSwaggerGen(options =>
{
    //https://github.com/microsoftgraph/msgraph-beta-sdk-dotnet/issues/285  !!!
    options.CustomSchemaIds(type => type.ToString());
    options.SwaggerDoc("v1", new OpenApiInfo { Title = "AspNet6WithOData", Version = "v1" });
});

builder.Logging.AddDbLogger(options =>
{
    builder.Configuration.GetSection("Logging")
    .GetSection("Database").GetSection("Options").Bind(options);
});

builder.Logging.AddAzureWebAppDiagnostics();
builder.Services.Configure<AzureFileLoggerOptions>(options =>
{
    options.FileName = "azure-diagnostics-";
    options.FileSizeLimit = 50 * 1024;
    options.RetainedFileCountLimit = 5;
});
builder.Services.Configure<AzureBlobLoggerOptions>(options =>
{
    options.BlobName = "log.txt";
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{

    app.UseDeveloperExceptionPage();
    app.UseMigrationsEndPoint();
    app.UseWebAssemblyDebugging();
}
else
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseODataBatching();

app.UseHttpsRedirection();

app.UseResponseCaching();


if (app.Environment.IsProduction())
{
    app.Use(async (context, next) =>
    {
        context.Response.GetTypedHeaders().CacheControl =
            new Microsoft.Net.Http.Headers.CacheControlHeaderValue()
            {
                Public = true,
                MaxAge = TimeSpan.FromSeconds(120)
            };
        context.Response.Headers[Microsoft.Net.Http.Headers.HeaderNames.Vary] =
            new string[] { "Accept-Encoding" };

        await next();
    });
}


app.UseBlazorFrameworkFiles();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthentication();
app.UseAuthorization();

app.UseODataBatching();

app.UseSwagger();
app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "AspNet6WithOData v1"));

app.MapRazorPages();
app.MapControllers();
app.MapFallbackToFile("index.html");
app.MapFallbackToFile("/product-list/product-page/view/{ProductParameter}", "index.html");

app.Run();


