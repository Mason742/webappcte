﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication.Internal;
using System.Security.Claims;
using System.Text.Json;

namespace CTEWebApp.Client
{
    public class CustomAccountFactory
     : AccountClaimsPrincipalFactory<CustomUserAccount>
    {
        public CustomAccountFactory(NavigationManager navigationManager,
            IAccessTokenProviderAccessor accessor) : base(accessor)
        {
        }

        public override async ValueTask<ClaimsPrincipal> CreateUserAsync(
            CustomUserAccount account, RemoteAuthenticationUserOptions options)
        {
            var initialUser = await base.CreateUserAsync(account, options);

            if (initialUser.Identity.IsAuthenticated)
            {
                if (account.CustomerId != null)
                    ((ClaimsIdentity)initialUser.Identity)
                            .AddClaim(new Claim("extension_CustomerId", account.CustomerId));

                ((ClaimsIdentity)initialUser.Identity)
                        .AddClaim(new Claim("given_name", account.GivenName));
                ((ClaimsIdentity)initialUser.Identity)
                        .AddClaim(new Claim("family_name", account.FamilyName));

                //foreach (var value in account.CustomerId))
                //{
                //((ClaimsIdentity)initialUser.Identity)
                //.AddClaim(new Claim("extension_CustomerId", value));
                //}

                //var claimsIdentity = (ClaimsIdentity)initialUser.Identity;
                //MapArrayClaimsToMultipleSeparateClaims(account, claimsIdentity);

                //}
            }

            return initialUser;
        }

        private void MapArrayClaimsToMultipleSeparateClaims(RemoteUserAccount account, ClaimsIdentity claimsIdentity)
        {
            foreach (var prop in account.AdditionalProperties)
            {
                var key = prop.Key;
                var value = prop.Value;
                if (value != null &&
                    (value is JsonElement element && element.ValueKind == JsonValueKind.Array))
                {
                    claimsIdentity.RemoveClaim(claimsIdentity.FindFirst(prop.Key));
                    var claims = element.EnumerateArray()
                        .Select(x => new Claim(prop.Key, x.ToString()));
                    claimsIdentity.AddClaims(claims);
                }
            }
        }

    }
}
