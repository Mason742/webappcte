﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication;
using Syncfusion.Blazor.Data;
using Syncfusion.Blazor.Grids;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using WebDB.Data.Models;

namespace CTEWebApp.Client.Pages.Admin
{
    public partial class AdminGridBase<TValue> : SfGrid<TValue>
    {
        public SfGrid<TValue> sfGrid;

        /// <summary>
        /// Specify column field to sort by otherwise default is Modified
        /// </summary>
        [Parameter]
        public string SortField { get; set; } = "Modified";
        [Parameter]
        public string OdataUrl { get; set; }
        [Parameter]
        public string OdataBatchUrl { get; set; }

        [Parameter]
        public RenderFragment AdditionalColumns { get; set; }

        protected SfDataManager DM;

        private bool Visibility { get; set; } = false;


        [Inject]
        public IAccessTokenProvider TokenProvider { get; set; }

        [Inject]
        public ILogger<AdminGridBase<TValue>> Logger { get; set; }

        public const int PAGE_COUNT = 5;
        public const int DEFAULT_PAGE_SIZE = 10;
        public string[] PageSizes = new string[] { "10", "20", "50" };
        protected IReadOnlyDictionary<string, object> props { get; set; }
        public override async Task SetParametersAsync(ParameterView parameters)
        {
            //Assign the additional parameters
            props = parameters.ToDictionary();
            await base.SetParametersAsync(parameters);
        }
        protected async override Task OnParametersSetAsync()
        {
            //AllowPaging = true;
            //AllowSorting = true;
            //AllowFiltering= true;
            //Toolbar = AdminHelper.DefaultDatagridToolbarItems;
            await base.OnParametersSetAsync();
        }

        protected override void OnAfterRender(bool firstRender)
        {
            base.OnAfterRender(firstRender);
            if (DM != null)
            {
                RemoteOptions Rm = (DM.DataAdaptor as ODataV4Adaptor).Options;
                Rm.UpdateType = HttpMethod.Put;
                (DM.DataAdaptor as ODataV4Adaptor).Options = Rm;
            }
        }

        public string AccessToken { get; set; }
        public string ErrorDetails = "";
        protected override async Task OnInitializedAsync()
        {
            if (DataSource == null)
            {
                Logger.LogInformation("Null DataSource");
            }

            if (typeof(TValue) == typeof(WebDB.Data.Models.Article))
            {
                TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(TValue), typeof(IArticleMetadata)), typeof(TValue));
            }
            else if (typeof(TValue) == typeof(WebDB.Data.Models.ArticleSeries))
            {
                TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(TValue), typeof(IArticleSeriesMetadata)), typeof(TValue));
            }
            else if (typeof(TValue) == typeof(WebDB.Data.Models.AuthorizedDistributor))
            {
                TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(TValue), typeof(IAuthorizedDistributorMetadata)), typeof(TValue));
            }
            else if (typeof(TValue) == typeof(WebDB.Data.Models.BkGsStock))
            {
                TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(TValue), typeof(IBkGsStockMetadata)), typeof(TValue));
            }
            else if (typeof(TValue) == typeof(WebDB.Data.Models.BrandCross))
            {
                TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(TValue), typeof(IBrandCrossMetadata)), typeof(TValue));
            }
            else if (typeof(TValue) == typeof(WebDB.Data.Models.Category))
            {
                TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(TValue), typeof(ICategoryMetadata)), typeof(TValue));
            }
            else if (typeof(TValue) == typeof(WebDB.Data.Models.CategoryGroup))
            {
                TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(TValue), typeof(ICategoryGroupMetadata)), typeof(TValue));
            }
            else if (typeof(TValue) == typeof(WebDB.Data.Models.CompanyDocument))
            {
                TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(TValue), typeof(ICompanyDocumentMetadata)), typeof(TValue));
            }
            else if (typeof(TValue) == typeof(WebDB.Data.Models.DistributorAuthorizedDomain))
            {
                TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(TValue), typeof(IDistributorAuthorizedDomainMetadata)), typeof(TValue));
            }
            else if (typeof(TValue) == typeof(WebDB.Data.Models.DistributorStock))
            {
                TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(TValue), typeof(IDistributorStockMetadata)), typeof(TValue));
            }
            else if (typeof(TValue) == typeof(WebDB.Data.Models.ErpInventory))
            {
                TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(TValue), typeof(IErpInventoryMetadata)), typeof(TValue));
            }
            else if (typeof(TValue) == typeof(WebDB.Data.Models.Group))
            {
                TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(TValue), typeof(IGroupMetadata)), typeof(TValue));
            }
            else if (typeof(TValue) == typeof(WebDB.Data.Models.Homepage))
            {
                TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(TValue), typeof(IHomepageMetadata)), typeof(TValue));
            }
            else if (typeof(TValue) == typeof(WebDB.Data.Models.PressRelease))
            {
                TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(TValue), typeof(IPressReleaseMetadata)), typeof(TValue));
            }
            else if (typeof(TValue) == typeof(WebDB.Data.Models.Product))
            {
                TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(TValue), typeof(IProductMetadata)), typeof(TValue));
            }
            else if (typeof(TValue) == typeof(WebDB.Data.Models.ProductImage))
            {
                TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(TValue), typeof(IProductImageMetadata)), typeof(TValue));
            }
            else if (typeof(TValue) == typeof(WebDB.Data.Models.Series))
            {
                TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(TValue), typeof(ISeriesMetadata)), typeof(TValue));
            }
            else if (typeof(TValue) == typeof(WebDB.Data.Models.SeriesArchive))
            {
                TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(TValue), typeof(ISeriesArchiveMetadata)), typeof(TValue));
            }
            else if (typeof(TValue) == typeof(WebDB.Data.Models.SeriesAttachment))
            {
                TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(TValue), typeof(ISeriesAttachmentMetadata)), typeof(TValue));
            }
            else if (typeof(TValue) == typeof(WebDB.Data.Models.SeriesCategory))
            {
                TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(TValue), typeof(ISeriesCategoryMetadata)), typeof(TValue));
            }
            else if (typeof(TValue) == typeof(WebDB.Data.Models.SeriesImage))
            {
                TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(TValue), typeof(ISeriesImageMetadata)), typeof(TValue));
            }
            else if (typeof(TValue) == typeof(WebDB.Data.Models.SeriesRelated))
            {
                TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(TValue), typeof(ISeriesRelatedMetadata)), typeof(TValue));
            }
            else if (typeof(TValue) == typeof(WebDB.Data.Models.TestLeadBuilder))
            {
                TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(TValue), typeof(ITestLeadBuilderMetadata)), typeof(TValue));
            }
            else if (typeof(TValue) == typeof(WebDB.Data.Models.TestLeadColor))
            {
                TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(TValue), typeof(ITestLeadColorMetadata)), typeof(TValue));
            }
            else if (typeof(TValue) == typeof(WebDB.Data.Models.TestLeadConnector))
            {
                TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(TValue), typeof(ITestLeadConnectorMetadata)), typeof(TValue));
            }
            else if (typeof(TValue) == typeof(WebDB.Data.Models.TestLeadLength))
            {
                TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(TValue), typeof(ITestLeadLengthMetadata)), typeof(TValue));
            }
            else if (typeof(TValue) == typeof(WebDB.Data.Models.TestLeadWire))
            {
                TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(TValue), typeof(ITestLeadWireMetadata)), typeof(TValue));
            }

            await base.OnInitializedAsync();
            var accessTokenResult = await TokenProvider.RequestAccessToken();
            AccessToken = string.Empty;

            if (accessTokenResult.TryGetToken(out var token))
            {
                AccessToken = token.Value;
            }
            Console.WriteLine($"AccessToken: {AccessToken}");
        }
        public virtual void BeginHandler(ActionEventArgs<TValue> actionEventArgs)
        {
            if (actionEventArgs.RequestType == Syncfusion.Blazor.Grids.Action.Add)
            {
                var type = actionEventArgs.Data.GetType();
                Logger.LogInformation(type.Name);

                Logger.LogInformation("Add -from virtual");
                if (type == typeof(WebDB.Data.Models.Article))
                {
                    (actionEventArgs.Data as WebDB.Data.Models.Article).Modified = DateTime.UtcNow;
                    (actionEventArgs.Data as WebDB.Data.Models.Article).Created = DateTime.UtcNow;
                }
                else
                {
                    var propModified = type.GetProperty("Modified");
                    if (propModified != null)
                    {
                        propModified.SetValue(actionEventArgs.Data, DateTime.UtcNow);
                    }
                    var propCreated = type.GetProperty("Created");
                    if (propCreated != null)
                    {
                        propCreated.SetValue(actionEventArgs.Data, DateTime.UtcNow);
                        return;
                    }
                    Logger.LogInformation("BeginHandler has no default 'Add' implementation");
                }
            }
            if (actionEventArgs.RequestType == Syncfusion.Blazor.Grids.Action.BeginEdit)
            {
                var type = actionEventArgs.Data.GetType();
                Logger.LogInformation(type.Name);

                Logger.LogInformation("BeginEdit  -from virtual");
                if (type == typeof(WebDB.Data.Models.Article))
                {
                    (actionEventArgs.Data as WebDB.Data.Models.Article).Modified = DateTime.UtcNow;
                }
                else
                {
                    var prop = type.GetProperty("Modified");
                    if (prop != null)
                    {
                        prop.SetValue(actionEventArgs.Data, DateTime.UtcNow);
                        return;
                    }
                    Logger.LogInformation("BeginHandler has no default 'BeginEdit' implementation");
                }
            }
            if (actionEventArgs.RequestType == Syncfusion.Blazor.Grids.Action.Save)
            {
                var type = actionEventArgs.Data.GetType();
                Logger.LogInformation(type.Name);
                Logger.LogInformation("Save  -from virtual");
            }
            if (actionEventArgs.RequestType == Syncfusion.Blazor.Grids.Action.Cancel)
            {
                var type = actionEventArgs.Data.GetType();
                Logger.LogInformation(type.Name);
            }

        }
#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
        public virtual async Task ActionCompleteHandler(ActionEventArgs<TValue> actionEventArgs)
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously
        {
            if (actionEventArgs.RequestType == Syncfusion.Blazor.Grids.Action.Save)
            {
                
            }
        }

        public void ActionFailure(FailureEventArgs args)
        {
            try
            {
                var jsonObj = System.Text.Json.Nodes.JsonNode.Parse(args.Error.InnerException.Message).AsObject();
                var errorMessage = jsonObj["value"].GetValue<string>();

                this.ErrorDetails = string.IsNullOrEmpty(args.Error.Message) ? "Unknown 404 Error" : errorMessage;
                Visibility = true;
                StateHasChanged();

            }
            catch
            {
                this.ErrorDetails = args.Error.Message;
                Visibility = true;

            }
        }

        private void OnDialogBtnClick()
        {
            this.Visibility = false;
        }
    }
}
