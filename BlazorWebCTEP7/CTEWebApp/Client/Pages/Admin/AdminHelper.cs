﻿using CTEWebApp.Shared.Helpers;
using CTEWebApp.Shared.Models;
using Syncfusion.Blazor.DropDowns;
using Syncfusion.Blazor.Grids;
using Syncfusion.Blazor.Inputs;
using Syncfusion.Blazor.RichTextEditor;
using System.Net.Http.Json;

namespace CTEWebApp.Client.Pages.Admin
{
    public static class AdminHelper
    {
        public static readonly List<object> DefaultDatagridContextMenuList = new() { " AutoFit", "AutoFitAll", "SortAscending", "SortDescending", "Copy", "Edit", "Delete", "Save", "Cancel", "PdfExport", "ExcelExport", "CsvExport", "FirstPage", "PrevPage", "LastPage", "NextPage" };
        public static readonly List<object> DefaultDatagridToolbarItems = new() { "Add", "Edit", "Delete" };
        public static readonly DialogSettings DefaultDialogParameters = new() { Width = "1024", Height = "1024", AllowDragging = false, EnableResize = true };
        public static readonly Dictionary<string, string> DefaultHeaderData = new() { { "WebCMS", "true" } };

        public static class OdataHelper
        {
            public static readonly string BaseAddress = "odata/";

            public static readonly string ArticlesUrl = GetOdataUrl("Article");
            public static readonly string ArticleRelatedsUrl = GetOdataUrl("ArticleRelated");
            public static readonly string ArticleSeriesUrl = GetOdataUrl("ArticleSeries");
            public static readonly string AuthorizedDistributorsUrl = GetOdataUrl("AuthorizedDistributor");
            public static readonly string BrandCrossesUrl = GetOdataUrl("BrandCross");
            public static readonly string DistributorAuthorizedDomainsUrl = GetOdataUrl("DistributorAuthorizedDomain");
            public static readonly string CategoriesUrl = GetOdataUrl("Category");
            public static readonly string CategoryGroupsUrl = GetOdataUrl("CategoryGroup");
            public static readonly string CompanyDocumentsUrl = GetOdataUrl("CompanyDocument");
            public static readonly string GroupsUrl = GetOdataUrl("Group");

            public static readonly string HomepagesUrl = GetOdataUrl("Homepage");
            public static readonly string PressReleasesUrl = GetOdataUrl("PressRelease");
            public static readonly string ProductsUrl = GetOdataUrl("Product");
            public static readonly string ProductImagesUrl = GetOdataUrl("ProductImage");
            public static readonly string SeriesUrl = GetOdataUrl("Series");
            public static readonly string SeriesAttachmentsUrl = GetOdataUrl("SeriesAttachment");
            public static readonly string SeriesImagesUrl = GetOdataUrl("SeriesImage");
            public static readonly string SeriesRelatedsUrl = GetOdataUrl("SeriesRelated");
            public static readonly string SeriesCategoriesUrl = GetOdataUrl("SeriesCategory");
            public static readonly string TestLeadBuildersUrl = GetOdataUrl("TestLeadBuilder");
            public static readonly string TestLeadWiresUrl = GetOdataUrl("TestLeadWire");
            public static readonly string TestLeadLengthsUrl = GetOdataUrl("TestLeadLength");
            public static readonly string TestLeadConnectorsUrl = GetOdataUrl("TestLeadConnector");
            public static readonly string TestLeadColorsUrl = GetOdataUrl("TestLeadColor");

            public static readonly string ContactFormsUrl = GetOdataUrl("ContactForm");
            public static readonly string RmaFormsUrl = GetOdataUrl("RmaForm");
            public static readonly string TestLeadBuilderEmailFormsUrl = GetOdataUrl("TestLeadBuilderEmailForm");
            public static readonly string TestLeadBuilderQuoteFormsUrl = GetOdataUrl("TestLeadBuilderQuoteForm");

            public static readonly string DistributorStocksUrl = GetOdataUrl("DistributorStock");
            public static readonly string ErpInventoriesUrl = GetOdataUrl("ErpInventory");
            public static readonly string BkGsStocksUrl = GetOdataUrl("BkGsStock");
            private static string GetOdataUrl(string controllerName)
            {
                return BaseAddress + controllerName;
            }
        }




        public class SeriesDTO
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }
        public class ProductDTO
        {
            public int Id { get; set; }
            public string PartNumber { get; set; }
        }

        public class ArticleDTO
        {
            public int Id { get; set; }
            public string Title { get; set; }
        }

        public class GroupDTO
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string ImageLink { get; set; }
        }

        public class CategoryDTO
        {
            public int Id { get; set; }
            public string DisplayName { get; set; }
            public string ImageLink { get; set; }
        }

        public class TestLeadConnectorDTO
        {
            public int Id { get; set; }
            public string Type { get; set; } = null!;

        }

        public class TestLeadWireDTO
        {
            public int Id { get; set; }
            public string PartNumberAndDescription { get; set; } = null!;
        }

        public static readonly IEditorSettings DecimalEditParams = new NumericEditCellParams
        {
            Params = new NumericTextBoxModel<object>() { ShowSpinButton = true, Min = 0, Format = "N4", Decimals = 4 }
        };


        public static readonly List<string> BrandEditParameters = new() { "CT", "GS", "CT,GS" };
        public static readonly IEditorSettings DefaultDropDownEditCellParams = new DropDownEditCellParams
        {
            Params = new DropDownListModel<object, object>() { AllowFiltering = true, ShowClearButton = true }
        };
        public static readonly List<ToolbarItemModel> DefaultRTEToolbarItems = new()
        {
            new ToolbarItemModel() { Command = ToolbarCommand.Bold },
            new ToolbarItemModel() { Command = ToolbarCommand.Italic },
            new ToolbarItemModel() { Command = ToolbarCommand.Underline },
            new ToolbarItemModel() { Command = ToolbarCommand.StrikeThrough },
            new ToolbarItemModel() { Command = ToolbarCommand.Separator },
            new ToolbarItemModel() { Command = ToolbarCommand.FontName },
            new ToolbarItemModel() { Command = ToolbarCommand.FontSize },
            new ToolbarItemModel() { Command = ToolbarCommand.Separator },
            new ToolbarItemModel() { Command = ToolbarCommand.FontColor },
            new ToolbarItemModel() { Command = ToolbarCommand.BackgroundColor },
            new ToolbarItemModel() { Command = ToolbarCommand.Separator },
            new ToolbarItemModel() { Command = ToolbarCommand.LowerCase },
            new ToolbarItemModel() { Command = ToolbarCommand.UpperCase },
            new ToolbarItemModel() { Command = ToolbarCommand.Separator },
            new ToolbarItemModel() { Command = ToolbarCommand.SuperScript },
            new ToolbarItemModel() { Command = ToolbarCommand.SubScript },
            new ToolbarItemModel() { Command = ToolbarCommand.Separator },
            new ToolbarItemModel() { Command = ToolbarCommand.Formats },
            new ToolbarItemModel() { Command = ToolbarCommand.Alignments },
            new ToolbarItemModel() { Command = ToolbarCommand.Separator },
            new ToolbarItemModel() { Command = ToolbarCommand.OrderedList },
            new ToolbarItemModel() { Command = ToolbarCommand.UnorderedList },
            new ToolbarItemModel() { Command = ToolbarCommand.Separator },
            new ToolbarItemModel() { Command = ToolbarCommand.Outdent },
            new ToolbarItemModel() { Command = ToolbarCommand.Indent },
            new ToolbarItemModel() { Command = ToolbarCommand.Separator },
            new ToolbarItemModel() { Command = ToolbarCommand.CreateLink },
            new ToolbarItemModel() { Command = ToolbarCommand.Image },
            new ToolbarItemModel() { Command = ToolbarCommand.CreateTable },
            new ToolbarItemModel() { Command = ToolbarCommand.Separator },
            new ToolbarItemModel() { Command = ToolbarCommand.ClearFormat },
            new ToolbarItemModel() { Command = ToolbarCommand.Separator },
            new ToolbarItemModel() { Command = ToolbarCommand.Print },
            new ToolbarItemModel() { Command = ToolbarCommand.SourceCode },
            new ToolbarItemModel() { Command = ToolbarCommand.FullScreen },
            new ToolbarItemModel() { Command = ToolbarCommand.Separator },
            new ToolbarItemModel() { Command = ToolbarCommand.Undo },
            new ToolbarItemModel() { Command = ToolbarCommand.Redo },
        };

    }


    public static class ODataResponseHelper<T> where T : class
    {
        public static async Task<List<T>> GetODataResponseValue(string apiRequest, HttpClient httpClient)
        {
            var response = await httpClient.GetFromJsonAsync<ODataResponse<T>>("odata/" + apiRequest);
            return response.Value;
        }
    }


    public enum LinkType
    {
        File = 0,
        ExternalLink = 1,
        Link = 2
    }
}
