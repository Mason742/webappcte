﻿namespace CTEWebApp.Client.Pages.Brand.Tlb
{
    public static class TlbHelper
    {
        public static class TlbMath
        {
            public const decimal CustomPartFee = 1.05m;
            public const decimal InchesPerCentimeter = (decimal)0.39370;
            public static decimal ConvertCentimetersToInches(decimal centimeters)
            {
                return Math.Round(InchesPerCentimeter * centimeters, 2);
            }

            public static decimal CalculateCost(decimal length, decimal costPerCentimeter, decimal connector1Cost, decimal connector2Cost, bool isStandardWire)
            {
                if (isStandardWire)
                {
                    return ((length * costPerCentimeter) + connector1Cost + connector2Cost);
                }
                else
                {
                    return ((length * costPerCentimeter) + connector1Cost + connector2Cost) * CustomPartFee;
                }
            }

            public static decimal GetMsrp(decimal cost)
            {
                return Math.Round(cost / 5, 2) * 5;
            }
        }
        public static string GetDiagramLink(string diagramLink, int connector1Id, int connector2Id)
        {
            if (string.IsNullOrWhiteSpace(diagramLink))
            {
                return $"https://as.caltestelectronics.com/public/test-lead-builder/diagrams/{connector1Id}{connector2Id}.jpg";
            }
            return diagramLink;
        }
    }
}
