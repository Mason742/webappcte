﻿namespace CTEWebApp.Client.Pages.Brand.Support.RmaForm
{
    public static class RmaFormHelper
    {
        public static readonly List<Choice> RepairOptions = new()
        {
               new Choice() { Value = "None", Name = "None" },
               new Choice() { Value = "Non_Warranty_Repair", Name = "Non-Warranty Repair" },
               new Choice() { Value = "Warranty_Repair", Name = "Warranty Repair" },
               new Choice() { Value = "Service_Warranty", Name = "Service Warranty (Previous Warranty Repair)" }
        };
        public static readonly List<Choice> CalibrationOptions = new()
        {
               new Choice() { Value = "None", Name = "None" },
               new Choice() { Value = "NTC", Name = "NIST Traceable Calibration" },
               new Choice() { Value = "NTCD", Name = "NIST Traceable Calibration with Data" },
               new Choice() { Value = "NTCBAD", Name = "NIST Traceable Calibration with Before & After Data" }
        };
        public class Choice
        {
            public string Value { get; set; }
            public string Name { get; set; }
        }
    }
}
