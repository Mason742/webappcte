﻿namespace HubSpotService.Services;
using Constants;
using CteCore;
using HubSpotService.Models.Association;
using HubSpotService.Models.Contact;
using HubSpotService.Models.Email;
using HubSpotService.Models.Search;
using HubSpotService.Models.Ticket;
using System.Net.Http.Json;
using System.Text.Json;
using System.Text.RegularExpressions;

public class CrmService
{
    //private const string ApiKeyParameter = "hapikey=";
    //private const string ApiKey = "cccf72f9-081e-40c3-b92c-edf6f3ded038";
    private const string Scheme = "Bearer";
    private const string Token = "pat-na1-791ae43f-40ef-47e7-8df6-cc1320d59953";
    private const string v3EndpointPath = "v3/objects/";

    private readonly HttpClient _httpClientV3;

    public CrmService(HttpClient httpClient)
    {
        httpClient.BaseAddress = new Uri("https://api.hubapi.com/crm/" + v3EndpointPath);
        httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, Token);
        _httpClientV3 = httpClient;
    }

    private string HubSpotContactId { get; set; }
    private string HubSpotTicketId { get; set; }
    private string HubSpotEmailId { get; set; }


    public async Task HubspotIntegration(string emailAddress, HubSpotOwner hubSpotOwner, object form)
    {


        //1. Check if contact exists to get contactID
        HubSpotContactId = await SearchContact(emailAddress);

        //2. Create contact, if it doesnt exists
        HubSpotContactId ??= await CreateContact(form);

        //3. Update contact subscriber information
        //HubSpotResponse.UpdateResponse.Response contactUpdatedResponse = await UpdateContact(rmaForm, hubSpotContactId);

        //4. create ticket
        HubSpotTicketId = await CreateTicket(form, hubSpotOwner.HubSpotOwnerId, hubSpotOwner.HubSpotTicketCategory);

        //5. associate ticket to contact
        await CreateAssociation(Objects.Tickets, HubSpotTicketId, Objects.Contacts, HubSpotContactId);

        //associate email to ticket
        _ = Task.Run(async () =>
        {
            Console.WriteLine("Wait 2 minutes to associate email to ticket");
            await Task.Delay(TimeSpan.FromSeconds(30));
            HubSpotEmailId = await SearchEmails(emailAddress, hubSpotOwner.FormattedTicketCategory);
            if (HubSpotEmailId == null)
            {
                return;
            }
            await CreateAssociation(Objects.Emails, HubSpotEmailId, Objects.Tickets, HubSpotTicketId);
        });
    }



    /// <summary>
    /// searches a contact in hubspot by email and returns contact id as string
    /// </summary>
    /// <param name="email"></param>
    /// <returns>contact id as string or null if not found</returns>
    public async Task<string> SearchContact(string email)
    {
        var searchRequest = new SearchRequest("email", "EQ", email.Trim());
        var searchResponse = await _httpClientV3.PostAsJsonAsync("contacts/search", searchRequest);
        var response = JsonSerializer.Deserialize<SearchResponse<ContactProperties>>(await searchResponse.Content.ReadAsStreamAsync());
        if(response.Results.Count <= 0)
        {
            return null;
        }
        var id = response.Results.FirstOrDefault().Id;
        return id;
    }

    public async Task<string> CreateContact(object form)
    {
        var contactRequest = new ContactRequest.CreateRequest(form);
        var createResponse = await _httpClientV3.PostAsJsonAsync("contacts", contactRequest);
        var response = JsonSerializer.Deserialize<ContactResponse.CreateResponse>(await createResponse.Content.ReadAsStreamAsync());
        var id = response.Id;
        return id;
    }

    public async Task<string> CreateTicket(object form, string ticketOwnerId, string hsTicketCategory)
    {
        var ticketRequest = new TicketRequest.CreateRequest(form,hsTicketCategory);
        Regex rgx = new("[^a-zA-Z0-9 -]");
        ticketRequest.Properties.Content = rgx.Replace(ticketRequest.Properties.Content, string.Empty);

        ticketRequest.Properties.HubspotOwnerId = ticketOwnerId;
        ticketRequest.Properties.HsTicketCategory = hsTicketCategory;

        var createResponse = await _httpClientV3.PostAsJsonAsync("tickets", ticketRequest);
        var response = JsonSerializer.Deserialize<TicketResponse.CreateResponse>(await createResponse.Content.ReadAsStreamAsync());
        var id = response.Id;
        return id;
    }

    public async Task<string> SearchEmails(string email, string hsEmailSubject)
    {

        var filters = new List<Filter>
        {
            new Filter { Operator = "CONTAINS_TOKEN", PropertyName = "hs_email_to_email", Value = email },
            new Filter { Operator = "CONTAINS_TOKEN", PropertyName = "hs_email_sender_email", Value = "noreply@caltestelectronics.com" },
            new Filter { Operator = "CONTAINS_TOKEN", PropertyName = "hs_email_subject", Value = hsEmailSubject }
        };
        var searchRequest = new SearchRequest(filters, "hs_createdate");
        var searchResponse = await _httpClientV3.PostAsJsonAsync("emails/search", searchRequest);
        var response = JsonSerializer.Deserialize<SearchResponse<EmailProperties>>(await searchResponse.Content.ReadAsStreamAsync());
        if (response.Results.Count <= 0)
        {
            return null;
        }
        var id = response.Results.FirstOrDefault().Id;
        return id;
    }

    public async Task<bool> CreateAssociation(string fromObjectType, string fromObjectId, string toObjectType, string toObjectTypeId, int? typeId = null)
    {
        var request = new AssociationRequest(fromObjectType, toObjectType);
        if (typeId == null)
        {
            var readResponse = await _httpClientV3.GetFromJsonAsync<AssociationResponse.ReadResponse>(request.RequestLabelsUrl);
            typeId = readResponse.Results.FirstOrDefault().TypeId;
        }

        string associationUrl = request.CreateAssociationUrl(fromObjectType, fromObjectId, toObjectType, toObjectTypeId, typeId);
        var json = "";
        var content = new StringContent(json, Encoding.UTF8, "application/json");
        var response = await _httpClientV3.PutAsync(associationUrl, content);
        return response.IsSuccessStatusCode;
    }
}
