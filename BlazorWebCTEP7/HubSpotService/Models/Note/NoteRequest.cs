﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HubSpotService.Models.Note
{
    public class NoteRequest
    {
        public class CreateRequest
        {
            [JsonPropertyName("properties")]
            public NoteProperties Properties { get; set; }

        }

    }
}
