﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HubSpotService.Models.Note
{
    public class NoteResponse : BaseResponse
    {
        public class CreateResponse : BaseCreateResponse<NoteProperties>
        {
        }
    }
}
