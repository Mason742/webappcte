﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HubSpotService.Models.Note
{
    public class NoteProperties
    {
        [JsonPropertyName("hs_timestamp")]
        public DateTime HsTimestamp { get; set; }

        [JsonPropertyName("hs_note_body")]
        public string HsNoteBody { get; set; }

        [JsonPropertyName("hubspot_owner_id")]
        public string HubspotOwnerId { get; set; }
    }
}
