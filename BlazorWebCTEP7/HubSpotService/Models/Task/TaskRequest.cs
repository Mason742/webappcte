﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HubSpotService.Models.Task
{
    public class TaskRequest
    {
        public class CreateRequest
        {
            [JsonPropertyName("properties")]
            public TaskProperties Properties { get; set; }
        }
    }
}
