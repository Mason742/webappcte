﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HubSpotService.Models.Task
{
    public class TaskProperties
    {
        [JsonPropertyName("hs_timestamp")]
        public DateTime HsTimestamp { get; set; }

        [JsonPropertyName("hs_task_body")]
        public string HsTaskBody { get; set; }

        [JsonPropertyName("hubspot_owner_id")]
        public string HubspotOwnerId { get; set; }

        [JsonPropertyName("hs_task_subject")]
        public string HsTaskSubject { get; set; }

        [JsonPropertyName("hs_task_status")]
        public string HsTaskStatus { get; set; }

        [JsonPropertyName("hs_task_priority")]
        public string HsTaskPriority { get; set; }
    }
}
