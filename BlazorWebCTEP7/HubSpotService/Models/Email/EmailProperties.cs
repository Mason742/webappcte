﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HubSpotService.Models.Email
{
    public class EmailProperties
    {
        [JsonPropertyName("hs_timestamp")]
        public DateTime HsTimestamp { get; set; }

        [JsonPropertyName("hubspot_owner_id")]
        public string HubspotOwnerId { get; set; }

        [JsonPropertyName("hs_email_direction")]
        public string HsEmailDirection { get; set; }

        [JsonPropertyName("hs_email_sender_email")]
        public string HsEmailSenderEmail { get; set; }

        [JsonPropertyName("hs_email_sender_firstname")]
        public string HsEmailSenderFirstname { get; set; }

        [JsonPropertyName("hs_email_sender_lastname")]
        public string HsEmailSenderLastname { get; set; }

        [JsonPropertyName("hs_email_to_email")]
        public string HsEmailToEmail { get; set; }

        [JsonPropertyName("hs_email_to_firstname")]
        public string HsEmailToFirstname { get; set; }

        [JsonPropertyName("hs_email_to_lastname")]
        public string HsEmailToLastname { get; set; }

        [JsonPropertyName("hs_email_status")]
        public string HsEmailStatus { get; set; }

        [JsonPropertyName("hs_email_subject")]
        public string HsEmailSubject { get; set; }

        [JsonPropertyName("hs_email_text")]
        public string HsEmailText { get; set; }
    }
}
