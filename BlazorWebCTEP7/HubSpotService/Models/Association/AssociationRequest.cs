﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HubSpotService.Models.Association
{
    public class AssociationRequest
    {
        public AssociationRequest(string fromObjectType, string toObjectType)
        {
            RequestLabelsUrl = $"/crm/v4/associations/{fromObjectType}/{toObjectType}/labels";
        }

        public string RequestLabelsUrl { get; set; }
        public string CreateAssociationUrl(string fromObjectType, string fromObjectId, string toObjectType, string toObjectTypeId, int? associationType)
        {
            return $"{fromObjectType}/{fromObjectId}/associations/{toObjectType}/{toObjectTypeId}/{associationType}";
        }
    }
}
