﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HubSpotService.Models.Association
{
    public class AssociationResponse
    {
        public class ReadResponse
        {
            [JsonPropertyName("results")]
            public List<Result> Results { get; set; }

            public class Result
            {
                [JsonPropertyName("category")]
                public string Category { get; set; }

                [JsonPropertyName("typeId")]
                public int TypeId { get; set; }

                [JsonPropertyName("label")]
                public string Label { get; set; }
            }
        }
    }
}
