﻿
using CteCore;
namespace HubSpotService.Models.Ticket
{
    public class TicketProperties
    {
        [JsonPropertyName("hs_pipeline")]
        public string HsPipeline { get; set; } = "0";

        [JsonPropertyName("hs_pipeline_stage")]
        public string HsPipelineStage { get; set; } = "1";

        [JsonPropertyName("hs_ticket_priority")]
        public string HsTicketPriority { get; set; } = "MEDIUM";

        [JsonPropertyName("hs_ticket_category")]
        public string HsTicketCategory { get; set; } = HsTicketCategories.ContactOther;

        [JsonPropertyName("hubspot_owner_id")]
        public string HubspotOwnerId { get; set; } 

        [JsonPropertyName("subject")]
        public string Subject { get; set; }

        [JsonPropertyName("content")]
        public string Content { get; set; }

        [JsonPropertyName("source_type")]
        public string Source { get; set; } = "WebForm";
    }

    
}
