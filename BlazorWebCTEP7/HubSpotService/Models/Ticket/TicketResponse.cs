﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HubSpotService.Models.Ticket
{
    public class TicketResponse :BaseResponse
    {
        public class CreateResponse :BaseCreateResponse<TicketProperties>
        {
        }
    }
}
