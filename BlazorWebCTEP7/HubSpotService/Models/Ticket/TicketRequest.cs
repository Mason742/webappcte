﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HubSpotService.Models.Ticket
{
    public class TicketRequest
    {
        public class CreateRequest
        {
            public CreateRequest(object form, string hsTicketCategory)
            {
                Properties = new TicketProperties();
                var formType = form.GetType();
                Properties.HsTicketCategory = hsTicketCategory;
                if (formType == typeof(ContactForm))
                {
                    var contactForm = (ContactForm)form;
                    Properties.Subject = $"{contactForm.Company}, {contactForm.FirstName} {contactForm.LastName}, {contactForm.ContactReasonChoice}";
                    Properties.Content = contactForm.Message;
                }
                else if (formType == typeof(RmaForm))
                {
                    var rmaForm = (RmaForm)form;
                    Properties.Subject = $"RMXXXXX, {rmaForm.Company}, {rmaForm.FirstName} {rmaForm.LastName}, {rmaForm.PartNumber}, {hsTicketCategory}";
                    Properties.Content = rmaForm.ProblemDescription + " " + rmaForm.CalibrationServiceChoice + " " + rmaForm.RepairOptionChoice;
                }
                else if (formType == typeof(TestLeadBuilderQuoteForm))
                {
                    var tlbQuoteForm = (TestLeadBuilderQuoteForm)form;
                    Properties.Subject = $"{tlbQuoteForm.Company}, {tlbQuoteForm.FirstName} {tlbQuoteForm.LastName}, {tlbQuoteForm.PartNumber}, {hsTicketCategory}";
                    Properties.Content = $"RFQ, {tlbQuoteForm.PartNumber}, Qty:{tlbQuoteForm.Quantity}";
                }
            }
            [JsonPropertyName("properties")]
            public TicketProperties Properties { get; set; }
        }
    }
}
