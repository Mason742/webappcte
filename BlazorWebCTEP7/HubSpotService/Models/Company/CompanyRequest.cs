﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HubSpotService.Models.Company
{
    public class CompanyRequest
    {
        public class CreateRequest
        {
            [JsonPropertyName("properties")]
            public CompanyProperties Properties { get; set; }
        }
    }
}
