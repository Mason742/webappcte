﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HubSpotService.Models.Company
{
    public class CompanyResponse : BaseResponse
    {
        public class CreateResponse : BaseCreateResponse<CompanyProperties>
        {

        }
    }
}
