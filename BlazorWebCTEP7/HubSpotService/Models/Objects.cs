﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HubSpotService.Models
{
    public static class Objects
    {
        public static readonly  string Contacts = "contacts";
        public static readonly string Companies = "companies";
        public static readonly string Deals = "deals";
        public static readonly string Tickets = "tickets";
        public static readonly string Emails = "emails";
        public static readonly string Tasks = "tasks";
        public static readonly string Notes = "notes";
    }

}
