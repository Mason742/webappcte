﻿using HubSpotService.Models.Contact;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HubSpotService.Models.Contact
{
    public class ContactResponse : BaseResponse
    {
        public class CreateResponse : BaseCreateResponse<ContactProperties>
        {
        }
    }
}
