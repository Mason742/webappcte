﻿using HubSpotService.Models.Contact;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HubSpotService.Models.Contact
{
    public class ContactRequest
    {
        public class CreateRequest
        {
            public CreateRequest(object form)
            {

                Properties = new ContactProperties();
                var formType = form.GetType();
                if (formType == typeof(ContactForm))
                {
                    var contactForm = (ContactForm)form;
                    Properties.Company = contactForm.Company;
                    Properties.Email = contactForm.Email;
                    Properties.City = contactForm.City;
                    Properties.Firstname = contactForm.FirstName;
                    Properties.Lastname = contactForm.LastName;
                    Properties.Phone = contactForm.Phone;
                    Properties.Country = contactForm.Country;
                    Properties.State = contactForm.StateOrProvince;
                    Properties.City = contactForm.City;
                }
                else if (formType == typeof(RmaForm))
                {
                    var rmaForm = (RmaForm)form;
                    Properties.Company = rmaForm.Company;
                    Properties.Email = rmaForm.Email;
                    Properties.City = rmaForm.City;
                    Properties.Firstname = rmaForm.FirstName;
                    Properties.Lastname = rmaForm.LastName;
                    Properties.Phone = rmaForm.Phone;
                    Properties.Country = rmaForm.Country;
                    Properties.State = rmaForm.StateOrProvince;
                    Properties.City = rmaForm.City;
                }
                else if (formType == typeof(TestLeadBuilderQuoteForm))
                {
                    var rmaForm = (TestLeadBuilderQuoteForm)form;
                    Properties.Company = rmaForm.Company;
                    Properties.Email = rmaForm.Email;
                    Properties.City = rmaForm.City;
                    Properties.Firstname = rmaForm.FirstName;
                    Properties.Lastname = rmaForm.LastName;
                    Properties.Phone = rmaForm.Phone;
                    Properties.Country = rmaForm.Country;
                    Properties.State = rmaForm.StateOrProvince;
                    Properties.City = rmaForm.City;
                }
                else
                {
                    throw new NotImplementedException("Unhandled form type");
                }
            }

            [JsonPropertyName("properties")]
            public ContactProperties Properties { get; set; }


        }

    }
}
