﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HubSpotService.Models
{
    public class BaseResponse
    {
        public class BaseCreateResponse<T>
        {
            [JsonPropertyName("id")]
            public string Id { get; set; }

            [JsonPropertyName("properties")]
            public T Properties { get; set; }

            [JsonPropertyName("createdAt")]
            public DateTime CreatedAt { get; set; }

            [JsonPropertyName("updatedAt")]
            public DateTime UpdatedAt { get; set; }

            [JsonPropertyName("archived")]
            public bool Archived { get; set; }
        }
    }
    
}
