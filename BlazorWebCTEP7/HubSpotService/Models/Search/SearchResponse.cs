﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HubSpotService.Models.Search
{
    public class SearchResponse<T> where T : class
    {
        [JsonPropertyName("total")]
        public int Total { get; set; }

        [JsonPropertyName("results")]
        public List<Result<T>> Results { get; set; }

        [JsonPropertyName("paging")]
        public Paging Paging { get; set; }
    }

    public class Next
    {
        [JsonPropertyName("after")]
        public string After { get; set; }

        [JsonPropertyName("link")]
        public string Link { get; set; }
    }

    public class Paging
    {
        [JsonPropertyName("next")]
        public Next Next { get; set; }
    }

    public class Result<T>
    {
        [JsonPropertyName("id")]
        public string Id { get; set; }

        [JsonPropertyName("properties")]
        public T Properties { get; set; }

        [JsonPropertyName("createdAt")]
        public DateTime CreatedAt { get; set; }

        [JsonPropertyName("updatedAt")]
        public DateTime UpdatedAt { get; set; }

        [JsonPropertyName("archived")]
        public bool Archived { get; set; }
    }
}
