﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HubSpotService.Models.Search
{
    public class SearchRequest
    {

        public SearchRequest(string propertyName, string filterOperator, string searchValue)
        {
            var filter = new Filter
            {
                PropertyName = propertyName,
                Operator = filterOperator,
                Value = searchValue,
            };

            FilterGroups = new List<FilterGroup>();
            FilterGroups.Add(new FilterGroup() { Filters = new List<Filter>() { filter } });

            Sorts = new List<Sort>();
            Sorts.Add(new Sort() { Direction = "DESCENDING", PropertyName = "createdate" });

            Limit = 1;
        }

        public SearchRequest(List<Filter> filters, string sortPropertyName = "createdate")
        {
            FilterGroups = new List<FilterGroup>();
            FilterGroups.Add(new FilterGroup() { Filters = filters });

            Sorts = new List<Sort>();
            Sorts.Add(new Sort() { Direction = "DESCENDING", PropertyName = sortPropertyName });

            Limit = 1;
        }

        [JsonPropertyName("filterGroups")]
        public List<FilterGroup> FilterGroups { get; set; }

        [JsonPropertyName("properties")]
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public List<string> ReturnedProperties { get; set; }

        [JsonPropertyName("sorts")]
        public List<Sort> Sorts { get; set; }

        [JsonPropertyName("limit")]
        public int Limit { get; set; }

        [JsonPropertyName("after")]
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public string After { get; set; }
    }
    public class Filter
    {
        [JsonPropertyName("propertyName")]
        public string PropertyName { get; set; }

        [JsonPropertyName("operator")]
        public string Operator { get; set; }

        [JsonPropertyName("value")]
        public string Value { get; set; }
    }

    public class FilterGroup
    {
        [JsonPropertyName("filters")]
        public List<Filter> Filters { get; set; }
    }

    public class Sort
    {
        [JsonPropertyName("propertyName")]
        public string PropertyName { get; set; }

        [JsonPropertyName("direction")]
        public string Direction { get; set; }
    }
}
